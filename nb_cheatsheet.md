# nb官方文档

![b](https://raw.githubusercontent.com/xwmx/nb/master/docs/assets/images/nb.png)

 [![建立状态](https://travis-ci.org/xwmx/nb.svg?branch=master)](https://travis-ci.org/xwmx/nb)

`nb` 是具有以下功能的命令行笔记，书签，归档和知识库应用程序：

*   纯文本数据存储，
*   [加密](#password-protected-encrypted-notes-and-bookmarks) ，
*   [过滤](#listing-notes) 和 [搜索](#-search) ，
*   [Git](https://git-scm.com/) 支持的 [版本控制](#-revision-history) 和 [同步](#-git-sync) ，
*   [Pandoc](https://pandoc.org/) 支持的 [转换](#%EF%B8%8F-import--export) ，
*   全球和本地 [笔记本](#-notebooks) ，
*   可自定义的 [颜色主题](#-color-themes) ，
*   通过 [插件的](#-plugins) 可扩展性 ，

以及所有这些功能，都在一个可移植，用户友好的脚本中。

`nb` 以 [Markdown](https://en.wikipedia.org/wiki/Markdown) ， [Emacs Org mode](https://orgmode.org/) 和 [LaTeX之](https://www.latex-project.org/) 类的基于文本的格式创建注释 ，可以处理任何格式的文件，可以将注释导入和导出为多种文档格式，还可以创建受密码保护的私有加密注释和书签。 使用 `nb` ，您可以使用Vim，Emacs，VS Code，Sublime Text和您喜欢的任何其他文本编辑器编写注释。 `nb` 可在任何标准Linux / Unix环境中使用，包括通过WSL的macOS和Windows。 可以安装 [可选的依赖项](#optional) 以增强功能，但是 `nb` 如果没有它们 ，则 可以很好地工作。

![家](https://xwmx.github.io/misc/nb/images/nb-theme-nb-home.png)

`nb` 也是功能强大的基于文本的CLI书签系统，包括：

*   具有正则表达式支持的缓存网页内容的本地全文搜索，
*   标记，
*   方便的过滤和列表，
*   [Internet Archive Wayback Machine](https://archive.org/web/) 快照查找损坏的链接，
*   在终端和常规的Web浏览器中轻松查看已加书签的页面。

页面信息会自动下载，编译并保存到为人类制作的普通Markdown文档中，因此书签就像其他注释一样易于编辑。

![书签](https://xwmx.github.io/misc/nb/images/nb-theme-raspberry-bookmarks.png)

`nb` 在后台 使用 [Git](https://git-scm.com/) 自动记录更改并将笔记本与远程存储库同步。 `nb` 还可以使用Dropbox等通用同步工具将笔记本电脑配置为同步笔记本，以便可以在任何设备上的其他应用程序中编辑笔记。

![欢迎](https://xwmx.github.io/misc/nb/images/nb-theme-console-empty.png)

`nb` is designed to be portable, future\-focused, and vendor independent, providing a full\-featured and intuitive experience within a highly composable user\-centric text interface. The entire program is a single well\-tested shell script that can be installed, copied, or `curl`ed almost anywhere and just work, using [progressive enhancement](https://en.wikipedia.org/wiki/Progressive_enhancement) for various experience improvements in more capable environments. `nb` works great whether you have one notebook with just a few notes or dozens of notebooks containing thousands of notes, bookmarks, and other items. `nb` makes it easy to incorporate other tools, writing apps, and workflows. `nb` can be used a little, a lot, once in a while, or for just a subset of features. `nb` is flexible.

📝 🔖 🔒 🔍 📔

# [](#nb)`nb`

[Installation](#installation) • [Overview](#overview) • [Help](#help)

### [](#installation)Installation

#### [](#dependencies)Dependencies

##### [](#required)Required

*   [Bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell))
    *   `nb` works perfectly with Zsh, fish, and any other shell set as your primary login shell, the system just needs to have Bash available on it.
*   [Git](https://git-scm.com/)
*   A text editor with command line support, such as:
    *   [Vim](https://en.wikipedia.org/wiki/Vim_\(text_editor\)),
    *   [Emacs](https://en.wikipedia.org/wiki/Emacs),
    *   [Visual Studio Code](https://code.visualstudio.com/),
    *   [Sublime Text](https://www.sublimetext.com/),
    *   [micro](https://github.com/zyedidia/micro),
    *   [nano](https://en.wikipedia.org/wiki/GNU_nano),
    *   [Atom](https://atom.io/),
    *   [TextMate](https://macromates.com/),
    *   [MacDown](https://macdown.uranusjr.com/),
    *   [some of these](https://github.com/topics/text-editor),
    *   [and many of these.](https://en.wikipedia.org/wiki/List_of_text_editors)

##### [](#optional)Optional

`nb` leverages standard command line tools and works in standard Linux / Unix environments. `nb` also checks the environment for some additional optional tools and uses them to enhance the experience whenever they are available.

Recommended:

*   [`bat`](https://github.com/sharkdp/bat)
*   [Pandoc](https://pandoc.org/)
*   [`rg` / ripgrep](https://github.com/BurntSushi/ripgrep)
*   [`tig`](https://github.com/jonas/tig)
*   [`w3m`](https://en.wikipedia.org/wiki/W3m)

Also supported for various enhancements:

[Ack](https://beyondgrep.com/), [`afplay`](https://ss64.com/osx/afplay.html), [Ag \- The Silver Searcher](https://github.com/ggreer/the_silver_searcher), [`exa`](https://github.com/ogham/exa), [`ffplay`](https://ffmpeg.org/ffplay.html), [ImageMagick](https://imagemagick.org/), [GnuPG](https://en.wikipedia.org/wiki/GNU_Privacy_Guard), [`highlight`](http://www.andre-simon.de/doku/highlight/en/highlight.php), [`imgcat`](https://www.iterm2.com/documentation-images.html), [kitty’s `icat` kitten](https://sw.kovidgoyal.net/kitty/kittens/icat.html), [Lynx](https://en.wikipedia.org/wiki/Lynx_(web_browser)), [Midnight Commander](https://en.wikipedia.org/wiki/Midnight_Commander), [`mpg123`](https://en.wikipedia.org/wiki/Mpg123), [MPlayer](https://en.wikipedia.org/wiki/MPlayer), [note\-link\-janitor](https://github.com/andymatuschak/note-link-janitor) (via [plugin](https://github.com/xwmx/nb/blob/master/plugins/backlink.nb-plugin)), [`pdftotext`](https://en.wikipedia.org/wiki/Pdftotext), [Pygments](https://pygments.org/), [Ranger](https://ranger.github.io/), [readability\-cli](https://gitlab.com/gardenappl/readability-cli), [`termpdf.py`](https://github.com/dsanson/termpdf.py)

#### [](#macos--homebrew)macOS / Homebrew

To install with [Homebrew](https://brew.sh/):

```
brew tap xwmx/taps
brew install nb

```

Installing `nb` with Homebrew also installs the recommended dependencies above and completion scripts for Bash and Zsh.

#### [](#ubuntu-windows-wsl-and-others)Ubuntu, Windows WSL, and others

##### [](#npm)npm

要使用 [npm](https://www.npmjs.com/package/nb.sh) 安装 ：

```
npm install -g nb.sh

```

后 `npm` 安装完成后，运行 `sudo nb completions install` 安装Bash和岩组完成脚本（推荐）。

在Ubuntu和WSL上，您可以运行 [`sudo nb env install`](#env) 以安装可选依赖项。

*`nb`也可以使用其原始包名称[notes.sh](https://www.npmjs.com/package/notes.sh)来提供，该包带有一个额外的`notes`可执行文件包装`nb`。*

##### [](#download-and-install)下载并安装

要以管理员身份安装，请复制并粘贴以下多行命令之一：

```
# install using wget
sudo wget https://raw.github.com/xwmx/nb/master/nb -O /usr/local/bin/nb &&
  sudo chmod +x /usr/local/bin/nb &&
  sudo nb completions install

# install using curl
sudo curl -L https://raw.github.com/xwmx/nb/master/nb -o /usr/local/bin/nb &&
  sudo chmod +x /usr/local/bin/nb &&
  sudo nb completions install

```

在Ubuntu和WSL上，您可以运行 [`sudo nb env install`](#env) 以安装可选依赖项。

###### [](#user-only-installation)仅限用户安装

要仅使用用户权限进行安装，只需将 `nb` 脚本 添加 到中 `$PATH` 。 `~/bin` 例如， 如果您已经有 目录，则可以使用以下命令之一：

```
# download with wget
wget https://raw.github.com/xwmx/nb/master/nb -O ~/bin/nb && chmod +x ~/bin/nb

# download with curl
curl -L https://raw.github.com/xwmx/nb/master/nb -o ~/bin/nb && chmod +x ~/bin/nb

```

仅具有用户许可权的安装不包括可选的依赖项或补全，但是 `nb` 在没有它们的情况下可以工作。 如果您有权 `sudo` 访问并想要安装完成脚本和依赖项，请运行以下命令：

```
sudo nb env install

```

##### [](#make)使

要使用 [Make进行](https://en.wikipedia.org/wiki/Make_(software)) 安装， [请](https://en.wikipedia.org/wiki/Make_(software)) 克隆该存储库，导航到克隆的根目录，然后运行：

```
sudo make install

```

这还将在所有系统上安装完成脚本，并在Ubuntu和WSL上安装推荐的依赖项。

##### [](#bpkg)bpkg

要安装 [bpkg](https://github.com/bpkg/bpkg) ：

```
bpkg install xwmx/nb

```

#### [](#tab-completion)制表符完成

`nb` 使用上述方法安装 时 ，假设您具有适当的系统权限或已安装， 则应启用Bash和Zsh选项卡补全功能 `sudo` 。 如果安装后无法完成安装 `nb` ，请参阅 [完成安装说明](https://github.com/xwmx/nb/tree/master/etc) 。

#### [](#updating)更新中

当 `nb` 使用像NPM或自制的软件包管理器安装，使用软件包管理器的升级功能更新 `nb` 到最新版本。 通过其他方法安装时， `nb` 可以使用 [`nb update`](#update) 子命令 将其更新为最新版本 。

## [](#overview)总览

[Notes](#-notes) • [Adding](#adding-notes) • [Listing](#listing-notes) • [Editing](#editing-notes) • [Viewing](#viewing-notes) • [Deleting](#deleting-notes) • [Bookmarks](#-bookmarks) • [Search](#-search) • [History](#-revision-history) • [Notebooks](#-notebooks) • [Git Sync](#-git-sync) • [Import / Export](#%EF%B8%8F-import--export) • [`set` & Settings](#%EF%B8%8F-set--settings) • [Color Themes](#-color-themes) • [Plugins](#-plugins) • [Shell](#-nb-interactive-shell) • [Shortcuts](#shortcut-aliases) • [Help](#help) • [Specifications](#specifications) • [Tests](#tests)

To get started, simply run:

```
nb

```

`nb` sets up your initial “home” notebook the first time it runs.

By default, notebooks and notes are global (at `~/.nb`), so they are always available to `nb` regardless of the current working directory. `nb` also supports [local notebooks](#global-and-local-notebooks).

### [](#-notes)📝 Notes

#### [](#adding-notes)Adding Notes

Use [`nb add`](#add) to create new notes:

```
# create a new note in your text editor
nb add

# create a new note with the filename "example.md"
nb add example.md

# create a new note containing "This is a note."
nb add "This is a note."

# create a new note with piped content
echo "Note content." | nb add

# create a new password-protected, encrypted note titled "Secret Document"
nb add --title "Secret Document" --encrypt

# create a new note in the notebook named "example"
nb example:add "This is a note."

```

`nb add` with no arguments or input will open the new, blank note in your environment’s preferred text editor. You can change your editor using the `$EDITOR` environment variable or [`nb set editor`](#editor).

`nb` files are [Markdown](https://daringfireball.net/projects/markdown/) files by default. The default file type can be changed to whatever you like using [`nb set default_extension`](#default_extension).

`nb add` behaves differently depending on the type of argument it receives. When a filename with extension is specified, a new note with that filename is opened in the editor:

```
nb add example.md

```

When a string is specified, a new note is immediately created with that string as the content and the editor is not opened:

```
> nb add "This is a note."
Added: [5] 20200101000000.md

```

`nb add <string>` is useful for quickly jotting down notes directly via the command line.

When no filename is specified, `nb add` uses the current datetime as the filename.

`nb add` can also recieve piped content, which behaves the same as `nb add <string>`:

```
# create a new note containing "Note content."
> echo "Note content." | nb add
Added: [6] 20200101000100.md

# create a new note containing the clipboard contents on macOS
> pbpaste | nb add
Added: [7] 20200101000200.md

# create a new note containing the clipboard contents using xclip
> xclip -o | nb add
Added: [8] 20200101000300.md

```

Content can be passed with the `--content` option, which will also create a new note without opening the editor:

```
nb add --content "Note content."

```

When content is piped, specified with `--content`, or passed as a string argument, use the `--edit` flag to open the file in the editor before the change is committed.

The title, filename, and content can also be specified with long and short options:

```
> nb add --filename "example.md" -t "Example Title" -c "Example content."
Added: [9] example.md "Example Title"

```

The `-t <title>` / `--title <title>` option will also set the filename to the title, lowercased with spaces and non\-filename characters replaced with underscores:

```
> nb add --title "Example Title" "Example content."
Added: [10] example_title.md "Example Title"

```

Files can be created with any file type either by specifying the extension in the filename or via the `--type <type>` option:

```
# open a new org mode file in the editor
nb add example.org

# open a new reStructuredText file in the editor
nb add --type rst

```

Notes can be tagged simply by adding hashtags anywhere in the document:

```
#tag1 #tag2

```

Search for tagged notes and bookmarks with [`nb search` / `nb q`](#search):

```
nb search "#tag1"

nb q "#tag2"

```

For a full list of options available for `nb add`, run [`nb help add`](#add).

##### [](#password-protected-encrypted-notes-and-bookmarks)Password\-Protected Encrypted Notes and Bookmarks

Password\-protected notes and [bookmarks](#-bookmarks) are created with the `-e` / `--encrypt` flag and are encrypted with AES\-256 using OpenSSL by default. GPG is also supported and can be configured with [`nb set encryption_tool`](#encryption_tool).

Each protected note and bookmark is encrypted individually with its own password. When an encrypted item is viewed, edited, or opened, `nb` will simply prompt for the item’s password before proceeding. After an item is edited, `nb` automatically re\-encrypts it and saves the new version.

Encrypted notes can also be decrypted using the OpenSSL and GPG command line tools directly, so you aren’t dependent on `nb` to decrypt your files.

##### [](#shortcut-alias-a)Shortcut Alias: `a`

`nb` includes single\-character shortcuts for many commands, including `a` for `add`:

```
# create a new note in your text editor
nb a

# create a new note with the filename "example.md"
nb a example.md

# create a new note containing "This is a note."
nb a "This is a note."

# create a new note containing the clipboard contents with xclip
xclip -o | nb a

# create a new note in the notebook named "example"
nb example:a

```

##### [](#other-aliases-create-new)Other Aliases: `create`, `new`

`nb add` can also be invoked with `nb create` and `nb new` for convenience:

```
# create a new note containing "Example note content."
nb new "Example note content."

# create a new note with the title "Example Note Title"
nb create --title "Example Note Title"

```

#### [](#listing-notes)Listing Notes

To list notes and notebooks, run [`nb ls`](#ls):

```
> nb ls
home
----
[3] example.md · "Example content."
[2] todos.md · "Todos:"
[1] ideas.md · "- Example idea one."

```

Notebooks are listed above the line, with the current notebook highlighted and/or underlined, depending on terminal capabilities. `nb ls` also includes a footer with example commands for easy reference. The notebook header and command footer can be configured or hidden with [`nb set header`](#header) and [`nb set footer`](#footer).

Notes from the current notebook are listed in the order they were last modified. By default, each note is listed with its id, filename, and an excerpt from the first line of the note. When a note has a title, the title is displayed instead of the filename and first line.

Titles can be defined within a note using [either Markdown `h1` style](https://daringfireball.net/projects/markdown/syntax#header) or [YAML front matter](https://jekyllrb.com/docs/front-matter/):

```
# Example Title

```

```
Todos
=====

```

```
---
title: Ideas
---

```

Once defined, titles will be displayed in place of the filename and first line in the output of `nb ls`:

```
> nb ls
home
----
[3] Example Title
[2] Todos
[1] Ideas

```

Pass an id, filename, or title to view the listing for that note:

```
> nb ls Todos
[2] Todos

```

```
> nb ls 3
[3] Example Title

```

If there is no immediate match, `nb` will list items with titles and filenames that fuzzy match the query:

```
> nb ls "idea"
[1] Ideas

```

A case\-insensitive regular expression can also be used to filter filenames and titles:

```
> nb ls "^example.*"
[3] Example Title

```

Multiple words act like an `OR` filter, listing any titles or filenames that match any of the words:

```
> nb ls example ideas
[3] Example Title
[1] Ideas

```

When multiple words are quoted, filter titles and filenames for that phrase:

```
> nb ls "example title"
[3] Example Title

```

For full text search, see [Search](#-search).

To view excerpts of notes, use the `--excerpt` or `-e` option, which optionally accepts a length:

```
> nb ls 3 --excerpt
[3] Example Title
-----------------
# Example Title

This is an example excerpt.

> nb ls 3 -e 8
[3] Example Title
-----------------
# Example Title

This is an example excerpt.

More example content:
- one
- two
- three

```

Several classes of file types are represented with emoji to make them easily identifiable in lists. For example, bookmarks and encrypted notes are listed with `🔖` and `🔒`:

```
> nb ls
home
----
[4] Example Note
[3] 🔒 encrypted-note.md.enc
[2] 🔖 Example Bookmark (example.com)
[1] 🔖 🔒 encrypted.bookmark.md.enc

```

File types include:

```
 🔉  Audio
 📖  Book
 🔖  Bookmark
 🔒  Encrypted
 📂  Folder
 🌄  Image
 📄  PDF, Word, or Open Office document
 📹  Video

```

By default, items are listed starting with the most recently modified. To reverse the order, use the `-r` or `--reverse` flag:

```
> nb ls
home
----
[2] Todos
[3] Example Title
[1] Ideas

> nb ls --reverse
[1] Ideas
[3] Example Title
[2] Todos

```

Notes can be sorted with the `-s` / `--sort` flag, which can be combined with `-r` / `--reverse`:

```
> nb ls
home
----
[2] Todos
[3] Example Title
[1] Ideas

> nb ls --sort
[1] Ideas
[2] Todos
[3] Example Title

> nb ls --sort --reverse
[3] Example Title
[2] Todos
[1] Ideas

```

`nb` with no subcommand behaves like an alias for `nb ls`, so the examples above can be run without the `ls`:

```
> nb
home
----
[2] Todos
[3] Example Title
[1] Ideas

> nb "^example.*"
[3] Example Title

> nb 3 --excerpt
[3] Example Title
-----------------
# Example Title

This is an example excerpt.

> nb 3 -e 8
[3] Example Title
-----------------
# Example Title

This is an example excerpt.

More example content:
- one
- two
- three

> nb --sort
[1] Ideas
[2] Todos
[3] Example Title

> nb --sort --reverse
[3] Example Title
[2] Todos
[1] Ideas

```

Short options can be combined for brevity:

```
# equivalent to `nb --sort --reverse --excerpt 2` and `nb -s -r -e 2`:
> nb -sre 2
[3] Example Title
-----------------
# Example Title

[2] Todos
---------
Todos
=====
[1] Ideas
---------
---
title: Ideas

```

`nb` and `nb ls` display the 20 most recently modified items. The default limit can be changed with [`nb set limit <number>`](#limit). To list a different number of items on a per\-command basis, use the `-n <limit>`, `--limit <limit>`, `--<limit>`, `-a`, or `--all` flags:

```
> nb -n 1
home
----
[5] Example Five
4 omitted. 5 total.

> nb --limit 2
home
----
[5] Example Five
[4] Example Four
3 omitted. 5 total.

> nb --3
home
----
[5] Example Five
[4] Example Four
[3] Example Three
2 omitted. 5 total.

> nb --all
home
----
[5] Example Five
[4] Example Four
[3] Example Three
[2] Example Two
[1] Example One

```

`nb ls` 在一个视图中 是的组合， [`nb notebooks`](#notebooks) 并且 [`nb list`](#list) 接受与相同的参数 `nb list` ，后者仅列出没有笔记本列表的便笺，默认情况下没有限制：

```
> nb list
[100] Example One Hundred
[99]  Example Ninety-Nine
[98]  Example Ninety-Eight
... lists all notes ...
[2]   Example Two
[1]   Example One

```

有关列出注释的选项的更多信息，请运行 [`nb help ls`](#ls) 和 [`nb help list`](#list) 。

#### [](#editing-notes)编辑笔记

您可以通过将注释的ID，文件名或标题传递给来在编辑器中编辑注释 [`nb edit`](#edit) ：

```
# edit note by id
nb edit 3

# edit note by filename
nb edit example.md

# edit note by title
nb edit "A Document Title"

# edit note 12 in the notebook named "example"
nb edit example:12

# edit note 12 in the notebook named "example", alternative
nb example:12 edit

# edit note 12 in the notebook named "example", alternative
nb example:edit 12

```

`edit` 带有标识符的其他子命令可以在标识符和子命令名称相反的情况下调用：

```
# edit note by id
nb 3 edit

```

`nb edit` 还可以接收管道内容，该内容将在不打开编辑器的情况下追加到指定的注释中：

```
echo "Content to append." | nb edit 1

```

可以使用 `--content` 选项 传递内容，该 选项还将在不打开编辑器的情况下附加内容：

```
nb edit 1 --content "Content to append."

```

当内容通过管道 传递 或指定为时 `--content` ，请使用该 `--edit` 标志在提交更改之前在编辑器中打开文件。

##### [](#editing-encrypted-notes)编辑加密笔记

注释加密后， `nb edit` 将提示您输入注释密码，在编辑器中打开未加密的内容，然后在完成编辑后自动重新加密注释。

##### [](#shortcut-alias-e)快捷方式别名： `e`

如 `add` ， `edit` 具有快捷方式别名 `e` ：

```
# edit note by id
nb e 3

# edit note by filename
nb e example.md

# edit note by title
nb e "A Document Title"

# edit note by id, alternative
nb 3 e

# edit note 12 in the notebook named "example"
nb e example:12

# edit note 12 in the notebook named "example", alternative
nb example:12 e

# edit note 12 in the notebook named "example", alternative
nb example:e 12

```

有关 `nb edit` 帮助信息，请运行 [`nb help edit`](#edit) 。

#### [](#viewing-notes)查看笔记

可以使用 [`nb show`](#show) 以下 方式查看注释 ：

```
# show note by id
nb show 3

# show note by filename
nb show example.md

# show note by title
nb show "A Document Title"

# show note by id, alternative
nb 3 show

# show note 12 in the notebook named "example"
nb show example:12

# show note 12 in the notebook named "example", alternative
nb example:12 show

# show note 12 in the notebook named "example", alternative
nb example:show 12

```

默认情况下， `nb show` 将打开的说明 [`less`](https://linux.die.net/man/1/less) ，语法高亮如果 [`bat`](https://github.com/sharkdp/bat) ， [`highlight`](http://www.andre-simon.de/doku/highlight/en/highlight.php) 或 [Pygments来做](https://pygments.org/) 安装。 您可以 `less` 使用以下键 进行导航 ：

```
Key               Function
---               --------
mouse scroll      Scroll up or down
arrow up or down  Scroll one line up or down
f                 Jump forward one window
b                 Jump back one window
d                 Jump down one half window
u                 Jump up one half window
/<query>          Search for <query>
n                 Jump to next <query> match
q                 Quit

```

*If `less` scrolling isn’t working in [iTerm2](https://www.iterm2.com/), go to* “Settings” \-> “Advanced” \-> “Scroll wheel sends arrow keys when in alternate screen mode” *and change it to* “Yes”. *[More info](https://stackoverflow.com/a/37610820)*

When [Pandoc](https://pandoc.org/) is available, use the `-r` / `--render` option to render the note to HTML and open it in your terminal browser:

```
nb show example.md --render
# opens example.md as an HTML page in w3m or lynx

```

`nb show` also supports previewing other file types in the terminal, depending on the tools available in the environment. Supported file types and tools include:

*   PDF files:
    *   [`termpdf.py`](https://github.com/dsanson/termpdf.py) with [kitty](https://sw.kovidgoyal.net/kitty/)
    *   [`pdftotext`](https://en.wikipedia.org/wiki/Pdftotext)
*   Audio files:
    *   [`mplayer`](https://en.wikipedia.org/wiki/MPlayer)
    *   [`afplay`](https://ss64.com/osx/afplay.html)
    *   [`mpg123`](https://en.wikipedia.org/wiki/Mpg123)
    *   [`ffplay`](https://ffmpeg.org/ffplay.html)
*   Images:
    *   [ImageMagick](https://imagemagick.org/) with a terminal that supports [sixels](https://en.wikipedia.org/wiki/Sixel)
    *   [`imgcat`](https://www.iterm2.com/documentation-images.html) with [iTerm2](https://www.iterm2.com/)
    *   [kitty’s `icat` kitten](https://sw.kovidgoyal.net/kitty/kittens/icat.html)
*   Folders / Directories:
    *   [`ranger`](https://ranger.github.io/)
    *   [午夜指挥官（ `mc` ）](https://en.wikipedia.org/wiki/Midnight_Commander)
*   Word文档：
    *   [潘多克](https://pandoc.org/)
*   EPUB电子书：
    *   [Pandoc](https://pandoc.org/) 用 [`w3m`](https://en.wikipedia.org/wiki/W3m) 或 [`lynx`](https://en.wikipedia.org/wiki/Lynx_(web_browser))

当 `nb show` 与其他文件类型一起 使用时 ，或者如果上述工具不可用， `nb show` 将在系统的首选应用程序中为每种类型打开文件。

`nb show` 还提供 用于查询有关项目信息的 [选项](#show) 。 例如，使用 `--added` / `-a` 和 `--updated` / `-u` 标志来打印添加或更新项目的日期和时间：

```
> nb show 2 --added
2020-01-01 01:01:00 -0700

> nb show 2 --updated
2020-02-02 02:02:00 -0700

```

`nb show` 主要用于查看终端中的项目。 要在系统的首选GUI应用程序中查看文件，请使用 [`nb open`](#open) 。

有关完整的 `nb show` 用法信息，请运行 [`nb help show`](#show) 。

##### [](#shortcut-alias-s)快捷方式别名： `s`

`show` 别名为 `s` ：

```
# show note by id
nb s 3

# show note by filename
nb s example.md

# show note by title
nb s "A Document Title"

# show note by id, alternative
nb 3 s

# show note 12 in the notebook named "example"
nb s example:12

# show note 12 in the notebook named "example", alternative
nb example:12 s

# show note 12 in the notebook named "example", alternative
nb example:s 12

```

##### [](#alias-view)别名： `view`

`nb show` `nb view` 为了方便 也可以调用 ：

```
# show note by id
nb view 3

# show note by filename
nb view example.md

# show note by title
nb view "A Document Title"

# show note by id, alternative
nb 3 view

```

#### [](#deleting-notes)删除笔记

要删除记事，请将其ID，文件名或标题传递给 [`nb delete`](#delete) ：

```
# delete note by id
nb delete 3

# delete note by filename
nb delete example.md

# delete note by title
nb delete "A Document Title"

# delete note by id, alternative
nb 3 delete

# delete note 12 in the notebook named "example"
nb delete example:12

# delete note 12 in the notebook named "example", alternative
nb example:12 delete

# show note 12 in the notebook named "example", alternative
nb example:delete 12

```

默认情况下， `nb delete` 将显示确认提示。 要跳过，请使用 `--force` / `-f` 选项：

```
nb delete 3 --force

```

##### [](#shortcut-alias-d)快捷方式别名： `d`

`delete` 具有别名 `d` ：

```
# delete note by id
nb d 3

# delete note by filename
nb d example.md

# delete note by title
nb d "A Document Title"

# delete note by id, alternative
nb 3 d

# delete note 12 in the notebook named "example"
nb d example:12

# delete note 12 in the notebook named "example", alternative
nb example:12 d

# delete note 12 in the notebook named "example", alternative
nb example:d 12

```

有关 `nb delete` 帮助信息，请运行 [`nb help delete`](#delete) 。

### [](#-bookmarks)🔖书签

`nb` 是功能强大的书签管理系统，使您可以查看，搜索和管理书签，链接和在线参考。 书签是Markdown注释，包含有关已标记页面的信息。

要创建新书签，请将URL作为第一个参数传递给 `nb` ：

```
nb https://example.com

```

`nb` 使用页面中的信息自动生成书签：

```
# Example Title (example.com)

<https://example.com>

## Description

Example description.

## Content

Example Title
=============

This domain is for use in illustrative examples in documents. You may
use this domain in literature without prior coordination or asking for
permission.

[More information\...](https://www.iana.org/domains/example)

```

`nb` embeds the page content in the bookmark, making it available for full text search with [`nb search`](#search). When [Pandoc](https://pandoc.org/) is installed, the HTML page content will be converted to Markdown. When [readability\-cli](https://gitlab.com/gardenappl/readability-cli) is installed, markup is cleaned up to focus on content.

In addition to caching the page content, you can also include a quote from the page using the `-q` / `--quote` option:

```
nb https://example.com --quote "Example quote line one.

Example quote line two."

```

```
# Example Title (example.com)

<https://example.com>

## Description

Example description.

## Quote

> Example quote line one.
>
> Example quote line two.

## Content

Example Title
=============

This domain is for use in illustrative examples in documents. You may
use this domain in literature without prior coordination or asking for
permission.

[More information\...](https://www.iana.org/domains/example)

```

Add a comment to a bookmark using the `-c` / `--comment` option:

```
nb https://example.com --comment "Example comment."

```

```
# Example Title (example.com)

<https://example.com>

## Description

Example description.

## Comment

Example comment.

## Content

Example Title
=============

This domain is for use in illustrative examples in documents. You may
use this domain in literature without prior coordination or asking for
permission.

[More information\...](https://www.iana.org/domains/example)

```

Bookmarks can be tagged using the `-t` / `--tags` option. Tags are converted into hashtags:

```
nb https://example.com --tags tag1,tag2

```

```
# Example Title (example.com)

<https://example.com>

## Description

Example description.

## Tags

#tag1 #tag2

## Content

Example Title
=============

This domain is for use in illustrative examples in documents. You may
use this domain in literature without prior coordination or asking for
permission.

[More information\...](https://www.iana.org/domains/example)

```

Search for tagged bookmarks with [`nb search` / `nb q`](#search):

```
nb search "#tag1"

nb q "#tag"

```

`nb search` / `nb q` automatically searches archived page content:

```
> nb q "example query"
[10] 🔖 example.bookmark.md "Example Bookmark (example.com)"
---------------------------------------------------------
5:Lorem ipsum example query.

```

Bookmarks can also be encrypted:

```
# create a new password-protected, encrypted bookmark
nb https://example.com --encrypt

```

Encrypted bookmarks require a password before they can be viewed or opened.

#### [](#listing-and-filtering-bookmarks)Listing and Filtering Bookmarks

[`nb bookmark`](#bookmark) and `nb bookmark list` can be used to list and filter only bookmarks:

```
> nb bookmark
Add: nb <url> Help: nb help bookmark
------------------------------------
[3] 🔖 🔒 example.bookmark.md.enc
[2] 🔖 Example Two (example.com)
[1] 🔖 Example One (example.com)

> nb bookmark list two
[2] 🔖 Example Two (example.com)

```

Bookmarks are also included in `nb`, `nb ls`, and `nb list`:

```
> nb
home
----
[7] 🔖 Example Bookmark Three (example.com)
[6] Example Note Three
[5] 🔖 Example Bookmark Two (example.net)
[4] Example Note Two
[3] 🔖 🔒 example-encrypted.bookmark.md.enc
[2] Example Note One
[1] 🔖 Example Bookmark One (example.com)

```

Use the [`--type <type>` / `--<type>`](#ls) option as a filter to display only bookmarks:

```
> nb --type bookmark
[7] 🔖 Example Bookmark Three (example.com)
[5] 🔖 Example Bookmark Two (example.net)
[3] 🔖 🔒 example-encrypted.bookmark.md.enc
[1] 🔖 Example Bookmark One (example.com)

> nb --bookmark
[7] 🔖 Example Bookmark Three (example.com)
[5] 🔖 Example Bookmark Two (example.net)
[3] 🔖 🔒 example-encrypted.bookmark.md.enc
[1] 🔖 Example Bookmark One (example.com)

```

`nb` saves the domain in the title, making it easy to filter by domain using any list subcommands:

```
> nb example.com
[7] 🔖 Example Bookmark Three (example.com)
[1] 🔖 Example Bookmark One (example.com)

```

For more listing options, see [`nb help ls`](#ls), [`nb help list`](#list), and [`nb help bookmark`](#bookmark).

##### [](#shortcut-alias-b)Shortcut Alias: `b`

`bookmark` can also be used with the alias `b`:

```
> nb b
Add: nb <url> Help: nb help bookmark
------------------------------------
[7] 🔖 Example Bookmark Three (example.com)
[5] 🔖 Example Bookmark Two (example.net)
[3] 🔖 🔒 example-encrypted.bookmark.md.enc
[1] 🔖 Example Bookmark One (example.com)

> nb b example.net
[5] 🔖 Example Bookmark Two (example.net)

```

#### [](#opening-and-viewing-bookmarked-pages)Opening and Viewing Bookmarked Pages

`nb` provides multiple ways to view bookmarked web pages.

[`nb open`](#open) opens the bookmarked page in your system’s primary web browser:

```
# open bookmark by id
nb open 3

# open bookmark 12 in the notebook named "example"
nb open example:12

# open bookmark 12 in the notebook named "example", alternative
nb example:12 open

# open bookmark 12 in the notebook named "example", alternative
nb example:open 12

```

[`nb peek`](#peek) (alias: `preview`) opens the bookmarked page in your terminal web browser, such as [w3m](https://en.wikipedia.org/wiki/W3m) or [Lynx](https://en.wikipedia.org/wiki/Lynx_(web_browser)):

```
# peek bookmark by id
nb peek 3

# peek bookmark 12 in the notebook named "example"
nb peek example:12

# peek bookmark 12 in the notebook named "example", alternative
nb example:12 peek

# peek bookmark 12 in the notebook named "example", alternative
nb example:peek 12

```

`open` and `peek` subcommands also work seamlessly with encrypted bookmarks. `nb` will simply prompt you for the bookmark’s password.

`open` and `peek` automatically check whether the URL is still valid. If the page has been removed, `nb` can check the [Internet Archive Wayback Machine](https://archive.org/web/) for an archived copy.

The preferred terminal web browser can be set using the `$BROWSER` environment variable, assigned in `~/.bashrc`, `~/.zshrc`, or similar:

```
export BROWSER=lynx

```

When `$BROWSER` is not set, `nb` looks for `w3m` and `lynx` and uses the first one it finds.

`$BROWSER` can also be used to easy specify the terminal browser for an individual command:

```
> BROWSER=lynx nb 12 peek
# opens the URL from bookmark 12 in lynx

> BROWSER=w3m nb 12 peek
# opens the URL from bookmark 12 in w3m

```

`nb show` and `nb edit` can also be used to view and edit bookmark files, which include the cached page converted to Markdown.

`nb show <id> --render` / `nb show <id> -r` displays the bookmark file converted to HTML in the terminal web browser, including all bookmark fields and the cached page content, providing a cleaned\-up, distraction\-free, locally\-served view of the page content along with all of your notes.

##### [](#shortcut-aliases-o-and-p)Shortcut Aliases: `o` and `p`

`open` and `peek` can also be used with the shortcut aliases `o` and `p`:

```
# open bookmark by id
nb o 3

# open bookmark 12 in the notebook named "example"
nb o example:12

# open bookmark 12 in the notebook named "example", alternative
nb example:12 o

# peek bookmark by id
nb p 3

# peek bookmark 12 in the notebook named "example"
nb p example:12

# peek bookmark 12 in the notebook named "example", alternative
nb example:12 p

```

#### [](#bookmark-file-format)Bookmark File Format

Bookmarks are identified by a `.bookmark.md` file extension. The bookmark URL is the first URL in the file within `<` and `>` characters. To create a minimally valid bookmark file with `nb add`:

```
nb add example.bookmark.md --content "<https://example.com>"

```

For a full overview, see [`nb` Markdown Bookmark File Format](#nb-markdown-bookmark-file-format).

#### [](#bookmark--a-command-line-tool-for-managing-bookmarks)`bookmark` – A command line tool for managing bookmarks.

`nb` includes [`bookmark`](#bookmark-help), a full\-featured command line interface for creating, viewing, searching, and editing bookmarks.

`bookmark` is a shortcut for the `nb bookmark` subcommand, accepting all of the same subcommands and options with identical behavior.

Bookmark a page:

```
> bookmark https://example.com --tags tag1,tag2
Added: [3] 🔖 20200101000000.bookmark.md "Example Title (example.com)"

```

List and filter bookmarks with `bookmark` and `bookmark list`:

```
> bookmark
Add: bookmark <url> Help: bookmark help
---------------------------------------
[3] 🔖 🔒 example.bookmark.md.enc
[2] 🔖 Example Two (example.com)
[1] 🔖 Example One (example.com)

> bookmark list two
[2] 🔖 Example Two (example.com)

```

View a bookmark in your terminal web browser:

```
> bookmark peek 2

```

Open a bookmark in your system’s primary web browser:

```
> bookmark open 2

```

Perform a full text search of bookmarks and archived page content:

```
> bookmark search "example query"
[10] 🔖 example.bookmark.md "Example Bookmark (example.com)"
---------------------------------------------------------
5:Lorem ipsum example query.

```

See [`bookmark help`](#bookmark-help) for more information.

### [](#-search)🔍 Search

Use [`nb search`](#search) to search your notes, with support for regular expressions and tags:

```
# search current notebook for "example query"
nb search "example query"

# search the notebook "example" for "example query"
nb example:search "example query"

# search all unarchived notebooks for "example query" and list matching items
nb search "example query" --all --list

# search for "Example" OR "Sample"
nb search "Example|Sample"

# search items containing the hashtag "#example"
nb search "#example"

# search with a regular expression
nb search "\d\d\d-\d\d\d\d"

# search bookmarks for "example"
nb search "example" --type bookmark

# search bookmarks for "example", alternative
nb b q "example"

# search the current notebook for "example query"
nb q "example query"

# search the notebook named "example" for "example query"
nb example:q "example query"

# search all unarchived notebooks for "example query" and list matching items
nb q -la "example query"

```

`nb search` prints the id number, filename, and title of each matched file, followed by each search query match and its line number, with color highlighting:

```
> nb search "example"
[314]  🔖 example.bookmark.md "Example Bookmark (example.com)"
----------------------------------------------------------
1:# Example Bookmark (example.com)

3:<https://example.com>

[2718] example.md "Example Note"
--------------------------------
1:# Example Note

```

To just print the note information line without the content matches, use the `-l` or `--list` option:

```
> nb search "example" --list
[314]  🔖 example.bookmark.md "Example Bookmark (example.com)"
[2718] example.md "Example Note"

```

`nb search` looks for [`rg`](https://github.com/BurntSushi/ripgrep), [`ag`](https://github.com/ggreer/the_silver_searcher), [`ack`](https://beyondgrep.com/), and [`grep`](https://en.wikipedia.org/wiki/Grep), in that order, and performs searches using the first tool it finds. `nb search` works mostly the same regardless of which tool is found and is perfectly fine using the environment’s built\-in `grep`. `rg`, `ag`, and `ack` are faster and there are some subtle differences in color highlighting.

##### [](#shortcut-alias-q)Shortcut Alias: `q`

`search` can also be used with the alias `q` (for “query”):

```
# search for "example" and print matching excerpts
nb q "example"

# search for "example" and list each matching file
nb q -l "example"

# search for "example" in all unarchived notebooks
nb q -a "example"

# search for "example" in the notbook named "sample"
nb sample:q "example"

```

For more information about search, see [`nb help search`](#search).

### [](#-revision-history)🗒 Revision History

Whenever a note is added, modified, or deleted, `nb` automatically commits the change to git transparently in the background.

Use [`nb history`](#history) to view the history of the notebook or an individual note:

```
# show history for current notebook
nb history

# show history for note number 4
nb history 4

# show history for note with filename example.md
nb history example.md

# show history for note titled "Example"
nb history Example

# show history for the notebook named "example"
nb example:history

# show history for the notebook named "example", alternative
nb history example:

# show the history for note 12 in the notebook named "example"
nb history example:12

```

`nb history` uses `git log` by default and prefers [`tig`](https://github.com/jonas/tig) when available.

### [](#-notebooks)📚 Notebooks

You can create additional notebooks, each of which has its own version history.

Create a new notebook with [`nb notebooks add`](#notebooks):

```
# add a notebook named example
nb notebooks add example

```

`nb` and `nb ls` list the available notebooks above the list of notes:

```
> nb
example · home
--------------
[3] Title Three
[2] Title Two
[1] Title One

```

Commands in `nb` run within the current notebook, and identifiers like id, filename, and title refer to notes within the current notebook. `nb edit 3`, for example, tells `nb` to `edit` note with id `3` within the current notebook.

To switch to a different notebook, use [`nb use`](#use):

```
# switch to the notebook named "example"
nb use example

```

If you are in one notebook and you want to perform a command in a different notebook without switching to it, add the notebook name with a colon before the command name:

```
# add a new note in the notebook "example"
nb example:add

# add a new note in the notebook "example", shortcut alias
nb example:a

# show note 5 in the notebook "example"
nb example:show 5

# show note 5 in the notebook "example", shortcut alias
nb example:s 5

# edit note 12 in the notebook "example"
nb example:edit 12

# edit note 12 in the notebook "example", shortcut alias
nb example:e 12

# search for "example query" in the notebook "example"
nb example:search "example query"

# search for "example query" in the notebook "example", shortcut alias
nb example:q "example query"

# show the revision history of the notebook "example"
nb example:history

```

The notebook name with colon can also be used as a modifier to the id, filename, or title:

```
# edit note 12 in the notebook "example"
nb edit example:12

# edit note 12 in the notebook "example", shortcut alias
nb e example:12

# edit note 12 in the notebook "example", alternative
nb example:12 edit

# edit note 12 in the notebook "example", alternative, shortcut alias
nb example:12 e

# show note titled "misc" in the notebook "example"
nb show example:misc

# show note titled "misc" in the notebook "example", shortcut alias
nb s example:misc

# delete note with filename "todos.md" in the notebook "example", alternative
nb example:todos.md delete

# delete note with filename "todos.md" in the notebook "example", alternative,
# shortcut alias
nb example:todos.md d

```

When a notebook name with colon is called without a subcommand, `nb` runs `nb ls` in the specified notebook:

```
> nb example:
example · home
--------------
[example:3] Title Three
[example:2] Title Two
[example:1] Title One

```

A bookmark can be created in another notebook by specifying the notebook name with colon, then a space, then the URL and bookmark options:

```
# create a new bookmark in a notebook named "sample"
> nb sample: https://example.com --tags tag1,tag2

```

Notes can also be moved between notebooks:

```
# move note 3 from the current notebook to "example"
nb move 3 example

# move note 5 in the notebook "example" to the notebook "sample"
nb move example:5 sample

```

##### [](#example-workflow)Example Workflow

The flexibility of `nb`’s argument handling makes it easy to build commands step by step as items are listed, filtered, viewed, and edited, particularly in combination with shell history:

```
# list items in the "example" notebook
> nb example:
example · home
--------------
[example:3] Title Three
[example:2] Title Two
[example:1] Title One

# filter list
> nb example: three
[example:3] Title Three

# view item
> nb example:3 show
# opens item in `less`

# edit item
> nb example:3 edit
# opens item in $EDITOR

```

##### [](#notebooks-and-tab-completion)Notebooks and Tab Completion

[`nb` tab completion](#tab-completion) is optimized for frequently running commands in various notebooks using the colon syntax, so installing the completion scripts is recommended and makes working with notebooks easy, fluid, and fun.

For example, listing the contents of a notebook is usually as simple as typing the first two or three characters of the name, then press the <tab> key, then press <enter>:

```
> nb exa<tab>
# completes to "example:"
> nb example:
example · home
--------------
[example:3] Title Three
[example:2] Title Two
[example:1] Title One

```

Scoped notebook commands are also available in tab completion:

```
> nb exa<tab>
# completes to "example:"
> nb example:hi<tab>
# completes to "example:history"

```

#### [](#notebooks-tags-and-taxonomy)Notebooks, Tags, and Taxonomy

`nb` is optimized to work well with a bunch of notebooks, so notebooks are a really good way to organize your notes and bookmarks by top\-level topic.

Tags are searchable across notebooks and can be created ad hoc, making notebooks and tags distinct and complementary organizational systems in `nb`.

Search for a tag in or across notebooks with [`nb search`](#search) / [`nb q`](#search):

```
# search for #tag in the current notebook
nb q "#tag"

# search for #tag in all notebooks
nb q "#tag" -a

# search for #tag in the "example" notebook
nb example:q "#tag"

```

#### [](#global-and-local-notebooks)Global and Local Notebooks

##### [](#global-notebooks)Global Notebooks

By default, all `nb` notebooks are global, making them always accessible in the terminal regardless of the current working directory. Global notebooks are stored in the directory configured in [`nb set nb_dir`](#nb_dir), which is `~/.nb` by default.

##### [](#local-notebooks)Local Notebooks

`nb` 还支持创建和使用本地笔记本。 本地笔记本是指不在系统外部任何位置的笔记本 `NB_DIR` 。 任何文件夹都可以是 `nb` 本地笔记本，这只是一个普通文件夹，已初始化为git存储库并包含 `nb` .index文件。 将文件夹初始化为 `nb` 本地笔记本是将结构化git版本添加到文档和其他文件的任何文件夹中的一种非常简单的方法。

在 `nb` 本地笔记本中运行时，该本地笔记本将设置为当前笔记本：

```
> nb
local · example · home
----------------------
[3] Title Three
[2] Title Two
[1] Title One

```

本地笔记本始终使用名称来指代 `local` ，否则无论何时从 本地笔记本中 运行命令，其行为都类似于全局笔记本：

```
# add a new note in the local notebook
nb add

# edit note 15 in the local notebook
nb edit 15

# move note titled "Todos" from the home notebook to the local notebook
nb move home:Todos local

# move note 1 from the local notebook to the home notebook
nb move 1 home

# search the local notebook for <query string>
nb search "query string"

# search the local notebook and all unarchived global notebooks for <query string>
nb search "query string" --all

```

可以使用以下方式创建本地笔记本 [`nb notebooks init`](#notebooks) ：

```
# initialize the current directory as a notebook
nb notebooks init

# create a new notebook at ~/example
nb notebooks init ~/example

# clone an existing notebook to ~/example
nb notebooks init ~/example https://github.com/example/example.git

```

也可以通过导出全局笔记本来创建本地笔记本：

```
# export global notebook named "example" to "../path/to/destination"
nb notebooks export example ../path/to/destination

# alternative
nb export example ../path/to/destination

```

也可以导入本地笔记本，从而使其成为全球性笔记本：

```
# import notebook or folder at "../path/to/notebook"
nb notebooks import ../path/to/notebook

# alternative
nb import ../path/to/notebook

```

`nb notebooks init` 并且 `nb notebooks import` 可以一起使用，以轻松地将现有文件的任何目录转换为全局 `nb` 笔记本：

```
> ls
example-directory

> nb notebooks init example-directory
Initialized local notebook: /home/username/example-directory

> nb notebooks import example-directory
Imported notebook: example-directory

> nb notebooks
example-directory
home

```

#### [](#archiving-notebooks)存档笔记本

笔记本可以使用 [`nb notebooks archive`](#notebooks) 以下 方式存档 ：

```
# archive the current notebook
nb notebooks archive

# archive the notebook named "example"
nb notebooks archive example

```

笔记本存档后，不会包含在 [`nb`](#ls) / [`nb ls`](#ls) 输出 [`nb search --all`](#search) 或制表符补全中，也不会自动与同步 [`nb sync --all`](#sync) 。

```
> nb
example1 · example2 · example3 · [1 archived]
---------------------------------------------
[3] Title Three
[2] Title Two
[1] Title One

```

仍然可以使用常规笔记本命令单独使用已存档的笔记本：

```
# switch the current notebook to the archived notebook "example"
nb use example

# run the `list` subcommand in the archived notebook "example"
nb example:list

```

使用以下命令检查笔记本的存档状态 [`nb notebooks status`](#notebooks) ：

```
> nb notebooks status example
example is archived.

```

使用 [`nb notebooks unarchive`](#notebooks) 以解除存档笔记本：

```
# unarchive the current notebook
nb notebooks unarchive

# unarchive the notebook named "example"
nb notebooks unarchive example

```

有关使用笔记本的更多信息，请参见 [`nb help notebooks`](#notebooks) 。

有关笔记本的技术详细信息，请参阅 [`nb`笔记本规格](#nb-notebook-specification) 。

### [](#-git-sync)🔄Git同步

通过使用 [`nb remote`](#remote) 以下命令 设置远程URL，可以将每个笔记本与远程git存储库同步 ：

```
# set the current notebook's remote to a private GitHub repository
nb remote set https://github.com/example/example.git

# set the remote for the notebook named "example"
nb example:remote set https://github.com/example/example.git

```

每次在该笔记本中运行命令时，任何带有远程URL的笔记本都会自动同步。

当您 `nb` 在多个系统上 使用 时，您可以将两个系统上的笔记本都设置到同一远程，并且 `nb` 每次笔记本发生更改时都将在后台保持所有同步。

由于每个笔记本都有自己的git历史记录，因此您可以使某些笔记本与远程同步，而其他笔记本仅在该系统上本地可用。

Many services provide free private git repositories, so git syncing with `nb` is easy, free, and vendor\-independent. You can also sync your notes using Dropbox, Drive, Box, Syncthing, or another syncing tool by changing your `nb` directory with [`nb set nb_dir <path>`](#nb_dir) and git syncing will still work simultaneously.

When you have an existing `nb` notebook in a git repository, simply pass the URL to [`nb notebooks add`](#notebooks) and `nb` will clone your existing notebook and start syncing changes automatically:

```
# create a new notebook named "example" cloned from a private GitLab repository
nb notebooks add example https://gitlab.com/example/example.git

```

Turn off syncing for a notebook by removing the remote:

```
# remove the remote from the current notebook
nb remote remove

# remove the remote from the notebook named "example"
nb example:remote remove

```

Automatic git syncing can be turned on or off with [`nb set auto_sync`](#auto_sync).

To sync manually, use [`nb sync`](#sync):

```
# manually sync the current notebook
nb sync

# manually sync the notebook named "example"
nb example:sync

```

To bypass `nb` syncing and run `git` commands directly within a notebook, use [`nb git`](#git):

```
# run `git fetch` in the current notebook
nb git fetch origin

# run `git status` in the notebook named "example"
nb example:git status

```

#### [](#private-repositories-and-git-credentials)Private Repositories and Git Credentials

Syncing with private repositories requires configuring git to not prompt for credentials. For repositories cloned over HTTPS, [credentials can be cached with git](https://docs.github.com/en/free-pro-team@latest/github/using-git/caching-your-github-credentials-in-git) . For repositories cloned over SSH, [keys can be added to the ssh\-agent](https://docs.github.com/en/free-pro-team@latest/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent) .

Use [`nb sync`](#sync) within a notebook to determine whether your configuration is working. If `nb sync` displays a password prompt, then follow the instructions above to configure your credentials. The password prompt can be used to authenticate, but `nb` does not cache or otherwise handle git credentials in any way, so there will likely be multiple password prompts during each sync if credentials are not configured.

#### [](#sync-conflict-resolution)Sync Conflict Resolution

`nb` 自动处理git操作，因此您永远不需要直接使用 `git` 命令行工具。 `nb` 同步时合并更改，并使用几种不同的策略来处理冲突。

当 [`nb sync`](#sync) 在文本文件中遇到冲突并且无法干净地合并重叠的本地和远程更改时， `nb` 请将这两个版本保存在文件中，并用git冲突标记分隔开，并显示一条消息，指出哪个文件包含冲突的文本。 使用 [`nb edit`](#edit) 删除冲突标志和删除任何不需要的文本。

例如，在以下文件中，在两个系统上更改了第二个列表项，而git无法确定我们要保留哪个列表项：

```
# Example Title

- List Item apple
<<<<<<< HEAD
- List Item apricot
=======
- List Item pluot
>>>>>>> 719od01... [nb] Commit
- List Item plum

```

本地更改位于以 `<<<<<<<` 和 开头的行之间 `=======` ，而远程更改位于 `=======` 和 处之间 `>>>>>>>` 。

通过保持这两个项目解决这个矛盾，只需编辑与文件 `nb edit` 并删除开头的行 `<<<<<<<` ， `=======` 以及 `>>>>>>>` ：

```
# Example Title

- List Item apple
- List Item apricot
- List Item pluot
- List Item plum

```

当 `nb` 二进制文件（例如加密的便笺）中发生冲突时，文件的两个版本均作为单独的文件保存在笔记本中，并 `--conflicted-copy` 从远程附加到版本的文件名中。 要解决二进制文件的冲突副本，请比较两个版本并手动合并它们，然后删除 `--conflicted-copy` 。

如果你遇到冲突 `nb` 说，它无法在所有的合并， [`nb git`](#git) 以及 [`nb run`](#run) 可用于笔记本电脑中执行Git和外壳操作，手动解决冲突。 还请 [打开一个问题，](https://github.com/xwmx/nb/issues/new) 其中包含任何相关详细信息，这些信息可以为自动处理此类案件的策略提供信息。

### [](#️-import--export)Import️导入/导出

可以使用将任何类型的文件导入笔记本 [`nb import`](#import) 。 [`nb edit`](#edit) 并将 [`nb open`](#open) 在系统的默认应用程序中针对该文件类型打开文件。

```
# import an image file
nb import ~/Pictures/example.png

# open image in your default image viewer
nb open example.png

# import a .docx file
nb import ~/Documents/example.docx

# open .docx file in Word or your system's .docx viewer
nb open example.docx

```

支持多个文件名和通配符：

```
# import all files and directories in the current directory
nb import ./*

# import all markdown files in the current directory
nb import ./*.md

# import example.md and sample.md in the current directory
nb import example.md sample.md

```

`nb import` 也可以直接从网络下载和导入文件：

```
# import a PDF file from the web
nb import https://example.com/example.pdf
# Imported "https://example.com/example.pdf" to "example.pdf"

# open example.pdf in your system's PDF viewer
nb open example.pdf

```

某些导入的文件类型具有指示器，以使其在列表中更易于识别：

```
> nb
home
----
[6] 📖 example-ebook.epub
[5] 🌄 example-picture.png
[4] 📄 example-document.docx
[3] 📹 example-video.mp4
[2] 🔉 example-audio.mp3
[1] 📂 Example Folder

```

注释，书签和其他文件可以使用导出 [`nb export`](#export) 。 如果 安装了 [Pandoc](https://pandoc.org/) ，则注释可以自动转换为 [Pandoc支持的](https://pandoc.org/MANUAL.html#option--to) 任何 [格式](https://pandoc.org/MANUAL.html#option--to) 。 默认情况下，输出格式由文件扩展名确定：

```
# export a Markdown note to a .docx Microsoft Office Word document
nb export example.md /path/to/example.docx

# export a note titled "Movies" to an HTML web page.
nb export Movies /path/to/example.html

```

要对 `pandoc` 选项 进行更多控制 ，请使用 [`nb export pandoc`](#export) 子命令：

```
# export note 42 as an epub with pandoc options
nb export pandoc 42 --from markdown_strict --to epub -o path/to/example.epub

```

[`nb export notebook`](#export) 并且 [`nb import notebook`](#import) 可以用来导出和导入笔记本电脑：

```
# export global notebook named "example" to "../path/to/destination"
nb export notebook example ../path/to/destination

# import notebook or folder at "../path/to/notebook"
nb import notebook ../path/to/notebook

```

[`nb export notebook`](#export) 和 [`nb import notebook`](#import) 行为像别名 [`nb notebooks export`](#notebooks) 和 [`nb notebooks import`](#notebooks) ，而子可以互换使用。

有关导入和导出笔记本的更多信息，请参见“ [全局和本地笔记本”](#global-and-local-notebooks) 。

有关 `nb import` 和 `nb export` 帮助信息，请参见 [`nb help import`](#import) 和 [`nb help export`](#export) 。

### [](#️-set--settings)⚙️ `set` 和设置

[`nb set`](#settings) 并 [`nb settings`](#settings) 打开设置提示，这是更改 `nb` 设置 的简便方法 。

```
nb set

```

要在提示中更新设置，请输入设置名称或编号，然后输入新值，并将 `nb` 设置添加到您的 `~/.nbrc` 配置文件中。

#### [](#example-editor)示例：编辑器

`nb` 可以使用该 `editor` 设置 配置为使用特定的命令行编辑器 。

可以通过将设置名称或编号传递给来启动设置提示 [`nb set`](#settings) ：

```
> nb set editor
[6]  editor
     ------
     The command line text editor to use with `nb`.

     • Example Values:

         atom
         code
         emacs
         macdown
         mate
         micro
         nano
         pico
         subl
         vi
         vim

EDITOR is currently set to vim

Enter a new value, unset to set to the default value, or q to quit.
Value:

```

通过将名称和值都传递给，也可以在没有提示的情况下更新设置 `nb set` ：

```
# set editor with setting name
> nb set editor code
EDITOR set to code

# set editor with setting number (6)
> nb set 6 code
EDITOR set to code

# set the color theme to blacklight
> nb set color_theme blacklight
NB_COLOR_THEME set to blacklight

# set the default `ls` limit to 10
> nb set limit 10
NB_LIMIT set to 10

```

使用 [`nb settings get`](#settings) 打印设置的值：

```
> nb settings get editor
code

> nb settings get 6
code

```

使用 [`nb settings unset`](#settings) 来取消设置并恢复到默认值：

```
> nb settings unset editor
EDITOR restored to the default: vim

> nb settings get editor
vim

```

`nb set` 和 `nb settings` 是引用同一子命令的别名，因此两个子命令名称可以互换使用。

有关详细信息 `set` ，并 `settings` 请参阅 [`nb help settings`](#settings) 和 [`nb settings list --long`](#settings-list---long) 。

### [](#-color-themes)🎨颜色主题

`nb` 使用颜色突出显示各种界面元素，包括id，当前笔记本名称，shell提示符和分隔线。

`nb` 包括几个内置的颜色主题，还支持用户定义的主题。 可以使用 [`nb set color_theme`](#color_theme) 以下命令 设置当前颜色主题 ：

```
nb set color_theme

```

#### [](#built-in-color-themes)内置颜色主题

##### [](#blacklight)`blacklight`

| ![黑光](https://xwmx.github.io/misc/nb/images/nb-theme-blacklight-home.png) | ![黑光](https://xwmx.github.io/misc/nb/images/nb-theme-blacklight-bookmarks.png) |
| --- | --- |

##### [](#console)`console`

| ![安慰](https://xwmx.github.io/misc/nb/images/nb-theme-console-home.png) | ![安慰](https://xwmx.github.io/misc/nb/images/nb-theme-console-bookmarks.png) |
| --- | --- |

##### [](#desert)`desert`

| ![沙漠](https://xwmx.github.io/misc/nb/images/nb-theme-desert-home.png) | ![沙漠](https://xwmx.github.io/misc/nb/images/nb-theme-desert-bookmarks.png) |
| --- | --- |

##### [](#electro)`electro`

| ![电的](https://xwmx.github.io/misc/nb/images/nb-theme-electro-home.png) | ![电的](https://xwmx.github.io/misc/nb/images/nb-theme-electro-bookmarks.png) |
| --- | --- |

##### [](#forest)`forest`

| ![森林](https://xwmx.github.io/misc/nb/images/nb-theme-forest-home.png) | ![森林](https://xwmx.github.io/misc/nb/images/nb-theme-forest-bookmarks.png) |
| --- | --- |

##### [](#monochrome)`monochrome`

| ![单色](https://xwmx.github.io/misc/nb/images/nb-theme-monochrome-home.png) | ![单色](https://xwmx.github.io/misc/nb/images/nb-theme-monochrome-bookmarks.png) |
| --- | --- |

##### [](#nb-default)`nb` （默认）

| ![b](https://xwmx.github.io/misc/nb/images/nb-theme-nb-home.png) | ![b](https://xwmx.github.io/misc/nb/images/nb-theme-nb-bookmarks.png) |
| --- | --- |

##### [](#ocean)`ocean`

| ![海洋](https://xwmx.github.io/misc/nb/images/nb-theme-ocean-home.png) | ![海洋](https://xwmx.github.io/misc/nb/images/nb-theme-ocean-bookmarks.png) |
| --- | --- |

##### [](#raspberry)`raspberry`

| ![覆盆子](https://xwmx.github.io/misc/nb/images/nb-theme-raspberry-home.png) | ![覆盆子](https://xwmx.github.io/misc/nb/images/nb-theme-raspberry-bookmarks.png) |
| --- | --- |

##### [](#unicorn)`unicorn`

| ![独角兽](https://xwmx.github.io/misc/nb/images/nb-theme-unicorn-home.png) | ![独角兽](https://xwmx.github.io/misc/nb/images/nb-theme-unicorn-bookmarks.png) |
| --- | --- |

##### [](#utility)`utility`

| ![效用](https://xwmx.github.io/misc/nb/images/nb-theme-utility-home.png) | ![效用](https://xwmx.github.io/misc/nb/images/nb-theme-utility-bookmarks.png) |
| --- | --- |

#### [](#custom-color-themes)自定义颜色主题

颜色主题是 具有 文件扩展名的 [`nb`插件](#-plugins) ， `.nb-theme` 并且包含一个 `if` 语句，该语句指示名称并将颜色环境变量设置为 `tput` ANSI颜色编号：

```
# turquoise.nb-theme
if [[ "${NB_COLOR_THEME}" == "turquoise" ]]
then
  export NB_COLOR_PRIMARY=43
  export NB_COLOR_SECONDARY=38
fi

```

将该主题作为完整文件查看： [`plugins/turquoise.nb-theme`](https://github.com/xwmx/nb/blob/master/plugins/turquoise.nb-theme)

可以使用 [`nb plugins`](#plugins) 以下 主题安装主题 ：

```
> nb plugins install https://github.com/xwmx/nb/blob/master/plugins/turquoise.nb-theme
Plugin installed:
/home/example/.nb/.plugins/turquoise.nb-theme

```

安装主题后，可使用 [`nb set color_theme`](#color_theme) 将其设置为当前主题：

```
> nb set color_theme turquoise
NB_COLOR_THEME set to turquoise

```

还可以分别覆盖原色和辅助色，使颜色主题易于自定义：

```
# open the settings prompt for the primary color
nb set color_primary

# open the settings prompt for the secondary color
nb set color_secondary

```

要查看可用颜色和数字的表，请运行：

```
nb set colors

```

#### [](#syntax-highlighting-theme)语法突出显示主题

`nb` 显示文件时用语法高亮 [`bat`](https://github.com/sharkdp/bat) ， [`highlight`](http://www.andre-simon.de/doku/highlight/en/highlight.php) 或 [Pygments来做](https://pygments.org/) 安装。

当 `bat` 安装时，语法高亮颜色主题可用于光亮和黑暗的终端的背景。 要查看可用主题的列表并设置语法突出显示颜色主题，请使用 [`nb set syntax_theme`](#syntax_theme) 。

### [](#-shell-theme-support)$ Shell主题支持

*   [`astral`Zsh主题](https://github.com/xwmx/astral) \-在提示的上下文行中显示当前笔记本的名称。

### [](#-plugins)🔌插件

`nb` 包括对插件的支持，可用于创建新的子命令，设计主题以及扩展的功能 `nb` 。

`nb` 支持两种类型的插件，以其文件扩展名标识：

`.nb-theme`

定义 [颜色主题的](#custom-color-themes) 插件 。

`.nb-plugin`

定义新的子命令并添加功能的插件。

插件由 [`nb plugins`](#plugins) 子命令 管理， 并安装在 `${NB_DIR}/.plugins` 目录中。

可以使用 [`nb plugins install`](#plugins) 子命令 从URL或路径安装插件 。

```
# install a plugin from a URL
nb plugins install https://raw.githubusercontent.com/xwmx/nb/master/plugins/copy.nb-plugin

# install a plugin from a standard GitHub URL
nb plugins install https://github.com/xwmx/nb/blob/master/plugins/example.nb-plugin

# install a theme from a standard GitHub URL
nb plugins install https://github.com/xwmx/nb/blob/master/plugins/turquoise.nb-theme

# install a plugin from a path
nb plugins install plugins/example.nb-plugin

```

本 `<url>` 应是完整的URL，插件文件。 `nb` 还可以识别常规GitHub URL，这些URL可与原始GitHub URL互换使用。

可以使用列出列出已安装的插件 [`nb plugins`](#plugins) ，该 插件可以 选择一个名称并显示完整路径：

```
> nb plugins
copy.nb-plugin
example.nb-plugin
turquoise.nb-theme

> nb plugins copy.nb-plugin
copy.nb-plugin

> nb plugins --paths
/home/example/.nb/.plugins/copy.nb-plugin
/home/example/.nb/.plugins/example.nb-plugin
/home/example/.nb/.plugins/turquoise.nb-theme

> nb plugins turquoise.nb-theme --paths
/home/example/.nb/.plugins/turquoise.nb-theme

```

使用 [`nb plugins uninstall`](#plugins) 卸载插件：

```
> nb plugins uninstall example.nb-plugin
Plugin successfully uninstalled:
/home/example/.nb/.plugins/example.nb-plugin

```

#### [](#creating-plugins)创建插件

插件使用与Bash兼容的Shell脚本语言编写，并具有 `.nb-plugin` 扩展名。

`nb` 包括一些示例插件：

*   [`example.nb-plugin`](https://github.com/xwmx/nb/blob/master/plugins/example.nb-plugin)
*   [`copy.nb-plugin`](https://github.com/xwmx/nb/blob/master/plugins/copy.nb-plugin)
*   [`ebook.nb-plugin`](https://github.com/xwmx/nb/blob/master/plugins/ebook.nb-plugin)

通过三个简单的步骤创建一个新的子命令：

##### [](#1-add-the-new-subcommand-name-with-_subcommands-add-name)1.使用以下命令添加新的子命令名称 `_subcommands add <name>` ：

```
_subcommands add "example"

```

##### [](#2-define-help-and-usage-text-with-_subcommands-describe-subcommand-usage)2.使用以下命令定义帮助和用法文本 `_subcommands describe <subcommand> <usage>` ：

```
_subcommands describe "example" <<HEREDOC
Usage:
  nb example

Description:
  Print "Hello, World!"
HEREDOC

```

##### [](#3-define-the-subcommand-as-a-function-named-with-a-leading-underscore)3.将子命令定义为一个函数，并用前划线表示：

```
_example() {
  printf "Hello, World!\\n"
}

```

而已！ 🎉

查看完整的插件： [`plugins/example.nb-plugin`](https://github.com/xwmx/nb/blob/master/plugins/example.nb-plugin)

与 `example.nb-plugin` 安装， `nb` 包括 `nb example` 子命令打印“你好，世界！”

举一个完整的例子，向它 [`copy.nb-plugin`](https://github.com/xwmx/nb/blob/master/plugins/copy.nb-plugin) 添加复制/重复功能， `nb` 并演示如何使用 `nb` 子命令和简单的shell脚本 创建插件 。

您可以使用本地安装在本地创建的任何插件 `nb plugins install <path>` ，也可以将其发布在GitHub，GitLab或其他在线位置上，并使用进行安装 `nb plugins install <url>` 。

#### [](#api)API

该 `nb` API是 [命令行界面](#nb-help) ，旨在实现可组合性，并提供了各种功能强大的选项，可用于与便笺，书签，笔记本和 `nb` 功能 进行交互 。 在插件中，可以使用子函数的函数名来调用子命令，这些函数名以前划线表示。 选项可用于以适合于解析和处理的格式输出信息：

```
# print the content of note 3 to standard output with no color
_show 3 --print --no-color

# list all unarchived global notebook names
_notebooks --names --no-color --unarchived --global

# list all filenames in the current notebook
_list --filenames --no-id --no-indicator

# print the path to the current notebook
_notebooks current --path

```

##### [](#selectors)选择器

[`nb`](#-notebooks) 用户可以在每个命令的基础上选择 [笔记本，](#-notebooks) 方法是在子命令名称或注释标识符（id，文件名，路径或标题）前添加笔记本名称，后跟冒号。 冒号前缀的参数称为“选择器”，它有两种类型：子命令选择器和标识符选择器。

*子命令选择器*

```
notebook:
notebook:show
notebook:history
notebook:a
notebook:q

```

*识别符选择器*

```
1
example.md
title
/path/to/example.md
notebook:1
notebook:example.md
notebook:title
notebook:/path/to/example.md

```

`nb` 自动搜索具有笔记本名称的选择器的参数，并在找到有效笔记本时更新当前笔记本。

标识符选择器与所有子命令选项一起作为参数传递给子命令。 使用 [`show <selector>`](#show) 约在选择指定的文件查询信息。 例如，要获取选择器指定文件的文件名，请使用 `show <selector> --filename` ：

```
_example() {
  local _selector="${1:-}"
  [[ -z "${_selector:-}" ]] && printf "Usage: example <selector>\\n" && exit 1

  # Get the filename using the selector.
  local _filename
  _filename="$(_show "${_selector}" --filename)"

  # Rest of subcommand function...
}

```

[`notebooks current --path`](#notebooks) 返回当前笔记本的路径：

```
# _example() continued:

# get the notebook path
local _notebook_path
_notebook_path="$(_notebooks current --path)"

# print the file at "${_notebook_path}/${_filename}" to standard output
cat "${_notebook_path}/${_filename}"

```

有关 [`copy.nb-plugin`](https://github.com/xwmx/nb/blob/master/plugins/copy.nb-plugin) 使用 [`show <selector> --filename`](#show) 和 [`notebooks current --path`](#notebooks) 以及其他使用下划线前缀的函数名称调用的子命令的 示例， 请参见 。

### [](#-nb-interactive-shell)\> `nb` 互动壳

`nb` 具有可与启动一个交互的shell [`nb shell`](#shell) ， `nb -i` 或者 `nb --interactive` ：

```
$ nb shell
__          _
\ \   _ __ | |__
 \ \ | '_ \| '_ \
 / / | | | | |_) |
/_/  |_| |_|_.__/
------------------
nb shell started. Enter ls to list notes and notebooks.
Enter help for usage information. Enter exit to exit.
nb> ls
home
----
[3] Example
[2] Sample
[1] Demo

nb> edit 3 --content "New content."
Updated: [3] Example

nb> bookmark https://example.com
Added: [4] 🔖 example.bookmark.md "Example Title (example.com)"

nb> ls
home
----
[4] 🔖 Example Title (example.com)
[3] Example
[2] Sample
[1] Demo

nb> bookmark url 4
https://example.com

nb> search "example"
[4] example.bookmark.md "Example (example.com)"
-----------------------------------------------
1:# Example (example.com)

3:<https://example.com>

[3] example.md "Example"
------------------------
1:# Example

nb> exit
$

```

该 `nb` Shell可以识别所有 `nb` 子命令和选项，从而提供了一种简化的，无干扰的处理方法 `nb` 。

### [](#shortcut-aliases)快捷方式别名

几个核心 `nb` 子命令具有单字符别名，以使它们可以更快地使用：

```
# `a` (add): add a new note named "example.md"
nb a example.md

# `b` (bookmark): list bookmarks
nb b

# `o` (open): open bookmark 12 in your web browser
nb o 12

# `p` (peek): open bookmark 6 in your terminal browser
nb p 6

# `e` (edit): edit note 5
nb e 5

# `d` (delete): delete note 19
nb d 19

# `s` (show): show note 27
nb s 27

# `q` (search): search notes for "example query"
nb q "example query"

# `h` (help): display the help information for the `add` subcommand
nb h add

# `u` (use): switch to example-notebook
nb u example-notebook

```

有关更多命令和选项，请运行 `nb help` 或 `nb help <subcommand>`

### [](#help)帮帮我

[nb](#nb-help) • [书签](#bookmark-help) • [子命令](#subcommands) • [插件](#plugins-1)

#### [](#nb-help)`nb help`

```
__          _
\ \   _ __ | |__
 \ \ | '_ \| '_ \
 / / | | | | |_) |
/_/  |_| |_|_.__/

[nb] Command line note-taking, bookmarking, archiving with plain-text data
storage, encryption, filtering and search, Git-backed versioning and syncing,
Pandoc-backed conversion, global and local notebooks, customizable color
themes, plugins, and more in a single portable, user-friendly script.

Help:
  nb help               Display this help information.
  nb help <subcommand>  View help information for <subcommand>.
  nb help --colors      View information about color settings.
  nb help --readme      View the `nb` README file.

Usage:
  nb
  nb [<ls options>...] [<id> | <filename> | <path> | <title> | <notebook>]
  nb [<url>] [<bookmark options>...]
  nb add [<filename> | <content>] [-c <content> | --content <content>]
         [-e | --encrypt] [-f <filename> | --filename <filename>]
         [-t <title> | --title <title>] [--type <type>]
  nb bookmark [<ls options>...]
  nb bookmark <url> [-c <comment> | --comment <comment>] [--edit]
              [-e | --encrypt] [-f <filename> | --filename <filename>]
              [-q | --quote] [-r <url> | --related <url>]... [--save-source]
              [--skip-content] [-t <tag1>,<tag2>... | --tags <tag1>,<tag2>...]
              [--title <title>]
  nb bookmark [list [<list-options>...]]
  nb bookmark (open | peek | url) (<id> | <filename> | <path> | <title>)
  nb bookmark (edit | delete) (<id> | <filename> | <path> | <title>)
  nb bookmark search <query>
  nb completions (check | install [-d | --download] | uninstall)
  nb count
  nb delete (<id> | <filename> | <path> | <title>) [-f | --force]
  nb edit (<id> | <filename> | <path> | <title>)
          [-c <content> | --content <content>] [--edit]
          [-e <editor> | --editor <editor>]
  nb export (<id> | <filename> | <path> | <title>) <path> [-f | --force]
            [<pandoc options>...]
  nb export notebook <name> [<path>]
  nb export pandoc (<id> | <filename> | <path> | <title>)
            [<pandoc options>...]
  nb git [checkpoint [<message>] | dirty]
  nb git <git-options>...
  nb help [<subcommand>] [-p | --print]
  nb help [-c | --colors] | [-r | --readme] | [-s | --short] [-p | --print]
  nb history [<id> | <filename> | <path> | <title>]
  nb import [copy | download | move] (<path>... | <url>) [--convert]
  nb import notebook <path> [<name>]
  nb init [<remote-url>]
  nb list [-e [<length>] | --excerpt [<length>]] [--filenames]
          [-n <limit> | --limit <limit> |  --<limit>] [--no-id]
          [--no-indicator] [-p | --pager] [--paths] [-s | --sort]
          [-r | --reverse] [-t <type> | --type <type> | --<type>]
          [<id> | <filename> | <path> | <title> | <query>]
  nb ls [-a | --all] [-e [<length>] | --excerpt [<length>]] [--filenames]
        [-n <limit> | --limit <limit> | --<limit>] [--no-id] [--no-indicator]
        [-p | --pager] [--paths] [-s | --sort] [-r | --reverse]
        [-t <type> | --type <type> | --<type>]
        [<id> | <filename> | <path> | <title> | <query>]
  nb move (<id> | <filename> | <path> | <title>) [-f | --force] <notebook>
  nb notebooks [<name>] [--archived] [--global] [--local] [--names]
               [--paths] [--unarchived]
  nb notebooks add <name> [<remote-url>]
  nb notebooks (archive | open | peek | status | unarchive) [<name>]
  nb notebooks current [--path | --selected | --filename [<filename>]]
                       [--global | --local]
  nb notebooks delete <name> [-f | --force]
  nb notebooks (export <name> [<path>] | import <path>)
  nb notebooks init [<path> [<remote-url>]]
  nb notebooks rename <old-name> <new-name>
  nb notebooks select <selector>
  nb notebooks show (<name> | <path> | <selector>) [--archived]
                    [--escaped | --name | --path | --filename [<filename>]]
  nb notebooks use <name>
  nb show (<id> | <filename> | <path> | <title>) [[-a | --added] |
          --filename | --id | --info-line | --path | [-p | --print]
          [-r | --render] | --selector-id | --title | --type [<type>] |
          [-u | --updated]]
  nb notebooks use <name>
  nb open (<id> | <filename> | <path> | <title> | <notebook>)
  nb peek (<id> | <filename> | <path> | <title> | <notebook>)
  nb plugins [<name>] [--paths]
  nb plugins install [<path> | <url>] [--force]
  nb plugins uninstall <name> [--force]
  nb remote [remove | set <url> [-f | --force]]
  nb rename (<id> | <filename> | <path> | <title>) [-f | --force]
            (<name> | --reset | --to-bookmark | --to-note)
  nb run <command> [<arguments>...]
  nb search <query> [-a | --all] [-t <type> | --type <type> | --<type>]
                    [-l | --list] [--path]
  nb set [<name> [<value>] | <number> [<value>]]
  nb settings [colors [<number> | themes] | edit | list [--long]]
  nb settings (get | show | unset) (<name> | <number>)
  nb settings set (<name> | <number>) <value>
  nb shell [<subcommand> [<options>...] | --clear-history]
  nb show (<id> | <filename> | <path> | <title>) [--added | --filename |
          --id | --info-line | --path | [-p | --print] [-r | --render] |
          --selector-id | --title | --type [<type>] | --updated]
  nb show <notebook>
  nb subcommands [add <name>...] [alias <name> <alias>]
                 [describe <name> <usage>]
  nb sync [-a | --all]
  nb update
  nb use <notebook>
  nb -i | --interactive [<subcommand> [<options>...]]
  nb -h | --help | help [<subcommand> | --readme]
  nb --no-color
  nb --version | version

Subcommands:
  (default)    List notes and notebooks. This is an alias for `nb ls`.
               When a <url> is provided, create a new bookmark.
  add          Add a new note.
  bookmark     Add, open, list, and search bookmarks.
  completions  Install and uninstall completion scripts.
  count        Print the number of notes.
  delete       Delete a note.
  edit         Edit a note.
  export       Export a note to a variety of different formats.
  git          Run `git` commands within the current notebook.
  help         View help information for the program or a subcommand.
  history      View git history for the current notebook or a note.
  import       Import a file into the current notebook.
  init         Initialize the first notebook.
  list         List notes in the current notebook.
  ls           List notebooks and notes in the current notebook.
  move         Move a note to a different notebook.
  notebooks    Manage notebooks.
  open         Open a bookmarked web page or notebook folder, or edit a note.
  peek         View a note, bookmarked web page, or notebook in the terminal.
  plugins      Install and uninstall plugins and themes.
  remote       Get, set, and remove the remote URL for the notebook.
  rename       Rename a note.
  run          Run shell commands within the current notebook.
  search       Search notes.
  settings     Edit configuration settings.
  shell        Start the `nb` interactive shell.
  show         Show a note or notebook.
  status       Run `git status` in the current notebook.
  subcommands  List, add, alias, and describe subcommands.
  sync         Sync local notebook with the remote repository.
  update       Update `nb` to the latest version.
  use          Switch to a notebook.
  version      Display version information.

Notebook Usage:
  nb <notebook>:[<subcommand>] [<identifier>] [<options>...]
  nb <subcommand> <notebook>:<identifier> [<options>...]

Program Options:
  -i, --interactive   Start the `nb` interactive shell.
  -h, --help          Display this help information.
  --no-color          Print without color highlighting.
  --version           Display version information.

More Information:
  https://github.com/xwmx/nb

```

#### [](#bookmark-help)`bookmark help`

```
    __                __                        __
   / /_  ____  ____  / /______ ___  ____ ______/ /__
  / __ \/ __ \/ __ \/ //_/ __ `__ \/ __ `/ ___/ //_/
 / /_/ / /_/ / /_/ / ,< / / / / / / /_/ / /  / ,<
/_.___/\____/\____/_/|_/_/ /_/ /_/\__,_/_/  /_/|_|

bookmark -- Command line bookmarking with tagging, encryption,
full-text page content search with regular expression support,
GUI and terminal browser support, and data stored in plain text
Markdown files with Git-backed versioning and syncing.

Usage:
  bookmark [<ls options>...]
  bookmark <url> [-c <comment> | --comment <comment>] [--edit]
              [-e | --encrypt] [-f <filename> | --filename <filename>]
              [-q | --quote] [-r <url> | --related <url>]... [--save-source]
              [--skip-content] [-t <tag1>,<tag2>... | --tags <tag1>,<tag2>...]
              [--title <title>]
  bookmark list [<list-options>...]
  bookmark (open | peek | url) (<id> | <filename> | <path> | <title>)
  bookmark (edit | delete) (<id> | <filename> | <path> | <title>)
  bookmark search <query>

Options:
  -c, --comment <comment>      A comment or description for this bookmark.
  --edit                       Open the bookmark in your editor before saving.
  -e, --encrypt                Encrypt the bookmark with a password.
  -f, --filename <filename>    The filename for the bookmark. It is
                               recommended to omit the extension so the
                               default bookmark extension is used.
  -q, --quote <quote>          A quote or excerpt from the saved page.
                               Alias: `--excerpt`
  -r, --related <url>          A URL for a page related to the bookmarked page.
                               Multiple `--related` flags can be used in a
                               command to save multiple related URLs.
  --save-source                Save the page source as HTML.
  --skip-content               Omit page content from the note.
  -t, --tags <tag1>,<tag2>...  A comma-separated list of tags.
  --title <title>              The bookmark title. When not specified,
                               `nb` will use the html <title> tag.

Subcommands:
  (default)  Add a new bookmark for <url>, or list bookmarks.
             Bookmarks can also be added with `nb <url>`
  delete     Delete a bookmark.
  edit       Edit a bookmark.
  list       List bookmarks in the current notebook.
             Shortcut Alias: `ls`
  open       Open the bookmarked page in your system's primary web browser.
             Shortcut Alias: `o`
  peek       Open the bookmarked page in your terminal web browser.
             Alias: `preview`
             Shortcut Alias: `p`
  search     Search bookmarks for <query>.
             Shortcut Alias: `q`
  url        Print the URL for the specified bookmark.

Description:
  Create, view, search, edit, and delete bookmarks.

  By default, the html page content is saved within the bookmark, making the
  bookmarked page available for full-text search. When Pandoc [1] is
  installed, the HTML content will be converted to Markdown before saving.
  When readability-cli [2] is install, markup is cleaned up to focus on
  content.

  `peek` opens the page in `w3m` [3] or `lynx` [4] when available.
  To specify a preferred browser, set the `$BROWSER` environment variable
  in your .bashrc, .zshrc, or equivalent, e.g., `export BROWSER="lynx"`.

  Bookmarks are identified by the `.bookmark.md` file extension. The
  bookmark URL is the first URL in the file within "<" and ">" characters:

    <https://www.example.com>

    1. https://pandoc.org/
    2. https://gitlab.com/gardenappl/readability-cli
    3. https://en.wikipedia.org/wiki/W3m
    4. https://en.wikipedia.org/wiki/Lynx_(web_browser)

Examples:
  bookmark https://example.com
  bookmark https://example.com --encrypt
  bookmark https://example.com --tags example,sample,demo
  bookmark https://example.com/about -c "Example comment."
  bookmark https://example.com/faqs -f example-filename
  bookmark https://example.com --quote "Example quote or excerpt."
  bookmark list
  bookmark search "example query"
  bookmark open 5

------------------------------------------
Part of `nb` (https://github.com/xwmx/nb).
For more information, see: `nb help`.

```

### [](#subcommands)子指令

[add](#add) • [bookmark](#bookmark) • [completions](#completions) • [count](#count) • [delete](#delete) • [edit](#edit) • [env](#env) • [export](#export) • [git](#git) • [help](#help-1) • [history](#history) • [import](#import) • [init](#init) • [list](#list) • [ls](#ls) • [move](#move) • [notebooks](#notebooks) • [open](#open) • [peek](#peek) • [plugins](#plugins) • [remote](#remote) • [rename](#rename) • [run](#run) • [search](#search) • [settings](#settings) • [shell](#shell) • [显示](#show) • [状态](#status) • [子命令](#subcommands-1) • [同步](#sync) • [更新](#update) • [使用](#use) • [版本](#version)

#### [](#add)`add`

```
Usage:
  nb add [<filename> | <content>] [-c <content> | --content <content>]
         [--edit] [-e | --encrypt] [-f <filename> | --filename <filename>]
         [-t <title> | --title <title>] [--type <type>]

Options:
  -c, --content <content>     The content for the new note.
  --edit                      Open the note in the editor before saving when
                              content is piped or passed as an argument.
  -e, --encrypt               Encrypt the note with a password.
  -f, --filename <filename>   The filename for the new note. The default
                              extension is used when the extension is omitted.
  -t, --title <title>         The title for a new note. If `--title` is
                              present, the filename will be derived from the
                              title, unless `--filename` is specified.
  --type <type>               The file type for the new note, as a file
                              extension.

Description:
  Create a new note.

  If no arguments are passed, a new blank note file is opened with
  `$EDITOR`, currently set to "example". If a non-option argument is
  passed, `nb` will treat it as a <filename≥ if a file extension is found.
  If no file extension is found, `nb` will treat the string as
  <content> and will create a new note without opening the editor.
  `nb add` can also create a new note with piped content.

  `nb` creates Markdown files by default. To create a note with a
  different file type, use the extension in the filename or use the `--type`
  option. To change the default file type, use `nb set default_extension`.

  When the `-e` / `--encrypt` option is used, `nb` will encrypt the
  note with AES-256 using OpenSSL by default, or GPG, if configured in
  `nb set encryption_tool`.

Examples:
  nb add
  nb add example.md
  nb add "Note content."
  nb add example.md --title "Example Title" --content "Example content."
  echo "Note content." | nb add
  nb add -t "Secret Document" --encrypt
  nb example:add
  nb example:add -t "Title"
  nb a
  nb a "Note content."
  nb example:a
  nb example:a -t "Title"

Aliases: `create`, `new`
Shortcut Alias: `a`

```

#### [](#bookmark)`bookmark`

```
Usage:
  nb bookmark [<ls options>...]
  nb bookmark <url> [-c <comment> | --comment <comment>] [--edit]
              [-e | --encrypt] [-f <filename> | --filename <filename>]
              [-q | --quote] [-r <url> | --related <url>]... [--save-source]
              [--skip-content] [-t <tag1>,<tag2>... | --tags <tag1>,<tag2>...]
              [--title <title>]
  nb bookmark list [<list-options>...]
  nb bookmark (open | peek | url) (<id> | <filename> | <path> | <title>)
  nb bookmark (edit | delete) (<id> | <filename> | <path> | <title>)
  nb bookmark search <query>

Options:
  -c, --comment <comment>      A comment or description for this bookmark.
  --edit                       Open the bookmark in your editor before saving.
  -e, --encrypt                Encrypt the bookmark with a password.
  -f, --filename <filename>    The filename for the bookmark. It is
                               recommended to omit the extension so the
                               default bookmark extension is used.
  -q, --quote <quote>          A quote or excerpt from the saved page.
                               Alias: `--excerpt`
  -r, --related <url>          A URL for a page related to the bookmarked page.
                               Multiple `--related` flags can be used in a
                               command to save multiple related URLs.
  --save-source                Save the page source as HTML.
  --skip-content               Omit page content from the note.
  -t, --tags <tag1>,<tag2>...  A comma-separated list of tags.
  --title <title>              The bookmark title. When not specified,
                               `nb` will use the html <title> tag.

Subcommands:
  (default)  Add a new bookmark for <url>, or list bookmarks.
             Bookmarks can also be added with `nb <url>`
  delete     Delete a bookmark.
  edit       Edit a bookmark.
  list       List bookmarks in the current notebook.
             Shortcut Alias: `ls`
  open       Open the bookmarked page in your system's primary web browser.
             Shortcut Alias: `o`
  peek       Open the bookmarked page in your terminal web browser.
             Alias: `preview`
             Shortcut Alias: `p`
  search     Search bookmarks for <query>.
             Shortcut Alias: `q`
  url        Print the URL for the specified bookmark.

Description:
  Create, view, search, edit, and delete bookmarks.

  By default, the html page content is saved within the bookmark, making the
  bookmarked page available for full-text search. When Pandoc [1] is
  installed, the HTML content will be converted to Markdown before saving.
  When readability-cli [2] is install, markup is cleaned up to focus on
  content.

  `peek` opens the page in `w3m` [3] or `lynx` [4] when available.
  To specify a preferred browser, set the `$BROWSER` environment variable
  in your .bashrc, .zshrc, or equivalent, e.g., `export BROWSER="lynx"`.

  Bookmarks are identified by the `.bookmark.md` file extension. The
  bookmark URL is the first URL in the file within "<" and ">" characters:

    <https://www.example.com>

    1. https://pandoc.org/
    2. https://gitlab.com/gardenappl/readability-cli
    3. https://en.wikipedia.org/wiki/W3m
    4. https://en.wikipedia.org/wiki/Lynx_(web_browser)

Examples:
  nb https://example.com
  nb example: https://example.com
  nb https://example.com --encrypt
  nb https://example.com --tags example,sample,demo
  nb https://example.com/about -c "Example comment."
  nb https://example.com/faqs -f example-filename
  nb https://example.com --quote "Example quote or excerpt."
  nb bookmark list
  nb bookmark search "example query"
  nb bookmark open 5
  nb b

Shortcut Alias: `b`

```

#### [](#completions)`completions`

```
Usage:
  nb completions (check | install [-d | --download] | uninstall)

Options:
  -d, --download  Download the completion scripts and install.

Description:
  Manage completion scripts. For more information, visit:
  https://github.com/xwmx/nb/blob/master/etc/README.md

```

#### [](#count)`count`

```
Usage:
  nb count

Description:
  Print the number of items in the current notebook.

```

#### [](#delete)`delete`

```
Usage:
  nb delete (<id> | <filename> | <path> | <title>) [-f | --force]

Options:
  -f, --force   Skip the confirmation prompt.

Description:
  Delete a note.

Examples:
  nb delete 3
  nb delete example.md
  nb delete "A Document Title"
  nb 3 delete --force
  nb example:delete 12
  nb delete example:12
  nb example:12 delete
  nb d 3
  nb 3 d
  nb d example:12
  nb example:12 d

Shortcut Alias: `d`

```

#### [](#edit)`edit`

```
Usage:
  nb edit (<id> | <filename> | <path> | <title>)
          [-c <content> | --content <content>] [--edit]
          [-e <editor> | --editor <editor>]

Options:
  -c, --content <content>  The content for the new note.
  --edit                   Open the note in the editor before saving when
                           content is piped or passed as an argument.
  -e, --editor <editor>    Edit the note with <editor>, overriding the editor
                           specified in the `$EDITOR` environment variable.

Description:
  Open the specified note in `$EDITOR` or <editor> if specified. Content
  piped to `nb edit` or passed using the `--content` option will will be
  appended to the file without opening it in the editor, unless the
  `--edit` flag is specified.

  Non-text files will be opened in your system's preferred app or program for
  that file type.

Examples:
  nb edit 3
  nb edit example.md
  nb edit "A Document Title"
  echo "Content to append." | nb edit 1
  nb 3 edit
  nb example:edit 12
  nb edit example:12
  nb example:12 edit
  nb e 3
  nb 3 e
  nb e example:12
  nb example:12 e

Shortcut Alias: `e`

```

#### [](#env)`env`

```
Usage:
  nb env [install]

Subcommands:
  install  Install dependencies on supported systems.

Description:
  Print program environment and configuration information, or install
  dependencies.

```

#### [](#export)`export`

```
Usage:
  nb export (<id> | <filename> | <path> | <title>) <path> [-f | --force]
            [<pandoc options>...]
  nb export notebook <name> [<path>]
  nb export pandoc (<id> | <filename> | <path> | <title>)
            [<pandoc options>...]

Options:
  -f, --force   Skip the confirmation prompt when overwriting an existing file.

Subcommands:
  (default)     Export a file to <path>. If <path> has a different extension
                than the source note, convert the note using `pandoc`.
  notebook      Export the notebook <name> to the current directory or <path>.
                Alias for `nb notebooks export`.
  pandoc        Export the file to standard output or a file using `pandoc`.
                `export pandoc` prints to standard output by default.

Description:
  Export a file or notebook.

  If Pandoc [1] is available, convert the note from its current format
  to the format of the output file as indicated by the file extension
  in <path>. Any additional arguments are passed directly to Pandoc.
  See the Pandoc help information for available options.

    1. https://pandoc.org/

Examples:
  # Export an Emacs Org mode note
  nb export example.org /path/to/example.org

  # Export a Markdown note to HTML and print to standard output
  nb export pandoc example.md --from=markdown_strict --to=html

  # Export a Markdown note to a .docx Microsoft Office Word document
  nb export example.md /path/to/example.docx

  # Export note 12 in the "sample" notebook to HTML
  nb export sample:12 /path/to/example.html

```

#### [](#git)`git`

```
Usage:
  nb git [checkpoint [<message>] | dirty]
  nb git <git-options>...

Subcommands:
  checkpoint    Create a new git commit in the current notebook and sync with
                the remote if `nb set auto_sync` is enabled.
  dirty         0 (success, true) if there are uncommitted changes in
                <notebook-path>. 1 (error, false) if <notebook-path> is clean.

Description:
  Run `git` commands within the current notebook directory.

Examples:
  nb git status
  nb git diff
  nb git log
  nb example:git status

```

#### [](#help-1)`help`

```
Usage:
  nb help [<subcommand>] [-p | --print]
  nb help [-c | --colors] | [-r | --readme] | [-s | --short] [-p | --print]

Options:
  -c, --colors  View information about color themes and color settings.
  -p, --print   Print to standard output / terminal.
  -r, --readme  View the `nb` README file.
  -s, --short   Print shorter help without subcommand descriptions.

Description:
  Print the program help information. When a subcommand name is passed, print
  the help information for the subcommand.

Examples:
  nb help
  nb help add
  nb help import
  nb h notebooks
  nb h e

Shortcut Alias: `h`

```

#### [](#history)`history`

```
Usage:
  nb history [<id> | <filename> | <path> | <title>]

Description:
  Display notebook history using `tig` [1] (if available) or `git log`.
  When a note is specified, the history for that note is displayed.

    1. https://github.com/jonas/tig

Examples:
  nb history
  nb history example.md
  nb 3 history
  nb history example:
  nb example:history
  nb example:history 12
  nb history example:12
  nb example:12 history

```

#### [](#import)`import`

```
Usage:
  nb import (<path>... | <url>)
  nb import copy <path>...
  nb import download <url> [--convert]
  nb import move <path>...
  nb import notebook <path> [<name>]

Options:
  --convert  Convert HTML content to Markdown.

Subcommands:
  (default) Copy or download the file(s) at <path> or <url>.
  copy      Copy the file(s) at <path> into the current notebook.
  download  Download the file at <url> into the current notebook.
  move      Move the file(s) at <path> into the current notebook.
  notebook  Import the local notebook at <path> to make it global.

Description:
  Copy, move, or download files into the current notebook or import
  a local notebook to make it global.

Examples:
  nb import ~/Pictures/example.png
  nb import ~/Documents/example.docx
  nb import https://example.com/example.pdf
  nb example:import https://example.com/example.jpg
  nb import ./*
  nb import ./*.md

```

#### [](#init)`init`

```
Usage:
  nb init [<remote-url>]

Description:
  Initialize the local data directory and generate configuration file for `nb`
  if it doesn't exist yet at:

      ~/.nbrc

Examples:
  nb init
  nb init https://github.com/example/example.git

```

#### [](#list)`list`

```
Usage:
  nb list [-e [<length>] | --excerpt [<length>]] [--filenames]
          [-n <limit> | --limit <limit> |  --<limit>] [--no-id]
          [--no-indicator] [-p | --pager] [--paths] [-s | --sort]
          [-r | --reverse] [-t <type> | --type <type> | --<type>]
          [<id> | <filename> | <path> | <title> | <query>]

Options:
  -e, --excerpt [<length>]        Print an excerpt <length> lines long under
                                  each note's filename [default: 3].
  --filenames                     Print the filename for each note.
  -n, --limit <limit>, --<limit>  The maximum number of notes to list.
  --no-id                         Don't include the id in list items.
  --no-indicator                  Don't include the indicator in list items.
  -p, --pager                     Display output in the pager.
  --paths                         Print the full path to each item.
  -s, --sort                      Order notes by id.
  -r, --reverse                   List items in reverse order.
  -t, --type <type>, --<type>     List items of <type>. <type> can be a file
                                  extension or one of the following types:
                                  archive, audio, book, bookmark, document,
                                  folder, image, note, text, video

Description:
  List notes in the current notebook.

  When <id>, <filename>, <path>, or <title> are present, the listing for the
  matching note will be displayed. When no match is found, titles and
  filenames will be searched for any that match <query> as a case-insensitive
  regular expression.

Indicators:
  🔉  Audio
  📖  Book
  🔖  Bookmark
  🔒  Encrypted
  📂  Folder
  🌄  Image
  📄  PDF, Word, or Open Office document
  📹  Video

Examples:
  nb list
  nb list example.md -e 10
  nb list --excerpt --no-id
  nb list --filenames --reverse
  nb list "^Example.*"
  nb list --10
  nb list --type document
  nb example:list

```

#### [](#ls)`ls`

```
Usage:
  nb ls [-a | --all] [-e [<length>] | --excerpt [<length>]] [--filenames]
        [--no-id] [--no-indicator] [-n <limit> | --limit <limit> | --<limit>]
        [-p | --pager] [--paths] [-s | --sort] [-r | --reverse]
        [-t <type> | --type <type> | --<type>]
        [<id> | <filename> | <path> | <title> | <query>]

Options:
  -a, --all                       Print all items in the notebook. Equivalent
                                  to no limit.
  -e, --excerpt [<length>]        Print an excerpt <length> lines long under
                                  each note's filename [default: 3].
  --filenames                     Print the filename for each note.
  -n, --limit <limit>, --<limit>  The maximum number of listed items.
                                  [default: 20]
  --no-id                         Don't include the id in list items.
  --no-indicator                  Don't include the indicator in list items.
  -p, --pager                     Display output in the pager.
  --paths                         Print the full path to each item.
  -s, --sort                      Order notes by id.
  -r, --reverse                   List items in reverse order.
  -t, --type <type>, --<type>     List items of <type>. <type> can be a file
                                  extension or one of the following types:
                                  archive, audio, book, bookmark, document,
                                  folder, image, note, text, video

Description:
  List notebooks and notes in the current notebook, displaying note titles
  when available. `nb ls` is a combination of `nb notebooks` and
  `nb list` in one view.

  When <id>, <filename>, <path>, or <title> are present, the listing for the
  matching note will be displayed. When no match is found, titles and
  filenames will be searched for any that match <query> as a case-insensitive
  regular expression.

  Options are passed through to `list`. For more information, see
  `nb help list`.

Indicators:
  🔉  Audio
  📖  Book
  🔖  Bookmark
  🔒  Encrypted
  📂  Folder
  🌄  Image
  📄  PDF, Word, or Open Office document
  📹  Video

Examples:
  nb
  nb --all
  nb ls
  nb ls example.md -e 10
  nb ls --excerpt --no-id
  nb ls --reverse
  nb ls "^Example.*"
  nb ls --10
  nb ls --type document
  nb example:
  nb example: -ae
  nb example:ls

```

#### [](#move)`move`

```
Usage:
  nb move (<id> | <filename> | <path> | <title>) [-f | --force] <notebook>

Options:
  -f, --force   Skip the confirmation prompt.

Description:
  Move the specified note to <notebook>.

Examples:
  nb move 1 example-notebook
  nb move example.md example-notebook
  nb example:move sample.md other-notebook
  nb move example:sample.md other-notebook
  nb mv 1 example-notebook

Shortcut Alias: `mv`

```

#### [](#notebooks)`notebooks`

```
Usage:
  nb notebooks [<name>] [--archived] [--global] [--local] [--names]
               [--paths] [--unarchived]
  nb notebooks add <name> [<remote-url>]
  nb notebooks (archive | open | peek | status | unarchive) [<name>]
  nb notebooks current [--path | --selected | --filename [<filename>]]
                       [--global | --local]
  nb notebooks delete <name> [-f | --force]
  nb notebooks (export <name> [<path>] | import <path>)
  nb notebooks init [<path> [<remote-url>]]
  nb notebooks rename <old-name> <new-name>
  nb notebooks select <selector>
  nb notebooks show (<name> | <path> | <selector>) [--archived]
                    [--escaped | --name | --path | --filename [<filename>]]
  nb notebooks use <name>

Options:
  --archived               List archived notebooks, or return archival status
                           with `show`.
  --escaped                Print the notebook name with spaces escaped.
  --filename [<filename>]  Print an available filename for the notebooks. When
                           <filename> is provided, check for an existing file
                           and provide a filename with an appended sequence
                           number for uniqueness.
  --global                 List global notebooks or the notebook set globally
                           with `use`.
  --local                  Exit with 0 if current within a local notebook,
                           otherwise exit with 1.
  -f, --force              Skip the confirmation prompt.
  --name, --names          Print the notebook name.
  --path, --paths          Print the notebook path.
  --selected               Exit with 0 if the current notebook differs from
                           the current global notebook, otherwise exit with 1.
  --unarchived             Only list unarchived notebooks.

Subcommands:
  (default)  List notebooks.
  add        Create a new global notebook. When an existing notebook's
             <remote-url> is specified, create the new global notebook as a
             clone of <remote-url>.
             Aliases: `notebooks create`, `notebooks new`
  archive    Set the current notebook or notebook <name> to "archived" status.
  export     Export the notebook <name> to the current directory or <path>,
             making it usable as a local notebook.
  import     Import the local notebook at <path> to make it global.
  init       Create a new local notebook. Specify a <path> or omit to
             initialize the current working directory as a local notebook.
             Specify <remote-url> to clone an existing notebook.
  current    Print the current notebook name or path.
  delete     Delete a notebook.
  open       Open the current notebook directory or notebook <name> in your
             file browser, explorer, or finder.
             Shortcut Alias: `o`
  peek       Open the current notebook directory or notebook <name> in the
             first tool found in the following list:
             `ranger` [1], `mc` [2], `exa` [3], or `ls`.
             Shortcut Alias: `p`
  rename     Rename a notebook.
  select     Set the current notebook from a colon-prefixed selector.
             Not persisted. Selection format: <notebook>:<identifier>
  show       Show and return information about a specified notebook.
  status     Print the archival status of the current notebook or
             notebook <name>.
  unarchive  Remove "archived" status from current notebook or notebook <name>.
  use        Switch to a notebook.

    1. https://ranger.github.io/
    2. https://en.wikipedia.org/wiki/Midnight_Commander
    3. https://github.com/ogham/exa

Description:
  Manage notebooks.

Examples:
  nb notebooks --names
  nb notebooks add sample
  nb notebooks add example https://github.com/example/example.git
  nb n current --path
  nb n archive example

Shortcut Alias: `n`

```

#### [](#open)`open`

```
Usage:
  nb open (<id> | <filename> | <path> | <title> | <notebook>)

Description:
  Open a note or notebook. When the note is a bookmark, open the bookmarked
  page in your system's primary web browser. When the note is in a text format
  or any other file type, `open` is the equivalent of `edit`. `open`
  with a notebook opens the notebook folder in the system's file browser.

Examples:
  nb open 3
  nb open example.bookmark.md
  nb 3 open
  nb example:open 12
  nb open example:12
  nb example:12 open
  nb o 3
  nb 3 o
  nb o example:12
  nb example:12 o

See also:
  nb help bookmark
  nb help edit

Shortcut Alias: `o`

```

#### [](#peek)`peek`

```
Usage:
  nb peek (<id> | <filename> | <path> | <title> | <notebook>)

Description:
  View a note or notebook in the terminal. When the note is a bookmark, view
  the bookmarked page in your terminal web browser. When the note is in a text
  format or any other file type, `peek` is the equivalent of `show`. When
  used with a notebook, `peek` opens the notebook folder first tool found in
  the following list: `ranger` [1], `mc` [2], `exa` [3], or `ls`.

    1. https://ranger.github.io/
    2. https://en.wikipedia.org/wiki/Midnight_Commander
    3. https://github.com/ogham/exa

Examples:
  nb peek 3
  nb peek example.bookmark.md
  nb 3 peek
  nb example:peek 12
  nb peek example:12
  nb example:12 peek
  nb p 3
  nb 3 p
  nb p example:12
  nb example:12 p

See also:
  nb help bookmark
  nb help show

Alias: `preview`
Shortcut Alias: `p`

```

#### [](#plugins)`plugins`

```
Usage:
  nb plugins [<name>] [--paths] [--force]
  nb plugins install [<path> | <url>] [--force]
  nb plugins uninstall <name>

Options:
  --paths  Print the full path to each plugin.

Subcommands:
  (default)  List plugins.
  install    Install a plugin from a <path> or <url>.
  uninstall  Uninstall the specified plugin.

Description:
  Manage plugins and themes.

Plugin Extensions:
  .nb-theme   Plugins defining color themes.
  .nb-plugin  Plugins defining new subcommands and functionality.

```

#### [](#remote)`remote`

```
Usage:
  nb remote
  nb remote remove
  nb remote set <url> [-f | --force]

Subcommands:
  (default)     Print the remote URL for the notebook.
  remove        Remove the remote URL from the notebook.
  set           Set the remote URL for the notebook.

Options:
  -f, --force   Skip the confirmation prompt.

Description:
  Get, set, and remove the remote repository URL for the current notebook.

Examples:
  nb remote set https://github.com/example/example.git
  nb remote remove

```

#### [](#rename)`rename`

```
Usage:
  nb rename (<id> | <filename> | <path> | <title>) [-f | --force]
            (<name> | --reset | --to-bookmark | --to-note)

Options:
  -f, --force     Skip the confirmation prompt.
  --reset         Reset the filename to the last modified timestamp.
  --to-bookmark   Preserve the existing filename and replace the extension
                  with ".bookmark.md" to convert the note to a bookmark.
  --to-note       Preserve the existing filename and replace the bookmark's
                  ".bookmark.md" extension with ".md" to convert the bookmark
                  to a Markdown note.

Description:
  Rename a note. Set the filename to <name> for the specified note file. When
  file extension is omitted, the existing extension will be used.

Examples:
  # Rename "example.md" to "example.org"
  nb rename example.md example.org

  # Rename note 3 ("example.md") to "New Name.md"
  nb rename 3 "New Name"

  # Rename "example.bookmark.md" to "New Name.bookmark.md"
  nb rename example.bookmark.md "New Name"

  # Rename note 3 ("example.md") to bookmark named "example.bookmark.md"
  nb rename 3 --to-bookmark

  # Rename note 12 in the "example" notebook to "sample.md"
  nb example:rename 3 "sample.md"

```

#### [](#run)`run`

```
Usage:
  nb run <command> [<arguments>...]

Description:
  Run shell commands within the current notebook directory.

Examples:
  nb run ls -la
  nb run find . -name 'example*'
  nb run rg example

```

#### [](#search)`search`

```
Usage:
  nb search <query> [-a | --all] [-t <type> | --type <type> | --<type>]
                    [-l | --list] [--path]

Options:
  -a, --all                     Search all unarchived notebooks.
  -l, --list                    Print the id, filename, and title listing for
                                each matching file, without the excerpt.
  --path                        Print the full path for each matching file.
  -t, --type <type>, --<type>   Search items of <type>. <type> can be a file
                                extension or one of the following types:
                                note, bookmark, document, archive, image,
                                video, audio, folder, text
Description:
  Search notes. Uses the first available tool in the following list:
    1. `rg`    https://github.com/BurntSushi/ripgrep
    2. `ag`    https://github.com/ggreer/the_silver_searcher
    3. `ack`   https://beyondgrep.com/
    4. `grep`  https://en.wikipedia.org/wiki/Grep

Examples:
  # search current notebook for "example query"
  nb search "example query"

  # search the notebook "example" for "example query"
  nb example:search "example query"

  # search all notebooks for "example query" and list matching items
  nb search "example query" --all --list

  # search notes for "Example" OR "Sample"
  nb search "Example|Sample"

  # search with a regular expression
  nb search "\d\d\d-\d\d\d\d"

  # search the current notebook for "example query"
  nb q "example query"

  # search the notebook named "example" for "example query"
  nb example:q "example query"

  # search all notebooks for "example query" and list matching items
  nb q -la "example query"

Shortcut Alias: `q`

```

#### [](#settings)`settings`

```
Usage:
  nb set [<name> [<value>] | <number> [<value>]]
  nb settings colors [<number> | themes]
  nb settings edit
  nb settings get   (<name> | <number>)
  nb settings list  [--long]
  nb settings set   (<name> | <number>) <value>
  nb settings show  (<name> | <number>)
  nb settings unset (<name> | <number>)

Subcommands:
  (default)  Open the settings prompt, to <name> or <number>, if present.
             When <value> is also present, assign <value> to the setting.
  colors     Print a table of available colors and their xterm color numbers.
             When <number> is provided, print the number in its color.
             `settings colors themes` prints a list of installed themes.
  edit       Open the `nb` configuration file in `$EDITOR`.
  get        Print the value of a setting.
  list       List information about available settings.
  set        Assign <value> to a setting.
  show       Print the help information and current value of a setting.
  unset      Unset a setting, returning it to the default value.

Description:
  Configure `nb`. Use `nb settings set` to customize a setting and
  `nb settings unset` to restore the default for a setting.

  Use the `nb set` alias to quickly assign values to settings:

    nb set color_theme blacklight
    nb set limit 40

Examples:
  nb settings
  nb set 5 "org"
  nb set color_primary 105
  nb set unset color_primary
  nb set color_secondary unset
  nb settings colors
  nb settings colors 105
  nb set limit 15

Alias: `set`

```

##### [](#auto_sync)`auto_sync`

```
[1]  auto_sync
     ---------
     By default, operations that trigger a git commit like `add`, `edit`,
     and `delete` will sync notebook changes to the remote repository, if
     one is set. To disable this behavior, set this to "0".

     • Default Value: 1

```

##### [](#color_primary)`color_primary`

```
[2]  color_primary
     -------------
     The primary color used to highlight identifiers and messages. Often this
     can be set to an xterm color number between 0 and 255. Some terminals
     support many more colors.

     • Default Value: 68 (blue) for 256 color terminals,
                      4  (blue) for  8  color terminals.

```

##### [](#color_secondary)`color_secondary`

```
[3]  color_secondary
     ---------------
     The color used for lines and footer elements. Often this can be set to an
     xterm color number between 0 and 255. Some terminals support many more
     colors.

     • Default Value: 8

```

##### [](#color_theme)`color_theme`

```
[4]  color_theme
     -----------
     The color theme.

     To view screenshots of the built-in themes, visit:

         https://git.io/nb-docs-color-themes

     `nb` supports custom, user-defined themes. To learn more, run:

         nb help --colors

     To change the syntax highlighting theme, use:

         nb set syntax_theme

     • Available themes:

         blacklight
         console
         desert
         electro
         forest
         monochrome
         nb
         ocean
         raspberry
         unicorn
         utility

     • Default Value: nb

```

##### [](#default_extension)`default_extension`

```
[5]  default_extension
     -----------------
     The default extension to use for note files. Change to "org" for Emacs
     Org mode files, "rst" for reStructuredText, "txt" for plain text, or
     whatever you prefer.

     • Default Value: md

```

##### [](#editor)`editor`

```
[6]  editor
     ------
     The command line text editor to use with `nb`.

     • Example Values:

         atom
         code
         emacs
         macdown
         mate
         micro
         nano
         pico
         subl
         vi
         vim

```

##### [](#encryption_tool)`encryption_tool`

```
[7]  encryption_tool
     ---------------
     The tool used for encrypting notes.

     • Supported Values: openssl, gpg
     • Default Value:    openssl

```

##### [](#footer)`footer`

```
[8]  footer
     ------
     By default, `nb` and `nb ls` include a footer with example commands.
     To hide this footer, set this to "0".

     • Default Value: 1

```

##### [](#header)`header`

```
[9]  header
     ------
     By default, `nb` and `nb ls` include a header listing available notebooks.
     Set the alignment, or hide the header with "0".

     • Supported Values:

         0  Hide Header
         1  Dynamic Alignment
              - Left justified when list is shorter than terminal width.
              - Center aligned when list is longer than terminal width.
         2  Center Aligned (default)
         3  Left Justified

     • Default Value: 1

```

##### [](#limit)`limit`

```
[10] limit
     -----
     The maximum number of notes included in the `nb` and `nb ls` lists.

     • Default Value: 20

```

##### [](#nb_dir)`nb_dir`

```
[11] nb_dir
     ------
     The location of the directory that contains the notebooks.

     For example, to sync all notebooks with Dropbox, create a folder at
     `~/Dropbox/Notes` and run: `nb settings set nb_dir ~/Dropbox/Notes`

     • Default Value: ~/.nb

```

##### [](#syntax_theme)`syntax_theme`

```
[12] syntax_theme
     ------------
     The syntax highlighting theme. View examples with:

         bat --list-themes

     • Available themes:

         1337
         DarkNeon
         Dracula
         GitHub
         Monokai Extended
         Monokai Extended Bright
         Monokai Extended Light
         Monokai Extended Origin
         Nord
         OneHalfDark
         OneHalfLight
         Solarized (dark)
         Solarized (light)
         Sublime Snazzy
         TwoDark
         ansi-dark
         ansi-light
         base16
         base16-256
         gruvbox
         gruvbox-light
         gruvbox-white
         zenburn

     • Default Value: base16

```

#### [](#shell)`shell`

```
Usage:
  nb shell [<subcommand> [<options>...] | --clear-history]

Optons:
  --clear-history  Clear the `nb` shell history.

Description:
  Start the `nb` interactive shell. Type "exit" to exit.

  `nb shell` recognizes all `nb` subcommands and options, providing
  a streamlined, distraction-free approach for working with `nb`.

  When <subcommand> is present, the command will run as the shell is opened.

Example:
  $ nb shell
  nb> ls 3
  [3] Example

  nb> edit 3 --content "New content."
  Updated: [3] Example

  nb> notebook
  home

  nb> exit
  $

```

#### [](#show)`show`

```
Usage:
  nb show (<id> | <filename> | <path> | <title>) [[-a | --added] |
          --filename | --id | --info-line | --path | [-p | --print]
          [-r | --render] | --selector-id | --title | --type [<type>] |
          [-u | --updated]]
  nb show <notebook>

Options:
  -a, --added      Print the date and time when the item was added.
  --filename       Print the filename of the item.
  --id             Print the id number of the item.
  --info-line      Print the id, filename, and title of the item.
  --path           Print the full path of the item.
  -p, --print      Print to standard output / terminal.
  -r, --render     Use `pandoc` [1] to render the file to HTML and display
                   in the terminal web browser. If either `pandoc` or a
                   browser are unavailable, `-r` / `--render` is ignored.
  --selector-id    Given a selector (e.g., notebook:example.md), print the
                   identifier portion (example.md).
  --title          Print the title of the note.
  --type [<type>]  Print the file extension or, when <type> is specified,
                   return true if the item matches <type>. <type> can be a
                   file extension or one of the following types:
                   archive, audio, bookmark, document, folder, image,
                   text, video
  -u, --updated    Print the date and time of the last recorded change.

Description:
  Show an item or notebook. Notes in text file formats can be rendered or
  printed to standard output. Non-text files will be opened in your system's
  preferred app or program for that file type.

  By default, the item will be opened using `less` or the program configured
  in the `$PAGER` environment variable. Use the following keys to navigate
  in `less` (see `man less` for more information):

    Key               Function
    ---               --------
    mouse scroll      Scroll up or down
    arrow up or down  Scroll one line up or down
    f                 Jump forward one window
    b                 Jump back one window
    d                 Jump down one half window
    u                 Jump up one half window
    /<query>          Search for <query>
    n                 Jump to next <query> match
    q                 Quit

  To skip the pager and print to standard output, use the `-p` / `--print`
  option.

  `-r` / `--render` automatically uses either `w3m` [2] or `lynx` [3].
  To specify a preferred browser, set the `$BROWSER` environment variable
  in your .bashrc, .zshrc, or equivalent, e.g., `export BROWSER="lynx"`.

  If `bat` [4], `highlight` [5], or Pygments [6] is installed, notes are
  printed with syntax highlighting.

    1. https://pandoc.org/
    2. https://en.wikipedia.org/wiki/W3m
    3. https://en.wikipedia.org/wiki/Lynx_(web_browser)
    4. https://github.com/sharkdp/bat
    5. http://www.andre-simon.de/doku/highlight/en/highlight.php
    6. https://pygments.org/

Examples:
  nb show 1
  nb show example.md --render
  nb show "A Document Title" --print --no-color
  nb 1 show
  nb example:show 12
  nb show example:12
  nb example:12 show
  nb s 1
  nb 1 s
  nb s example:12
  nb example:12 s

Alias: `view`
Shortcut Alias: `s`

```

#### [](#status)`status`

```
Usage:
  nb status

Description:
  Run `git status` the current notebook.

```

#### [](#subcommands-1)`subcommands`

```
Usage:
  nb subcommands [add <name>...] [alias <name> <alias>]
                 [describe <name> <usage>]

Subcommands:
  add       Add a new subcommand.
  alias     Create an <alias> of a given subcommand <name>, with linked help.
            Note that aliases must also be added with `subcommands add`.
  describe  Set the usage text displayed with `nb help <subcommand>`.
            This can be assigned as a heredoc, which is recommended, or
            as a string argument.

Description:
  List, add, alias, and describe subcommands. New subcommands, aliases, and
  descriptions are not persisted, so `add`, `alias`, `describe` are
  primarily for plugins.

```

#### [](#sync)`sync`

```
Usage:
  nb sync [-a | --all]

Options:
  -a, --all   Sync all unarchived notebooks.

Description:
  Sync the current local notebook with the remote repository.

Private Repositories and Git Credentials:
  Syncing with private repositories requires configuring git to not prompt
  for credentials.

  For repositories cloned over HTTPS, credentials can be cached with git.
  For repositories cloned over SSH, keys can be added to the ssh-agent.

  More Information:
    https://github.com/xwmx/nb#private-repositories-and-git-credentials

Sync Conflict Resolution:
  When `nb sync` encounters a conflict in a text file and can't merge
  overlapping local and remote changes, both versions are saved in the
  file, separated by git conflict markers. Use `nb edit` to remove the
  conflict markers and delete any unwanted text.

  When `nb sync` encounters a conflict in a binary file, such as an
  encrypted note or bookmark, both versions of the file are saved in the
  notebook as individual files, one with `--conflicted-copy` appended to
  the filename.

  More Information:
    https://github.com/xwmx/nb#sync-conflict-resolution

```

#### [](#update)`update`

```
Usage:
  nb update

Description:
  Update `nb` to the latest version. You will be prompted for
  your password if administrator privileges are required.

  If `nb` was installed using a package manager like npm or
  Homebrew, use the package manager's upgrade functionality instead
  of this command.

```

#### [](#use)`use`

```
Usage:
  nb use <notebook>

Description:
  Switch to the specified notebook. Shortcut for `nb notebooks use`.

Example:
  nb use example

Shortcut Alias: `u`

```

#### [](#version)`version`

```
Usage:
  nb version

Description:
  Display version information.

```

### [](#plugins-1)外挂程式

[反向链接](#backlink) • [复制](#copy) • [电子书](#ebook) • [示例](#example)

#### [](#backlink)`backlink`

```
Usage:
  nb backlink [--force]

Description:
  Add backlinks to notes. Crawl notes in a notebook for [[wiki-style links]]
  and append a "Backlinks" section to each linked file that lists passages
  referencing the note.

  To link to a note from within another note, surround the title of the
  target note in double square brackets:

      Example with link to [[Target Note Title]] in content.

  Depends on note-link-janitor:
    https://github.com/andymatuschak/note-link-janitor

    Requirement: every note in the notebook must have a title.

```

#### [](#copy)`copy`

```
Usage:
  nb copy (<id> | <filename> | <path> | <title>)

Description:
  Create a copy of the specified item in the current notebook.

Alias: `duplicate`

```

#### [](#ebook)`ebook`

```
Usage:
  nb ebook new <name>
  nb ebook publish

Subcommands:
  ebook new      Create a new notebook initialized with placeholder files for
                 authoring an ebook.
  ebook publish  Generate a .epub file using the current notebook contents.

Description:
  Ebook authoring with `nb`.

  `nb ebook new` creates a notebook populated with initial placeholder files
  for creating an ebook. Edit the title page and chapters using normal `nb`
  commands, then use `nb ebook publish` to generate an epub file.

  Chapters are expected to be markdown files with sequential numeric
  filename prefixes for ordering:

    01-example.md
    02-sample.md
    03-demo.md

  Create new chapters with `nb add`:

    nb add --filename "04-chapter4.md"

  title.txt contains the book metadata in a YAML block. For more information
  about the fields for this file, visit:

    https://pandoc.org/MANUAL.html#epub-metadata

  stylesheet.css contains base styling for the generated ebook. It can be used
  as it is and can also be edited using `nb edit`.

  As with all `nb` notebooks, changes are recorded automatically in git,
  providing automatic version control for all ebook content, source, and
  metadata files.

  Generated epub files are saved in the notebook and can be previewed in the
  terminal with `nb show`. Export a generated epub file with `nb export`:

    nb export 12 .

More info:
  https://pandoc.org/epub.html

```

#### [](#example)`example`

```
Usage:
  nb example

Description:
  Print "Hello, World!"

```

## [](#specifications)技术指标

### [](#nb-markdown-bookmark-file-format)`nb` 降价书签文件格式

#### [](#extension)延期

`.bookmark.md`

#### [](#description)描述

`nb` 书签是Markdown文档，使用用户输入和来自书签页面的数据的组合创建。 该 `nb` 书签格式的目的是可读的，可编辑，并明确组织了最大的方便。

书签由 `.bookmark.md` 文件扩展名 标识 。 书签URL是 `<` 和 `>` 字符中 文件中的第一个URL 。 使用以下命令创建最低限度的书签文件 `nb add` ：

```
nb add example.bookmark.md --content "<https://example.com>"

```

这将创建一个名称为以下内容的文件 `example.bookmark.md` ：

```
<https://example.com>

```

在完整书签中，信息分为多个部分，每个书签部分均由Markdown `h2` 标题 指示 。

#### [](#example-1)例

```
# Example Title (example.com)

<https://example.com>

## Description

Example description.

## Quote

> Example quote line one.
>
> Example quote line two.

## Comment

Example comment.

## Related

- <https://example.net>
- <https://example.org>

## Tags

#tag1 #tag2

## Content

Example Title
=============

This domain is for use in illustrative examples in documents. You may
use this domain in literature without prior coordination or asking for
permission.

[More information\...](https://www.iana.org/domains/example)

## Source

```html
<!doctype html>
<html>
  <head>
    <title>Example Title</title>
    <meta name="description" content="Example description." />
  </head>

  <body>
    <h1>Example Title</h1>
    <p>
      This domain is for use in illustrative examples in documents. You may
      use this domain in literature without prior coordination or asking for
      permission.
    </p>
    <p>
      <a href="https://www.iana.org/domains/example">More information...</a>
    </p>
  </body>
</html>
```

```

#### [](#elements)元素

##### [](#title)标题

`Optional`

`h1` 包含标题页的HTML `<title>` 或 [`og:title`](https://ogp.me/) 标签（如果存在） 的内容的 markdown 标题 ，后跟括号中的域。

###### [](#examples)例子

```
# Example Title (example.com)

```

```
# (example.com)

```

##### [](#url)网址

`Required`

带书签的资源的URL，带有尖括号（ `<` ， `>` ）。

这是唯一必需的元素。

##### [](#-description)`## Description`

`Optional`

包含书签页面的元描述或 [`og:description`](https://ogp.me/) 标签（如果存在） 内容的文本元素 。

##### [](#-quote)`## Quote`

`Optional`

降价引用块，其中包含用户指定的书签资源摘录。

##### [](#-comment)`## Comment`

`Optional`

包含用户编写的注释的文本元素。

##### [](#-related)`## Related`

`Optional`

与书签资源相关 的尖括号（ `<` ， `>` ）URL的 Markdown列表 。

##### [](#-tags)`## Tags`

`Optional`

标记列表，表示为由各个空格分隔的主题标签。

##### [](#-content)`## Content`

`Optional`

书签页面的全部内容，转换为Markdown。

本 `## Content` 部分使页面内容可在本地用于全文搜索和页面内容查看。 源HTML被转换为嵌入式Markdown，以减少标记的数量，使其更具可读性，并使页面内容易于在终端Web浏览器中作为markdown和简化的HTML在终端中查看。

##### [](#-source)`## Source`

`Optional`

具有 `html` 语言标识符的 受保护代码块， 其中包含来自已标记页面的源HTML。

`nb` 默认情况下不保存页面源。 `nb` 当 `pandoc` 无法 将源HTML页面内容 转换为Markdown 时，使用此部分来保存源HTML页面内容 。

### [](#nb-notebook-specification)`nb` 笔记本规格

一个 `nb` 笔记本是包含一个有效的目录 `.git` 目录，表明它已被初始化为一个Git仓库和 `.index` 文件。

#### [](#index-file)`.index` 文件

笔记本索引是 `.index` 在笔记本目录中 命名的文本文件 。 `.index` 包含一个文件名列表，每行一个，每个文件名的行号代表ID。 `.index` 包含在git信息库中，因此ID会在整个系统中保留。

##### [](#operations)运作方式

`add`

将包含文件名的新行追加到 `.index` 。

`update`

`.index` 用新文件名 覆盖现有文件 名。

`delete`

删除文件名，保留换行符，将行留空。

`reconcile`

删除重复的行，保留现有的空行， `add` 新文件的 `delete` 条目和删除文件的条目。

`rebuild`

删除和重建 `.index` ，列出最近修改过的文件，反向进行。

##### [](#index-subcommand)`index` 子指令

`nb` `.index` 使用内部 `index` 子命令 管理 。

###### [](#nb-help-index)`nb help index`

```
Usage:
  nb index add <filename>
  nb index delete <filename>
  nb index get_basename <id>
  nb index get_id <filename>
  nb index get_max_id
  nb index rebuild
  nb index reconcile
  nb index show
  nb index update <existing-filename> <new-filename>
  nb index verify

Subcommands:
  add           Add <filename> to the index.
  delete        Delete <filename> from the index.
  get_basename  Print the filename / basename at the specified <id>.
  get_id        Get the id for <filename>.
  get_max_id    Get the maximum id for the notebook.
  rebuild       Rebuild the index, listing files by last modified, reversed.
                Some ids will change. Prefer `nb index reconcile`.
  reconcile     Remove duplicates and update index for added and deleted files.
  show          Print the index.
  update        Overwrite the <existing-filename> entry with <new-filename>.
  verify        Verify that the index matches the notebook contents.

Description:
  Manage the index for the current notebook. This subcommand is used
  internally by `nb` and using it manually will probably corrupt
  the index. If something goes wrong with the index, fix it with
  `nb index reconcile`.

  The index is a text file named '.index' in the notebook directory. .index
  contains a list of filenames and the line number of each filename
  represents the id. .index is included in the git repository so ids are
  preserved across systems.

```

#### [](#archived-notebooks)存档的笔记本

当笔记本包含以 `.archived` 笔记本目录的根级别 命名的文件时，该笔记本被视为已存档 。

## [](#tests)测验

要运行 [测试套件](https://xwmx.github.io/nb/test) ，请安装 [Bats](https://github.com/bats-core/bats-core) 和 [建议的依赖项](#optional) ，然后 `bats test` 在项目根目录中 运行 。