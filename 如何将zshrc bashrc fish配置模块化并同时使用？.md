## 序言
最开始的时候我使用bash后面则使用zsh，有时候我又有点喜欢fish。那么，有没有一种办法，可以将他们模块化，然后再根据我的心情来想用哪个就哪个呢？
有的，当然有。

## 将bashrc模块化

通常我们所有的配置内容都存放在`~/.bashrc`当中。当内容不多的时候，这样做当然是方便的，然而，当内容过多的时候，有时候我们想要修改或者查找内容的时候，就会有点不方便了。
因此，在模块化大行其肆的情况下，将你的`~/.bashrc` 模块化也就成为了必然。下面，我们开始尝试将你的配置模块化。

```shell
# 1. 创建dotfiles以及bashrc.d并且进入该文件夹
mkdir -p ~/dotfiles/bashrc.d/ && cd $_

# 2. 分别创建配置文件
touch default.bashrc alias.bashrc path.bashrc

# 当前文件夹结构
└── bashrc.d
    ├── alias.bashrc
    ├── default.bashrc
    └── path.bashrc

# 3. 备份你的bashrc文件

mv ~/.bashrc ~/.backup.bashrc 

# 4. 将你的配置文件复制到新文件夹的默认配置下
cp ~/.backup.bashrc ~/dotfiles/bashrc.d/default.bashrc

# 5. 重新创建~/.bashrc文件
vim ~/.bashrc 
# 写入以下内容
for file in ~/dotfiles/bashrc.d/*.bashrc;
do
  source $file
done

# 6. 令你的配置内容生效
source ~/.bashrc

# 7.如果你的配置没问题，那么就可以开始分离并模块化你的配置文件
# 将你的alias内容==>alias.bashrc 
# 将你的path内容==>path.bashrc 
# 将你剩余的配置内容==>default.bashrc
# 你也可以新建各种以bashrc结尾的文件，以后修改也只需要在这个文件夹里的文件进行修改，大大的降低了维护成本。
```

## 将zshrc模块化

同样的，你也可以用类似的内容管理你的`~/.zshrc`。
```shell 
# 创建相关文件夹并进入
mkdir -p ~/dotfiles/zshrc.d/ && cd $_
# 创建配置文件
touch default.zshrc alias.zshrc path.zshrc
# 复制系统配置文件到指定配置文件中
cp ~/.zshrc ~/dotfiles/zshrc.d/default.zshrc 
# 备份系统配置文件
mv ~/.zshrc ~/.backup.zshrc
# 新建zshrc文件
vim ~/.zshrc
# 将下列内容复制到你的配置文件
for file in ~/dotfiles/zshrc.d/*.zshrc ;
do
  source $file
done
# 重载配置
zsh 
```
## 创建共享文件夹,使配置共享

### 创建共享文件夹与配置文件
由于zsh和bash兼容,所以,像alias与path实际上可以写在一起,因此,创建一个共享文件夹,就可以使得一个配置,两个一起使用了.

![20201215093854_e6e4382346df5afc531b88ab9c157484.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20201215093854_e6e4382346df5afc531b88ab9c157484.png)
如上图所示,我们新建一个
```shell 
cd dotfiles
mkdir -p public.d/alias.d public.d/path.d
touch public.d/alias.d/default.alias public.d/path.d/default.path
```
根据上述命令,我们会得到这样一个类似这样的文件夹结构:
```shell
.
├── bashrc.d
│   ├── alias.bashrc
│   ├── default.bashrc
│   └── path.bashrc
├── public.d
│   ├── alias.d
│   │   └── default.alias
│   └── path.d
│       └── default.path
└── zshrc.d
    ├── alias.zshrc
    ├── default.zshrc
    └── path.zshrc
```

然后打开你的配置文件并输入内容:
```shell
vim ~/.zshrc
# 将下列内容复制到你的配置文件
for file in ~/dotfiles/zshrc.d/*.zshrc ~/dotfiles/public.d/alias.d/*.alias ~/dotfiles/public.d/path.d/*.path;
do
  source $file
done

# 打开bashrc 
vim ~/.bashrc
# 写入下列内容
for file in ~/dotfiles/bashrc.d/*.bashrc ~/dotfiles/public.d/alias.d/*.alias ~/dotfiles/public.d/path.d/*.path;
do
  source $file
done

```
### 进行测试

然后,在`~/dotfiles/public.d/alias.d/default.alias` 写入一个测试信息.
比如我写入了一个`alias ls="ls -a"`.
在zsh下完全没问题.
切换到bashrc,再次输入ls,发现alias生效
到目前为止,基本上你已经实现了zsh与bash混用了.

## 如何与fishshell混用?
fishshell本身就已经很模块化了,所以,我们只需要将~/.config/fish文件夹复制到dotfiles下.
复制后,你的文件夹大概如下图所示:
![20201215110550_75559d7155bad9351db609ea53cf2de9.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20201215110550_75559d7155bad9351db609ea53cf2de9.png)

fishshell和bash不兼容,一个是path,使用set定义,一个是alias使用函数定义.
alias可以使用下列函数

```shell
# 创建导入文件
vim ~/.config/fish/functions/import_bash_aliases.fish
```
将下列代码复制到文件当中.
`~/.bash_aliases`要替换成你自己的alias所在地址.
如果你按照前面的教程来做的话,那么你的地址应该是在`~/dotfiles/public.d/alias.d/*.alias`.

```shell
# Fish function to import bash aliases v0.2
# Copyright (C) 2016 Malte Biermann
# Copyright (C) 2017 Rodrigo Bermudez Schettino
# Released under the GPL
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Imports aliases defined in ~/.bash_aliases formatted as alias name='command'
function import_bash_aliases --description 'bash aliases to .fish function files'
    for a in (cat ~/.bash_aliases  | grep "^alias")
        #echo $a
        
        set aname (echo $a | sed "s/alias \(.*\)='.*/\1/")
        # To import aliases with commands enclosed in double quotes uncomment
        # the following line
        # set aname (echo $a | sed 's/alias \(.*\)=".*/\1/')
        #echo $aname
        
        set command (echo $a | sed "s/alias.*='\(.*\)'/\1/")
        # To import aliases with commands enclosed in double quotes uncomment
        # the following line
        # set command (echo $a | sed 's/alias.*="\(.*\)"/\1/')
        #echo $command
        
        echo "Processing alias $aname as $command"
        
        # TODO: Check if abbreviation already exists and avoid overwriting it
        # This can be achieved by using the fish function in
        # https://github.com/fish-shell/fish-shell/blob/036b708d9906d4f1fcc7ab2389aa06cf5ec05d11/share/functions/abbr.fish#L130
        abbr -a $aname $command
    end
end
```
注意,这个文件只能导入没有空格的命令,也就是一个简单缩写,局限性很大.

个人推荐使用bash和zsh混用内置,毕竟fish不兼容,需要专门配置.
