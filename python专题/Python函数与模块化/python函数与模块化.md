## 序言
Python中的函数以及衍生出来的模块化是相当重要的概念.
之前一直搞不清楚,这里搞个强化,把他弄清楚.

## 最简单的模块化案例

新建一个项目文件夹,然后新建一个hello.py文件并且输入以下内容:
```python
def say():
    print('hello,world')

```
再新建一个main.py文件,然后输入以下内容:
```py
import hello 
hello.say()
```
然后,在终端输入以下内容:
```
python main.py
```
![20210113093633_a8002c611b0621b7a49488a8d02b36cb.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210113093633_a8002c611b0621b7a49488a8d02b36cb.png)
将得到上面的图.很明显,你通过在main中导入了hello文件,在main中调用了hello文件中的内容.

这个就是最简单的模块化,通过上述操作,你可以将一个很大的文件拆分到不同的文件,然后只在主文件里调用其他文件即可.

## import 与from import 的关系
import是导入文件,上面的举例我们已经看到了.
from import 则是具体到函数.我们还是以上面的两个文件为例.
前面的hello.py文件不变.我们将main.py中的文件更换为下列内容:
```py
from hello import say
say()
```
在终端输入
```
python main.py
```
![20210113095448_7687c88b651fbeb9cc4299cff04f8dea.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210113095448_7687c88b651fbeb9cc4299cff04f8dea.png)

如上图所示,依然成功输出了内容.

### 注意问题
1. 使用as作为缩写
```python
import hello as h
```
上面这种书写形式可以将hello简写成为h依然成立.
2. 多个模块分开导入
```py
# 推荐模式
import hello   
import say 
import how 

# 不推荐模式
import hello,say,how 


# 导入sys模块的argv,winver成员，并为其指定别名v、wv
from sys import argv as v, winver as wv
# 使用导入成员（并指定别名）的语法，直接使用成员的别名访问
print(v[0])
print(wv)
# 不推荐模式
from sys import * 
# 上面这种导入模式会导致函数模块冲突

```

## if __name__ == '__main__':在函数模块化中的作用

### 为什么模块结尾会有if \_\_name\_\_ == ‘\_\_main\_\_’代码块？
![20210113161335_facbb9acc9dfe2a29d71bf5aa23a7384.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210113161335_facbb9acc9dfe2a29d71bf5aa23a7384.png)
在很多脚本的结尾会有类似于这样的代码块：

if \_\_name\_\_ == '\_\_main\_\_':
    main()

1.  if \_\_name\_\_ == '\_\_main\_\_':
2.  main()

if \_\_name\_\_ == '\_\_main\_\_':
    main()

这段代码是干什么用的呢？

简单来说，这段代码是根据模块调用方式来选择性执行代码块的。

因为每一个模块都有两种执行（调用）方式，即**作为主函数执行**和**作为模块执行**。

其中作为主函数执行，就是在终端直接用`python`python命令调用，比如说我自己写了一个叫`mymodule.py`mymodule.py的模块，那么在终端执行`python mymodule.py` python mymodule.py就是将该模块作为主函数执行，这样会执行该模块中的所有代码。

假如这个`mymodule.py`mymodule.py需要在其他模块中以`import`import的方式导入使用的话，就是以模块形式执行。**在作为模块执行的时候，被导入的模块里面的一些代码是不需要甚至是要避免执行的，如果存在`if __name__ == '__main__'`if \_\_name\_\_ == '\_\_main\_\_'判断，那么该判断内的代码块会被忽略。**


假如我们有两个文件.其中demo.py文件内容如下:
```Python 
name = "Python教程"
add = "http://c.biancheng.net/python"
print(name,add)
def say():
    print("人生苦短，我学Python！")
class CLanguage:
    def __init__(self,name,add):
        self.name = name
        self.add = add
    def say(self):
        print(self.name,self.add)

say()
clangs = CLanguage("C语言中文网","http://c.biancheng.net")
clangs.say()   
```
test.py文件内容如下:
```python
import demo
```
也即test文件几乎等同于demo文件.让我们分别打印这两个文件.
![20210113102106_eee083476bb51fbeccc9dd76bb1dd4fe.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210113102106_eee083476bb51fbeccc9dd76bb1dd4fe.png)

通过上图你会发现这两个文件内容几乎相同.
那么问题来了,假如我在test文件当中调用demo文件时,有些内容为并不想输出,这个时候该怎么办呢?
可以用`if __name__ == '__main__':`来解决.
我们将demo文件中的内容改为下列形式:
```py
name = "Python教程"
add = "http://c.biancheng.net/python"
print(name,add)
def say():
    print("人生苦短，我学Python！")
class CLanguage:
    def __init__(self,name,add):
        self.name = name
        self.add = add
    def say(self):
        print(self.name,self.add)

if __name__ == '__main__':
    say()
    clangs = CLanguage("C语言中文网","http://c.biancheng.net")
    clangs.say()
```
然后我们继续打印两个文件,看看会发生什么.
![20210113102540_491ec0f728b6c7d250bf2fc2945d65b8.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210113102540_491ec0f728b6c7d250bf2fc2945d65b8.png)

如上图所示,你会发现,在修改之后,被`if __name__ == '__main__':`包裹起来的内容不再被打印了.
只有当你准确打印demo.py文件时,才会才会完整的输出内容.
简单来说,通过`if __name__ == '__main__':`,你可以轻松做到让函数模块在调用时有选择的输出内容.

## 模块说明文档
每个模块都应当有一个说明文档,这样方便日后调阅以及出问题时可以排查.
在模块文件的头部添加一个多行备注即可.
例如,在上面的demo.py文件的头部加入以下内容.
```py
'''
demo 模块中包含以下内容：
name 字符串变量：初始值为“Python教程”
add    字符串变量：初始值为“http://c.biancheng.net/python”
say() 函数
CLanguage类：包含 name 和 add 属性和 say() 方法。
'''
```
在test.py文件中追加下列内容
```py
import demo
print(demo.__doc__)
```
即可做到打印模块的说明文档.

## 使用'_'与'__'以及'__all__'来过滤函数

当我们向文件导入某个模块时，导入的是该模块中那些名称不以下划线（单下划线“\_”或者双下划线“\_\_”）开头的变量、函数和类。因此，如果我们不想模块文件中的某个成员被引入到其它文件中使用，可以在其名称前添加下划线。

以前面章节中创建的 demo.py 模块文件和 test.py 文件为例（它们位于同一目录），各自包含的内容如下所示：



```py
#demo.py
def say():
    print("人生苦短，我学Python！")
def CLanguage():
    print("C语言中文网：http://c.biancheng.net")
def disPython():
    print("Python教程：http://c.biancheng.net/python")
#test.py
from demo import *
say()
CLanguage()
disPython()

```

执行 test.py 文件，输出结果为：

人生苦短，我学Python！
C语言中文网：http://c.biancheng.net
Python教程：http://c.biancheng.net/python

在此基础上，如果 demo.py 模块中的 disPython() 函数不想让其它文件引入，则只需将其名称改为 \_disPython() 或者 \_\_disPython()。修改之后，再次执行 test.py，其输出结果为：

```shell
人生苦短，我学Python！
C语言中文网：http://c.biancheng.net
Traceback (most recent call last):
  File "C:/Users/mengma/Desktop/2.py", line 4, in <module>
    disPython()
NameError: name 'disPython' is not defined

```
显然，test.py 文件中无法使用未引入的 disPython() 函数。

### 使用__all__变量来确定导入哪些成员

大多数情况下我们不会使用到这个变量,但是应该需要对其有所了解.
导入模块时,默认情况下会导入该模块下的所有成员,但是有时候我们不需要那么多,除了使用'_'单独过滤成员外,还可以使用'__all__'变量来指定导入变量成员.
假设有一个文件demo.py ,内容如下:
```python
def say():
    print("人生苦短，我学Python！")
def CLanguage():
    print("C语言中文网：http://c.biancheng.net")
def disPython():
    print("Python教程：http://c.biancheng.net/python")
__all__ = ["say","CLanguage"]
```
从上我们可以看到,这是一个拥有三个函数的文件,如果我们直接使用import的话,他会将三个函数都导入,当我们使用'__all__'时,他就会导入你指定的两个函数.
__all__ 变量仅限于在其它文件中以“from 模块名 import *”的方式引入。也就是说，如果使用以下 2 种方式引入模块，则 __all__ 变量的设置是无效的。

1) 以“import 模块名”的形式导入模块。通过该方式导入模块后，总可以通过模块名前缀（如果为模块指定了别名，则可以使用模快的别名作为前缀）来调用模块内的所有成员（除了以下划线开头命名的成员）。

仍以 demo.py 模块文件和 test.py 文件为例，修改它们的代码如下所示：
#demo.py
```
def say():
    print("人生苦短，我学Python！")
def CLanguage():
    print("C语言中文网：http://c.biancheng.net")
def disPython():
    print("Python教程：http://c.biancheng.net/python")
__all__ = ["say"]

```#test.py
```
import demo
demo.say()
demo.CLanguage()
demo.disPython()

```运行 test.py 文件，其输出结果为：
```
人生苦短，我学Python！
C语言中文网：http://c.biancheng.net
Python教程：http://c.biancheng.net/python

```
可以看到，虽然 demo.py 模块文件中设置有  __all__ 变量，但是当以“import demo”的方式引入后，__all__ 变量将不起作用。

2) 以“from 模块名 import 成员”的形式直接导入指定成员。使用此方式导入的模块，__all__ 变量即便设置，也形同虚设。

仍以 demo.py 和 test.py 为例，修改 test.py 文件中的代码，如下所示：
```
from demo import say
from demo import CLanguage
from demo import disPython
say()
CLanguage()
disPython()

```
运行 test.py，输出结果为：
```
```
人生苦短，我学Python！
C语言中文网：http://c.biancheng.net
Python教程：http://c.biancheng.net/python


```

## 使用'__init__.py'文件指定文件夹为存放多个模块的python包
在一个大型项目中(Django),我们经常可以看到'__init__'文件存放在很多地方.
实际开发中，一个大型的项目往往需要使用成百上千的 [Python](http://c.biancheng.net/python/) 模块，如果将这些模块都堆放在一起，势必不好管理。而且，使用模块可以有效避免变量名或函数名重名引发的冲突，但是如果模块名重复怎么办呢？因此，Python提出了包（Package）的概念。

什么是包呢？简单理解，包就是文件夹，只不过在该文件夹下必须存在一个名为“\_\_init\_\_.py” 的文件。

> 注意，这是 Python 2.x 的规定，而在 Python 3.x 中，\_\_init\_\_.py 对包来说，并不是必须的。

每个包的目录下都必须建立一个 \_\_init\_\_.py 的模块，可以是一个空模块，可以写一些初始化代码，其作用就是告诉 Python 要将该目录当成包来处理。

注意，\_\_init\_\_.py 不同于其他模块文件，此模块的模块名不是 \_\_init\_\_，而是它所在的包名。例如，在 settings 包中的 \_\_init\_\_.py 文件，其模块名就是 settings。

包是一个包含多个模块的文件夹，它的本质依然是模块，因此包中也可以包含包。例如，在前面章节中，我们安装了 numpy 模块之后可以在 Lib\\site\-packages 安装目录下找到名为 numpy 的文件夹，它就是安装的 numpy 模块（其实就是一个包），它所包含的内容如图 1 所示。

![](http://c.biancheng.net/uploads/allimg/190221/2-1Z2211A453U1.gif)
图 1 numpy包（模块）

从图 1 可以看出，在 numpy 包（模块）中，有必须包含的 \_\_init\_\_.py 文件，还有 matlib.py 等模块源文件以及 core 等子包（也是模块）。这正印证了我们刚刚讲过的，包的本质依然是模块，包可以包含包。

> Python 库：相比模块和包，库是一个更大的概念，例如在 Python 标准库中的每个库都有好多个包，而每个包中都有若干个模块。

## python包的创建与导入

### 三步创建包
在有了包的概念之后,我们来学习下如何创建一个python包以及导入.

1. 创建一个文件夹,名字就是包的名
```
mkdir my_package && cd $_ # 创建包文件夹并且进入
```
2. 创建'__init__.py'文件,内容可以为空
```
touch __init__.py
```
3. 创建模块文件
```
touch module01.py module02.py
```
![20210113115220_2662d37ad5d0b2e934520421f73b735a.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210113115220_2662d37ad5d0b2e934520421f73b735a.png)

文件夹结构如上图所示,这个时候一个包文件夹就创建好了.

### 包的导入
通过前面的学习我们知道，包其实本质上还是模块，因此导入模块的语法同样也适用于导入包。无论导入我们自定义的包，还是导入从他处下载的第三方包，导入方法可归结为以下 3 种：

1.  `import 包名[.模块名 [as 别名]]`
2.  `from 包名 import 模块名 [as 别名]`
3.  `from 包名.模块名 import 成员名 [as 别名]`
>注意，导入包的同时，会在包目录下生成一个含有 __init__.cpython-36.pyc 文件的 __pycache__ 文件夹。

### 1) import 包名[.模块名 [as 别名]]
以前面创建好的 my_package 包为例，导入 module1 模块并使用该模块中成员可以使用如下代码：
```
import my_package.module1
my_package.module1.display("http://c.biancheng.net/java/")

```
运行结果为：
```
http://c.biancheng.net/java/

```
可以看到，通过此语法格式导入包中的指定模块后，在使用该模块中的成员（变量、函数、类）时，需添加“包名.模块名”为前缀。当然，如果使用 as 给包名.模块名”起一个别名的话，就使用直接使用这个别名作为前缀使用该模块中的方法了，例如：
```
import my_package.module1 as module
module.display("http://c.biancheng.net/python/")

```
程序执行结果为：
```
http://c.biancheng.net/python/

```

另外，当直接导入指定包时，程序会自动执行该包所对应文件夹下的 __init__.py 文件中的代码。例如：
```
import my_package
my_package.module1.display("http://c.biancheng.net/linux_tutorial/")

```
直接导入包名，并不会将包中所有模块全部导入到程序中，它的作用仅仅是导入并执行包下的 __init__.py 文件，因此，运行该程序，在执行 __init__.py 文件中代码的同时，还会抛出 AttributeError 异常（访问的对象不存在）：
```shell
http://c.biancheng.net/python/
Traceback (most recent call last):
  File "C:\Users\mengma\Desktop\demo.py", line 2, in <module>
    my_package.module1.display("http://c.biancheng.net/linux_tutorial/")
AttributeError: module 'my_package' has no attribute 'module1'

```

我们知道，包的本质就是模块，导入模块时，当前程序中会包含一个和模块名同名且类型为 module 的变量，导入包也是如此：
```py
import my_package
print(my_package)
print(my_package.__doc__)
print(type(my_package))

```
运行结果为：
```
http://c.biancheng.net/python/
<module 'my_package' from 'C:\\Users\\mengma\\Desktop\\my_package\\__init__.py'>

http://c.biancheng.net/


创建第一个 Python 包

<class 'module'>
```
### 2) from 包名 import 模块名 [as 别名]

仍以导入 my_package 包中的 module1 模块为例，使用此语法格式的实现代码如下：
```py
from my_package import module1
module1.display("http://c.biancheng.net/golang/")

```
运行结果为：
```
http://c.biancheng.net/python/
http://c.biancheng.net/golang/

```
可以看到，使用此语法格式导入包中模块后，在使用其成员时不需要带包名前缀，但需要带模块名前缀。

当然，我们也可以使用 as 为导入的指定模块定义别名，例如：
```py
from my_package import module1 as module
module.display("http://c.biancheng.net/golang/")

```
此程序的输出结果和上面程序完全相同。

同样，既然包也是模块，那么这种语法格式自然也支持 from 包名 import * 这种写法，它和 import 包名 的作用一样，都只是将该包的 __init__.py 文件导入并执行。
### 3) from 包名.模块名 import 成员名 [as 别名]
此语法格式用于向程序中导入“包.模块”中的指定成员（变量、函数或类）。通过该方式导入的变量（函数、类），在使用时可以直接使用变量名（函数名、类名）调用，例如：
```py
from my_package.module1 import display
display("http://c.biancheng.net/shell/")

```
运行结果为：
```
http://c.biancheng.net/python/
http://c.biancheng.net/shell/

```

当然，也可以使用 as 为导入的成员起一个别名，例如：
```py
from my_package.module1 import display as dis
dis("http://c.biancheng.net/shell/")

```
该程序的运行结果和上面相同。

另外，在使用此种语法格式加载指定包的指定模块时，可以使用 * 代替成员名，表示加载该模块下的所有成员。例如：
```py
from my_package.module1 import *
display("http://c.biancheng.net/python")
```

## 使用dir()查看模块成员,使用__all__查看模块成员变量

```py
import string
print(dir(string)) # 查看模块中的成员
# 执行结果 
# ['Formatter', 'Template', '_ChainMap', '_TemplateMetaclass', '__all__', '__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', '_re', '_string', 'ascii_letters', 'ascii_lowercase', 'ascii_uppercase', 'capwords', 'digits', 'hexdigits', 'octdigits', 'printable', 'punctuation', 'whitespace']
print([e for e in dir(string) if not e.startswith('_')]) # 查看时忽略特殊成员,即成员前面有_符号的
# 执行结果
# ['Formatter', 'Template', 'ascii_letters', 'ascii_lowercase', 'ascii_uppercase', 'capwords', 'digits', 'hexdigits', 'octdigits', 'printable', 'punctuation', 'whitespace']
print(string.__all__)
# 执行结果
# ['ascii_letters', 'ascii_lowercase', 'ascii_uppercase', 'capwords', 'digits', 'hexdigits', 'octdigits', 'printable', 'punctuation', 'whitespace', 'Formatter', 'Template']
```
总结:
1. 通过 dir() 函数获取到的模块成员，不仅包含供外部文件使用的成员，还包含很多“特殊”（名称以 2 个下划线开头和结束）的成员，列出这些成员，对我们并没有实际意义。
2. 显然通过列表推导式，可在 dir() 函数输出结果的基础上，筛选出对我们有用的成员并显示出来
3. 和 dir() 函数相比，__all__ 变量在查看指定模块成员时，它不会显示模块中的特殊成员，同时还会根据成员的名称进行排序显示
4. 不过需要注意的是，并非所有的模块都支持使用 __all__ 变量，因此对于获取有些模块的成员，就只能使用 dir() 函数。

## __file__属性：查看模块的源文件路径

>前面章节提到，当指定模块（或包）没有说明文档时，仅通过 help() 函数或者 __doc__ 属性，无法有效帮助我们理解该模块（包）的具体功能。在这种情况下，我们可以通过 __file__ 属性查找该模块（或包）文件所在的具体存储位置，直接查看其源代码。

仍以前面章节创建的 my_package 包为例，下面代码尝试使用 __file__ 属性获取该包的存储路径：
```py
import my_package
print(my_package.__file__)

```
程序输出结果为：
```

C:\Users\mengma\Desktop\my_package\__init__.py

```
注意，因为当引入 my_package 包时，其实际上执行的是 __init__.py 文件，因此这里查看 my_package 包的存储路径，输出的 __init__.py 文件的存储路径。

再以 string 模块为例：
```py
import string
print(string.__file__)

```
程序输出结果为：
```
D:\python3.6\lib\string.py

```

>由此，通过调用 __file__ 属性输出的绝对路径，我们可以很轻易地找到该模块（或包）的源文件。
注意，并不是所有模块都提供 __file__ 属性，因为并不是所有模块的实现都采用 Python 语言，有些模块采用的是其它编程语言（如 C 语言）。

## 参考文章
* [浅谈Python的模块化编程 | Clarmy](http://www.clarmy.net/2018/09/09/briefly-discuss-pythons-modular-programming/)
>中文友好,图文并茂
* [Python模块化编程 | PizzaLi](https://pizzali.github.io/2018/11/21/Python%E6%A8%A1%E5%9D%97%E5%8C%96%E7%BC%96%E7%A8%8B/)
>有实际案例可以参考