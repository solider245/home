## 这两个是最难的

## js对象常见

JavaScript对象:属性和方法的容器;

对象的属性之间一定要用逗号隔开;

对象的方法定义了一个函数，并作为对象的属性存储。

对象方法通过添加 () 调用 (作为一个函数)。

比如:

```js
var person={
"name":"小明",
"age":"18",
"like":function(){
            return "喜欢打篮球,弹吉他";
      }
}
```

javaScript对象也可以先创建，再添加属性和属性值，比如：

```js
var person=new Object();
person.name='小明'；
person.sex='男'；
person.method=function(){
  return this.name+this.sex;
}
```

javaScript对象中属性具有唯一性（这里的属性包括方法），如果有两个重复的属性，则以最后赋值为准。比如同时存在两个play:

```js
var person = {
    name: "小明",
    age: 18,
    sex: "男",
    play: "football",
    play: function () {
        return "like paly football";
    }
};
```

使用 var name = person.fullName(); 调用对象函数时，fullName 会被立即执行：

```js
var person = {
    firstName: "John",
    lastName : "Doe",
    id : 5566,
    fullName : function()
    {
      console.log("person.fullName");
    }
};

var name = person.fullName();
console.log(name);

```
控制台会先打印 **person.fullName** ，再打印 **name**。

JavaScript 对象是键值对的容器，“键”必须为字符串，“值” 可以是 JavaScript 中包括 null 和 undefined 的任意数据类型。

代码实例：

```js
var bird = {
    name : "Amy",
    age : 1,
    color : "white",
    skill : function () {
        console.log("Fly");
    },
    nickname : null,
    play : undefined
}
```
## js函数

### 在使用 return 语句时，函数会停止执行，并返回指定的值。例如：

```js
sayHi();
function sayHi(name,message){
    document.write("return 语句执行前。");
    return;
    alert("hello" + name +"," + message);//这一行永远不会被调用
}
```

### 作为参数的的变量称为形参,带入的参数称为实参。

```js
function myFunction(a,b){ return a*b;}  // 形参
document.getElementById("demo").innerHTML=myFunction(4,3);  // 实参

```

### ES6 新增箭头函数，定义函数时更加简洁、易读。

```js
// 传统定义函数方式
function Test () {
  //
}

const Test = function () {
  //
}

// 使用箭头函数定义函数时可以省略 function 关键字
const Test = (...params) => {
  //
}

// 该函数只有一个参数时可以简写成：
const Test = param => {
  return param;
}

console.log(Test('hello'));   // hello

```
### 对象方法中绑定
下面实例中，this 是 person 对象，person 对象是函数的所有者：


```js
var person = {
  firstName  : "John",
  lastName   : "Doe",
  id         : 5566,
  myFunction : function() {
    return this;
  }
};

```

```js
var person = {
  firstName: "John",
  lastName : "Doe",
  id       : 5566,
  fullName : function() {
    return this.firstName + " " + this.lastName;
  }
};
```

### 显式函数绑定
>在 JavaScript 中函数也是对象，对象则有方法，apply 和 call 就是函数对象的方法。这两个方法异常强大，他们允许切换函数执行的上下文环境（context），即 this 绑定的对象。

>在下面实例中，当我们使用 person2 作为参数来调用 person1.fullName 方法时, this 将指向 person2, 即便它是 person1 的方法：

实例
```js
var person1 = {
  fullName: function() {
    return this.firstName + " " + this.lastName;
  }
}
var person2 = {
  firstName:"John",
  lastName: "Doe",
}
person1.fullName.call(person2);  // 返回 "John Doe"

```
使用 HTML 、JavaScript 创建一个简单的计算器，包含加、减、乘、除四个功能：

```js
var result_1;
//加法
function add() {alert(1);
  var a = getFirstNumber();
  var b = getTwiceNumber();
  var re =Number( a) +Number( b);
  sendResult(re);
}

//减法
function subtract() {
  var a = getFirstNumber();
  var b = getTwiceNumber();
  var re = a - b;
  sendResult(re);
}

//乘法
function ride() {
  var a = getFirstNumber();
  var b = getTwiceNumber();
  var re = a * b;
  sendResult(re);
}

//除法
function devide() {
  var a = getFirstNumber();
  var b = getTwiceNumber();
  var re = a / b;
  sendResult(re);
}

//给p标签传值
function sendResult(result_1) {
  var num = document.getElementById("result")
  num.innerHTML = result_1;
}

//获取第一位数字
function getFirstNumber() {
  var firstNumber = document.getElementById("first").value;
  return firstNumber;
}

//获取第二位数字
function getTwiceNumber() {
  var twiceNumber = document.getElementById("twice").value;
  return twiceNumber;
}
```
**更多实例**
*   [https://c.runoob.com/codedemo/3015](https://c.runoob.com/codedemo/3015)
*   [https://c.runoob.com/codedemo/4026](https://c.runoob.com/codedemo/4026)
*   [https://c.runoob.com/codedemo/4721](https://c.runoob.com/codedemo/4721)
*   [https://c.runoob.com/codedemo/3654](https://c.runoob.com/codedemo/3654)
*   [https://c.runoob.com/codedemo/4786](https://c.runoob.com/codedemo/4786)
*   [https://c.runoob.com/codedemo/3124](https://c.runoob.com/codedemo/3124)
*   [https://c.runoob.com/codedemo/3880](https://c.runoob.com/codedemo/3880)