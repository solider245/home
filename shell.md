## shell/bash脚本


## 一个脚本的要素：
1. 名称为 xxx.sh，没有后缀也行
2. 开头要有#!/bin/bash 
3. 脚本需要755权限
4. ./脚本名称 就可以运行脚本了
5. 解释权运行 /bin/sh test.sh or /bin/php test.php

`script.sh`

```bash
#!/bin/bash
echo "My First Script!"
```

运行脚本

```bash
chmod 755 script.sh # chmod +x script.sh
./script.sh
/bin/sh test.sh
/bin/php test.php
```
>注意，一定要写成 ./test.sh，而不是 test.sh，运行其它二进制的程序也一样，直接写 test.sh，linux 系统会去 PATH 里寻找有没有叫 test.sh 的，而只有 /bin, /sbin, /usr/bin，/usr/sbin 等在 PATH 里，你的当前目录通常不在 PATH 里，所以写成 test.sh 是会找不到命令的，要用 ./test.sh 告诉系统说，就在当前目录找。