## Nodejs常见调用子进程的方法

1. `child_process` 自带模块
2.   [ShellJS](https://www.npmjs.com/package/shelljs)
3.   [cli](https://www.npmjs.com/package/cli)
4. [execa](https://github.com/sindresorhus/execa)

child_process属于系统自带，但是不是特别好用，或者说用起来不是很顺手，因此有了另外三个第三方模块。
shelljs以及cli看上去似乎都没有execa好用，所以今天这里先说说execa的用法。

## execa

### 介绍

[execa](https://github.com/sindresorhus/execa)是更好的子进程管理工具（A better child\_process）。本质上就是衍生一个 shell，传入的 command 字符串在该 shell 中直接处理。

### 安装

```shell
mkdir demo && cd demo
npm init -y # 项目初始化 或者 yarn init -y

npm i execa --save # yarn add execa --save
npm install babel-core --save-dev
npm install babel-register --save-dev
```



```js
const execa = require('execa');
execa("ls").then(result => {
    console.log(result);
    console.log(result.stdout);
});

```
![](https://zxljack.com/wp-content/uploads/2019/02/execa1.png)


## 参考文章

[execa – 张歆琳](https://zxljack.com/2019/02/execa/)
[nodejs的execa库使用 - 瀚海星空 - 周海汉博客](http://abloz.com/tech/2018/08/21/nodejs-execa/)
[如何使用Node.js运行Shell脚本文件或命令？ | 通过VithalReddy | stackFAME | 中](https://medium.com/stackfame/how-to-run-shell-script-file-or-command-using-nodejs-b9f2455cb6b7)
[yargs/cliui: easily create complex multi-column command-line-interfaces.](https://github.com/yargs/cliui)