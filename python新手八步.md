## 序言
整理一下Python的新手问题,方便学习.

## 一.python的环境搭建与安装
使用pyenv来管理python的环境.他在全平台通用.而且,他支持将安装包提前下载到缓存文件夹,然后再安装.有了这个软件支持,可以解决python环境问题,毕竟这个问题产生都非常的不好解决.

## 二.python最基础的两个命令print与input

print是输出,input是输入,Python的一切命令都是围绕这两个函数来的,熟悉,掌握了这两个命令,对其他函数与模块才能慢慢摸索.

```py
a = input()
print(a)
```
以上是一个最简单的输入与输出案例.
你输入什么就打印什么.
接下来我们可以加入一些字符串,让输入与输出更加明确一些.

```py
a = input('请在这里输入你的名字:')
print('你好,'+ a)
```
如上所示,在input里面,我们增加了一行字符串,在print里,我们也增加了一个'你好'的字符串.通过符号+,就会打印一个'你好'和变量a组合起来的字符串.
现在,我们再引入两个变量分别代替两个字符串,看看接下来会产生这么变化.

```py
b = '请在这里输入你的名字:'
a = input(b)
c = '你好,'
print(c + a)
```
![20210102065701_85170a2f0cc65c4e0c56364be52c81d6.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210102065701_85170a2f0cc65c4e0c56364be52c81d6.png)

如上图所示,虽然使用了四行代码,但是却新手友好,比较适合新手看懂项目.

## 三.python中数字的加减乘除余
```
+	Addition	x + y	
-	Subtraction	x - y	
*	Multiplication	x * y	
/	Division	x / y	
%	Modulus	x % y	
**	Exponentiation	x ** y	
//	Floor division	x // y
```

```py
# 三角形ABC，c*2=a*2+b*2
s = '三角形ABC'
a = int(input('请输入a的角度:'))
b = int(input('请输入b的角度:'))
c = 180 -a -b
print('c的角度是' + str(c))
if a+b == 90:
    print(s+'是直角三角形')
else:
    print(s+'不是直角三角形')
```
我们假设有一个三角形ABC,他的三个角分别是abc,其中,a和b都来自用户输入.我们通过a+b的和,来判断c的值,如果a+b=90,那么他就是直角三角形.
代码如上所述.
这里引入了两个系统自带函数,int和str.用于在数字和字符串之间转化.
因为加减乘除只能在数字之间进行,字符串之间是无法进行的.
同时,引入了第一个条件循环if,else.
所以,总结下本小结的知识点:
1. int和str函数用于字符串和数字之间互相转换
2. if else条件来帮忙判断

```py
# 三角形ABC，c*2=a*2+b*2
s = '三角形ABC'
a = int(input('请输入a的角度:'))
b = int(input('请输入b的角度:'))
c = 180 -a -b
print('c的角度是' + str(c))
if a+b == 90 and a==b:
    print(s+'是等腰直角三角形')
elif a + b == 90:
    print(s+'是直角三角形')    
else:
    print(s+'不是直角三角形')
```
我们知道,三角形当中还有等腰直角三角形,条件是a+b=90且a=b. 
所以,多条件和和逻辑我们可以写成上面这种形式,方便满足两种条件,两种情况.

## 函数的使用

### 简单函数调用
```py
#!/usr/bin/python3

def hello():
    """
    一个最简单的hello函数
    """
    print(f'你好,帅哥')
    
hello()
```

### 传参函数调用

```py
def hello(name):
    """
    一个最简单的hello函数
    """
    print(f'你好,{name}')
    
hello('jack') 
```
>调用的时候会直接输出jack

```py
def hello(name):
    """
    一个最简单的hello函数
    """
    print(f'你好,{name}')
    
hello(input('请输入您的名字,来宾:')) 
```
>调用时候,会根据用户的输入而反馈

### return返回键的使用
return a + b # 返回一个值
return a # 返回一个值
