## 什么是Poetry

Poetry 和 Pipenv 类似，是一个 Python 虚拟环境和依赖管理工具，它还提供了包管理功能，比如打包和发布。你可以把它看做是 Pipenv 和 Flit 这些工具的超集。它可以让你用 Poetry 来同时管理 Python 库和 Python 程序。
<!-- more -->

## 安装
官方推荐:
```shell
curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python
```
缺点是如果你的网络无法连接github,那么就无法安装.

pip安装
```shell
pip install --user poetry
```
有时候找不到poetry是因为你的环境路径不在你的配置里.添加进去就可以了.
```
source $HOME/.poetry/env
```

## 其他设置

### 设置镜像源
如果你的pypi源已经设置为国内,则没必要进行这步.
```lua
[[tool.poetry.source]]
name = "douban"
url = "https://pypi.doubanio.com/simple/"
```
### 为Bash，Fish或Zsh启用制表符完成

`poetry` 支持为Bash，Fish和Zsh生成完成脚本。 请参阅参考资料 `poetry help completions` 以获取全部详细信息，但要点很简单，只需使用以下任一方法即可：

```shell
# Bash
poetry completions bash > /etc/bash_completion.d/poetry.bash-completion

# Bash (macOS/Homebrew)
poetry completions bash > $(brew --prefix)/etc/bash_completion.d/poetry.bash-completion

# Fish
poetry completions fish > ~/.config/fish/completions/poetry.fish

# Zsh
poetry completions zsh > ~/.zfunc/_poetry

# Zsh (macOS/Homebrew)
poetry completions zsh > $(brew --prefix)/share/zsh/site-functions/_poetry
```

*注意：* 您可能需要重新启动外壳程序才能使更改生效。

对于 `zsh` ，您必须在 `~/.zshrc` 之前 添加以下行 `compinit` （不适用于自制设置）：

```shell
fpath+=~/.zfunc
```

## 项目配置文件
* `config.toml`全局项目文件
* `pyproject.toml`,每个项目都要有.
* `envs.toml`文件，用来存储哪些虚拟环境指定了Python解释器的版本

## 使用poetry

1. 将项目初始化 在你的已有项目目录里使用
    ```
    poetry init # 如果是新项目,则poetry new <项目名字>

    ```
2. 创建虚拟环境
    ```
    poetry install 
    ```
3. 激活虚拟环境
    ```
    poetry run <你的命令> # 例如： poetry run python flask.py
    poetry shell 
    ```
## poetry发布步骤

### 1.创建您要在PyPi上发布的包
诗歌有一个漂亮的命令，它可以立刻创建项目树 `poetry new <package_name>` 。

```sh
/<package_name>
├── README.rst # I personnaly change it to .md as I prefer writing in Markdown
├── <package_name>
│   └── __init__.py
├── pyproject.toml
└── tests
    ├── __init__.py
    └── test_package_name.py
```
### 2.填写配置内容`pyproject.toml`
```toml
[tool.poetry]
name = "vspoetry"
version = "0.1.0"
description = "Description of your package"
authors = ["MattiooFR <dugue.mathieu@gmail.com>"]
keywords = ["keyword", "another_keyword"]
readme = "README.md"
license = "MIT"
homepage = "https://github.com/MattiooFR/package_name"
repository = "https://github.com/MattiooFR/package_name"
include = [
    "LICENSE",
]

[tool.poetry.dependencies]
python = "^3.5"

[tool.poetry.dev-dependencies]

[tool.poetry.scripts]
cli_command_name = 'package_name:function'

[build-system]
requires = ["poetry>=0.12"]
build-backend = "poetry.masonry.api"
```
文件的开头非常简单。 让我们专注于这三个部分：

*   **[tool.poetry.dependencies]** ：在这里您可以列出程序包需要工作的所有依赖项。 就像旧 `requirements.txt` 文件一样。 您可以手动完成此操作，然后调用命令 `poetry install` 以将其全部安装以用于软件包开发和工作目的。 如果您使用 `poetry add <dependency_name>` （相当于 `pip install <dependency_name>` ），诗歌将为您添加。

*   **[tool.poetry.dev-dependencies]** ：如果您需要开发依赖项，那就可以使用它们。 同样，您也可以将它们安装在一起， `poetry add <dependency_name> --dev (or -D)` 并且诗歌也可以将它们放置在 `pyproject.toml` 文件 中的正确位置 。

*   **[tool.poetry.scripts]** ：如果您希望您的程序包具有可从终端调用的脚本，则最后一个块非常重要。

    ```
    script_name = '{package_name}:{function_name}'
    ```
![20210107101550_6909370e43f6118647bd28c6988223ed.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210107101550_6909370e43f6118647bd28c6988223ed.png)



顺便说一句，如果仍然需要 `requirements.txt` 所有依赖项（例如，如果您使用Heroku），则可以使用以下命令轻松拥有它：

```
poetry export -f requirements.txt > requirements.txt
```
### 3. 构建包 poetry build
准备好包后，只需 `poetry build` 创建包文件

```sh
❯ poetry build
Building package_name (0.1.0)
 - Building sdist
 - Built package_name-0.1.0.tar.gz

 - Building wheel
 - Built package_name-0.1.0-py3-none-any.whl
```

您可以通过以下方式测试您的包裹

```
pip install <path_to_package_name-0.1.0-py3-none-any.whl>
```

如果一切正常，恭喜！ 现在，将其发布到PyPi，以便其他人也可以使用您的出色软件包。

### 4. 发布包 poetry publish

发布之前，您应该首先在 [PyPi](https://pypi.org/account/register/) 上创建一个帐户 。 如果要在正式库上发布之前想在测试库上发布， 也可以在 [TestPyPi](https://test.pypi.org/account/register/) 上注册一个帐户 。

您的软件包需要构建，因此 `poetry build` 如果尚未完成 ，请首先运行 。

然后只需运行 `poetry publish` ，您的包将在PyPi上发布：

```
❯ poetry publish

Publishing vspoetry (0.1.0) to PyPI
Username: Mattioo
Password:
 - Uploading vspoetry-0.1.0-py3-none-any.whl 100%
 - Uploading vspoetry-0.1.0.tar.gz 100%
```

如果您需要更新软件包，只需在 `pyproject.toml` 文件中 增加版本 并使用 `poetry publish` （在 使用 构建新软件包之后 `poetry build` ）。

完成后，您可以安装软件包：

```
pip install <your_package>

poetry add <your_package> # but lets be honest now you use Poetry so you would do this !
```

恭喜您在PyPi上发布了您的软件包，并体验了使用诸如Poetry之类的工具的简便性。

编码愉快！


## 常用命令大全
```shell
poetry init       # 如果在已有的项目中使用来创建一个pyproject.toml文件

poetry new <项目名字>  #使用poetry创建一个新依赖管理项目

poetry install  # 创建虚拟环境 （确保当前目录存在pyproject.toml文件）

poetry shell    # 激活虚拟环境

poetry search requests pendulum # 搜索软件包

poetry add flask # ：安装最新稳定版本的flask

poetry add pytest --dev # : 指定为开发依赖，会写到pyproject.toml中的[tool.poetry.dev-dependencies]区域

poetry add flask=2.22.0 #: 指定具体的版本

poetry install #: 安装pyproject.toml文件中的全部依赖

poetry install --no-dev ： 只安装非development环境的依赖，一般部署时使用

poetry run python -V  # 查看python版本

poetry update          #   更新所有锁定版本的依赖

poetry update    <依赖name>  # 更新某个指定的依赖

poetry remove   <依赖name>   #  卸载删除一个模块

## 环境路径
poetry env -h # 查看帮助文档
poetry env list <--full-path> # 查找当前项目的虚拟环境,后缀如果加上,可以显示绝对路径
poetry env use python3.7     #   让poetry使用python3


#其他还有：
poetry show		#	追踪 & 更新包

poetry show --tree	#  添加--tree 参数选项可以查看依赖关系：

poetry export -f requirements.txt > requirements.txt # 导出

# 全局配置命令
poetry config virtualenvs.create false # 示范使用
poetry config virtualenvs.create --unset # 重置全局配置 
poetry config virtualenvs.create --local --unset # 项目重置
poetry config --list # 列出当前项目的配置
poetry config cache-dir E:\Documents\Library # 修改虚拟环境路径


# 发布包
poetry build # 构建包
poetry config repositories.testpypi https://test.pypi.org/simple # 接下来，将测试PyPI添加为备用包存储库：
poetry publish -r testpypi # 发布包到testpypi
pip install --indent-url https://test.pypi.org/simple/ <你的包名> # 测试安装
poetry publish # 安装测试没问题后就可以发布了.

```

## 配置文件举例
`pyproject.toml`
```toml
[tool.poetry]
name = "my-package"
version = "0.1.0"
description = "The description of the package"

license = "MIT"

authors = [
    "Sébastien Eustace <sebastien@eustace.io>"
]

readme = 'README.md'  # Markdown files are supported

repository = "https://github.com/sdispater/poetry"
homepage = "https://github.com/sdispater/poetry"

keywords = ['packaging', 'poetry']

[tool.poetry.dependencies]
python = "~2.7 || ^3.2"  # Compatible python versions must be declared here
toml = "^0.9"
# Dependencies with extras
requests = { version = "^2.13", extras = [ "security" ] }
# Python specific dependencies with prereleases allowed
pathlib2 = { version = "^2.2", python = "~2.7", allow-prereleases = true }
# Git dependencies
cleo = { git = "https://github.com/sdispater/cleo.git", branch = "master" }

# Optional dependencies (extras)
pendulum = { version = "^1.4", optional = true }

[tool.poetry.dev-dependencies]
pytest = "^3.0"
pytest-cov = "^2.4"

[tool.poetry.scripts]
my-script = 'my_package:main'
```

## 参考文章
* [用 Poetry 创建并发布 Python 包](https://www.pythonf.cn/read/18917)
>中文友好,就是内容太简洁
* [Python包管理之poetry基本使用 - 知乎](https://zhuanlan.zhihu.com/p/110721747)
>中文友好,但是同样内容不多
* [Poetry: 让Python依赖管理和打包变得容易 - Python开发社区 | CTOLib码库](https://www.ctolib.com/sdispater-poetry.html)
> 官方档案,非常详细,有找不到的内容都在这里

* [Python Poetry 入门教程（代码依赖管理） | Python 技术论坛](https://learnku.com/python/t/38708)
> 翻译的海外文章,有项目案例,适合看看
* [使用Poetry在PyPi上发布程序包| 脑分类](https://www.brainsorting.dev/posts/publish-a-package-on-pypi-using-poetry/)
>一个非常简单的教学文章,而且经过测试成功发布了.
* [用诗歌创建和发布Python包·JF的开发博客](https://johnfraney.ca/posts/2019/05/28/create-publish-python-package-poetry/)
>在打包之前还有测试一块,如果要涉入这块可以参考这篇文章
* [用诗歌创建和发布Python包·JF的开发博客](https://johnfraney.ca/posts/2019/05/28/create-publish-python-package-poetry/)
>着重讲了如何将应用作为脚本执行的方法.主要是需要Main函数

