## 序言
sh模块是用来代替subprocess的一个模块,他比sub在使用上要更简便.
官方文档和地址如下.
* [常见问题解答— sh 1.14.0文档](https://amoffat.github.io/sh/sections/faq.html#how-do-i-execute-a-bash-builtin)
* [amoffat / sh：启动Python进程](https://github.com/amoffat/sh)

下面我们来看看这个模块的日常是如何使用的

## 常见命令

### 安装模块
```sh 
pip install sh # 安装模块
```
### 导入模块与导入具体子模块

```python
# 最简单的用法
import sh 
a=sh.ls("-l","-a")
print(a)

# 导入子模块
from sh import ip,git
a = ip.address()
print(a) 
# 查看git状态
a = git('status')
print(a)
```
### 子命令的两种写入方式
```py
# 子命令
>>> from sh import git, sudo
>>> print(git.branch("-v"))
>>> print(git("branch", "-v"))
>>> print(sudo.ls("/root"))
>>> print(sudo("/bin/ls", "/root"))

# with 环境
>>> with sh.contrib.sudo(_with=True):
        print(ls("/root"))
# _with=True 关键字告诉命令它正处于 with 环境中, 以便可以正确地运行.

#将多个参数传递给命令时，每个参数必须是一个单独的字符串：

from sh import tar
tar("cvf", "/tmp/test.tar", "/my/home/directory/")
# 这将不起作用：

from sh import tar
tar("cvf /tmp/test.tar /my/home/directory")
```

## 参考文章
[在 Python 中使用 SH 模块执行 SHELL 命令 - 冬日の草原](https://amito.me/2018/Using-SH-in-Python/)