## 序言
因为在使用git的时候,经常需要更换代理链接,所以这里尝试自己开发一个软件,在输入原始地址的时候,可以自动替换代理链接.
软件命名暂时使用cit.
主要功能:
![20210122143710_6fed7ab811c46204871a9a4cc7b839a2.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210122143710_6fed7ab811c46204871a9a4cc7b839a2.png)
* git clone url 加速下载

使用到的模块预估:
1. sh模块,可以让软件调用系统命令
2. fire模块,用于封装命令

软件流程:
1. 读取用户输入的链接地址,暂定为url 
2. 将用户url替换为代理地址
3. 使用git clone命令开始下载


## 报错解决

### 找不到模块
>ModuleOrPackageNotFound # 找不到模块或包
![20210119220145_c3d97b8f6b307f9172949616dbdc11d4.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210119220145_c3d97b8f6b307f9172949616dbdc11d4.png)
>>构造时找不到模块或者包
>解决方法:
>>![20210119220303_e0b7c269a8105bd49a919320c42c524a.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210119220303_e0b7c269a8105bd49a919320c42c524a.png)
>>注意:include表示的子文件夹,根文件夹一般已经包含了.

### 无法导入
目前发现,是因为创建的包的名字的问题所产生的一系列附加问题.
改成from xx import xx 后解决了,但是依然会产生报错.