**在编程中，模块是功能齐全的单元，可以在项目之间共享和重用。它们使我们作为开发人员的生活更加轻松，因为我们可以使用它们来扩展我们的应用程序，而不必使用自己编写的功能。它们还使我们能够组织和分离代码，从而使应用程序更易于理解，调试和维护。**

在本文中，我将研究如何在Node.js中使用模块，重点是如何导出和使用它们。

## 不同的模块格式

由于JavaScript最初没有模块的概念，因此随着时间的推移，出现了各种竞争性格式。 以下是要注意的主要列表：

*   的 [异步模块定义（AMD）](https://en.wikipedia.org/wiki/Asynchronous_module_definition) 格式在浏览器中使用，并且使用 `define` 函数来定义模块。
*   所述 [CommonJS的（CJS）](https://en.wikipedia.org/wiki/CommonJS) 格式用于在Node.js的和用途 `require` 和 `module.exports` 定义的依赖关系和模块。 npm生态系统基于此格式构建。
*   的 [ES模块（ESM）](https://www.sitepoint.com/understanding-es6-modules/) 格式。 从ES6（ES2015）开始，JavaScript支持本机模块格式。 它使用 `export` 关键字导出模块的公共API，并使用 `import` 关键字导入 模块 。
*   该 [System.register](https://github.com/systemjs/systemjs/blob/master/docs/system-register.md) 格式被设计成内ES5支持ES6模块。
*   的 [通用模块定义（UMD）](https://riptutorial.com/javascript/example/16339/universal-module-definition--umd-) 格式可以在浏览器和在Node.js的均可使用 当模块需要由许多不同的模块加载程序导入时，这很有用。

请注意，本文仅涉及 **CommonJS格式** （Node.js中的标准）。 如果您想阅读任何其他格式，我建议您使用 SitePoint作者Jurgen Van de Moere撰写的 [这篇文章](https://www.jvandemo.com/a-10-minute-primer-to-javascript-modules-module-formats-module-loaders-and-module-bundlers/) 。

## 要求一个模块

Node.js带有 [一组内置模块](https://www.w3schools.com/nodejs/ref_modules.asp) ，我们可以在我们的代码中使用它们，而无需安装它们。 为此，我们需要使用 `require` 关键字 的模块 并将结果分配给变量。 然后可以使用它来调用模块公开的任何方法。

例如，要列出目录的内容，可以使用 [文件系统模块](https://nodejs.org/api/fs.html) 及其 `readdir` 方法：

```javascript
const fs = require('fs');
const folderPath = '/home/jim/Desktop/';

fs.readdir(folderPath, (err, files) => {
  files.forEach(file => {
    console.log(file);
  });
});

```

请注意，在CommonJS中，模块被同步加载并按照它们发生的顺序进行处理。

## 创建和导出模块

现在让我们看看如何创建我们自己的模块并将其导出以在程序的其他地方使用。 首先创建一个 `user.js` 文件并添加以下内容：

```javascript
const getName = () => {
  return 'Jim';
};

exports.getName = getName;

```

现在 `index.js` ，在同一文件夹中 创建一个 文件并添加以下内容：

```javascript
const user = require('./user');
console.log(`User: ${user.getName()}`);

```

使用运行程序 `node index.js` ，您应该在终端上看到以下输出：

```text
User: Jim

```

那么这里发生了什么？ 好吧，如果您查看 `user.js` 文件，您会注意到我们正在定义一个 `getName` 函数，然后使用 `exports` 关键字使其可以在其他位置导入。 然后在 `index.js` 文件中，我们将导入此函数并执行它。 还要注意，在 `require` 语句中，模块名称以 `./` 开头，因为它是本地文件。 另请注意，无需添加文件扩展名。

### 导出多种方法和值

我们可以用相同的方式导出多个方法和值：

```javascript
const getName = () => {
  return 'Jim';
};

const getLocation = () => {
  return 'Munich';
};

const dateOfBirth = '12.01.1982';

exports.getName = getName;
exports.getLocation = getLocation;
exports.dob = dateOfBirth;

```

并在 `index.js` ：

```javascript
const user = require('./user');
console.log(
  `${user.getName()} lives in ${user.getLocation()} and was born on ${user.dob}.`
);

```

上面的代码产生了这一点：

```text
Jim lives in Munich and was born on 12.01.1982.

```

请注意，我们给导出 `dateOfBirth` 变量 提供的名称 可以是我们喜欢的任何 名称 （ `dob` 在这种情况下）。 它不必与原始变量名相同。

### 语法变化

我还应该提到，可以随便导出方法和值，而不仅仅是在文件末尾。

例如：

```javascript
exports.getName = () => {
  return 'Jim';
};

exports.getLocation = () => {
  return 'Munich';
};

exports.dob = '12.01.1982';

```

而且由于有了 [销毁任务](https://www.sitepoint.com/es6-destructuring-assignment/) ，我们可以选择要导入的内容：

![](data:image/jpeg;base64,/9j/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAeABQDASIAAhEBAxEB/8QAGAAAAwEBAAAAAAAAAAAAAAAAAAUHBAb/xAAjEAAABgIDAAIDAAAAAAAAAAAAAQIDBREEIQYHEhMiMmFx/8QAFgEBAQEAAAAAAAAAAAAAAAAAAAMC/8QAHBEAAgICAwAAAAAAAAAAAAAAAAIBEQMSISIx/9oADAMBAAIRAxEAPwDru0+zJXjvKlxkb8aUNtpUdo9GZmOPe7h5M3RqcTsr0wLfL8NxM6eVKklknlN/Gv2j1f7GDF47Fyh5LLLmMtTVtr8NloRfLlVtVW4MPHbgadcSubN8RwpCSUR5LxGo6TWr1oAdxeCiOj2MRn8GkkkqKrALFWq518NGSZFjumq68ndfwTDp19t+VnDSy82pK6Sayq02YqDxKNBkmt6OwtjYtmOccViNNtm4f2q9gZGoAAAH/9k=) ![](https://cdn.sanity.io/images/708bnrs8/production/6394fe56ed921ff724d9f39b1315c74670dd6f2b-333x500.jpg?w=165&h=248&fit=crop)

![](https://cdn.sanity.io/images/708bnrs8/production/6394fe56ed921ff724d9f39b1315c74670dd6f2b-333x500.jpg?w=165&h=248&fit=crop)

### 释放自动化的力量

Node Node.JS食谱的向导法宝典，可自动执行工作流程

[立即阅读这本书](https://www.sitepoint.com/premium/books/automating-with-node-js/read)

```javascript
const { getName, dob } = require('./user');
console.log(
  `${getName()} was born on ${dob}.`
);

```

如您所料，此日志：

```text
Jim was born on 12.01.1982.

```

## 导出默认值

在上面的示例中，我们分别导出函数和值。 这对于整个应用程序可能需要的辅助函数很方便，但是当您有一个仅导出一件事的模块时，更常见的用法是 `module.exports` ：

```javascript
class User {
  constructor(name, age, email) {
    this.name = name;
    this.age = age;
    this.email = email;
  }

  getUserStats() {
    return `
      Name: ${this.name}
      Age: ${this.age}
      Email: ${this.email}
    `;
  }
}

module.exports = User;

```

并在 `index.js` ：

```javascript
const User = require('./user');
const jim = new User('Jim', 37, 'jim@example.com');

console.log(jim.getUserStats());

```

上面的代码记录如下：

```text
Name: Jim
Age: 37
Email: jim@example.com

```

## `module.exports` 和 之间有什么区别 `exports` ？

在网上旅行时，您可能会遇到以下语法：

```javascript
module.exports = {
  getName: () => {
    return 'Jim';
  },

  getLocation: () => {
    return 'Munich';
  },

  dob: '12.01.1982',
};

```

在这里，我们分配要导出到 `exports` 属性 的函数和值 `module` ，当然，这很好用：

```javascript
const { getName, dob } = require('./user');
console.log(
  `${getName()} was born on ${dob}.`
);

```

这将记录以下内容：

```text
Jim was born on 12.01.1982.

```

那么，什么 *是* 之间的区别 `module.exports` 和 `exports` ？ 一个只是另一个的方便别名吗？

好吧，但不是很…

为了说明我的意思，让我们更改代码 `index.js` 以记录的值 `module` ：

```javascript
console.log(module);

```

这将产生：

```text
Module {
  id: '.',
  exports: {},
  parent: null,
  filename: '/home/jim/Desktop/index.js',
  loaded: false,
  children: [],
  paths:
   [ '/home/jim/Desktop/node_modules',
     '/home/jim/node_modules',
     '/home/node_modules',
     '/node_modules' ] }

```

如您所见， `module` 具有一个 `exports` 属性。 让我们添加一些东西：

```javascript
// index.js
exports.foo = 'foo';
console.log(module);

```

输出：

```text
Module {
  id: '.',
  exports: { foo: 'foo' },
  ...

```

分配的属性 `exports` 还会将它们添加到中 `module.exports` 。 这是因为（至少从最初开始） `exports` 是对的引用 `module.exports` 。

### 那么我应该使用哪一个呢？

由于 `module.exports` 和 `exports` 都指向同一个对象，它通常不会不管你用。 例如：

```javascript
exports.foo = 'foo';
module.exports.bar = 'bar';

```

此代码将导致模块的导出对象为 `{ foo: 'foo', bar: 'bar' }` 。

但是，有一个警告。 分配 `module.exports` 给 您的 是从模块导出的内容。

因此，请采取以下措施：

```javascript
exports.foo = 'foo';
module.exports = () => { console.log('bar'); };

```

这只会导致导出匿名函数。 该 `foo` 变量将被忽略。

如果您想进一步了解它们之间的区别，建议您参阅 [本文](https://www.hacksparrow.com/nodejs/exports-vs-module-exports.html) 。

## 结论

模块已经成为JavaScript生态系统不可或缺的一部分，从而使我们可以从较小的部分组成大型程序。 我希望本文能为您提供在Node.js中与它们一起使用的良好介绍，以及帮助使它们的语法神秘化。

如果您有任何疑问或意见，请随时 [跳至SitePoint论坛](https://www.sitepoint.com/community/) 开始讨论。