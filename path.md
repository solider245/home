## linux下查看和添加PATH环境变量

>$PATH：决定了shell将到哪些目录中寻找命令或程序，PATH的值是一系列目录，当您运行一个程序时，Linux在这些目录下进行搜寻编译链接

## PATH 声明格式

```shell
PATH=$PATH:<PATH 1>:<PATH 2>:<PATH 3>:------:<PATH N>
export PATH=$PATH:/path/to/dir1
export PATH=$PATH:/path/to/dir1:/path/to/dir2
PATH=$PATH:/path/to/dir1; export PATH
```

## 可用 export 命令查看PATH值
`export`

## 单独查看PATH环境变量
```shell
echo $PATH
```
## 查看当前shell
```shell
echo $0 # 等同于echo $SHELL
```

## 添加PATH环境变量，可用
```shell
export PATH=$PATH:/place/with/the/file
export PATH=$PATH:/usr/local/bin
echo 'export PATH="$HOME/.local/bin/:$PATH"' >>~/.bashrc 
echo 'export PATH=$PATH:/usr/local/bin'  >> ~/.bash_profile
# HOME=~ 
```