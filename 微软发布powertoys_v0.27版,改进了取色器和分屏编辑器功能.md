## 序言

>我们0.27发布周期的目标是专注于开发团队和最终用户的最终用户体验，稳定性，可访问性，本地化和生活质量改善
>
>## `Where is the Video Conference utility?`
>
>这大约需要一周的时间。 我们需要在此处完成其他工作才能将其发送出去。 这将是0.28实验版，它将是0.27 +视频会议实用程序。 为了更高优先级的工作，我们跳过了0.26版本。
>
>## `Release Notes`
>
>**一般**
>
>*   安装程序改进，包括黑暗模式
>*   解决了大量的可访问性问题。
>*   致力于本地化工作。 如果您发现问题，请\[使我们知道，以便我们进行纠正\] \[loc\-bug\]。
>
>**颜色选择器**
>
>*   [@martinchrzan](https://github.com/martinchrzan) 和 [@ niels9001](https://github.com/niels9001) 完成了界面更新和新编辑经验[](https://github.com/niels9001)
>
>**花式区**
>
>*   现在，多显示器编辑器的体验大大提高了可发现性。
>*   重新启动时忘记区域
>*   增加了没有布局的功能
>
>**图像调整器**
>
>*   界面更新
>
>**PowerToys Run**
>
>*   删除了未使用的依赖项
>
>**电源重命名**
>
>*   通过Boost库添加了Lookbehind支持
>
>有关整个提交历史，请查看 [0.27版本](https://github.com/microsoft/PowerToys/issues?q=is%3Aissue+project%3Amicrosoft%2FPowerToys%2F14) 。 以下是此版本中的一些项目>符号。
>
>我想直接叫出 [@davidegiacometti](https://github.com/davidegiacometti) ， [@gordonwatts](https://github.com/gordonwatts) ， [@martinchrzan](https://github.com/martinchrzan) ， [@ niels9001](https://github.com/niels9001) ， [@ P\-风暴](https://github.com/p-storm) ， [@TobiasSekan](https://github.com/TobiasSekan) ， [@亚伦\-容克](https://github.com/Aaron-Junker) ， [@htcfreek](https://github.com/htcfreek) 和 [@ alannt777](https://github.com/alannt777) 他们继续社会各界的支持和帮助，直接让PowerToys的更好件软件。


没有什么大更新.
