## Nodejs

## 安装

```shell
# MacOs
brew install node
# ubuntu
sudo apt-get install nodejs
sudo apt-get install npm
# 二进制安装
wget https://nodejs.org/dist/v10.9.0/node-v10.9.0-linux-x64.tar.xz    # 下载
tar xf  node-v10.9.0-linux-x64.tar.xz       #// 解压
cd node-v10.9.0-linux-x64/                  #// 进入解压目录
./bin/node -v                               #// 执行node命令 查看版本
ln -s /usr/software/nodejs/bin/npm   /usr/local/bin/ # 设置软连接
ln -s /usr/software/nodejs/bin/node   /usr/local/bin/ # 设置软连接
# 源码安装不推荐
nodejs官网 https://nodejs.org/zh-cn/download/
```
## nodejs常用命令
```shell
#升级更新
npm install npm -g
#初始化
npm init
#全局安装
npm install express -g
#本地安装
npm install express
#搜索
npm search express
#更新
npm update express
#卸载
npm uninstall 
#使用安装的包
var express = require('express');
# 查看当前仓库地址
npm config get registry
# npm更改加速地址
npm config set registry https://registry.npm.taobao.org
npm config set registry https://mirrors.huaweicloud.com/repository/npm/ #华为源
# npm官方地址：https://registry.npmjs.org
# 使用nrm进行切换镜像源
npm install -g nrm
# 查看其他源
nrm ls
# 增加源
nrm add 仓库名称 仓库地址 
# 使用某个源
nrm use 地址名称
#报错: npm resource busy or locked.先删除之前的node_modules再重新安装
npm cache clean
```


## 参考链接
[Node.js 安装配置 | 菜鸟教程](https://www.runoob.com/nodejs/nodejs-install-setup.html)
