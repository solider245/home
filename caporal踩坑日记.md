# caporal命令行工具踩坑日记
Caporal一个全功能的框架，用于使用Node.js构建出色的命令行应用程序。在学习commander的时候有人推荐这个框架，所以过来踩个坑
<!-- more -->

## 安装
```sh
mkdir caporal-test && cd caporal-test
npm init -y 初始化
npm install @caporal/core
```

## 词汇表

我们将在本指南中引用以下表达式：

1.   **Program 程序** ：使用Caporal构建的cli应用程序
2.   **Command 命令** ：程序中的命令。 一个程序可以有多个命令。
3.   **Action 动作** ：执行命令时，每个命令都有一个关联的动作运行。
4.   **Argument 参数** ：命令可以在命令后传递一个或多个参数。
5.   **Options 选项** ： 可以指定 长短选项，例如 `-f` 或 `--file` 。

这五个参数非常重要，教程基本上都是围绕着五个教程来写的

## 编写程序

### hello world

一个最简单的hello world程序：
```js
#!/usr/bin/env node
// file: hello-world.js (make the file executable using `chmod +x hello.js`)

// Caporal provides you with a program instance
const { program } = require("@caporal/core")

// Simplest program ever: this program does only one thing
program.action(({ logger }) => {
  logger.info("Hello, world!")
})

// always run the program at the end
program.run()

/* 
# Now, in your terminal:

$ ./hello-world.js
Hello, world!

*/
```
>该程序仅包含一个动作。动作是在运行程序或其命令之一时执行的功能。

需要包含的要素如下：
1. #!/usr/bin/env node 
   > 要在抬头
2. program.run() 
   >始终在最后
3. chmod +x 文件名  
   >如果无法输出，可能是文件没有权限，用这个命令追加
```sh
node index.js # 测试命令
info: Hello, world! # 输出 
```
## 传参

### 传入一个参数

修改index.js文件如下：
```js
#!/usr/bin/env node

const { program } = require("@caporal/core")

program
    .argument("<name>", "Name to greet")
    .action(({ logger, args }) => {
        logger.info("Hello, %s!", args.name)
    })

program.run()

/*
$ ./hello.js Matt
Hello, Matt!
*/
```
>尖括号与方括号

>尖括号（例如 `<item>` ）表示 **要求** 输入，而方括号（例如 `[env]` ）表示 **可选** 输入。 在上面的示例中， `<name>` 参数是强制性的。
```sh  
node index.js heihei #测试代码

```
### 传入多个参数
```js
#!/usr/bin/env node
const { program } = require("@caporal/core")
program
  .argument("<name>", "Name to greet")
  .argument("<other-name>", "Another argument")
  .argument("[third-arg]", "This argument is optional")
  .action(({ logger, args }) =>
    // Notice that args are camel-cased, so <other-name> becomes otherName
    logger.info("Hello, %s and %s!", args.name, args.otherName),
  )

program.run()
```
测试：
```
node index.js 刘备 关羽 #测试
info: Hello, 刘备 and 关羽! # 输出成功
```
### 要点

1. <>中的参数一定要填，[]中的参数则是选填
2. 传参后的参数，如果是驼峰命名需要将中间的-去掉

## 选项
>让我们通过--greeting在程序中添加一个选项来修改问候语。

```js
#!/usr/bin/env node
const program = require("@caporal/core")
program
  .argument("<name>", "Name to greet")
  .option("-g, --greating <word>", "Greating to use", {
    default: "Hello",
  })
  .action(({ logger, args, options }) => {
    logger.info("%s, %s!", options.greating, args.name)
  })

program.run()

/*
$ ./hello.js Matt --greeting Hi
Hi, Matt!

$ ./hello.js Matt
Hello, Matt!
*/

```
注意：
1. -g ,--greating, 长参数用双破折号，短参数用单破折号