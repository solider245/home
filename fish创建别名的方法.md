## 序言
fish创建别名的方法和bash,zsh有点不一样。这里做一个汇总，方便以后再翻阅。

## 常见方法汇总

* 临时使用
```fish
alias rm="rm -i"
```
临时使用和zsh,bash差不多。
* 保存到系统
```shell
alias rm="rm -i"
funcsave rm
```
* 在配置文件`~/.config/fish/config.fish`中设置:
```
alias rm="rm -i"
```
然后
```shell
source ~/.config/fish/config.fish

```

* 构建复杂别名
在`~/.config/fish/functions`文件夹下创建同名文件例如`ac.fish`，然后写入下列内容
```shell
function ac
    git add .
    git commit -m "add all and commit"
end
```
> 这里构建了一个ac函数，他会保存当前所有改动并提交一次

这是因为fish中alias是作为函数而保存在`~/.config/fish/functions`文件夹下

![20201214155126_0251242d4d0d0e642b96f5c5972a5972.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20201214155126_0251242d4d0d0e642b96f5c5972a5972.png)

如上所示，每一个alias都是作为一个单独的函数而存在。你可以观察。
![20201214155222_742e5878bbded53a0ce9a19d088a46bd.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20201214155222_742e5878bbded53a0ce9a19d088a46bd.png)
如上，函数的形式，你也可以直接进去编辑。

## 其他方法

### Fish函数导入bash别名作为鱼的缩写

在函数配置文件夹下创建函数`import_bash_aliases.fish`，并写入以下代码：

```shell
# Fish function to import bash aliases v0.2
# Copyright (C) 2016 Malte Biermann
# Copyright (C) 2017 Rodrigo Bermudez Schettino
# Released under the GPL
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Imports aliases defined in ~/.bash_aliases formatted as alias name='command'
function import_bash_aliases --description 'bash aliases to .fish function files'
    for a in (cat ~/.bash_aliases  | grep "^alias")
        #echo $a
        
        set aname (echo $a | sed "s/alias \(.*\)='.*/\1/")
        # To import aliases with commands enclosed in double quotes uncomment
        # the following line
        # set aname (echo $a | sed 's/alias \(.*\)=".*/\1/')
        #echo $aname
        
        set command (echo $a | sed "s/alias.*='\(.*\)'/\1/")
        # To import aliases with commands enclosed in double quotes uncomment
        # the following line
        # set command (echo $a | sed 's/alias.*="\(.*\)"/\1/')
        #echo $command
        
        echo "Processing alias $aname as $command"
        
        # TODO: Check if abbreviation already exists and avoid overwriting it
        # This can be achieved by using the fish function in
        # https://github.com/fish-shell/fish-shell/blob/036b708d9906d4f1fcc7ab2389aa06cf5ec05d11/share/functions/abbr.fish#L130
        abbr -a $aname $command
    end
end
```


* 自动创建别名函数
在`~/.config/fish/functions`文件夹中创建一个`mkalias.fish`函数，然后写入：

```shell
function mkalias --argument key value
  echo alias $key=$value
  alias $key=$value
  funcsave $key
end
```
>这个函数用不了，我还没理解怎么用法，但是这里做一个备忘
## 参考文章
[linux-在鱼壳中定义别名-代码日志](https://stackoverflow.com/questions/2762994/define-an-alias-in-fish-shell)
>这里集合了大多问题与解决方法
[别名-创建函数-fish-shell 3.1.2文档](https://fishshell.com/docs/current/cmds/alias.html)
>官方文档，最权威的文档
[在鱼和bash外壳中创建别名](https://www.linuxliteos.com/forums/tutorials/creating-aliases-in-fish-and-bash-shell/)
[Fish函数导入bash别名作为鱼的缩写](https://gist.github.com/rodrigobdz/007168cbe7244782ddabe061b784db35)
>很强大的功能