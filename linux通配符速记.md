## 序言

总是忘记通配符应该怎么使用，这里做一个速记，方便日后回忆。
网上很多通配符教程每次看后就忘。这里结合案例，方便新手记录。
```
1. |     #管道符，或者（正则）
2. >     #输出重定向
3. >>    #输出追加重定向
4. <     #输入重定向
5. <<    #追加输入重定向
6. ~     #当前用户家目录
7. `` $() #引用命令被执行后的结果
8. $     #以。。。结尾（正则）
9. ^     #以。。。开头（正则）
10. *     #匹配全部字符，通配符
11. ？    #任意一个字符，通配符
12. #       #注释
13. &       #让程序或脚本切换到后台执行
14. &&      #并且 同时成立
15. []      #表示一个范围（正则，通配符）
16. {}      #产生一个序列（通配符）
17. .       #当前目录的硬链接
18. ..      #上级目录的硬链接

```
以上是我们常见的18种常用通配符，不要被吓到，刚开始不要记那么多，把他们保存好，然后让我们从实际案例中来学习如何使用他们。

## {}

创建文件的命令是touch，假如我们要批量创建文件，那么我们可以使用{}。
```shell
ubuntu@solider245:~/doc/通配符$ touch hello{1,2,3,4,5}.md
ubuntu@solider245:~/doc/通配符$ ls
hello1.md  hello2.md  hello3.md  hello4.md  hello5.md
```
如上所示，这样的话，你将得到hello开头，1，2，3，4，5结尾的五个md文件。
同理，假如我们要删除这五个创建的文件，那么我们可以使用如下命令：
```shell
ubuntu@solider245:~/doc/通配符$ rm -rfi hello{1,2,3,4,5}.md
rm: remove regular empty file 'hello1.md'? y
rm: remove regular empty file 'hello2.md'? y
rm: remove regular empty file 'hello3.md'? y
rm: remove regular empty file 'hello4.md'? y
rm: remove regular empty file 'hello5.md'? y
ubuntu@solider245:~/doc/通配符$ ls
```
我添加了指定i，方便新手更加友善的交互。

通过上面的学习，我们基本上可以掌握如何利用通配符{}来进行批量创建和删减文件。

当然，由于1，2，3，4，5是一个数列，那么我们可以巧妙的利用这个规则来创建文件吗？
有的。
```shell
ubuntu@solider245:~/doc/通配符$ touch hello{1..5}.md
ubuntu@solider245:~/doc/通配符$ ls
hello1.md  hello2.md  hello3.md  hello4.md  hello5.md
```
如上所示，我们使用{1..5}这个办法,巧妙的创建了5个文件。同理，字母a -z也是一样，接下来我们继续创建。
```shell
touch hello{a..z}.md
ubuntu@solider245:~/doc/通配符$ ls
hello1.md  helloa.md  hellof.md  hellok.md  hellop.md  hellou.md  helloz.md
hello2.md  hellob.md  hellog.md  hellol.md  helloq.md  hellov.md
hello3.md  helloc.md  helloh.md  hellom.md  hellor.md  hellow.md
hello4.md  hellod.md  helloi.md  hellon.md  hellos.md  hellox.md
hello5.md  helloe.md  helloj.md  helloo.md  hellot.md  helloy.md
```
如上所示，我们批量创建了a-z的所有文件。相信通过以上内容，你可以很轻易的掌握通配符{开始..结束}

## *匹配全部字符

接下来我们开始学习*。型号通常用于查找或者删除。我们前面创建了那么多文件，我们可以使用*来匹配所有文件并删除。
```shell
ubuntu@solider245:~/doc/通配符$ rm -rfi *
ubuntu@solider245:~/doc/通配符$ ls
ubuntu@solider245:~/doc/通配符$
```
如上所示，我们可以使用rm -rfi *来删除当前目录下的所有文件。

### *部分匹配 
除了全部匹配之外，我们经常遇到的就是，我们有些字符是确认的，而有些字符却是模糊的。
下面，我们使用以下命令，批量创建文件：
```lua
ubuntu@solider245:~/doc/通配符$ touch {01..05}hell{a..z}.{md,txt}
ubuntu@solider245:~/doc/通配符$ ls
01hella.md   01hellw.md   02hells.md   03hello.md   04hellk.md   05hellg.md
01hella.txt  01hellw.txt  02hells.txt  03hello.txt  04hellk.txt  05hellg.txt
01hellb.md   01hellx.md   02hellt.md   03hellp.md   04helll.md   05hellh.md
01hellb.txt  01hellx.txt  02hellt.txt  03hellp.txt  04helll.txt  05hellh.txt
01hellc.md   01helly.md   02hellu.md   03hellq.md   04hellm.md   05helli.md
01hellc.txt  01helly.txt  02hellu.txt  03hellq.txt  04hellm.txt  05helli.txt
01helld.md   01hellz.md   02hellv.md   03hellr.md   04helln.md   05hellj.md
01helld.txt  01hellz.txt  02hellv.txt  03hellr.txt  04helln.txt  05hellj.txt
01helle.md   02hella.md   02hellw.md   03hells.md   04hello.md   05hellk.md
01helle.txt  02hella.txt  02hellw.txt  03hells.txt  04hello.txt  05hellk.txt
01hellf.md   02hellb.md   02hellx.md   03hellt.md   04hellp.md   05helll.md
01hellf.txt  02hellb.txt  02hellx.txt  03hellt.txt  04hellp.txt  05helll.txt
01hellg.md   02hellc.md   02helly.md   03hellu.md   04hellq.md   05hellm.md
01hellg.txt  02hellc.txt  02helly.txt  03hellu.txt  04hellq.txt  05hellm.txt
01hellh.md   02helld.md   02hellz.md   03hellv.md   04hellr.md   05helln.md
01hellh.txt  02helld.txt  02hellz.txt  03hellv.txt  04hellr.txt  05helln.txt
01helli.md   02helle.md   03hella.md   03hellw.md   04hells.md   05hello.md
01helli.txt  02helle.txt  03hella.txt  03hellw.txt  04hells.txt  05hello.txt
01hellj.md   02hellf.md   03hellb.md   03hellx.md   04hellt.md   05hellp.md
01hellj.txt  02hellf.txt  03hellb.txt  03hellx.txt  04hellt.txt  05hellp.txt
01hellk.md   02hellg.md   03hellc.md   03helly.md   04hellu.md   05hellq.md
01hellk.txt  02hellg.txt  03hellc.txt  03helly.txt  04hellu.txt  05hellq.txt
01helll.md   02hellh.md   03helld.md   03hellz.md   04hellv.md   05hellr.md
01helll.txt  02hellh.txt  03helld.txt  03hellz.txt  04hellv.txt  05hellr.txt
01hellm.md   02helli.md   03helle.md   04hella.md   04hellw.md   05hells.md
01hellm.txt  02helli.txt  03helle.txt  04hella.txt  04hellw.txt  05hells.txt
01helln.md   02hellj.md   03hellf.md   04hellb.md   04hellx.md   05hellt.md
01helln.txt  02hellj.txt  03hellf.txt  04hellb.txt  04hellx.txt  05hellt.txt
01hello.md   02hellk.md   03hellg.md   04hellc.md   04helly.md   05hellu.md
01hello.txt  02hellk.txt  03hellg.txt  04hellc.txt  04helly.txt  05hellu.txt
01hellp.md   02helll.md   03hellh.md   04helld.md   04hellz.md   05hellv.md
01hellp.txt  02helll.txt  03hellh.txt  04helld.txt  04hellz.txt  05hellv.txt
01hellq.md   02hellm.md   03helli.md   04helle.md   05hella.md   05hellw.md
01hellq.txt  02hellm.txt  03helli.txt  04helle.txt  05hella.txt  05hellw.txt
01hellr.md   02helln.md   03hellj.md   04hellf.md   05hellb.md   05hellx.md
01hellr.txt  02helln.txt  03hellj.txt  04hellf.txt  05hellb.txt  05hellx.txt
01hells.md   02hello.md   03hellk.md   04hellg.md   05hellc.md   05helly.md
01hells.txt  02hello.txt  03hellk.txt  04hellg.txt  05hellc.txt  05helly.txt
01hellt.md   02hellp.md   03helll.md   04hellh.md   05helld.md   05hellz.md
01hellt.txt  02hellp.txt  03helll.txt  04hellh.txt  05helld.txt  05hellz.txt
01hellu.md   02hellq.md   03hellm.md   04helli.md   05helle.md
01hellu.txt  02hellq.txt  03hellm.txt  04helli.txt  05helle.txt
01hellv.md   02hellr.md   03helln.md   04hellj.md   05hellf.md
01hellv.txt  02hellr.txt  03helln.txt  04hellj.txt  05hellf.txt
```
`touch {01..05}hell{a..z}.{md,txt}`
在解释下这行命令，我们批量创建以01-05开头，以a-z结尾，文件后缀为md或者txt的文件。怎么样？轻轻松松就创建了这么多文件，如果不用通配符手动创建的话，怕是要累死。

接下来，我们通过ls查询命令，来熟知通配符*所处位置的影响：
```lua
ubuntu@solider245:~/doc/通配符$ ls *hello.md
01hello.md  02hello.md  03hello.md  04hello.md  05hello.md
ubuntu@solider245:~/doc/通配符$ ls *hell*.md
01hella.md  01hellr.md  02helli.md  02hellz.md  03hellq.md  04hellh.md  04helly.md  05hellp.md
01hellb.md  01hells.md  02hellj.md  03hella.md  03hellr.md  04helli.md  04hellz.md  05hellq.md
01hellc.md  01hellt.md  02hellk.md  03hellb.md  03hells.md  04hellj.md  05hella.md  05hellr.md
01helld.md  01hellu.md  02helll.md  03hellc.md  03hellt.md  04hellk.md  05hellb.md  05hells.md
01helle.md  01hellv.md  02hellm.md  03helld.md  03hellu.md  04helll.md  05hellc.md  05hellt.md
01hellf.md  01hellw.md  02helln.md  03helle.md  03hellv.md  04hellm.md  05helld.md  05hellu.md
01hellg.md  01hellx.md  02hello.md  03hellf.md  03hellw.md  04helln.md  05helle.md  05hellv.md
01hellh.md  01helly.md  02hellp.md  03hellg.md  03hellx.md  04hello.md  05hellf.md  05hellw.md
01helli.md  01hellz.md  02hellq.md  03hellh.md  03helly.md  04hellp.md  05hellg.md  05hellx.md
01hellj.md  02hella.md  02hellr.md  03helli.md  03hellz.md  04hellq.md  05hellh.md  05helly.md
01hellk.md  02hellb.md  02hells.md  03hellj.md  04hella.md  04hellr.md  05helli.md  05hellz.md
01helll.md  02hellc.md  02hellt.md  03hellk.md  04hellb.md  04hells.md  05hellj.md
01hellm.md  02helld.md  02hellu.md  03helll.md  04hellc.md  04hellt.md  05hellk.md
01helln.md  02helle.md  02hellv.md  03hellm.md  04helld.md  04hellu.md  05helll.md
01hello.md  02hellf.md  02hellw.md  03helln.md  04helle.md  04hellv.md  05hellm.md
01hellp.md  02hellg.md  02hellx.md  03hello.md  04hellf.md  04hellw.md  05helln.md
01hellq.md  02hellh.md  02helly.md  03hellp.md  04hellg.md  04hellx.md  05hello.md
ubuntu@solider245:~/doc/通配符$ ls *hell*.*
01hella.md   01hellt.md   02hellm.md   03hellf.md   03helly.md   04hellr.md   05hellk.md
01hella.txt  01hellt.txt  02hellm.txt  03hellf.txt  03helly.txt  04hellr.txt  05hellk.txt
01hellb.md   01hellu.md   02helln.md   03hellg.md   03hellz.md   04hells.md   05helll.md
01hellb.txt  01hellu.txt  02helln.txt  03hellg.txt  03hellz.txt  04hells.txt  05helll.txt
01hellc.md   01hellv.md   02hello.md   03hellh.md   04hella.md   04hellt.md   05hellm.md
01hellc.txt  01hellv.txt  02hello.txt  03hellh.txt  04hella.txt  04hellt.txt  05hellm.txt
01helld.md   01hellw.md   02hellp.md   03helli.md   04hellb.md   04hellu.md   05helln.md
01helld.txt  01hellw.txt  02hellp.txt  03helli.txt  04hellb.txt  04hellu.txt  05helln.txt
01helle.md   01hellx.md   02hellq.md   03hellj.md   04hellc.md   04hellv.md   05hello.md
01helle.txt  01hellx.txt  02hellq.txt  03hellj.txt  04hellc.txt  04hellv.txt  05hello.txt
01hellf.md   01helly.md   02hellr.md   03hellk.md   04helld.md   04hellw.md   05hellp.md
01hellf.txt  01helly.txt  02hellr.txt  03hellk.txt  04helld.txt  04hellw.txt  05hellp.txt
01hellg.md   01hellz.md   02hells.md   03helll.md   04helle.md   04hellx.md   05hellq.md
01hellg.txt  01hellz.txt  02hells.txt  03helll.txt  04helle.txt  04hellx.txt  05hellq.txt
01hellh.md   02hella.md   02hellt.md   03hellm.md   04hellf.md   04helly.md   05hellr.md
01hellh.txt  02hella.txt  02hellt.txt  03hellm.txt  04hellf.txt  04helly.txt  05hellr.txt
01helli.md   02hellb.md   02hellu.md   03helln.md   04hellg.md   04hellz.md   05hells.md
01helli.txt  02hellb.txt  02hellu.txt  03helln.txt  04hellg.txt  04hellz.txt  05hells.txt
01hellj.md   02hellc.md   02hellv.md   03hello.md   04hellh.md   05hella.md   05hellt.md
01hellj.txt  02hellc.txt  02hellv.txt  03hello.txt  04hellh.txt  05hella.txt  05hellt.txt
01hellk.md   02helld.md   02hellw.md   03hellp.md   04helli.md   05hellb.md   05hellu.md
01hellk.txt  02helld.txt  02hellw.txt  03hellp.txt  04helli.txt  05hellb.txt  05hellu.txt
01helll.md   02helle.md   02hellx.md   03hellq.md   04hellj.md   05hellc.md   05hellv.md
01helll.txt  02helle.txt  02hellx.txt  03hellq.txt  04hellj.txt  05hellc.txt  05hellv.txt
01hellm.md   02hellf.md   02helly.md   03hellr.md   04hellk.md   05helld.md   05hellw.md
01hellm.txt  02hellf.txt  02helly.txt  03hellr.txt  04hellk.txt  05helld.txt  05hellw.txt
01helln.md   02hellg.md   02hellz.md   03hells.md   04helll.md   05helle.md   05hellx.md
01helln.txt  02hellg.txt  02hellz.txt  03hells.txt  04helll.txt  05helle.txt  05hellx.txt
01hello.md   02hellh.md   03hella.md   03hellt.md   04hellm.md   05hellf.md   05helly.md
01hello.txt  02hellh.txt  03hella.txt  03hellt.txt  04hellm.txt  05hellf.txt  05helly.txt
01hellp.md   02helli.md   03hellb.md   03hellu.md   04helln.md   05hellg.md   05hellz.md
01hellp.txt  02helli.txt  03hellb.txt  03hellu.txt  04helln.txt  05hellg.txt  05hellz.txt
01hellq.md   02hellj.md   03hellc.md   03hellv.md   04hello.md   05hellh.md
01hellq.txt  02hellj.txt  03hellc.txt  03hellv.txt  04hello.txt  05hellh.txt
01hellr.md   02hellk.md   03helld.md   03hellw.md   04hellp.md   05helli.md
01hellr.txt  02hellk.txt  03helld.txt  03hellw.txt  04hellp.txt  05helli.txt
01hells.md   02helll.md   03helle.md   03hellx.md   04hellq.md   05hellj.md
01hells.txt  02helll.txt  03helle.txt  03hellx.txt  04hellq.txt  05hellj.txt
```
如上所示，每增加一个通配符，增加的文件就非常的多。这里就不演示删除文件了，后面大家可以自行解决。
我们接下来开始学习?通配符（必须是英文命令的问号，不是中文命令的）

## ？    #任意一个字符，通配符
如前面所示，问号的意思是匹配任意一个字符，而*则是匹配全部字符，那么他们在使用上有什么区别呢？

```lua
ubuntu@solider245:~/doc/通配符$ ls ?hello.md
ls: cannot access '?hello.md': No such file or directory
ubuntu@solider245:~/doc/通配符$ ls ??hello.md
01hello.md  02hello.md  03hello.md  04hello.md  05hello.md
ubuntu@solider245:~/doc/通配符$ ls ??hello.??
01hello.md  02hello.md  03hello.md  04hello.md  05hello.md
ubuntu@solider245:~/doc/通配符$ ls ??hello.???
01hello.txt  02hello.txt  03hello.txt  04hello.txt  05hello.txt
ubuntu@solider245:~/doc/通配符$ ls ??hello.?
ls: cannot access '??hello.?': No such file or directory
```
如上所示，当我们在hello前匹配一个问号的时候，查找不到文件，但是匹配两个问号的时候，文件就出来了。
同样的，我们在后缀匹配一个问号的时候找不到文件，匹配两个问号的时候，找到的是md结尾的文件，匹配3个问号的时候找到的是txt结尾的文件。

?和*型号的区别在于，?强制匹配字符个数，一个问号就匹配一个字符，两个问号则匹配两个字符，三个问号则匹配3个字符。

## [] #表示一个范围（正则，通配符）

有了*号，？有时候我们还想再缩小一点通配范围，这个时候我们可以用[]。
比如我们使用?的时候，他可能把0-9之内的所有数字都通配了，那么，这个时候我们使用[0-5],那么系统就只会通配0-5.我们还是以具体的例子来举例。
```lua
ubuntu@solider245:~/doc/通配符$ ls 0?hello.md
01hello.md  02hello.md  03hello.md  04hello.md  05hello.md
ubuntu@solider245:~/doc/通配符$ ls 0[0-3]hello.md
01hello.md  02hello.md  03hello.md
ubuntu@solider245:~/doc/通配符$ ls 0[!0-3]hello.md
04hello.md  05hello.md
```
如上所示，当你使用[0-3]的时候，通配范围就缩小了，只有3个文件，当你在前面加一个感叹号（^）时就表示反向选择，因此就只剩下了两个文件。
[]是通配符中最强大的符号，也是最需要时间去掌握的。初学时掌握基本的通配和反向通配。其他形式可以保存好，以后需要的时候再查询即可。

这里把通配符的常用类型再列一次，以后就可以留存了。
| 通配符 | 匹配项 |
| --- | --- |
| \* | 匹配任意多个字符(包括0个和1个) |
| ？ | 匹配任意单个字符(不包括0个) |
| \[characters\] | 匹配任意一个属于字符集中的字符 |
| \[!characters\] | 匹配任意一个不属于字符集中的字符 |
| \[\[:class:\]\] | 匹配任意一个属于指定字符类中的字符 |

表2 最常见的字符类

| 字符类 | 匹配项 |
| --- | --- |
| \[:alnum:\] | 匹配任意一个字母或数字 |
| \[:alpha:\] | 匹配任意一个字母 |
| \[:digit\] | 匹配任意一个数字 |
| \[:lower:\] | 匹配任意一个小写字母 |
| \[:upper:\] | 匹配任意一个大写字母 |

表3 通配符示例

| 形式 | 匹配项 |
| --- | --- |
| \* | 所有文件 |
| g\* | 以g开头的任一文件 |
| b\*.txt | 以b开头，中间有任意多个字符，并以.txt结尾的任一文件 |
| Data??? | 以Data开头，后面跟3个字符的任一文件 |
| \[abc\]\* | 以a,b,c任一一个开头的任一文件 |
| BACKUP.\[0\-9\]\[0\-9\]\[0\-9\] | 以BACKUP.开头，后面跟随3个数字的任一文件 |
| \[\[:upper:\]\]\* | 以大写字母开头的任一文件 |
| \[!\[:digit:\]\]\* | 不以数字开头的任一文件 |
| \*\[\[:lower\]123\] | 以小写字母或数字1、2、3中任一结尾的任一文件 |

## 参考文献

[Linux操作文件与目录——cp、mv、mkdir、rm和ln命令_mathlxj的博客-CSDN博客](https://blog.csdn.net/mathlxj/article/details/106354273)
[linux文件名匹配——通配符使用 - CS408 - 博客园](https://www.cnblogs.com/lixuwu/p/7816335.html)
[命令行通配符教程 - 阮一峰的网络日志](http://www.ruanyifeng.com/blog/2018/09/bash-wildcards.html)
[12_Linux基础知识及命令————使用通配符来操作文件-重剑无锋 大巧不工-51CTO博客](https://blog.51cto.com/wuyelan/1393143)
[linux|批量创建文件、文件夹或删除——通配符_王春兰的博客-CSDN博客](https://blog.csdn.net/qq_28296925/article/details/81163373)