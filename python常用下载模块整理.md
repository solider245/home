## 序言
因为开发cit软件遇到了下载问题,这里刚好整理下目前python常见的下载模块.exports

## python下载问题解决方法

### wget模块
1.  安装 
```
pip install wget 
```
2. 使用 
```py
>>> import wget
>>> url = 'http://www.futurecrew.com/skaven/song_files/mp3/razorback.mp3'
>>> filename = wget.download(url)
100% [................................................] 3841532 / 3841532>
>> filename
'razorback.mp3'
```
**编辑：** 您还可以使用该 `out` 参数来使用自定义输出目录，而不是当前工作目录。

```py
>>> output_directory = <directory_name>
>>> filename = wget.download(url, out=output_directory)
>>> filename
'razorback.mp3'
```



### pywget模块
1. 安装
```
pip install pywget 
```
2. 使用 
```py 
from pywget import wget 
wget.download('url')
```

### download模块
[下载·PyPI](https://pypi.org/project/download/)
1. 安装
```shell
pip install download
```
2. 使用 
```py
# 下载文件
from download import download
path = download(url, file_path)
#名为file_name的文件将下载到file_path的文件夹中。
#文件类型
#如果您的文件是zip文件，则可以添加标志：

path = download(url, file_path, kind="zip")
#在这种情况下，将下载文件，然后将其解压缩到file_name指定的文件夹中。
#支持的格式为'file'，'zip'，'tar'，'tar.gz' 默认为file。

#进度条
#文件下载过程中是否显示进度条。默认为True：
path = download(url, file_path, progressbar=True)

#更换
#如果为True，且URL指向单个文件，则尽可能覆盖旧文件。默认为False：
path = download(url, file_path, replace=False)

#超时
#URL打开超时（以秒为单位）。默认为10秒：
path = download(url, file_path, timeout=10)

#详细
#是否将下载状态打印到屏幕上。默认为True：
path = download(url, file_path, verbose=True)

#经常问的问题
#为什么偶尔会出现“错误文件大小为……”错误？
#有时，当您尝试下载文件时，从中下载的服务器将返回下载完成的信息，否则未下载。下载将检查最终下载的文件是否正确。如果不是，它将抛出此错误。在这种情况下，您应该尝试重新下载文件。
```
### 使用urllib模块下载 
```py
import urllib 
print "downloading with urllib" 
url = 'http://www.pythontab.com/test/demo.zip'  
print "downloading with urllib"
urllib.urlretrieve(url, "demo.zip")
```

### 使用urllib2模块下载
1. 安装
>系统自带模块,所以不用安装
2. 使用
```py
import urllib2
print "downloading with urllib2"
url = 'http://www.pythontab.com/test/demo.zip' 
f = urllib2.urlopen(url) 
data = f.read() 
with open("demo2.zip", "wb") as code:     
    code.write(data)
```

### 通过requests模块下载
1. 安装
```shell
pip install requests
poetry add requests
```
2. 使用:
```py
# 下载文件
import requests 
print "downloading with requests"
url = 'http://www.pythontab.com/test/demo.zip' 
r = requests.get(url) 
with open("demo3.zip", "wb") as code:
        code.write(r.content)
# 下载图片
import requests
image_url = "https://www.python.org/static/community_logos/python-logo-master-v3-TM.png"
r = requests.get(image_url) 
with open("python_logo.png",'wb') as f:
    f.write(r.content)
# 大文件下载
# 原因:如果文件比较大的话，那么下载下来的文件先放在内存中，内存还是比较有压力的。所以为了防止内存不够用的现象出现，我们要想办法把下载的文件分块写到磁盘中
import requests
file_url = "http://codex.cs.yale.edu/avi/db-book/db4/slide-dir/ch1-2.pdf"
r = requests.get(file_url, stream=True)
with open("python.pdf", "wb") as pdf:
    for chunk in r.iter_content(chunk_size=1024):
        if chunk:
            pdf.write(chunk)

# 批量文件下载
import requests
from bs4 import BeautifulSoup
archive_url = "http://www-personal.umich.edu/~csev/books/py4inf/media/"

def get_video_links():
    r = requests.get(archive_url)
    soup = BeautifulSoup(r.content, 'html5lib')
    links = soup.findAll('a')
    video_links = [archive_url + link['href'] for link in links if link['href'].endswith('mp4')]
    return video_links

def download_video_series(video_links):
    for link in video_links:
        file_name = link.split('/')[-1]

        print("Downloading file:%s" % file_name)
        r = requests.get(link, stream=True)

        # download started
        with open(file_name, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024 * 1024):
                if chunk:
                    f.write(chunk)

        print("%s downloaded!\n" % file_name)


    print("All videos downloaded!")

    return


if __name__ == "__main__":
    video_links = get_video_links()
    download_video_series(video_links)
```
3. 报错解决方法:
```py
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# 通过requests库下载文件
url = 'https://www.gipsa.usda.gov/fgis/exportgrain/CY2016.csv'
r = requests.get(url,verify = False)
print(r.content)
with open("myCY2016.csv", "wb") as code:
    code.write(r.content)

```



报错原因: 
```shell
The problem you are having is caused by an untrusted SSL certificate.
Like @dirk mentioned in a previous comment, the *quickest* fix is setting verify=False
.
Please note that this will cause the certificate not to be verified. **This will expose your application to security risks, such as man-in-the-middle attacks.**
Of course, apply judgment. As mentioned in the comments, this *may* be acceptable for quick/throwaway applications/scripts, *but really should not go to production software*.
If just skipping the certificate check is not acceptable in your particular context, consider the following options, your best option is to set the verify
 parameter to a string that is the path of the .pem
 file of the certificate (which you should obtain by some sort of secure means).
So, as of version 2.0, the verify
 parameter accepts the following values, with their respective semantics:
True
: causes the certificate to validated against the library's own trusted certificate authorities (Note: you can see which Root Certificates Requests uses via the Certifi library, a trust database of RCs extracted from Requests: [Certifi - Trust Database for Humans](http://certifiio.readthedocs.org/en/latest/)).
False
: bypasses certificate validation *completely*.
Path to a CA_BUNDLE file for Requests to use to validate the certificates.

Source: [Requests - SSL Cert Verification](http://docs.python-requests.org/en/master/user/advanced/?highlight=ssl#ssl-cert-verification)
Also take a look at the cert
 parameter on the same link.

您遇到的问题是由不受信任的SSL证书引起的。
与前面的评论中提到的@dirk一样，*quickest*修复方法是设置verify=False。
请注意，这将导致证书无法验证。**这将使您的应用程序面临安全风险，例如中间人攻击。**当然，请应用判断。正如在评论中提到的，对于快速/一次性的应用程序/脚本来说，这是可以接受的，但实际上不应该用于生产软件。
如果仅跳过证书检查在您的特定上下文中是不可接受的，请考虑以下选项，您最好的选择是将verify参数设置为一个字符串，该字符串是证书的.pem文件的路径（您应该通过某种安全方式获得该文件）。
因此，从版本2.0开始，verify参数接受以下值及其各自的语义：True:根据库自己的可信证书颁发机构验证证书（注意：您可以通过Certifi库（从请求中提取的RCs的信任数据库）查看请求使用的根证书：[Certifi-人类信任数据库](http://certifiio.readthedocs.org/en/latest/)).
False:完全绕过证书验证*。
用于验证证书的请求的CA\ U捆绑包文件的路径。
来源：[请求-SSL证书验证](http://docs.python-requests.org/en/master/user/advanced/？highlight=ssl#ssl cert verification）还可以查看同一链接上的cert参数。
```
[(stackoverflow)](https://link.jianshu.com/?t=http://stackoverflow.com/questions/10667960/python-requests-throwing-up-sslerror)







## 其他
>一个多线程下载示例
```py
import math
import random
import threading

import requests
from clint.textui import progress

# You must define a proxy list
# I suggests https://free-proxy-list.net/
proxies = {
    0: {'http': 'http://34.208.47.183:80'},
    1: {'http': 'http://40.69.191.149:3128'},
    2: {'http': 'http://104.154.205.214:1080'},
    3: {'http': 'http://52.11.190.64:3128'}
}


# you must define the list for files do you want download
videos = [
    "https://i.stack.imgur.com/g2BHi.jpg",
    "https://i.stack.imgur.com/NURaP.jpg"
]

downloaderses = list()


def downloaders(video, selected_proxy):
    print("Downloading file named {} by proxy {}...".format(video, selected_proxy))
    r = requests.get(video, stream=True, proxies=selected_proxy)
    nombre_video = video.split("/")[3]
    with open(nombre_video, 'wb') as f:
        total_length = int(r.headers.get('content-length'))
        for chunk in progress.bar(r.iter_content(chunk_size=1024), expected_size=(total_length / 1024) + 1):
            if chunk:
                f.write(chunk)
                f.flush()


for video in videos:
    selected_proxy = proxies[math.floor(random.random() * len(proxies))]
    t = threading.Thread(target=downloaders, args=(video, selected_proxy))
    downloaderses.append(t)

for _downloaders in downloaderses:
    _downloaders.start()
```

## 参考文章
* [使用Requests下载一个文件 - 简书](https://www.jianshu.com/p/8d4a2a387ff9)
* [python下载文件的三种方法 - 简书](https://www.jianshu.com/p/e137b03a1cd2)
* [python下载文件----requests - 知乎](https://zhuanlan.zhihu.com/p/37824910)
