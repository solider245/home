## git常见问题与解决方法

## git：到底什么是 fast-forwards ?
>这个问题常见于目前仓库和远程仓库没有共同源头，所以推送时候报错

解决方法一：
```shell
git rebase origin/master #等于先拉取合并
git push -u origin master #这样再推送集就基本没问题了
```
## github同步远程上游仓库'upstream'方法
当我们从一个远程上游仓库fork之后，发现无法同远程上游仓库同步，原因在于我们没有添加远程仓库的的upstream。
### 方法一
```shell
cd <仓库地址>
git remote -v #  查看仓库远程地址 
git remote add upstream <url># 追加上游代码仓库
git remote -v # 重新查看，出现upstream表示成功
git status #检查本地是否有未提交的修改
git add -A 或者 git add filename
git commit -m "your note"
git push origin master
git status
git fetch upstream #抓取远程仓库更新，如果没有更新则什么都不显示
git checkout master #切换到 master 分支
git merge upstream/master #合并远程的master分支
```
### 方法二
从主库中提个mr到的fork仓库

比如：

[![image](https://user-images.githubusercontent.com/18069202/91786222-b14b1400-ec39-11ea-8dba-2de739765be7.png)](https://user-images.githubusercontent.com/18069202/91786222-b14b1400-ec39-11ea-8dba-2de739765be7.png)

注意左边是的仓库，然后create pull request。创建了完了以后，在你的仓库点击merge。

[![image](https://user-images.githubusercontent.com/18069202/91786302-e8b9c080-ec39-11ea-9923-e7d3c867c285.png)](https://user-images.githubusercontent.com/18069202/91786302-e8b9c080-ec39-11ea-9923-e7d3c867c285.png)

链接：[JornShen/kubernetes#2](https://github.com/JornShen/kubernetes/pull/2)

然后你就可以直接用的你自己的仓库了。
## `github pull request`的方法
当你在你fork的仓库修改好了文件之后，先提交到你的远程仓库，然后到你的远程上游仓库创建一个pull request即可。

## git subtree 命令
```shell 
# 在父仓库中新增子仓库
git subtree add --prefix=sub/libpng https://github.com/test/libpng.git master --squash
# --squash 参数表示不拉取历史信息，只生成一条commit信息
# --prefix=sub/libpng 会创建一个sub/libpng目录
git remote add -f libpng https://github.com/test/libpng.git 
git subtree add --prefix=sub/libpng libpng master --squash 
git subtree pull --prefix=sub/libpng libpng master --squash 
git subtree push --prefix=sub/libpng libpng master
```

## 参考链接
[Github进行fork后如何与原仓库同步：重新fork很省事，但不如反复练习版本合并 · Issue #67 · selfteaching/the-craft-of-selfteaching](https://github.com/selfteaching/the-craft-of-selfteaching/issues/67)
