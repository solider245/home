# 常见管理工具汇总

1. [dotbot](https://github.com/anishathalye/dotbot)
2. yadm
3. [GNU Stow](https://github.com/aspiers/stow#)
4. [dotdrop](https://github.com/deadc0de6/dotdrop)

<!-- more -->
## 参考文章

* [【譯】使用 GNU stow 管理你的點文件 - Farseerfc的小窩](https://farseerfc.me/using-gnu-stow-to-manage-your-dotfiles.html)
* [TheLocehiliosan / yadm：另一个Dotfiles管理员](https://github.com/TheLocehiliosan/yadm)
* [aspiers / stow：GNU Stow-稀树草原git仓库的镜像，偶尔会有更多的出血边缘分支](https://github.com/aspiers/stow#)
* [anishathalye / dotbot：引导您的点文件的工具️](https://github.com/anishathalye/dotbot)
* [bash-如何使用Dotbot添加新的点文件？ - 堆栈溢出](https://stackoverflow.com/questions/61091881/how-do-i-add-a-new-dotfile-with-dotbot)
* [使用dotbot引导您的Dotfiles-Elliot DeNolf](https://www.elliotdenolf.com/posts/bootstrap-your-dotfiles-with-dotbot)