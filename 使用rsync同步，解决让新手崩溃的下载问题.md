## 序言
linux新手开发者通常会遇到这样的问题，就是远程服务器和本地服务器下载和上传问题。
每次上传和下载，都会让自己临近崩溃。这里，我们可以使用rsync，简单构建本地服务器和远程服务器的同步文件夹，变相解决文件同步问题。

<!-- more -->

## 使用方法

假设我们有两台服务器，分别是A和B。A,B的地址如下：
* A ：192.168.1.1  无公网
* B ：200.200.2.2  有公网

现在，我们要在A,B之间进行同步，最简单的方法就是使用rsync。
1. 首先，我们在公网服务器B创建一个share文件夹。
```shell
mkdir share && cd share # 创建目录并进入
pwd # 查看当前目录
echo "这是一个自述文档">> README.md # 创建一个自述文档,并写入内容
```
2. 本地服务器创建一个share同步文件夹
```sh 
mkdir share 
```
3. 同步本地文件夹到远程公网服务器B 
```sh
rsync -avzP /home/solider/share/ ubuntu@200.200.2.2:/home/ubuntu/share/
```
4. 同步远程公网服务器文件夹到本地
```
rsync -avzP ubuntu@200.200.2.2:/home/ubuntu/share/ /home/solider/share/
```
参数解析：
```
-v :展示详细的同步信息
-a :归档模式，相当于 -rlptgoD
    -r :递归目录
    -l :同步软连接文件
    -p :保留权限
    -t :将源文件的"modify time"同步到目标机器
    -g :保持文件属组
    -o :保持文件属主
    -D :和--devices --specials 一样，保持设备文件和特殊文件
-z :发送数据前，先压缩再传输
-H :保持硬链接
-n :进行试运行，不作任何更改
-P same as --partial --progress
    --partial :支持断点续传
    --progress :展示传输的进度
--delete :如果源文件消失，目标文件也会被删除
--delete-excluded :指定要在目的端删除的文件
--delete-after :默认情况下，rsync 是先清理目的端的文件再开始数据同步；如果使用此选项，则 rsync
会先进行数据同步，都完成后再删除那些需要清理的文件。
--exclude=PATTERN :排除匹配 PATTERN 的文件
--exclude-from=FILE :如果要排除的文件很多，可以统一写在某一文件中
-e ssh :使用 SSH 加密隧道传输
```

## 优化使用
### 使用alias简化操作
如果每次都打这么多操作的话，一个是容易忘记，二一个是容易输错命令，所以可以用alias来简化
```shell
alias rsget='rsync -avzP ubuntu@200.200.2.2:/home/ubuntu/share/ /home/solider/share/'
alias rspush='rsync -avzP /home/solider/share/ ubuntu@200.200.2.2:/home/ubuntu/share/'
```
在命令行输入以上两行命令，这样的话，以后就不用每次输入这么长的命令了。
要使alias长期生效，那么你可以把上面内容写入你的`~/.bashrc`文件中。这样就不用每次输入了。