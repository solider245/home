## 序言

typer是fastapi的作者的作品,刚出来没多久,但是简略的看了下,发现似乎很受大家欢迎.
刚好自己在写命令行软件,所以就用这个框架试试咯.

这个框架是基于click制作的,所以在使用上和click有点类似.其他的问题暂时还没有发现.

以下是官网给出的资料:
>**文档** ： [https](https://typer.tiangolo.com) : [//typer.tiangolo.>com](https://typer.tiangolo.com)
>
>**源代码** ： [https](https://github.com/tiangolo/typer) : [//github.>com/tiangolo/typer](https://github.com/tiangolo/typer)
>
>---
>
>Typer是一个用于构建 CLI 应用程序 的库 ，用户将 **喜欢使用它，** 而开发人员>将 **喜欢创建** 。 基于Python 3.6+类型提示。
>
>主要功能是：
>
>*   **直观的编写** ：强大的编辑器支持。 完成 无处不在。 调试时间更少。 设计>易于使用和学习。 减少阅读文档的时间。
>*   **易于使用** ：最终用户易于使用。 自动帮助和所有外壳的自动完成。
>*   **短** ：最小化代码重复。 每个参数声明中的多个功能。 更少的错误。
>*   **从简单开始** ：最简单的示例仅向您的应用添加两行代码： **1 import，1 >function call** 。
>*   **增大** ：尽可能多地增加复杂性，创建带有选项和参数的任意复杂的命令树和子命令组。

## 最简单的例子

### 安装
```sh
pip install typer 
poetry add typer

```
### 起手代码
```Python
import typer


def main(name: str):
    typer.echo(f"Hello {name}")


if __name__ == "__main__":
    typer.run(main)
```

这个例子创建了一个最简单的命令行应用.可以使用进行测试.
```
python main.py <name>
```

### 拥有两个命令的示例
```python
import typer

app = typer.Typer()


@app.command()
def hello(name: str):
    typer.echo(f"Hello {name}")


@app.command()
def goodbye(name: str, formal: bool = False):
    if formal:
        typer.echo(f"Goodbye Ms. {name}. Have a good day.")
    else:
        typer.echo(f"Bye {name}!")


if __name__ == "__main__":
    app()
```

>*   明确创建一个 `typer.Typer` 应用。
>    *   前一个 `typer.run` 实际上为您隐式创建了一个。
>*   用 `@app.command()`添加两个子命令 。
>*   执行 `app()` 本身，就好像它是一个函数（而不是 `typer.run` ）一样。

![20210108064932_3f33cb31c54e560789c48e1d0b105464.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210108064932_3f33cb31c54e560789c48e1d0b105464.png)
执行代码后如上图所示.

创建app之后,会自动创建帮助命令.同时增加了goodbye与hello两个命令.

