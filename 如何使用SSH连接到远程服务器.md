### 介绍

SSH是系统管理员必须掌握的基本工具。

SSH或 *Secure Shell* 是用于安全登录到远程系统的协议。 这是访问远程Linux服务器的最常用方法。

在本指南中，我们将讨论如何使用SSH连接到远程系统。

## 基本语法

要使用SSH连接到远程系统，我们将使用 `ssh` 命令。 该命令的最基本形式为：

```bash
ssh remote_host

```

复制

在 `remote_host` 本例中为您试图连接到IP地址或域名。

该命令假定您在远程系统上的用户名与本地系统上的用户名相同。

如果您的用户名在远程系统上不同，则可以使用以下语法来指定它：

```bash
ssh remote_username@remote_host

```

复制

连接到服务器后，可能会要求您通过提供密码来验证您的身份。 稍后，我们将介绍如何生成要使用的密钥而不是密码。

要退出ssh会话并返回到本地Shell会话，请输入：

```bash
exit

```

复制

## SSH如何工作？

SSH通过将客户端程序连接到 名为 的 **ssh服务器来工作** `sshd` 。

在上一节中， `ssh` 是客户端程序。 在 *SSH服务器* 上已经运行 `remote_host` 我们指定的。

在您的服务器上，该 `sshd` 服务器应该已经在运行。 如果不是这种情况，则可能需要通过基于Web的控制台或本地串行控制台访问服务器。

启动ssh服务器所需的过程取决于您所使用的Linux发行版。

在Ubuntu上，您可以通过键入以下命令来启动ssh服务器：

```bash
sudo systemctl start ssh

```

复制

那应该启动sshd服务器，然后您可以远程登录。

## 如何配置SSH

更改SSH的配置时，将更改sshd服务器的设置。

在Ubuntu中，主sshd配置文件位于 `/etc/ssh/sshd_config` 。

编辑之前，请备份此文件的当前版本：

```bash
sudo cp /etc/ssh/sshd_config{,.bak}

```

复制

用文本编辑器打开它：

```bash
sudo nano /etc/ssh/sshd_config

```

复制

您将只想在此文件中保留大多数选项。 但是，您可能需要看一些：

/ etc / ssh / sshd\_config

```bash
Port 22
```

复制

端口声明指定sshd服务器将在哪个端口上监听连接。 默认情况下是 `22` 。 除非有特殊原因，否则您可能应该单独保留此设置。 如果您 *确实要* 更改端口，则稍后我们将向您展示如何连接到新端口。

/ etc / ssh / sshd\_config

```bash
HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_dsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
```

复制

主机密钥声明指定在哪里查找全局主机密钥。 稍后我们将讨论什么是主机密钥。

/ etc / ssh / sshd\_config

```bash
SyslogFacility AUTH
LogLevel INFO
```

复制

这两个项目指示应该发生的日志记录级别。

如果您在使用SSH时遇到困难，那么增加日志记录数量可能是发现问题所在的好方法。

/ etc / ssh / sshd\_config

```bash
LoginGraceTime 120
PermitRootLogin yes
StrictModes yes
```

复制

这些参数指定一些登录信息。

`LoginGraceTime` 指定在不成功登录的情况下保持连接存活的秒数。

最好将此时间设置为比正常登录所花费的时间高一点。

`PermitRootLogin` 选择是否 允许 **root** 用户登录。

在大多数情况下，应将其更改为 `no` 创建具有访问权限（通过 `su` 或 `sudo` ）并可以通过ssh登录 的用户帐户时 。

`strictModes` 是一个安全防护员，如果每个人都可以读取身份验证文件，它将拒绝登录尝试。

当配置文件不安全时，这可以防止尝试登录。

/ etc / ssh / sshd\_config

```bash
X11Forwarding yes
X11DisplayOffset 10
```

复制

这些参数配置了称为 *X11转发的功能* 。 这使您可以在本地系统上查看远程系统的图形用户界面（GUI）。

必须在服务器上启用此选项，并在与该 `-X` 选项 连接期间将其与SSH客户端一起 提供。

进行更改后，通过键入 `CTRL+X` 和 `Y` ，然后按 来保存并关闭文件 `ENTER` 。

如果您在中更改了任何设置 `/etc/ssh/sshd_config` ，请确保您重新加载sshd服务器以实现您的修改：

```bash
sudo systemctl reload ssh

```

复制

您应该彻底测试您的更改，以确保它们以您期望的方式运行。

进行更改时，最好使几个会话处于活动状态。 如有必要，这将允许您还原配置。

## 如何使用密钥登录SSH

能够使用密码登录到远程系统虽然很有帮助，但是设置 *基于密钥的身份验证* 是一个更好的主意 。

### 基于密钥的身份验证如何工作？

基于密钥的身份验证通过创建一对密钥进行工作： *私钥* 和 *公钥* 。

该 *私钥* 位于客户机上，并固定和保密。

该 *公钥* 可以给任何人或放置您要访问的任何服务器上。

当您尝试使用密钥对进行连接时，服务器将使用公共密钥为客户端计算机创建一条消息，该消息只能用私有密钥读取。

然后，客户端计算机将适当的响应发送回服务器，服务器将知道客户端是合法的。

设置密钥后，整个过程将自动完成。

### 如何创建SSH密钥

SSH密钥必须要登录的计算机上生成 *的* 。 这通常是您的本地计算机。

在命令行中输入以下内容：

```bash
ssh-keygen -t rsa

```

复制

按Enter接受默认值。 您的密钥将在 *〜/ .ssh / id\_rsa.pub* 和 *〜/ .ssh / id\_rsa中创建* 。

`.ssh` 通过键入以下内容 进入 目录：

```bash
cd ~/.ssh

```

复制

查看文件的权限：

```bash
ls -l

```

复制

```
Output-rw-r--r-- 1 demo demo  807 Sep  9 22:15 authorized_keys
-rw------- 1 demo demo 1679 Sep  9 23:13 id_rsa
-rw-r--r-- 1 demo demo  396 Sep  9 23:13 id_rsa.pub

```

如您所见，该 `id_rsa` 文件仅对所有者可读和可写。 这就是将其保密的方式。

`id_rsa.pub` 但是， 该 文件可以共享，并具有适合此活动的权限。

### 如何将公钥传输到服务器

如果当前对服务器具有基于密码的访问权限，则可以通过发出以下命令将公用密钥复制到该服务器：

```bash
ssh-copy-id remote_host

```

复制

这将启动SSH会话。 输入密码后，它将把您的公共密钥复制到服务器的授权密钥文件中，这将使您下次无需密码即可登录。

## 客户端选项

通过SSH连接时，可以选择许多可选标志。

为了与远程主机的 `sshd` 配置 中的设置匹配，可能需要其中一些 。

例如，如果您在 `sshd` 配置中 更改了端口号 ，则需要通过键入以下内容在客户端上匹配该端口：

```bash
ssh -p port_number remote_host

```

复制

如果只希望在远程系统上执行单个命令，则可以在主机后指定它，如下所示：

```bash
ssh remote_host command_to_run

```

复制

您将连接到远程计算机，进行身份验证，然后将执行命令。

如前所述，如果在两台计算机上都启用了X11转发，则可以通过键入以下内容来访问该功能：

```bash
ssh -X remote_host

```

复制

如果您在计算机上拥有适当的工具，则在远程系统上使用的GUI程序现在将在本地系统上打开其窗口。

## 禁用密码认证

如果创建了SSH密钥，则可以通过禁用仅密码身份验证来增强服务器的安全性。 除控制台外，登录服务器的唯一方法是通过与服务器上已安装的公钥配对的私钥。

**警告：**在继续此步骤之前，请确保已为服务器安装了公共密钥。否则，您将被锁定！

作为 **根** 或用户与须藤权限，打开 `sshd` 配置文件：

```bash
sudo nano /etc/ssh/sshd_config

```

复制

找到读取的行 `Password Authentication` ，并通过删除开头取消注释 `#` 。 然后，您可以将其值更改为 `no` ：

/ etc / ssh / sshd\_config

```bash
PasswordAuthentication no
```

复制

不应该需要修改两个设置（前提是你没有修改之前本文件）是 `PubkeyAuthentication` 和 `ChallengeResponseAuthentication` 。 它们是默认设置的，应如下所示：

/ etc / ssh / sshd\_config

```bash
PubkeyAuthentication yes
ChallengeResponseAuthentication no
```

复制

进行更改后，保存并关闭文件。

现在，您可以重新加载SSH守护程序：

```bash
sudo systemctl reload ssh

```

复制

现在应该禁用密码身份验证，并且只能通过SSH密钥身份验证来访问服务器。

## 结论

学习SSH的方法是值得的，即使仅仅是因为这是一种常见的活动。

使用各种选项时，您会发现更多高级功能，可以使您的生活更轻松。 SSH一直很受欢迎，因为它安全，轻便并且在各种情况下都有用。