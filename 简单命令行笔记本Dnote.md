# Dnote
Dnote是面向程序员的简单命令行笔记本。

通过提供一种**无需离开终端即可** 毫不费力地捕获和检索信息的方法， 可以 **使您集中精力** 。 它还提供了无缝的**多设备同步** 和**Web界面** 。

[![Dnote命令行界面演示](https://github.com/dnote/dnote/raw/master/assets/cli.gif "Dnote命令行界面")](https://github.com/dnote/dnote/blob/master/assets/cli.gif)
<!-- more -->
## 安装
在macOS上，可以使用Homebrew进行安装：

```shell
brew tap dnote/dnote
brew install dnote
```

在Linux或macOS上，可以使用安装脚本：

```
curl -s https://www.getdnote.com/install | sh

```

否则，您可以从 [发布页面](https://github.com/dnote/dnote/releases) 手动为您的平台下载二进制文件 。
注意：
>Dnote分为命令行cli版本与server服务器版本，下载的时候找准。

## 用法
基本上官方的的动图已经把所有的用法都写好了，我这里就说说主要注意的点：

默认创建的文件格式是Markdown格式。
配置文件存放在`~/.dnote/dnoterc`。这是一个`Yaml`文件，修改默认编辑器修改后面的editor即可，默认是vi
所有的内容存放在`~/.dnote/dnote.db`中。好处是方便程序化，坏处是不太适合新人。

一般流程

1. `dnote add python`  
   >创建一个Python文件夹，然后编辑器会默认打开一个文件，开头输入#标题内容，会根据标题内容生成一篇文章，其他内容自己填写
2. `dnote add python`
   >后续有python相关的内容全部用这个命令加入其中
3. `dnote view`
   >先查看当前有多少个笔记本（文件夹）
4. `dnote view python`
   >查看python笔记本下面的笔记，会列出所有的文章，每篇文章前面有一个序号，想要查看只要加上序号即可。
5. `dnote view 6`
   >查看序号为6的内容         
这里基本上就是dnote cli的主要功能了。dnote find我试了下，发现总是不成功。

如下图所示：

![20201110105733_de61188a73181be55e9b716f180b5ad9.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20201110105733_de61188a73181be55e9b716f180b5ad9.png)

如果你对数据库有点了解的话，详细你会很快理解。

## dnote server

dnote最强之处在于可以自己架设服务器，如果不能架设服务器的话，那dnote相比其他记事本软件似乎并没有太强。
下面是dnote的官方安装文档。
本指南记录了在您自己的计算机上安装Dnote服务器的步骤。 如果您喜欢Docker，请参阅 [Docker指南](https://github.com/dnote/dnote/blob/master/host/docker/README.md) 。

## `[](#overview)Overview`

Dnote服务器作为单个二进制文件出现，您可以直接下载并运行。 它使用Postgres作为数据库。

## `[](#installation)Installation`

1.  安装Postgres 11+。
2.  `dnote` 通过运行 创建 数据库 `createdb dnote`
3.  从 [发布页面](https://github.com/dnote/dnote/releases) 下载官方的Dnote服务器发布 。
4.  解压缩档案并将 `dnote-server` 可执行文件移至 `/usr/local/bin` 。

```shell
tar -xzf dnote-server-$version-$os.tar.gz
mv ./dnote-server /usr/local/bin
```

4.  运行Dnote

```shell
GO_ENV=PRODUCTION \
OnPremise=true \
DBHost=localhost \
DBPort=5432 \
DBName=dnote \
DBUser=$user \
DBPassword=$password \
WebURL=$webURL \
SmtpHost=$SmtpHost \
SmtpPort=$SmtpPort \
SmtpUsername=$SmtpUsername \
SmtpPassword=$SmtpPassword \
DisableRegistration=false
  dnote-server start
```

替换 `$user` ， `$password` 与拥有Postgres的用户的凭据 `dnote` 数据库。

替换 `$webURL` 为服务器的完整URL，不带斜杠（例如 `https://your.server` ）。

更换 `$SmtpHost` ， `SmtpPort` ， `$SmtpUsername` ， `$SmtpPassword` 与实际值，如果你想通过电子邮件接收间隔重复。

如果要禁用用户注册，请 替换 `DisableRegistration` 为 `true` 。

默认情况下，dnote服务器将在端口3000上运行。

## `[](#configuration)Configuration`

至此，Dnote在您的计算机中已完全正常运行。 API，前端应用程序和后台任务都在单个二进制文件中。 让我们再采取一些步骤来配置Dnote。

### `[](#configure-nginx)Configure Nginx`

要使其可以从Internet访问，您需要配置Nginx。

1.  安装nginx。
2.  `/etc/nginx/sites-enabled/dnote` 使用以下内容 创建一个新文件 ：

```
server {
	server_name my-dnote-server.com;

	location / {
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $remote_addr;
		proxy_set_header Host $host;
		proxy_pass http://127.0.0.1:3000;
	}
}

```

3.  替换 `my-dnote-server.com` 为服务器的URL。
4.  通过运行以下命令重新加载nginx配置：

```
sudo service nginx reload

```

### `[](#configure-apache2)Configure Apache2`

1.  安装Apache2并安装/启用mod\_proxy。
2.  `/etc/apache2/sites-available/dnote.conf` 使用以下内容 创建一个新文件 ：

```
<VirtualHost *:80>
    ServerName notes.example.com

    ProxyRequests Off
    ProxyPreserveHost On
    ProxyPass / http://127.0.0.1:3000/ keepalive=On
    ProxyPassReverse / http://127.0.0.1:3000/
    RequestHeader set X-Forwarded-HTTPS "0"
</VirtualHost>

```

3.  通过运行以下命令启用dnote站点并重新启动Apache2服务：

```
a2ensite dnote
sudo service apache2 restart

```

现在，您可以访问上的Dnote前端应用程序 `/` 和上的API `/api` 。

### `[](#configure-tls-by-using-letsencrypt)Configure TLS by using LetsEncrypt`

建议使用HTTPS。 使用LetsEncrypt获取证书并在Nginx中配置TLS。

在Dnote Server的未来版本中，将始终需要HTTPS。

### `[](#run-dnote-as-a-daemon)Run Dnote As a Daemon`

我们可以使用 `systemd` Dnote作为后台程序在后台运行，并在系统重启时自动启动它。

1.  `/etc/systemd/system/dnote.service` 使用以下内容 创建一个新文件 ：

```
[Unit]
Description=Starts the dnote server
Requires=network.target
After=network.target

[Service]
Type=simple
User=$user
Restart=always
RestartSec=3
WorkingDirectory=/home/$user
ExecStart=/usr/local/bin/dnote-server start
Environment=GO_ENV=PRODUCTION
Environment=OnPremise=true
Environment=DBHost=localhost
Environment=DBPort=5432
Environment=DBName=dnote
Environment=DBUser=$DBUser
Environment=DBPassword=$DBPassword
Environment=DBSkipSSL=true
Environment=WebURL=$WebURL
Environment=SmtpHost=
Environment=SmtpPort=
Environment=SmtpUsername=
Environment=SmtpPassword=

[Install]
WantedBy=multi-user.target

```

更换 `$user` ， `$WebURL` ， `$DBUser` ，和 `$DBPassword` 值与实际值。

或者，如果你想发送间隔重复throught电子邮件，填入 `SmtpHost` ， `SmtpPort` ， `SmtpUsername` ，和 `SmtpPassword` 。

2.  通过运行重新加载更改 `sudo systemctl daemon-reload` 。
3.  通过运行启用守护 `sudo systemctl enable dnote` .\`
4.  通过运行启动守护程序 `sudo systemctl start dnote`

### `[](#configure-clients)Configure clients`

让我们配置Dnote客户端以连接到自托管的Web API端点。

#### `[](#cli)CLI`

我们需要修改CLI的配置文件。 它应该是在 `~/.dnote/dnoterc` 第一次运行CLI 时 在 生成的 。

以下是示例配置：

```yaml
editor: nvim
apiEndpoint: https://api.getdnote.com
```

只需将for的值更改为 `apiEndpoint` 自托管实例的完整URL，后跟“ / api”，然后保存配置文件。

例如

```yaml
editor: nvim
apiEndpoint: my-dnote-server.com/api
```

#### `[](#browser-extension)Browser extension`

导航到“设置”标签，然后设置“ API URL”和“ Web URL”的值。

## 总结
如果不架设服务器的话，并不是很好用，如果会架设服务器的话，作为一个简单的群体协作记事本或许会不错。