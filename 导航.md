## 序言

把一些常用工具放在这里导航会比较好

## 博客
[tankywoo / simiki：Simiki是一个简单的Wiki框架，使用Python编写。](https://github.com/tankywoo/simiki)
>使用非常简单,主题一目了然,或许比较适合个人整理资料,但是阅读上明显没有vuepress好.
[佐拉](https://www.getzola.org/)
> rust构建的博客.号称一秒内完成构造.
## 教程
### go语言教程
[inancgumus / learngo：1000多种手工制作的Go示例，练习和测验](https://github.com/inancgumus/learngo?utm_campaign=explore-email&utm_medium=email&utm_source=newsletter&utm_term=daily)

## 备份工具
[11 年后重新打造的 rdiff-backup 2.0 有什么新功能？ | Linux 中国 - 知乎](https://zhuanlan.zhihu.com/p/260317428)

## 评论工具
[快速开始 | Waline](https://waline.js.org/quick-start.html#%E8%8E%B7%E5%8F%96app-id-%E5%92%8C-app-key)
>可以认为是valine,有免费版本和收费版本,普通版本免费即可

## docker 与k8s相关
[wagoodman / dive：用于探索docker映像中每个图层的工具](https://github.com/wagoodman/dive)
![](https://github.com/wagoodman/dive/raw/master/.data/demo.gif)
[CodersOS / linux-iso-creator：从安装脚本创建实时分发](https://github.com/CodersOS/linux-iso-creator)
>使用docker创建cd,iso文件.
## 运维
### 测试服务器速度脚本
```shell
bash <(curl -s https://raw.githubusercontent.com/sedgwickz/chores/main/scripts/ping.sh)
```
### 对象存储与文件服务器
[juicedata / juicefs：JuiceFS是基于Redis和S3构建的分布式POSIX文件系统。](https://github.com/juicedata/juicefs)

[juicedata / juicesync：一种在任何云或区域之间移动数据的工具。](https://github.com/juicedata/juicesync)

### 负载均衡
[概念-Traefik](https://doc.traefik.io/traefik/getting-started/concepts/)
![](https://doc.traefik.io/traefik/assets/img/traefik-concepts-2.png)
>和云更契合的nginx,而且更简便

一键测速主流服务器的速度
## 部署工具
[无人驾驶飞机/无人驾驶飞机：无人驾驶飞机是本地容器的持续交付平台](https://github.com/drone/drone)
Jenkins 是目前最常用的持续集成工具，拥有近 50% 的市场份额，它还是很多技术团队的第一个使用的自动化工具。但是随着自动化领域的持续发展，Jenkins 逐渐暴露出了一些问题，例如缺乏功能、维护问题、依赖关系和扩展问题等等。

本文将为大家介绍几个持续集成中常用的 Jenkins 替代方案。


### **1\. BuildMaster**

![](https://pic2.zhimg.com/v2-3ef7e97f650ada839fdca202cc70412d_b.jpg)

项目地址：[https://inedo.com/buildmasterInedo](https://link.zhihu.com/?target=https%3A//inedo.com/buildmasterInedo) 的 BuildMaster 是 Jenkins 替代方案之一，开发人员能够用它将软件发布到各种环境，为各种平台提供全面的持续集成能力，使团队有能力创建私有的自助发布管理平台，单独处理自己的应用程序并私有部署。更重要的是，避免自动发布未经测试的软件。因为无需精通流水线即可使用，所以用户对它的简洁性都非常满意。

### **2\. Microtica**

![](https://pic3.zhimg.com/v2-15b26d1ce9a9d4d91706d0241589deae_b.jpg)

项目地址：[https://microtica.com/Microtica](https://link.zhihu.com/?target=https%3A//microtica.com/Microtica) 是 DevOps 自动化工具，从创建云基础设施到使用 Kubernetes 交付应用程序和服务，覆盖了整个软件交付过程。Microtica 的开箱即用组件为用户提供可重用的代码片段，无需额外编码即可帮你在几分钟内搭建起底层架构。通过微服务生成器，开发人员可以自动化地创建微服务。通过已集成的预上线 Kubernetes 和本地 Kubernetes 仪表板，只要点一点鼠标就能创建出可伸缩的应用程序。Microtica 流水线定义每个组件和微服务的工作流。用户可以随时自动或手动触发它们，获取整个构建的概览。用户可以在 Microtica 网站内执行所有的操作，每次变更都有 Slack 通知。最后一点，Microtica 允许开发人员设置自动化的休眠周期，降低 AWS 成本。一旦启动节约模式，Microtica 会自动运行，防止过度消费。而且，节省了多少钱还可在成本仪表板中看到。

### **3\. GitLab**

![](https://pic2.zhimg.com/v2-f7ad85b56ee63042bd501677513955e5_b.jpg)

项目地址：[https://about.gitlab.com/GitLab](https://link.zhihu.com/?target=https%3A//about.gitlab.com/GitLab) 是在线 CI 平台，开发团队可以有效地使用各种开发工具，更快、更安全。通过集中统一的版本控制系统进行规划、构建和管理代码。此外，GitLab 使用户可以使用 Docker 和 Kubernetes 来处理构建输出、容器、应用打包和依赖项。有人表示 GitLab 很容易集成。但是，它有时会有一些令人讨厌的 bug 和限制，也缺少一些完全自动化的特性。

### **4\. CircleCI**

![](https://pic3.zhimg.com/v2-f8a12279ae1be35d481ef5a19c8e7166_b.jpg)

项目地址：[https://circleci.com/CircleCI](https://link.zhihu.com/?target=https%3A//circleci.com/CircleCI) 是一种可伸缩的 Jenkins 替代方案，它可以在任何环境（如 Python 接口服务或 Docker 集群）中运行。它消除了不稳定性并增强了应用程序的一致性。它支持多种语言，比如 C++、.NET、JavaScript、PHP、Ruby 和 Python。当最近的构建触发后，可自动取消队列中以及正在构建的任务。它可以与 GitHub、GitHub 企业版和 Bitbucket 集成。TrustRadius 用户说，自动构建是 CircleCI 的最大优势，但有时候任务太耗时。

### **5\. Bamboo**

![](https://pic1.zhimg.com/v2-e8cc52c79b7d080d83e3e3181a4dda18_b.jpg)

项目地址：[https://www.atlassian.com/software/bambooAtlassian](https://link.zhihu.com/?target=https%3A//www.atlassian.com/software/bambooAtlassian) 的 Bamboo 是持续集成服务，可以自动从一个地方创建、监听和发布应用。它与 JIRA 应用程序和 Bitbucket 集成很方便。此外，Bamboo 集成了 Docker、Git、SVN 和 Amazon S3 存储。基于对仓库中变更的检测，可触发构建并推送来自 Bitbucket 的通知。它既可托管，也可在本地使用。G2 用户 说，Bamboo 构建过程的可视化很棒，但是一些术语和集成还不太容易理解。

### **6\. TravisCI**

![](https://pic4.zhimg.com/v2-56d3d996ca598c1091e8b9ee5d10178f_b.jpg)

项目地址：[https://travis\-ci.org/TravisCI](https://link.zhihu.com/?target=https%3A//travis-ci.org/TravisCI) 是持续集成托管服务，开发人员可以使用它来开发和验证 GitHub 和 Bitbucket 托管的应用程序。它可以测试所有 pull 请求，以确保不会发布出去未测试过的代码。用户可以登录 GitHub 来创建项目，包括配置快速激活的预安装数据库和资源。有评论说，TravisCI 非常适合想要快速开始构建的小项目。然而，在意构建的依赖关系、性能和可靠性的大项目，可能会遇到一些问题。

### **7\. Semaphore**

![](https://pic2.zhimg.com/v2-dfc7f3fd8470e291af634914edfe87a5_b.jpg)

项目地址：[https://semaphoreci.com/productSemaphore](https://link.zhihu.com/?target=https%3A//semaphoreci.com/productSemaphore) 是 Jenkins 替代方案之一，它覆盖整个 CI/CD 过程，支持 GitHub、Kubernetes、iOS、Docker，并预装了 100 多个工具。它可以自动化任何持续交付流水线，并提供自定义步骤、并行执行、依赖管理等。有人表示，Semaphore 构建非常快速，而且操作简单。然而，有用户表示，界面有时会令人困惑，而且部署流水线的方法有限。

### **8\. Buddy**

![](https://pic1.zhimg.com/v2-b2e46df513efcb6cccfb0e3389141ea4_b.jpg)

项目地址：[https://buddy.works/Buddy](https://link.zhihu.com/?target=https%3A//buddy.works/Buddy) 是 CI/CD 平台，它通过简单的 UI/UX 来减少配置和维护 Jenkins 的工作量，这使得创建、评估和部署应用程序变得非常简单。您可以在 15 分钟内通过具有即时 YAML 导出功能的图形化界面完成配置。它可以在云端和本地使用，并提供完整的 Docker 和 Kubernetes 支持。有用户反馈，Buddy 很容易操作，但是价格太贵。

### **9\. [http://Drone.io](https://link.zhihu.com/?target=http%3A//Drone.io)**

![](https://pic2.zhimg.com/v2-75d3c6ce26d8620d63a557de4d60a921_b.jpg)

项目地址：[https://drone.io/Drone.io](https://link.zhihu.com/?target=https%3A//drone.io/Drone.io) 是自助 CD 平台，它使用简单的 YAML 配置文件和 Dockercompose 的超集在 Docker 容器中创建和执行流水线。运行时会自动下载独立的 Docker，它执行容器中的每个流水线步骤。[http://Drone.io](https://link.zhihu.com/?target=http%3A//Drone.io) 有 Docker 镜像，可以从 Dockerhub 下载。用户反馈，[http://Drone.io](https://link.zhihu.com/?target=http%3A//Drone.io) 是 Jenkins 替代品之一，易于操作，是很好的企业解决方案，但是缺少一些特性，需要进一步定制。

### **10\. GoCD**

![](https://pic4.zhimg.com/v2-3f9c990484ff9fccb6937908de8d0be3_b.png)

项目地址：[https://www.gocd.org/GoCD](https://link.zhihu.com/?target=https%3A//www.gocd.org/GoCD) 是 ThoughtWorks 的持续集成开源服务。您可以使用它来简化动态工作流的模拟和可视化。它提供持续交付和优雅的设计来构建 CD 流水线，支持并行和顺序执行，可以随时部署任何版本，有活跃的支持社区。用户反馈，GoCD 与跨服务器扩展不兼容，但优点是可以自定义流程。

### **11\. TeamCity**

![](https://pic1.zhimg.com/v2-4fa92e71c034c7a87b3214ae25d0e158_b.jpg)

项目地址：[https://www.jetbrains.com/teamcity/TeamCity](https://link.zhihu.com/?target=https%3A//www.jetbrains.com/teamcity/TeamCity) 是 JetBrains 的 CI/CD 工具。它允许用户在代码提交之前构建、监视和执行自动化测试，从而维护干净的代码库。它提供了全面的 VCS 集成，使 CI 服务器始终保持正常运行，即使没有任何构建。它可以与 Amazon EC2、Microsoft Azure 和 VMware vSphere 集成。用户反馈，TeamCity 是现代化的、健壮的和开放的解决方案，为流水线提供开发人员友好的环境，但是需要仔细对待服务配置。

### **12\. Buildkite**

![](https://pic1.zhimg.com/v2-3dc7ebfb9dcbf1a50064ecd15b98d6a8_b.jpg)

项目地址：[https://buildkite.com/Buildkite](https://link.zhihu.com/?target=https%3A//buildkite.com/Buildkite) 是开源平台，可以在上面运行 CI 流水线。它提供了源码控制、聊天支持，并且不需要访问源码。你可以将基础设施作为代码系统来进行调度，从而使你可以通过他们的网页平台监视和控制所有流水线。然而，该平台缺少一些 DevOps 流程，比如源码管理和安全测试。

### **13\. Zuul**

![](https://pic2.zhimg.com/v2-950e365e3cb08f1bd93be71ca4b445c9_b.jpg)

项目地址：[https://zuul\-ci.org/Zuul](https://link.zhihu.com/?target=https%3A//zuul-ci.org/Zuul) 是开源 CI 工具，主要解决 Jenkins 在 CI 测试中的问题，提供以最快的速度测试序列化的未来状态的能力。主要差异是，它可以测试多个仓库的代码，以确保如果某个变更破坏当前项目或其他项目，则不让该变更传递到生产环境中，称为 co\-gating。多年来，Zuul 已经成为自动合并、构建和测试项目变更的工具。对于企业用户来说，它是构建大量必须彼此同步工作的项目的理想选择。

### **14\. 结论**

很多开发团队仍在使用 Jenkins，然而它不再是唯一的 CI 工具。不断改进工作方式，会有多种方法让你更轻松、更快、更一致地完成工作。固守传统或忽视创新，将失去竞争优势。

## 前端
[tal-tech/electron-playground: This is a project to quickly experiment and learn electron related APIs](https://github.com/tal-tech/electron-playground)

[imgcook/imove: Move your mouse, generate code from flow chart](https://github.com/imgcook/imove?utm_campaign=explore-email&utm_medium=email&utm_source=newsletter&utm_term=daily)
>流程图代码生成器
![20210113140132_95eff032bcd0ec018010ed970774b6c2.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210113140132_95eff032bcd0ec018010ed970774b6c2.png)

## linux安卓手机系统
[Aid Learning](http://www.aidlearning.net/index.html)
![20201227223225_9cd21153d82f2cbeb673a66d75b9bb0e.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20201227223225_9cd21153d82f2cbeb673a66d75b9bb0e.png)

## vscode
### 常见插件
### 主题和外观：

*   原子一黑暗主题\-Mahmoud Ali

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--2Sf9ZXKo--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/zpqclqfiaz1m31y7myfu.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--2Sf9ZXKo--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/zpqclqfiaz1m31y7myfu.png)

*   阿玉\-Teabyii

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--DwiMJs8a--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/985g073iueoayev34vji.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--DwiMJs8a--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/985g073iueoayev34vji.png)

*   社区材料主题\-Mattia Astorino

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--ygjvYX6P--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/zhexk5bnearh5fw02s5p.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--ygjvYX6P--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/zhexk5bnearh5fw02s5p.png)

*   德古拉官方\-德古拉主题

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--Eegr5DI8--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/o7be4u0ekmnr1p7jyauq.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--Eegr5DI8--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/o7be4u0ekmnr1p7jyauq.png)

*   氦气图标主题\-Helgard Richard Ferreira

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--sedWKBgd--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/swxays19guwu7am5x11h.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--sedWKBgd--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/swxays19guwu7am5x11h.png)

*   材质图标主题\-Philipp Kief

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--IimDXIPj--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/08vzvvue5po43gdh2xbq.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--IimDXIPj--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/08vzvvue5po43gdh2xbq.png)

*   材质主题图标\-Mattia Astorino

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--tWSikccm--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/w07kb6ejvxuq4azjhor4.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--tWSikccm--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/w07kb6ejvxuq4azjhor4.png)

*   One Dark Pro\-二进制化

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--gEvPokVH--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/r9rmjt1y9dz1qm839pj3.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--gEvPokVH--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/r9rmjt1y9dz1qm839pj3.png)

*   一个Monokai主题\-Joshua Azemoh

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--4rcSflSA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/mdjr1usbiqto3lqgwqrc.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--4rcSflSA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/mdjr1usbiqto3lqgwqrc.png)

*   Palenight主题\-Olaolu Olawuyi

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--Ta_Yttq_--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/vpgm8ggmpblivvdiz1u5.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--Ta_Yttq_--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/vpgm8ggmpblivvdiz1u5.png)

*   熊猫主题\-熊猫主题（我的最爱之一！）

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--LmjgQOYv--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/7nveq4o8fd04e1qglavo.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--LmjgQOYv--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/7nveq4o8fd04e1qglavo.png)

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--p9RXtXuR--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/rkgf0dv9rgnl76li1g36.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--p9RXtXuR--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/rkgf0dv9rgnl76li1g36.png)

> 我的一个熊猫主题项目偷看

*   Synthwave'84\-Robb Owen

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--Bq2F1rdt--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/5vb7lzbn89su3j5kn924.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--Bq2F1rdt--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/5vb7lzbn89su3j5kn924.png)

*   vscode\-icons\-VSCode图标团队

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--srsB9Tmf--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/16rsok8xseo3jlf8d5yu.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--srsB9Tmf--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/16rsok8xseo3jlf8d5yu.png)

### [](#tools-and-productivity)工具和生产力：

### [](#auto-close-tag-jun-han)自动关闭标签\-Jun Han

*   该扩展名在编写HTML或XML语法时自动添加结束标记

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--XiRP5S6r--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/x79gmxh7p4e6vneum6da.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--XiRP5S6r--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/x79gmxh7p4e6vneum6da.png)

### [](#auto-import-steoates)自动导入\-引导

*   可能是最有用的扩展 `import` ，如果您忘记导入代码段并在代码中调用了该代码段 ，此扩展会自动生成该 语句。
*   在制作TypeScript或React应用程序时派上用场

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--sWAI0lJt--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/jed8ejul3w8rbig9880r.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--sWAI0lJt--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/jed8ejul3w8rbig9880r.png)

### [](#auto-rename-tag-jun-han)自动重命名标签\-Jun Han

*   重命名开始标签时，此扩展名会自动重命名结束标签。

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--1KLLqmxE--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/3o5ur78qnkx1ia8qhtph.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--1KLLqmxE--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/3o5ur78qnkx1ia8qhtph.png)

### [](#auto-open-markdown-preview-hnw)自动打开降价预览\-hnw

*   在编辑器中编辑README文件时，此扩展名使您可以直接在编辑器中查看更改。 您也可以在另一个窗格中打开预览，所做的更改将自动反映出来。

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--MUbYkwv1--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/3zh3zuj45vc0qwl28uen.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--MUbYkwv1--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/3zh3zuj45vc0qwl28uen.png)

### [](#better-align-wwm)更好地对齐\-wwm

*   该扩展使您可以轻松地缩进代码

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--bQtCdwrk--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/6xyuowcx76hxlcu5r176.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--bQtCdwrk--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/6xyuowcx76hxlcu5r176.png)

### [](#better-comments-aaron-bond)更好的评论\-亚伦·邦德

*   此扩展使您可以在代码中添加漂亮的注释。 它允许您添加突出显示，注释，警报等

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--lmkXzCY0--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/6y8ffj8dzgpp21oduyez.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--lmkXzCY0--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/6y8ffj8dzgpp21oduyez.png)

### [](#bracket-pair-colorizer-coenraads)支架对着色器\-CoenraadS

*   此扩展是市场上最好的扩展之一，突出显示了匹配的括号，因此，作为开发人员，在关闭错误类型的括号时您不会感到困惑

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--EaKkAJhm--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/dfcrdsd8xb6v10rbl4os.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--EaKkAJhm--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/dfcrdsd8xb6v10rbl4os.png)

### [](#code-time-software)代码时间\-软件

*   我倾向于记录特定日期的编码时间，此扩展名可以帮助我解决这一问题。 不仅如此，它还提供了对花费在特定文件上的时间（总数）的详细分析。 行编码等

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--NerPwiP_--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/t0zjioudbagp7z0i044p.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--NerPwiP_--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/t0zjioudbagp7z0i044p.png)

### [](#es7-reactreduxgraphqlreactnative-snippets-dsznajder)ES7 React / Redux / GraphQL / React\-Native片段\-dsznajder

*   作为React开发人员，此扩展可以在需要在文件中编写React语法时为我提供帮助。 它会自动生成 `function` 和 `import` 语句，并执行更多操作。

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--ODzLGoeb--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/vh8u32718hcbyp00b5j7.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--ODzLGoeb--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/vh8u32718hcbyp00b5j7.png)

### [](#live-server-ritwick-dey)实时服务器\-Ritwick Dey

*   可能是最受关注的，这是一个必备扩展，因为它会自动创建一个服务器供您查看文件。 文件中的任何更改都会自动反映在服务器中。

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--IDQmNLTB--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/se3c0d7bwypi83huf8p8.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--IDQmNLTB--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/se3c0d7bwypi83huf8p8.png)

### [](#path-intellisense-christian-kohler)路径智慧\-Christian Kohler

*   当您导入/导出文件时，此扩展名会自动完成文件名。

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--NFNwexhv--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/aotw2df80gkz9vzycvzl.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--NFNwexhv--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/aotw2df80gkz9vzycvzl.png)

### [](#prettier-code-formatter-prettier)漂亮\-代码格式化\-漂亮

*   整齐地格式化您的代码。 它包括对JavaScript，JSX，Vue，Angular等的支持。

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--Xbt23yC8--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/8b8clk5qppq3tweg9bnv.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--Xbt23yC8--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/8b8clk5qppq3tweg9bnv.png)

### [](#react-proptypes-intellisense-of-human-bondage)React PropTypes Intellisense\-人类的束缚

*   `props` 在React中 查找类型 并自动生成要添加到React组件中的代码段

[![替代文字](https://res.cloudinary.com/practicaldev/image/fetch/s--KVq9190v--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/masktqrh10ox8eomngla.png)](https://res.cloudinary.com/practicaldev/image/fetch/s--KVq9190v--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/masktqrh10ox8eomngla.png)

> 这些都是我使用的所有VS Code扩展。 希望您喜欢我的文章，并使用这些有用的扩展程序玩得开心:)


## Python
[frostming / pdm：具有PEP 582支持的现代Python软件包管理器。](https://github.com/frostming/pdm)
![](https://camo.githubusercontent.com/20960a1486ff6c5100e317970e3eab4f197b2652525f3d36eddf1d513cd0fc37/68747470733a2f2f61736369696e656d612e6f72672f612f6a6e69664e3330706a6658624f395765324b714f64584568422e737667)
>旨在替代pipenv和poetry的Python包管理工具，国人开发

### [tqdm / tqdm：Python和CLI的快速，可扩展进度栏](https://github.com/tqdm/tqdm)
![](https://raw.githubusercontent.com/tqdm/tqdm/master/images/tqdm.gif)

### 打包工具,将python打包成exe或者二进制软件
[Nuitka/Nuitka: Nuitka is a Python compiler written in Python. It's fully compatible with Python 2.6, 2.7, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, and 3.9. You feed it your Python app, it does a lot of clever things, and spits out an executable or extension module.](https://github.com/Nuitka/Nuitka)


## 命令行
[Shell启动脚本-Flowblok的博客](https://blog.flowblok.id.au/2013-02/shell-startup-scripts.html)
[explainshell.com - sed(1) - stream editor for filtering and transforming text](https://www.explainshell.com/explain/1/sed)
>shell解释器,非常好的解释各种shell命令,很直观

### 命令行图形工具
[mkaz / termgraph：一个Python命令行工具，可在终端上绘制基本图形](https://github.com/mkaz/termgraph)
![20210114090525_a150b64c80a8449b6430e75cfeb50241.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210114090525_a150b64c80a8449b6430e75cfeb50241.png)
### 翻译工具
* [soimort / translate-shell：使用Google Translate，Bing Translator，Yandex.Translate等的命令行翻译器。](https://github.com/soimort/translate-shell)
![20210112211331_3e3464e52c96237f6d352ffbe1aee5d2.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210112211331_3e3464e52c96237f6d352ffbe1aee5d2.png)

* [felixonmars/ydcv: 有道翻译](https://github.com/felixonmars/ydcv)
![20210112211636_3725930959843851623b6eee55a8478b.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210112211636_3725930959843851623b6eee55a8478b.png)
### 网络监控
[如何在Linux中安装vnStat和vnStati监视网络流量](https://www.tecmint.com/install-vnstat-and-vnstati-to-monitor-linux-network-traffic/)
### 录屏工具
[faressoft/terminalizer: 🦄 Record your terminal and generate animated gif images or share a web player](https://github.com/faressoft/terminalizer#installation)
>命令行代码可以转成gif动图
### shell命令记录工具
[slim-bean / loki-shell：如何使用Loki从任何地方存储和集中化您的Shell历史记录，并使用ctrl-r和fzf搜索它](https://github.com/slim-bean/loki-shell)
![](https://pic1.zhimg.com/v2-f9ea80e679234b4efda15efb26c11618_b.webp)
### tmux
* [关于— tmuxp 1.7.0a3文档](https://tmuxp.git-pull.com/about.html)
>一个管理tmux的python软件
* [ivaaaan / smug：用Go编写的tmux会话管理器](https://github.com/ivaaaan/smug)
![](https://raw.githubusercontent.com/ivaaaan/gifs/master/smug.gif)
>简单让你的vim变成ide，看上去非常强大，就是不知道好不好用
### vim
* [RRethy / vim-illuminate：illuminate.vim-Vim插件，用于自动突出显示光标下方单词的其他用法](https://github.com/RRethy/vim-illuminate)

![](https://camo.githubusercontent.com/fbf33923a14af05412768996455d3f45f17b629d55134c309523f299e437eeb3/68747470733a2f2f6d656469612e67697068792e636f6d2f6d656469612f5a4f37517451576f425032545a396d6b58712f67697068792e676966)

>一个自动显示当前单词的插件
* [amix / vimrc：最终的Vim配置（vimrc）](https://github.com/amix/vimrc)
>一个2.3w星的vim配置
[Vim - Quick Reference Cheat Sheet](https://quickref.me/vim)
>一个vim备忘录网站
![20210104224117_7f42987845b8455f5ee31f7ea1578e62.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210104224117_7f42987845b8455f5ee31f7ea1578e62.png)

### fish 
[jorgebucaran / replay.fish：运行Bash命令以重播Fish中的更改。 🍤](https://github.com/jorgebucaran/replay.fish)
>一个fish插件，用以兼容bash

### 字体
[pwaller/pyfiglet: An implementation of figlet written in Python](https://github.com/pwaller/pyfiglet)
[使用Python pyfiglet模块的ASCII艺术](https://www.tutorialspoint.com/ascii-art-using-python-pyfiglet-module)
![20201213142419_68395b948ac1a750efb2998b14b2cdca.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20201213142419_68395b948ac1a750efb2998b14b2cdca.png)

[icons8 / titanic：一组动画图标+将其插入网页的代码](https://github.com/icons8/titanic)

![](https://github.com/icons8/titanic/raw/master/docs/images/animated-icons-preview.gif)
[bytedance/IconPark: 🍎Transform an SVG icon into multiple themes, and generate React icons，Vue icons，svg icons](https://github.com/bytedance/IconPark)
>今日头条开源的图标库

![](https://camo.githubusercontent.com/d1b8a01c4f258e9d300f6aec1cf295eca4da1c16e44175d9d1307fbcc793f493/68747470733a2f2f7366312d647963646e2d746f732e7073746174702e636f6d2f6f626a2f6564656e2d636e2f6c737777686568376e7570776e75686f672f69636f6e732f69636f6e2d64616e63652e676966)


## 其他工具
[奇幻地图概念设计工具：Wonderdraft - 知乎](https://zhuanlan.zhihu.com/p/300692554)
![20201220124110_2479a3cca92087f3c74ac5d3e22bf427.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20201220124110_2479a3cca92087f3c74ac5d3e22bf427.png)
>一款用来制作类似游戏或者奇幻地图的工具

## windows工具
### 压缩工具
[图压 - 简单易用的图片压缩软件](https://tuya.xinxiao.tech/)

## chrome插件
[Simple Allow Copy - Chrome 网上应用店](https://chrome.google.com/webstore/detail/simple-allow-copy/aefehdhdciieocakfobpaaolhipkcpgc/related)
>无限复制
[IE Tab - Chrome 网上应用店](https://chrome.google.com/webstore/detail/ie-tab/hehijbfgiekmjfkfjpbkbammjbdenadd/related)
>兼容用IE打开，防止网站不兼容