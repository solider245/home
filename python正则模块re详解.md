## 序言
接触到了字符串匹配这块,发现绕不开re,所以这里记录以下.

## 使用pyre2替代re 
pyre2是re的超集,在正常使用的情况下,速度大概提升了5000%.如下图所示
![20210118183120_585fd499dc74a4406bb18c773c395c3a.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210118183120_585fd499dc74a4406bb18c773c395c3a.png)

### 安装
```shell
pip install re2
poetry add re2
```
### 使用
```Py
try:
    import re2 as re
except ImportError:
    import re
else:
    re.set_fallback_notification(re.FALLBACK_WARNING)
# 下面这种简单的也行
try:
    import re2 as re
except ImportError:
    import re
```
接下来像RE一样正常使用即可。

## re的6种使用方法
1. findall 返回一个包含所有匹配项的列表
2. search 返回一个匹配对象如果字符串中的任何地方有匹配项
3. split 返回一个列表，其中字符串在每个匹配子处被拆分
4. sub 用字符串替换一个或多个匹配项
5. compile 函数用于编译正则表达式，生成一个正则表达式（ Pattern ）对象，供 match() 和 search() 这两个函数使用
6. re.finditer 和 findall 类似，在字符串中找到正则表达式所匹配的所有子串，并把它们作为一个迭代器返回
7. match 返回一个匹配对象包含以下三个类型
  Match对象具有用于检索有关搜索信息和结果的属性和方法：
   * `.span()` 返回一个元组，其中包含比赛的开始和结束位置。
   *  `.string` 返回传递给函数 
   * `.group()` 的字符串，返回匹配的字符串部分

### 常用代码举例

```py
#打印所有匹配项的列表：  
import re  
txt = "The rain in Spain" 

# findall()函数 查找并返回一个列表
x = re.findall("ai", txt) #打印所有匹配项的列表
x = re.findall("Portugal", txt) # 如果找不到匹配项，则返回一个空列表

# search()函数 查找并返回第一个匹配对象
x = re.search("\s", txt);print("第一个空格字符位置:", x.start()) 
# 搜索字符串中的第一个空格字符,如果找不到返回None
x = re.search("Portugal", txt) # 进行搜索，返回不匹配的内容

# split() 分割函数 
x = re.split("\s", txt) # 在每个空格字符处分割
# 您可以通过指定maxsplit 参数来控制出现次数
x = re.split("\s", txt, 1) #仅在第一次出现时才拆分字符串

# sub（）替换函数 该sub()函数将匹配项替换为您选择的文本
x = re.sub("\s", "9", txt) #将每个空格字符替换为数字9
x = re.sub("\s", "9", txt, 2) # 只替换最早遇到的2个
# 以上内容打印对象全部为下面内容
print(x)

# match() 匹配函数 包含有关搜索和结果信息的对象
#注意：如果没有匹配项，None则将返回值，而不是匹配对象
x = re.search("ai", txt);print(x)# 返回一个对象
x = re.search(r"\bS\w+", txt);print(x.span()) #打印第一个匹配项的位置（开始和结束位置）。正则表达式查找以大写字母“ S”开头的所有单词
x = re.search(r"\bS\w+", txt); print(x.string)# 打印传递给函数的字符串
x = re.search(r"\bS\w+", txt); print(x.group())#打印匹配的字符串部分。正则表达式查找以大写字母“ S”开头的所有单词
.span()返回一个元组，其中包含比赛的开始和结束位置。
.string返回传递给函数 
.group()的字符串，返回匹配的字符串部分
```


### 简单代码举例


```py
#1. re.match 尝试从字符串的起始位置匹配一个模式，如果不是起始位置匹配成功的话，match()就返回none。

import re
 
line="this hdr-biz 123 model server 456"
pattern=r"123"
matchObj = re.match( pattern, line)
 

#2. re.search 扫描整个字符串并返回第一个成功的匹配。

import re
 
line="this hdr-biz model server"
pattern=r"hdr-biz"
m = re.search(pattern, line)

#3. Python 的re模块提供了re.sub用于替换字符串中的匹配项。

import re
 
line="this hdr-biz model args= server"
patt=r'args='
name = re.sub(patt, "", line)


#4. compile 函数用于编译正则表达式，生成一个正则表达式（ Pattern ）对象，供 match() 和 search() 这两个函数使用。

import re
 
pattern = re.compile(r'\d+') 
 

#5. re.findall 在字符串中找到正则表达式所匹配的所有子串，并返回一个列表，如果没有找到匹配的，则返回空列表。

import re
 
line="this hdr-biz model args= server"
patt=r'server'
pattern = re.compile(patt)
result = pattern.findall(line)

#6. re.finditer 和 findall 类似，在字符串中找到正则表达式所匹配的所有子串，并把它们作为一个迭代器返回。

import re
 
it = re.finditer(r"\d+","12a32bc43jf3")
for match in it:
  print (match.group() )
 
```
### 另外一种说法:匹配,查找,替换,分割
1. search(pattern, string, flags=0)      在一个字符串中查找匹配

2. findall(pattern, string ,flags=0)     找到匹配，返回所有匹配部分的列表

3. sub(pattern, repl, string , count=0, flags=0)    将字符串中匹配正则表达式的部分替换为其他值

4. split(pattern, string ,maxsplit=0, flags=0)  根据匹配分割字符串，返回分隔符串组成的列表




## re.search和re.match有什么区别？

`search` ⇒在字符串中的任何地方找到东西，然后返回一个匹配对象。

`match` ⇒ 在字符串 的 *开头* 查找内容， 然后返回匹配对象


`re.match` 锚定在字符串的开头。 这与换行符无关，因此它与 `^` 在模式中 使用不一样 。

*   `search` 扫描整个字符串。

*   `match` 仅扫描字符串的开头。

以下Ex表示：

```
>>> a = "123abc"
>>> re.match("[a-z]+",a)
None
>>> re.search("[a-z]+",a)
abc
```


如 [重新匹配文档所述](http://docs.python.org/2/library/re.html#re.match) ：

> 如果 **字符串开头的** 零个或多个字符 与正则表达式模式匹配，则返回相应的 `MatchObject` 实例。 `None` 如果字符串与模式不匹配，则 返回； 否则 返回 false。 请注意，这与零长度匹配不同。
>
> 注意：如果要在字符串中的任何位置找到匹配项，请 `search()` 改用。

`re.search` 搜索整个字符串，如 [文档所述](http://docs.python.org/2/library/re.html#re.search) ：

> **扫描字符串以** 查找正则表达式模式产生匹配的位置，然后返回相应的 `MatchObject` 实例。 `None` 如果字符串中没有位置与模式匹配，则 返回； 否则 返回 false。 请注意，这与在字符串中的某个点找到零长度匹配不同。

因此，如果您需要匹配字符串的开头，或者匹配整个字符串，请使用 `match` 。 它更快。 否则使用 `search` 。

该文档中有一个 [专门针对 `match` vs.的 部分 `search`](http://docs.python.org/2/library/re.html#search-vs-match) ，还涵盖了多行字符串：

> python提供两种不同的基本操作基于正则表达式： `match` 检查是否有比赛 **才刚刚开始** 的字符串，而 `search` 用于匹配检查 **任何地方** 的字符串（这是Perl并默认情况下）。
>
> 请注意， `match` 可以从不同的 `search` 使用开头的正则表达式时，即使 `'^'` ： `'^'` 只在字符串的开头匹配，或 `MULTILINE` 模式换行也紧随其后。 的“ `match` ”操作成功 *的仅当模式匹配**开始**的字符串* ，不考虑模式，或者在通过可选的给定的起始位置 `pos` 的参数的它是否换行符先于不管。

现在，聊够了。 现在来看一些示例代码：

```py
# example code:
string_with_newlines = """something
someotherthing"""

import re

print re.match('some', string_with_newlines) # matches
print re.match('someother',
               string_with_newlines) # won't match
print re.match('^someother', string_with_newlines,
               re.MULTILINE) # also won't match
print re.search('someother',
                string_with_newlines) # finds something
print re.search('^someother', string_with_newlines,
                re.MULTILINE) # also finds something

m = re.compile('thing$', re.MULTILINE)

print m.match(string_with_newlines) # no match
print m.match(string_with_newlines, pos=4) # matches
print m.search(string_with_newlines,
               re.MULTILINE) # also matches
```
### match比search快点测试案例与图片

> match比搜索快得多，因此如果不使用regex.search（“ word”），则可以进行regex.match（（。\*？）word（。\*？）），如果您使用数百万个样品。

[@ivan\_bilan在上面接受的答案下的评论](https://stackoverflow.com/questions/180986/what-is-the-difference-between-re-search-and-re-match#comment62326091_180993) 让我开始思考，如果这样的 *黑客* 实际上正在加速任何事情，那么让我们找出您将真正获得多少性能。

我准备了以下测试套件：

```py
import random
import re
import string
import time

LENGTH = 10
LIST_SIZE = 1000000

def generate_word():
    word = [random.choice(string.ascii_lowercase) for _ in range(LENGTH)]
    word = ''.join(word)
    return word

wordlist = [generate_word() for _ in range(LIST_SIZE)]

start = time.time()
[re.search('python', word) for word in wordlist]
print('search:', time.time() - start)

start = time.time()
[re.match('(.*?)python(.*?)', word) for word in wordlist]
print('match:', time.time() - start)

```

我进行了10次测量（1M，2M，...，10M个单词），得出以下图表：

[![匹配与搜索正则表达式速度测试线图](https://i.stack.imgur.com/g55A6.png)](https://i.stack.imgur.com/g55A6.png)

所得的直线令人惊讶地（实际上并不那么令人惊讶）直线。 鉴于这种特定的模式组合 ，该 **`search`功能（略）更快** 。 该测试的*实质是* ： *避免过度优化代码。*

## 英文说明



## 参考文章
[Python字符串匹配----6种方法的使用_qq_34500270的博客-CSDN博客_python字符串匹配](https://blog.csdn.net/qq_34500270/article/details/82899057)
[Python 正则表达式 字符串的匹配、替换、分割、查找_Hebe-CSDN博客_python正则表达式替换字符串](https://blog.csdn.net/u011138533/article/details/62904006)