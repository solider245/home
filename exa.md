替代ls。

您一天列出文件数百次。为什么花时间斜视黑白文字？

exa是一种改进的文件列表器，具有更多功能和更好的默认设置。它使用颜色来区分文件类型和元数据。它知道符号链接，扩展属性和Git。它小巧，快速，只有一个二进制文件。
<!-- more -->
## 常用命令与安装
```shell
# 安装
sudo apt install exa 
# 设置快捷命令
alias exa='ls'
```
## 常见参数意义
#### `-1` ， `--oneline`

[每行](https://the.exa.website/features/grid-view#oneline) 显示 [一个文件](https://the.exa.website/features/grid-view#oneline) 。

#### `-G` ， `--grid`

将文件显示 [为网格](https://the.exa.website/features/grid-view) 。

这是默认值。

#### `-l` ， `--long`

[在表中](https://the.exa.website/features/long-view) 显示文件 及其元数据。

#### `-x` ， `--across`

[跨](https://the.exa.website/features/grid-view#across) 而不是向下 对网格 [进行](https://the.exa.website/features/grid-view#across) 排序 。

#### `-R` ， `--recurse`

[递归](https://the.exa.website/docs#recurse) 到目录。

#### `-T` ， `--tree`

[像树一样](https://the.exa.website/features/tree-view) 递归到目录 [中](https://the.exa.website/features/tree-view) 。

#### `-L` ， `--level=DEPTH`

限制 [递归](https://the.exa.website/docs#recurse) 的深度 ，因此exa仅下降到给定的次数。

#### `-F` ， `--classify`

按文件名 显示 [文件类型指示符](https://the.exa.website/colours#classify) 。

#### `--color` ， `--colour=WHEN`

配置何时使用端子颜色。 默认选择为 `automatic` ，表示在exa写入终端时使用颜色，否则将关闭。

*   `**automatic**`或`**auto**`仅在写入终端时显示颜色。
*   `**always**` 显示它们，即使不是。
*   `**never**`不显示他们，即使它*是*。

### 筛选和排序选项

#### `-a` ， `--all`

显示 [隐藏和“点”文件](https://the.exa.website/features/filtering#dotfiles) 。

使用两次也可以显示 `.` 和 `..` 目录。

#### `-d` ， `--no-recurse` ， `--list-dirs`

列出目录 [而不递归](https://the.exa.website/docs#dirs) 到 目录中 ，就像它们是常规文件一样。

#### `-r` ， `--reverse`

颠倒 [排序顺序](https://the.exa.website/features/filtering#sorting) 。

#### `-s` ， `--sort=SORT_FIELD`

配置按哪个字段 [排序](https://the.exa.website/features/filtering#sorting) 。

*   `**name**`或`**filename**`按名称排序，不区分大小写。
*   `**Name**`或`**Filename**`按名称*区分大小写*。
*   `**cname**`或`**cfilename**`按名称排序，不区分大小写和*规范*。
*   `**Cname**`或`**Cfilename**`按名称排序，区分大小写和规范。
*   `**.name**`或`**.filename**`按名称排序（*不带前导点）*，不区分大小写。
*   `**.Name**`或`**.Filename**`按名称排序，不区分大小写。
*   `**size**`或`**filesize**`按大小排序，首先列出较小的文件。
*   `**ext**`或`**extension**`按文件扩展名排序，不区分大小写。
*   `**Ext**`或`**Extension**`按文件扩展名*区分大小写*。
*   `**mod**`或`**modified**`按文件修改日期排序，首先列出较旧的文件。
*   `**acc**`或`**accessed**`按文件访问日期排序。
*   `**cr**`或`**created**`按文件创建日期排序。
*   `**inode**` 按文件索引节点排序。
*   `**type**`按文件*类型*（目录，套接字，链接）排序。
*   `**none**` 禁用排序，并以任意顺序列出文件。

该 `modified` 场有别名 `date` ， `time` 和 `new` 和 `newest` 。 另外，因为我们通常认为大约日期比较，其 *反向* 有别名 `age` ， `old` 和 `oldest` 。

以大写字母开头的字段将以大写字母排序，然后以小写字母排序。

*规范* 排序意味着数字将被视为数字字符串，而不是数字。 通常， 排序 在 `9` 之前 `10` ，但是排序依据 `Cname` 将在排序 `10` 之前， `9` 因为它 `1` 首先 看到 数字。

#### `-I` ， `--ignore-glob=GLOBS`

[忽略文件的](https://the.exa.website/features/filtering#ignoring) 管道模式（用管道分隔） 。

#### `--git-ignore`

忽略中 [提到的 `.gitignore`](https://the.exa.website/features/git) 文件 。

#### `--group-directories-first`

[排序时，](https://the.exa.website/features/filtering#sorting) 在其他文件之前列出目录 。

### 长视选项

当使用 `--long` （ `-l` ） 运行时，这些选项可用 ：

#### `-b` ， `--binary`

列出 带有*二进制* 前缀的 [文件大小](https://the.exa.website/features/long-view#sizes) 。

#### `-B` ， `--bytes`

列出 [文件大小（](https://the.exa.website/features/long-view#sizes) 以字节为单位），不带 *任何* 前缀。

#### `-g` ， `--group`

列出每个文件的 [组](https://the.exa.website/features/long-view#owners-and-groups) 。

#### `-h` ， `--header`

将标题行添加到表中的每一列。

#### `-H` ， `--links`

列出每个文件的 [硬链接数](https://the.exa.website/features/long-view#metadata) 。

#### `-i` ， `--inode`

列出每个文件的 [索引节点号](https://the.exa.website/features/long-view#metadata) 。

#### `-m` ， `--modified`

使用 [修改后的时间戳字段](https://the.exa.website/features/long-view#timestamps) 。

#### `-S` ， `--blocks`

列出每个文件的 [文件系统块计数](https://the.exa.website/features/long-view#metadata) 。

#### `-t` ， `--time=WORD`

其构成 [时间戳字段](https://the.exa.website/features/long-view#timestamps) 到列表（ `modified` ， `accessed` ， `created` ）。

#### `--time-style=STYLE`

配置 [时间戳记](https://the.exa.website/features/long-view#timestamps) 应采用的 格式 。

*   `**default**`使用当前区域设置打印月份名称，指定时间戳到分钟在次*电流*年，下降到每天往年。
*   `**iso**` 除了使用一个月作为数字外，它不需要查找语言环境。
*   `**long-iso**` 指定不使用语言环境或当前年份的时间戳，直到分钟。
*   `**full-iso**`指定不超过*毫秒*的时间戳，包括不超过分钟的偏移量，而不使用语言环境或当前年份。

#### `-u` ， `--accessed`

使用 [访问的时间戳字段](https://the.exa.website/features/long-view#timestamps) 。

#### `-U` ， `--created`

使用 [创建的时间戳字段](https://the.exa.website/features/long-view#timestamps) 。

#### `-@` ， `--extended`

列出每个文件的 [扩展属性](https://the.exa.website/features/xattrs) 和大小。

#### `--git`

列出每个文件的 [Git状态](https://the.exa.website/features/git) （如果已跟踪）。

#### `--color-scale` ， `--colour-scale`

突出显示 [文件大小](https://the.exa.website/features/long-view#sizes) 级别 。