## python
## 安装
[python华为国内加速下载地址](https://mirrors.huaweicloud.com/python/)
### 临时加速安装
```shell
pip install --trusted-host https://mirrors.huaweicloud.com -i https://mirrors.huaweicloud.com/repository/pypi/simple
<some-package>
```
### pip配置文件
Pip的配置文件为用户根目录下的：`~/.pip/pip.conf`（Windows路径为：`C:\Users\<UserName>\pip\pip.ini`）
配置如下：
```
[global]
index-url = https://mirrors.huaweicloud.com/repository/pypi/simple
trusted-host = mirrors.huaweicloud.com
timeout = 120
```
## pip常用命令
```shell
#安装Pip
sudo apt-get install python-pip
# 脚本安装
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py   # 下载安装脚本
sudo python get-pip.py    # 运行安装脚本
#如果要设为默认需要升级 pip 到最新的版本 (>=10.0.0) 后进行配置：
pip install pip -U
pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
# Pip的配置文件为用户根目录下的：
`~/.pip/pip.conf`（Windows路径为：`C:\Users\<UserName>\pip\pip.ini`）
#显示版本和路径
pip --version
#获取帮助
pip --help
#升级 pip

pip install -U pip
#如果这个升级命令出现问题 ，可以使用以下命令：

sudo easy_install --upgrade pip
#安装包

pip install SomePackage              # 最新版本
pip install SomePackage==1.0.4       # 指定版本
pip install 'SomePackage>=1.0.4'     # 最小版本

#比如我要安装 Django。用以下的一条命令就可以，方便快捷。

pip install Django==1.7

#升级包
pip install --upgrade SomePackage #升级指定的包，通过使用==, >=, <=, >, < 来指定一个版本号。

#卸载包
pip uninstall SomePackage
#搜索包
pip search SomePackage
#显示安装包信息
pip show 
#查看指定包的详细信息

pip show -f SomePackage

#列出已安装的包

pip list
#查看可升级的包

pip list -o
#pip 升级
#Linux 或 macOS 
pip install --upgrade pip    # python2.x
pip3 install --upgrade pip   # python3.x
#Windows 平台升级：
python -m pip install -U pip   # python2.x
python -m pip3 install -U pip    # python3.x
```