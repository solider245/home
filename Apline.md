**Apline**

## 更换镜像


```shell
### 阿里源
sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories
### 清华源
sed -i 's/dl-cdn.alpinelinux.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apk/repositories
### 华为源
sed -i 's/dl-cdn.alpinelinux.org/mirrors.huaweicloud.com/g' /etc/apk/repositories
```
## 常用命令
```shell
# 升级
apk update
# 搜索
apk search
# 安装
apk install
```