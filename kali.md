kali

## kali linux安装好后，我们需要对里面的工具进行升级！

1.  安装文本编辑器：sudo apt-get install leafpad -y
2.  用文本编辑器打开文件：leafpad /etc/apt/sources.list
3.  在后面添加源：

```shell
deb http://mirrors.ustc.edu.cn/kali kali-rolling main non-free contrib
deb-src http://mirrors.ustc.edu.cn/kali kali-rolling main non-free contrib
```

接下来开始更新：

1.  先执行：apt update
2.  再执行：apt full-upgrade

查看系统版本：grep VERSION /etc/os-release