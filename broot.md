在Linux系统上安装broot的方法
<!-- more -->

本文介绍在Linux系统上安装broot的方法。broot是一个出色的工具，您可以利用它来完全更改在您喜欢的Linux终端（使用bash、zsh或fish）中导航目录树的方式。

**来自预编译的二进制文件**

```shell
# 国内用户需要使用代理
wget https://dystroy.org/broot/download/x86_64-linux/broot

mv broot /usr/local/bin

sudo mv broot /usr/local/bin

sudo chmod +x /usr/local/bin/broot

```
**使用Homebrew**

如果您使用Homebrew，则可以使用brew install命令：

```shell
brew install broot

```
参考：[在Linux下安装Homebrew（Linuxbrew）的方法](https://ywnz.com/linuxjc/4666.html)。

**从源码/git**

您需要安装Rust开发环境。

获取Canop/broot存储库，地址是https://github.com/Canop/broot，移至broot目录，然后运行：

```shell
git clone https://github.com/Canop/broot.git

cargo install --path .

```
启动broot时，它将检查br shell函数是否似乎已安装（或已被拒绝）。如果需要，并且如果所使用的shell似乎兼容（支持的shell是bash、zsh和fish），则broot要求允许注册该shell函数：

## 参考文章
[在Linux系统上安装broot的方法_Linux教程_云网牛站](https://ywnz.com/linuxjc/7312.html)