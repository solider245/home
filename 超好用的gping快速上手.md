## 序言
之前大家想要看看某个地址能不能连通都是使用ping来执行的，类似下图。
![20201117103558_d0b7b3a7f2f07dbfebecd4543cc3f2a6.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20201117103558_d0b7b3a7f2f07dbfebecd4543cc3f2a6.png)

这个软件的好处是快，但是并不怎么直观，现在有一款更好用的gping出来了。

如下图所示：
![图片](https://doc-1255533533.cos.ap-shanghai.myqcloud.com/images/gping%E5%B1%95%E7%A4%BA.gif)

和上面的文字版相比，有非常明显的区别。

## 下载
```shell
# linux下载与解压
wget https://g.ioiox.com/https://github.com/orf/gping/releases/download/v0.1.7/gping-Linux-x86_64.tar.gz
tar -zxvf gping-Linux-x86_64.tar.gz
sudo mv gping /usr/local/gping
gping <网址>
```
就可以使用了。
当然，为了方便，你可以使用alias：
```shell
alias ping='gping'
```
这样的话，以后你用ping命令就和gping命令一样了。
你也可以把上面的命令放入你的`~/.bashrc`或者`~/.zshrc`。这样就不用每次登录之后都要输入一次命令了。

## 其他用法
```shell
gping -b 10 <网址>
```
在ping的时候你可以加参数b，显示你要Ping的次数，默认是100，可以增加次数，方便对比。

其他就没什么了。