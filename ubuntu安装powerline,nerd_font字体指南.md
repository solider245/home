## 序言
系统自带的字体通常不好用，而且很多软件都对字体有要求，因此在这里做个小记录，把ubuntu常用字体安装做一个记录。

## Powerline电力线字体安装
需要`Powerline`字体。 使用以下命令安装它：

```shell
# Debian / Ubuntu
$ sudo apt-get install fonts-powerline -y

# Fedora
$ sudo dnf install powerline-fonts

# Any other Linux

git clone https://github.com/powerline/fonts.git --depth=1
cd fonts
./install.sh
cd ..
rm -rf fonts
```

## firacode字体安装

对于 [FiraCode](https://github.com/tonsky/FiraCode) 字体，请通过以下方式安装：

```shell
# Ubuntu
$ sudo apt install fonts-firacode

# Arch Linux / Manjaro
$ sudo pacman -S otf-fira-code

# Gentoo
$ emerge -av media-fonts/fira-code

# Fedora
dnf copr enable evana/fira-code-fonts
dnf install fira-code-fonts
```

## nerd字体安装脚本

1. 创建一个脚本：
```
vim [yourname].sh
```
2. 复制下列内容到你的脚本
```shell
#/bin/bash
# install DroidSansMono Nerd Font --> u can choose another at: https://www.nerdfonts.com/font-downloads
echo "[-] Download fonts [-]"
echo "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/DroidSansMono.zip"
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/DroidSansMono.zip
unzip DroidSansMono.zip -d ~/.fonts
fc-cache -fv
echo "done!"
```
3. 打开终端，输入：
```shell
bash [yourname].sh
```

### 注意事项
1. 可以使用 `~/.local/share/fonts` 代替 `~/.fonts`
2. 如果报错，请检查是否安装`fontconfig`
   ```shell
   sudo apt-get install fontconfig -y
   ```
3. 检查字体是否安装成功
   ```shell 
   fc-list | grep "<name-of-font>"
   ```
4. 查看目前拥有的字体
   ```shell
   fc-list -f '%{file}\n' | sort
   ```
5. 更改建议以手动安装到其中， `/usr/local/share/fonts` 而不是 `/usr/share/fonts` 反映评论和最佳实践。
6. GNU状态下可以使用[字体管理器](https://apps.ubuntu.com/cat/applications/font-manager/)

## 参考文章
[如何安装字体？ -询问Ubuntu](https://askubuntu.com/questions/3697/how-do-i-install-fonts?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa)
  >超级详细的介绍，必看

[在ubuntu上安装书呆子字体](https://gist.github.com/matthewjberger/7dd7e079f282f8138a9dc3b045ebefa0)
[书呆子字体-标志性字体聚合器，标志符号/图标集合和字体修补程序](https://www.nerdfonts.com/font-downloads)