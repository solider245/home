## 序言

我之前使用过 [Pure](https://github.com/sindresorhus/pure) 和 [Spaceship](https://github.com/denysdovhan/spaceship-prompt) ，现在是尝试其他“提示”的正确时机。 我们将看到如何在安装Starship Shell提示符。
starship主要有以下功能：

*   **利用电力线字体将各种信息（和提示）表示为符号**
*   **当最后一个存在非零代码的命令时，提示符变为红色**
*   **仅在与登录用户名不同的情况下显示用户名（并且显然用于root / ssh会话）**
*   **与Git集成（显示当前的Git分支和存储库状态），并可以显示Node.js，Rust，Ruby，Python和Go版本**
*   **在当前目录中显示货物（Rust），npm（Node.js）和诗歌（Python）的软件包版本**
*   **显示当前电池电量和状态**
*   **镍壳环境检测**
*   **显示执行上一条命令（如果超过可配置的阈值）所花费的时间**
*   **具有在后台运行的作业的指示器**

## 主要步骤
1. 安装字体
2. 下载Starship
3. 将Starship配置文件写入你的shell配置

下面开始进行

## 安装Powerline字体
```shell
sudo apt-get install fonts-powerline
```
## 下载starship文件
```shell
curl -s https://api.github.com/repos/starship/starship/releases/latest \
  | grep browser_download_url \
  | grep x86_64-unknown-linux-gnu \
  | cut -d '"' -f 4 \
  | wget -qi -

# 国内用户请用下面这个链接
wget https://github.91chifun.workers.dev//https://github.com/starship/starship/releases/download/v0.47.0/starship-x86_64-unknown-linux-gnu.tar.gz  
# 解压文件
tar xvf starship-*.tar.gz
# 将解压文件移动到目标路径
sudo mv starship /usr/local/bin/
# 检查安装的版本
starship --version
```
## 配置Zsh / Bash / Fish Shell
将以下几行添加到您的Shell配置文件中。

```shell
# Bash
$ vim ~/.bashrc
eval "$(starship init bash)"

# Zsh
$ vim ~/.zshrc
eval "$(starship init zsh)"

# Fish
$ vim ~/.config/fish/config.fish
eval (starship init fish)
```
## 报错
安装p10k字体之后，有可能会引起冲突。可以输入以下命令解决：
```shell 
echo '(( ! ${+functions[p10k]} )) || p10k finalize' >>! ~/.zshrc
# 然后添加下列命令到你的~/.zshrc中
typeset -g POWERLEVEL9K_INSTANT_PROMPT=off
# typeset -g POWERLEVEL9K_TRANSIENT_PROMPT=always #Powerlevel10k会在接受命令行时减少所有提示。

# typeset -g POWERLEVEL9K_TRANSIENT_PROMPT=same-dir #：Powerlevel10k将仅减少来自同一目录的重复提示

```
你也可以通过`p10k configure`重新配置你的字体。

## 参考文章
[New Powerlevel10k Feature: Transient Prompt : zsh](https://www.reddit.com/r/zsh/comments/dsh1g3/new_powerlevel10k_feature_transient_prompt/)

[Starship是Rust中写的最小且快速的Shell提示-Linux Uprising Blog](https://www.linuxuprising.com/2019/09/starship-is-minimal-and-fast-shell.html)
[如何为Bash / Zsh / Fish安装Starship Shell Prompt。 极客计算](https://computingforgeeks.com/how-to-install-starship-shell-prompt-for-bash-zsh-fish/)