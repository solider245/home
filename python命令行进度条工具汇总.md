## 序言
想要给自己的软件加一个动作条,翻来覆去,发现有很多种,所以这里汇总一下.
python的进度条主要分两种,一种是下载文件进度条,一种是程序循环执行进度条.
下载文件进度条主要通过统计文件的数据或者长度来判断程序的执行进度.
程序循环则通过当前循环数在列表或者数组中的位置来判断.

主要的下载动作条有以下几种:

1. requests 库自动构建
2. 使用clint包,作者也是requests的作者
3. tqdm
4. 使用click库
5. 手写代码制作

主要的循环进度动作条有以下几种方法:


## requests库构建

我刚刚为此编写了一种超级简单的方法（将其略微修改），以将pdf刮出某个站点。 注意，它仅在基于UNIX的系统（Linux，Mac OS）上正常运行，因为Powershell无法处理“ \\ r”

```py
import requests

link = "http://indy/abcde1245"
file_name = "download.data"
with open(file_name, "wb") as f:
    print "Downloading %s" % file_name
    response = requests.get(link, stream=True)
    total_length = response.headers.get('content-length')

    if total_length is None: # no content length header
        f.write(response.content)
    else:
        dl = 0
        total_length = int(total_length)
        for data in response.iter_content(chunk_size=4096):
            dl += len(data)
            f.write(data)
            done = int(50 * dl / total_length)
            sys.stdout.write("\r[%s%s]" % ('=' * done, ' ' * (50-done)) )
            sys.stdout.flush()

```

它使用了 [请求库，](https://requests.readthedocs.io/en/master/) 因此您需要安装它。 这会将类似以下内容的内容输出到您的控制台中：

> \>下载download.data
>
> \> \[=============\]

脚本中进度条的宽度为52个字符（2 `[]` 个字符就是进度的50个字符）。 每个 `=` 代表下载量的2％。

## 使用clint包
您可以使用“ [clint](https://github.com/kennethreitz/clint) ”包（由“ requests”由同一作者编写）将一个简单的进度条添加到您的下载中，如下所示：

```py
from clint.textui import progress

r = requests.get(url, stream=True)
path = '/some/path/for/file.txt'
with open(path, 'wb') as f:
    total_length = int(r.headers.get('content-length'))
    for chunk in progress.bar(r.iter_content(chunk_size=1024), expected_size=(total_length/1024) + 1):
        if chunk:
            f.write(chunk)
            f.flush()

```

这将为您提供动态输出，如下所示：

```
[################################] 5210/5210 - 00:00:01

```

它也应该在多个平台上工作！ 您 [也可以](https://github.com/kennethreitz/clint/blob/master/examples/progressbar.py) 使用.dots和.mill而不是.bar将条形 [更改](https://github.com/kennethreitz/clint/blob/master/examples/progressbar.py) 为点或微调器。

请享用！

## 使用tqdm

### 案例一
这是 [TQDM文档中](https://github.com/tqdm/tqdm#hooks-and-callbacks) 建议的技术 。

```py
import urllib.request

from tqdm import tqdm

class DownloadProgressBar(tqdm):
    def update_to(self, b=1, bsize=1, tsize=None):
        if tsize is not None:
            self.total = tsize
        self.update(b * bsize - self.n)

def download_url(url, output_path):
    with DownloadProgressBar(unit='B', unit_scale=True,
                             miniters=1, desc=url.split('/')[-1]) as t:
        urllib.request.urlretrieve(url, filename=output_path, reporthook=t.update_to)
```
![](https://i.stack.imgur.com/eJ21m.gif)

### 案例二
很抱歉迟到了答案； 刚刚更新了 `tqdm` 文档：

[https://github.com/tqdm/tqdm/#hooks\-and\-callbacks](https://github.com/tqdm/tqdm/#hooks-and-callbacks)

使用 `urllib.urlretrieve` 和面向对象：

```py
import urllib
from tqdm.auto import tqdm

class TqdmUpTo(tqdm):
    """Provides `update_to(n)` which uses `tqdm.update(delta_n)`."""
    def update_to(self, b=1, bsize=1, tsize=None):
        """
        b  : Blocks transferred so far
        bsize  : Size of each block
        tsize  : Total size
        """
        if tsize is not None:
            self.total = tsize
        self.update(b * bsize - self.n)  # will also set self.n = b * bsize

eg_link = "https://github.com/tqdm/tqdm/releases/download/v4.46.0/tqdm-4.46.0-py2.py3-none-any.whl"
eg_file = eg_link.split('/')[-1]
with TqdmUpTo(unit='B', unit_scale=True, unit_divisor=1024, miniters=1,
              desc=eg_file) as t:  # all optional kwargs
    urllib.urlretrieve(
        eg_link, filename=eg_file, reporthook=t.update_to, data=None)
    t.total = t.n

```

或使用 `requests.get` 和文件包装器：

```py
import requests
from tqdm.auto import tqdm

eg_link = "https://github.com/tqdm/tqdm/releases/download/v4.46.0/tqdm-4.46.0-py2.py3-none-any.whl"
eg_file = eg_link.split('/')[-1]
response = requests.get(eg_link, stream=True)
with tqdm.wrapattr(open(eg_file, "wb"), "write", miniters=1,
                   total=int(response.headers.get('content-length', 0)),
                   desc=eg_file) as fout:
    for chunk in response.iter_content(chunk_size=4096):
        fout.write(chunk)

```

您当然可以混合搭配技术。

### 案例三

有一个关于 [request](https://pypi.org/project/requests/) 和 [tqdm](https://pypi.org/project/tqdm/) 的答案 。

```py
import requests
from tqdm import tqdm

def download(url: str, fname: str):
    resp = requests.get(url, stream=True)
    total = int(resp.headers.get('content-length', 0))
    with open(fname, 'wb') as file, tqdm(
        desc=fname,
        total=total,
        unit='iB',
        unit_scale=True,
        unit_divisor=1024,
    ) as bar:
        for data in resp.iter_content(chunk_size=1024):
            size = file.write(data)
            bar.update(size)

```

要点： [https](https://gist.github.com/yanqd0/c13ed29e29432e3cf3e7c38467f42f51) : [//gist.github.com/yanqd0/c13ed29e29432e3cf3e7c38467f42f51](https://gist.github.com/yanqd0/c13ed29e29432e3cf3e7c38467f42f51)

### 案例四
该 [`tqdm`](https://tqdm.github.io/) 软件包现在包含一个旨在处理这种情况的函数： [`wrapattr`](https://tqdm.github.io/docs/tqdm/#wrapattr) 。 您只需包装对象的 `read` （或 `write` ）属性，然后tqdm即可处理其余部分。 这是一个简单的下载功能，可将所有功能与一起使用 `requests` ：

```py
def download(url, filename):
    import functools
    import pathlib
    import shutil
    import requests
    import tqdm

    r = requests.get(url, stream=True, allow_redirects=True)
    if r.status_code != 200:
        r.raise_for_status()  # Will only raise for 4xx codes, so...
        raise RuntimeError(f"Request to {url} returned status code {r.status_code}")
    file_size = int(r.headers.get('Content-Length', 0))

    path = pathlib.Path(filename).expanduser().resolve()
    path.parent.mkdir(parents=True, exist_ok=True)

    desc = "(Unknown total file size)" if file_size == 0 else ""
    r.raw.read = functools.partial(r.raw.read, decode_content=True)  # Decompress if needed
    with tqdm.tqdm.wrapattr(r.raw, "read", total=file_size, desc=desc) as r_raw:
        with path.open("wb") as f:
            shutil.copyfileobj(r_raw, f)

    return path
```


## 使用click
我认为您也可以使用 [click](http://click.pocoo.org/5/utils/#showing-progress-bars) ，它也有一个不错的进度栏库。

```py
import click
with click.progressbar(length=total_size, label='Downloading files') as bar:
    for file in files:
        download(file)
        bar.update(file.size)

```

请享用 ！

## 通过int计算,手工统计
**#ToBeOptimized\-基准** 如果您想困惑自己的大脑并手工制作逻辑

**＃定义进度条功能**

```py
def print_progressbar(total,current,barsize=60):
    progress=int(current*barsize/total)
    completed= str(int(current*100/total)) + '%'
    print('[' , chr(9608)*progress,' ',completed,'.'*(barsize-progress),'] ',str(i)+'/'+str(total), sep='', end='\r',flush=True)

```

**＃示例代码**

```py
total= 6000
barsize=60
print_frequency=max(min(total//barsize,100),1)
print("Start Task..",flush=True)
for i in range(1,total+1):
  if i%print_frequency == 0 or i == 1:
    print_progressbar(total,i,barsize)
print("\nFinished",flush=True)

```

**＃进度栏快照：**

以下几行仅用于说明。 在命令提示符下，您将看到单个进度条，其中显示了增量进度。

```
[ 0%............................................................] 1/6000

[██████████ 16%..................................................] 1000/6000

[████████████████████ 33%........................................] 2000/6000

[██████████████████████████████ 50%..............................] 3000/6000

[████████████████████████████████████████ 66%....................] 4000/6000

[██████████████████████████████████████████████████ 83%..........] 5000/6000

[████████████████████████████████████████████████████████████ 100%] 6000/6000

```

祝你好运，享受！

## python循环进度条方法汇总
### 系统自带time与sys模块

有特定的库（ [例如此处的](http://pypi.python.org/pypi/progressbar2) 库 ），但也许可以做一些简单的事情：

```py
import time
import sys

toolbar_width = 40

# setup toolbar
sys.stdout.write("[%s]" % (" " * toolbar_width))
sys.stdout.flush()
sys.stdout.write("\b" * (toolbar_width+1)) # return to start of line, after '['

for i in xrange(toolbar_width):
    time.sleep(0.1) # do real work here
    # update the bar
    sys.stdout.write("-")
    sys.stdout.flush()

sys.stdout.write("]\n") # this ends the progress bar

```
另一个案例:
上面的建议非常好，但是我认为大多数人只想要一个现成的解决方案，不依赖外部软件包，而是可以重用的。

我获得了以上所有方面的优点，并将其与测试用例一起成为一个函数。

要使用它，只需复制“ def update\_progress（progress）”下的行，而不复制测试脚本。 不要忘记导入sys。 每当您需要显示或更新进度条时，请调用此函数。

通过直接将“ \\ r”符号发送到控制台以将光标移回开始位置来工作。 python中的“ print”不能将上述符号用于此目的，因此我们需要'sys'

```py
import time, sys

# update_progress() : Displays or updates a console progress bar
## Accepts a float between 0 and 1. Any int will be converted to a float.
## A value under 0 represents a 'halt'.
## A value at 1 or bigger represents 100%
def update_progress(progress):
    barLength = 10 # Modify this to change the length of the progress bar
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done...\r\n"
    block = int(round(barLength*progress))
    text = "\rPercent: [{0}] {1}% {2}".format( "#"*block + "-"*(barLength-block), progress*100, status)
    sys.stdout.write(text)
    sys.stdout.flush()

# update_progress test script
print "progress : 'hello'"
update_progress("hello")
time.sleep(1)

print "progress : 3"
update_progress(3)
time.sleep(1)

print "progress : [23]"
update_progress([23])
time.sleep(1)

print ""
print "progress : -10"
update_progress(-10)
time.sleep(2)

print ""
print "progress : 10"
update_progress(10)
time.sleep(2)

print ""
print "progress : 0->1"
for i in range(101):
    time.sleep(0.1)
    update_progress(i/100.0)

print ""
print "Test completed"
time.sleep(10)

```

这是测试脚本的结果显示的内容（最后一个进度条动画）：

```
progress : 'hello'
Percent: [----------] 0% error: progress var must be float
progress : 3
Percent: [##########] 100% Done...
progress : [23]
Percent: [----------] 0% error: progress var must be float

progress : -10
Percent: [----------] 0% Halt...

progress : 10
Percent: [##########] 100% Done...

progress : 0->1
Percent: [##########] 100% Done...
Test completed
```
第三个案例:
**这个答案不依赖于外部软件包** ，我还认为 *大多数人只想要现成的代码* 。 可以通过自定义以下代码来适应您的需求：进度 `'#'` 条 `size` ， 进度 条 ，文本 `prefix` 等。

```py
import sys

def progressbar(it, prefix="", size=60, file=sys.stdout):
    count = len(it)
    def show(j):
        x = int(size*j/count)
        file.write("%s[%s%s] %i/%i\r" % (prefix, "#"*x, "."*(size-x), j, count))
        file.flush()
    show(0)
    for i, item in enumerate(it):
        yield item
        show(i+1)
    file.write("\n")
    file.flush()

```

用法：

```py
import time

for i in progressbar(range(15), "Computing: ", 40):
    time.sleep(0.1) # any calculation you need

```

输出：

```
Computing: [################........................] 4/15

```

*   *不需要第二个线程* 。 上面的某些解决方案/软件包需要。 例如，第二个线程可能是个问题 `jupyter notebook` 。

*   *与任何可迭代的作品一起工作* 意味着 `len()` 可以使用的 任何东西 。 A `list` ， `dict` 例如a `['a', 'b', 'c' ... 'g']`

*   *与生成器一起使用时，* 只需用list（）包装它即可。 例如 `for i in progressbar(list(your_generator), "Computing: ", 40):`

您还可以通过将文件更改 `sys.stderr` 为例如 来更改输出

### 手打

一个非常简单的方法：

```py
def progbar(count: int) -> None:
    for i in range(count):
        print(f"[{i*'#'}{(count-1-i)*' '}] - {i+1}/{count}", end="\r")
        yield i
    print('\n')

```

以及用法：

```py
from time import sleep

for i in progbar(10):
    sleep(0.2) #whatever task you need to do
```

----

在这里搜索等效解决方案后，我根据自己的需要做了一个简单的进度课程。 我认为我可能会发布它。

```py
from __future__ import print_function
import sys
import re

class ProgressBar(object):
    DEFAULT = 'Progress: %(bar)s %(percent)3d%%'
    FULL = '%(bar)s %(current)d/%(total)d (%(percent)3d%%) %(remaining)d to go'

    def __init__(self, total, width=40, fmt=DEFAULT, symbol='=',
                 output=sys.stderr):
        assert len(symbol) == 1

        self.total = total
        self.width = width
        self.symbol = symbol
        self.output = output
        self.fmt = re.sub(r'(?P<name>%\(.+?\))d',
            r'\g<name>%dd' % len(str(total)), fmt)

        self.current = 0

    def __call__(self):
        percent = self.current / float(self.total)
        size = int(self.width * percent)
        remaining = self.total - self.current
        bar = '[' + self.symbol * size + ' ' * (self.width - size) + ']'

        args = {
            'total': self.total,
            'bar': bar,
            'current': self.current,
            'percent': percent * 100,
            'remaining': remaining
        }
        print('\r' + self.fmt % args, file=self.output, end='')

    def done(self):
        self.current = self.total
        self()
        print('', file=self.output)

```


---

范例：

```py
from time import sleep

progress = ProgressBar(80, fmt=ProgressBar.FULL)

for x in xrange(progress.total):
    progress.current += 1
    progress()
    sleep(0.1)
progress.done()

```

将打印以下内容：

`[======== ] 17/80 ( 21%) 63 to go`

手打案例:
我用 `format()` 方法制作了一个负载条。 这是我的解决方案：

```py
import time

loadbarwidth = 23

for i in range(1, loadbarwidth + 1):
    time.sleep(0.1)

    strbarwidth = '[{}{}] - {}\r'.format(
        (i * '#'),
        ((loadbarwidth - i) * '-'),
        (('{:0.2f}'.format(((i) * (100/loadbarwidth))) + '%'))
    )

    print(strbarwidth ,end = '')

print()

```

输出：

```
[#######################] - 100.00%
```
### 使用enlighten库
您也可以使用 [enlighten](https://pypi.org/project/enlighten) 。 主要优点是您可以同时登录，而不会覆盖进度条。

```py
import time
import enlighten

manager = enlighten.Manager()
pbar = manager.counter(total=100)

for num in range(1, 101):
    time.sleep(0.05)
    print('Step %d complete' % num)
    pbar.update()

```

它还处理多个进度条。

```py
import time
import enlighten

manager = enlighten.Manager()
odds = manager.counter(total=50)
evens = manager.counter(total=50)

for num in range(1, 101):
    time.sleep(0.05)
    if num % 2:
        odds.update()
    else:
        evens.update()
```

### pyprog库
尝试PyProg。 PyProg是Python的开源库，用于创建超级可自定义的进度指示器和栏。

当前版本为1.0.2； 它托管在Github上，可在PyPI上使用（下面的链接）。 它与Python 3＆2兼容，也可以与Qt Console一起使用。

真的很容易使用。 如下代码：

```py
import pyprog
from time import sleep

# Create Object
prog = pyprog.ProgressBar(" ", "", 34)
# Update Progress Bar
prog.update()

for i in range(34):
    # Do something
    sleep(0.1)
    # Set current status
    prog.set_stat(i + 1)
    # Update Progress Bar again
    prog.update()

# Make the Progress Bar final
prog.end()

```

将产生：

```
Initial State:
Progress: 0% --------------------------------------------------

When half done:
Progress: 50% #########################-------------------------

Final State:
Progress: 100% ##################################################

```

我之所以制作PyProg，是因为我需要一个简单但可自定义的进度条库。 您可以使用轻松地安装它 `pip install pyprog` 。

PyProg Github： [https](https://github.com/Bill13579/pyprog) : [//github.com/Bill13579/pyprog](https://github.com/Bill13579/pyprog)
PyPI： [https](https://pypi.python.org/pypi/pyprog/) ://pypi.python.org/pypi/pyprog/

### tqdm方法
使用 [tqdm](https://github.com/tqdm/tqdm) （ `conda install tqdm` 或 `pip install tqdm` ），您可以在一秒钟内将进度表添加到循环中：

```py
from time import sleep
from tqdm import tqdm
for i in tqdm(range(10)):
    sleep(3)

 60%|██████    | 6/10 [00:18<00:12,  0.33 it/s]

```

另外，还有一个 [笔记本版本](https://github.com/tqdm/tqdm/#ipython-jupyter-integration) ：

```py
from tqdm.notebook import tqdm
for i in tqdm(range(100)):
    sleep(3)

```

您可以使用 `tqdm.auto` 而不是 `tqdm.notebook` 在终端机和笔记本电脑上工作。

[`tqdm.contrib`](https://tqdm.github.io/docs/contrib) 包含了一些辅助功能，以做这样的事情 `enumerate` ， `map` 和 `zip` 。 中有并发映射 [`tqdm.contrib.concurrent`](https://tqdm.github.io/docs/contrib.concurrent) 。

使用 [`tqdm.contrib.telegram`](https://tqdm.github.io/docs/contrib.telegram) 或 从Jupyter笔记本电脑断开连接后，您甚至可以将进度发送到手机 [`tqdm.contrib.discord`](https://tqdm.github.io/docs/contrib.discord) 。

![](https://raw.githubusercontent.com/tqdm/img/src/screenshot-telegram.gif)


注： [progressbar2](https://pypi.org/project/progressbar2/) 是叉子 [进度](https://pypi.python.org/pypi/progressbar) 尚未保持多年。

### 使用alive-progress
为了以有用的方式使用任何进度条框架，以获取实际进度百分比和估计的ETA，您需要能够声明将要执行的步骤数。

因此，您的计算功能位于另一个线程中，是否可以将其拆分为多个逻辑步骤？ 您可以修改其代码吗？

您不需要以任何方式进行重构或拆分，您可以 `yield` 在某些地方 放置一些strategy ，或者如果它具有 *for循环* ，只需一个！

这样，您的函数将如下所示：

```py
def compute():
    for i in range(1000):
        time.sleep(.1)  # process items
        yield  # insert this and you're done!

```

然后只需安装：

```
pip install alive-progress

```

并像这样使用它：

```py
from alive_progress import alive_bar

with alive_bar(1000) as bar:
    for i in compute():
        bar()

```

要获得一个很棒的进度条！

```
|█████████████▎                      | ▅▃▁ 321/1000 [32%] in 8s (40.1/s, eta: 16s)

```

免责声明：我是alive\-progress的作者，但它应该可以很好地解决您的问题。 阅读 [https://github.com/rsalmei/alive\-progress上](https://github.com/rsalmei/alive-progress) 的文档 ，这是它可以做什么的示例：

![活着的进步](https://raw.githubusercontent.com/rsalmei/alive-progress/master/img/alive-demo.gif)

![活着的进步](https://raw.githubusercontent.com/rsalmei/alive-progress/master/img/showtime-spinners.gif)

[分享](https://stackoverflow.com/a/57635171/10585157 "简短的永久链接到这个答案")

分享此答案的链接 （包括您的用户ID）

复制链接 [CC BY\-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/ "该帖子的当前许可：CC BY-SA 4.0")

| [编辑](https://stackoverflow.com/posts/57635171/edit "修改和完善此帖子") | 跟随

遵循此答案以接收通知

### Progress模块

从 [https://pypi.python.org/pypi/progress](https://pypi.python.org/pypi/progress) 尝试进度 。

```py
from progress.bar import Bar

bar = Bar('Processing', max=20)
for i in range(20):
    # Do some work
    bar.next()
bar.finish()

```

结果将是如下所示的条形：

```
Processing |#############                   | 42/100
```



## 参考文章
[Python进度条和下载-代码日志](https://stackoverflow.com/questions/15644964/python-progress-bar-and-downloads)
>主要是下载进度条
[星期二工具：太丰富了！](https://bigl.es/tuesday-tooling-thats-rich/)
>把rich的主要功能都列出来了,而且还有详细的案例,可以学习
[使用Rich更好的Python控制台应用程序](https://jslvtr.com/posts/better-python-console-apps-with-rich)
>有非常好的简单案例,适合新手学习
![](https://jslvtr.com/images/better-python-console-apps-with-rich/prompting.gif)
[Python进度栏-堆栈溢出](https://stackoverflow.com/questions/3160699/python-progress-bar)
[进度显示-丰富的9.6.1文档](https://rich.readthedocs.io/en/latest/progress.html)
[rsalmei / alive-progress：一种新型的进度条，具有实时吞吐量，eta和非常酷的动画！](https://github.com/rsalmei/alive-progress)
>一个超级酷炫的进度条模块
![](https://raw.githubusercontent.com/rsalmei/alive-progress/master/img/showtime-spinners.gif)
[适用于Python的控制台进度栏模块](https://pythonawesome.com/a-console-progress-bar-module-for-python/)
[给Python代码加上酷炫进度条的几种姿势_CSDN博客 - MdEditor](https://www.mdeditor.tw/pl/pB2F)
