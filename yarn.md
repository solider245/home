## Yarn

## 安装

```shell
# 使用Npm全局安装
sudo npm install yarn -g 
# Ubuntu/Debian直接用apt安装
sudo apt install yarn 
# 如果失败，就需要添加GPG密钥以及存储库
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
sudo sh -c 'echo "deb https://dl.yarnpkg.com/debian/ stable main" >> /etc/apt/sources.list.d/yarn.list'
sudo apt update
sudo apt install yarn
#查看yarn版本，看看有没有安装成功
yarn --version 
```
## yarn常用命令
```shell
# 使用默认值初始化项目，不加参数y则需要回答一些问题
yarn init -y 
# 添加某个包
yarn add <包名> 
# 添加指定的版本
yarn add package@version-or-tag 
# 全局命令 
yarn global
#你可以使用以下命令将特定依赖项升级到其最新版本，它将查看所​​涉及的包是否具有较新的版本，并且会相应地对其进行更新：
yarn upgrade <包名>
# 清除缓存
yarn cache clean

# 你还可以通过以下方式更改已添加的依赖项的版本：
yarn upgrade package_name@version_or_tag

# 你还可以使用一个命令将项目的所有依赖项升级到它们的最新版本：
yarn upgrade
#你可以通过以下方式从项目的依赖项中删除包：
yarn remove <包名>
# 安装所有项目依赖项
yarn or yarn install 
# 使用以下命令删除 Yarn 及其依赖项。
sudo apt purge yarn
# yarn 升级自己
yarn set version latest
yarn set version from sources
# 你也应该从源列表中把存储库信息一并删除掉：
sudo rm /etc/apt/sources.list.d/yarn.list
# yarn bin位置
yarn global bin 
# yarn全局安装位置
yarn global dir
# yarn安装位置
yarn bin 
```
## 源管理软件yrm
```shell
# 安装
npm install -g yrm
# 查看仓库
yrm ls 
# 增加源
yrm add 仓库名 仓库链接
# 使用某个仓库
yrm use 仓库名
```
## yarn与Npm冲突
npm 的 `bin` 路径在 `C:/Users/james/AppData/Roaming/npm`，所以我曾经想把 yarn 的 `bin` 路径指向这里，然而结果是让我删掉了整个这个目录，重新装了一遍 node.js(为了重装 npm)。

yarn 会把 `C:/Users/james/AppData/Roaming/npm/node_modules` 目录的结构改掉，并把之前用 npm 安装的一些模块删除掉，包括 npm 本身依赖的模块，最终导致 npm 不可用。

总的来说，虽然 npm 和 yarn 作用类似，但它们仍然是不同的两个模块系统，尽可能避免混用，以避免产生冲突。