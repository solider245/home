## ssh常用
用来记录ssh常规用法
## ssh生成/添加SSH公钥
```shell
ssh-keygen -t rsa -C "xxxxx@xxxxx.com" 
#生成公钥，xxxxx@xxxxx.com最好一台电脑一台民资，防止忘记，回车三次即可生成ssh key
cat ~/.ssh/id_rsa.pub
#通过查看 ~/.ssh/id_rsa.pub 文件内容，获取到你的 public key
ssh -T git@gitee.com
#与gitee通信
ssh -T git@github.com
#与github通信
```
*   您可能会看到类似如下的警告：

    ```shell
    > The authenticity of host 'github.com (IP ADDRESS)' can't be established.
    > RSA key fingerprint is SHA256:nThbg6kXUpJWGl7E1IGOCspRomTxdCARLviKw6E5SY8.
    > Are you sure you want to continue connecting (yes/no)?
    ```

*   验证您看到的消息中的指纹匹配步骤 2 中的消息之一，然后输入 `yes`：

    ```shell
    > Hi username! You've successfully authenticated, but GitHub does not
    > provide shell access.
    ```