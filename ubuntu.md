## ubuntu加速
```shell
#先替换                                       
sudo sed -i "s@http://.*archive.ubuntu.com@http://mirrors.huaweicloud.com@g" /etc/apt/sources.list
sudo sed -i "s@http://.*security.ubuntu.com@http://mirrors.huaweicloud.com@g" /etc/apt/sources.list
# 再升级
sudo apt update && sudo apt upgrade -y
```
## 安装中文字体
### locale

### locale查看当前系统默认采用的字符集。

```shell
LANG=en_US.UTF-8
LC_CTYPE="en_US.UTF- 8"                 #用户所使用的语言符号及其分类
LC_NUMERIC="en_US.UTF- 8"               #数字
LC_TIME="en_US.UTF-8"                   #时间显示格式
LC_COLLATE="en_US.UTF-8"                #比较和排序习惯
LC_MONETARY="en_US.UTF-8"               #LC_MONETARY
LC_MESSAGES="en_US.UTF- 8"              #信息主要是提示信息,错误信息,状态信息,标题,标签,按钮和菜单等
LC_PAPER="en_US.UTF- 8"                 #默认纸张尺寸大小
LC_NAME="en_US.UTF-8"                   #姓名书写方式
LC_ADDRESS="en_US.UTF-8"                #地址书写方式
LC_TELEPHONE="en_US.UTF-8"              #电话号码书写方式
LC_MEASUREMENT="en_US.UTF-8"            #度量衡表达方式
LC_IDENTIFICATION="en_US.UTF-8"         #对自身包含信息的概述
LC_ALL=

```

local设定的优先级关系，LC\_ALL是最上级设定或者强制设定（LC\_ALL的值将覆盖所有其他的locale设定），而LANG是默认设定值。

```shell
LC_ALL > LC_* >LANG

```

### 查看系统内安装的locale：

```shell
locale -a
```
```shell
#查看系统类型
cat /proc/version
# 查看中文字体
fc-list :lang=zh-cn
# 安装字体
sudo apt install -y --force-yes --no-install-recommends fonts-wqy-microhei
sudo apt install -y --force-yes --no-install-recommends ttf-wqy-zenhei
# 安装中文语言包
sudo apt-get install -y language-pack-zh-hans language-pack-zh-hans-base
# 替代命令 
sudo apt-get install -y language-pack-zh-han*
# 运行语言支持检查,这个经常失败，可别做
sudo apt install -y $(check-language-support)
# 修改配置文件，支持中/英文环境：
vim /etc/environment 
```
```shell
# 英文
LC_CTYPE=en_US.UTF-8
LC_ALL=en_US.UTF-8
LANG=en_US.UTF-8
LANGUAGE=en_US:en

# 中文
LC_CTYPE=zh_CN.UTF-8
LC_ALL=zh_CN.UTF-8
LANG=zh_CN.UTF-8
LANGUAGE=zh_CN:zh
```
使其生效：

```shell
sudo locale-gen
或
sudo dpkg-reconfigure locales
```