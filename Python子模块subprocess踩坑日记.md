<!--
 * @Author: 中箭的吴起
 * @Date: 2020-11-17 14:55:02
 * @LastEditTime: 2020-11-18 17:45:08
 * @LastEditors: 中箭的吴起
 * @Description: 
 * @FilePath: \studybook\home\Python子模块subprocess踩坑日记.md
 * @日行一善，每日一码
-->
## 序言
用git从github下载东西的速度越来越慢了。每次下载的时候，都要去找一下加速软件转换链接，于是想着能不能做一个软件来替换。这样的话，每次只要放上github原链接就行。再也不用去找其他链接了。
另外，代理的链接并不是很稳定，有时候这个快一些，有时候那个快一些，所以，能不能顺便做一个测速，看看哪个更快就替换哪个？
除非了github加速之外，还能不能顺便把其他常用的软件也加速了？
所以，先定以下几个小目标吧：

**主要目标：**
1. 给git clone加速
2. 使用wget 下载rc的时候，也能加速
3. 下载链接失败的时候，自动跳转到其他链接
3. 通过ping来测速，选择一个最快的代理来加速下载。

**额外目标：**
1. 可以下载常用的软件，比如python,go等
2. 可以加速安装其他软件，比如python、go，或者ubuntu更换源

## 代码实战

```python
import subprocess
subprocess.run(['ls'])
```
打开编辑器，输入以上内容,输出以下内容：
```shell
❯ python hello.py
hello.py
```
1. 导入了subprocess模块
2. 使用了ls命令，得到了当前目录下的文件.

继续添加抬头：
```py
#!/usr/bin/env python
import subprocess
subprocess.run(['ls'])
```
这样的话,文件就变成了一个脚本，同时执行：
```sh
❯ chmod 755 hello.py
❯ ./hello.py
hello.py
```
如上所示，文件可以执行，这样方便后面调用。

### 引入变量cmd

```py
#!/usr/bin/env python
import subprocess

cmd = 'ls'

subprocess.run([cmd])
```
如上所示，依然可以输出。
但是多行命令输出的时候遇到问题。
仔细排查后，发现原因如下：
> run的命令是一个数组，每个参数之间用逗号隔开，等于命令行中的空格。一个完整的命令需要用[]包括起来。，因此正确的写法如下所示

```py
#!/usr/bin/env python
import subprocess

# cmd = 'git clone https://github.com/nodejs/node.git'
cmd = ['git','clone','https://github.com/nodejs/node.git']
subprocess.run(cmd)
```
执行脚本后就可以正确的输出命令到命令行了。

### 使用Url替换了链接
```py
#!/usr/bin/env python
import subprocess

# cmd = 'git clone https://github.com/nodejs/node.git' 这个是错误写法

url = 'https://github.com/nodejs/node.git'

cmd = ['git','clone',url]
subprocess.run(cmd)
```
虽然命令可以执行，但是由于网络问题，node依然无法下载下来。所以我们来寻找代理。

```sh
# 原始链接
url = 'https://github.com/nodejs/node.git'
# 常见四个镜像地址
Cnpm = 'https://github.com.cnpmjs.org/nodejs/node.git'
Fastgit = 'https://hub.fastgit.org/nodejs/node.git'
GitClone =  'https://gitclone.com/github.com/nodejs/node.git'
Cloudflare = 'https://github.91chifun.workers.dev//https://github.com/nodejs/node.git'
```
如上图所示，我们对比一下原始链接与常见四个代理地址的区别，可以得出以下结论：
1. cnpm地址是在原始地址`github.com`后面追加了`.cnpmjs.org`这个字符串
2. fastgit镜像是直接把`github.com`地址替换为了`hub.fastgit.org`
3. gitclone 是在`github.com`前面追加了一个`gitclone.com/`字符串
4. Cloudflare是直接在整个网址前面追加了`https://github.91chifun.workers.dev//`这个字符串

所以，我们新建四个参数
```py
Cnpm加速参数 = '.cnpmjs.org'
Fastgit加速参数 = 'hub.fastgit.org'
GitClone加速参数 =  'gitclone.com/'
Cloudflare加速参数 = 'https://github.91chifun.workers.dev//'
```
### 现在，每位开始拼接地址。

```sh
Cloudflare = Cloudflare加速参数 + url
print(Cloudflare)
```
打印后发现地址正确，接下来我们修改一下参数，并且运行整个代码，看看是否可以通过新的下载地址来下载Node.
经过试验，发现成功，如下图所示：

![20201117165631_085238592f7b0617f1bdfbe2a29756f9.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20201117165631_085238592f7b0617f1bdfbe2a29756f9.png)


### 修改其他地址

Cloudflare的加速很简单，简单的两个字符串拼接就可以了。
其他三个地址，则需要在指定位置插入指定的参数才行。这里，我们通过在列表指定位置插入数据来实现。

列表插入参考代码：
```py
def insertStr():
    # 有一个字符串
    str_1 = '月圆星疏夜,/n'
    # 把字符串转为 list
    str_list = list(str_1)
    # 字符数， 可以利用这个在某个位置插入字符
    #count = len(str_list)
    # 找到 斜杠的位置
    nPos = str_list.index('/')
    # 在斜杠位置之前 插入要插入的字符
    str_list.insert(nPos, '佳人何处在!')
    # 将 list 转为 str
    str_2 = "".join(str_list)
    print(str_2)
```
执行步骤如下：
1. 将字符串序列化为列表并使用一个参数来代替
2. 找到你要插入数据的位置
3. 使用`.insert()`在对应位置插入字符
4. 将列表重新转换成字符串
5. 打印即可

### 修改cnpm代理地址

cnpm的地址是在网址`github.com`后面追加了`.cnpmjs.org`这些字符串。
所以我们先找到原始网址，查看m所在的位置。
经过查询是18.如果你不知道的话，可以输入以下代码：

```py
url_demo = 'https://github.com/nodejs/node.git'
list1 = list(url_demo)

for index,item in enumerate(list1):
    print (index,item)
```
会输出下图：
![20201118161542_de0f6ed5a21c64636bfccca0045b55c3.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20201118161542_de0f6ed5a21c64636bfccca0045b55c3.png)

如果你觉得竖着看不太方便的话，可以在输出的时候追加`end = ""`，这样就会横向打印。
![20201118161739_4f5d570908992ee13eeee7be74795440.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20201118161739_4f5d570908992ee13eeee7be74795440.png)

如上图所示，我个人认为竖着看更直观。

有了位置之后，我们使用列表函数`.insert()`来将参数插入到对应位置。
代码如下：
```py
url_demo = 'https://github.com/nodejs/node.git'
list1 = list(url_demo)
Cnpm加速参数 = '.cnpmjs.org'
list1.insert(18,Cnpm加速参数)
Cnpm_url = ''.join(list1)
# print(Cnpm_url)
```

同理可得，gitclone代理链接应该如下：
```Py
url_demo = 'https://github.com/nodejs/node.git'
list2 = list(url_demo)
gitclone加速参数 = 'gitclone.com/'
list2.insert(8,gitclone加速参数)
gitclone_url = ''.join(list2)
print(gitclone_url)
```

fastgit的代理前面说过，是字符串替换。把`github.com`替换成`hub.fastgit.org`即可。
测试代码如下：

```py
url_demo = 'https://github.com/nodejs/node.git'

fast_url = url_demo.replace('github.com', 'hub.fastgit.org')
print(fast_url)
```