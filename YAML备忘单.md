## 链接

*   [YAML 1.2规格](http://www.yaml.org/spec/1.2/spec.html)
*   [在线YAML Linter（Ruby）](http://www.yamllint.com/)
*   [在线YAML解析器（Python）](http://yaml-online-parser.appspot.com/)
*   [合格\-YAML模式验证器（Ruby）](http://www.kuwata-lab.com/kwalify/)

## YAML语法范例

### YAML标量

标量类型

```
a: 1        # integer
a: 1.234    # float
b: 'abc'    # string
b: "abc"
b: abc
c: false    # boolean type
d: 2015-04-05   # date type

```

强制字符串

```
b: !str 2015-04-05

```

### YAML序列

#### 简单序列

```
 array:
   - 132
   - 2.434
   - 'abc'

```

#### 序列顺序

```
 my_list_of_lists:
   - [1, 2, 3]
   - [4, 5, 6]

```

### YAML哈希

嵌套哈希

```
 my_hash:
   subkey:
     subsubkey1: 5
     subsubkey2: 6
   another:
     somethingelse: 'Important!'

```

使用JSON语法进行哈希处理（可以混合）

```
 my_hash: {nr1: 5, nr2: 6}

```

### YAML HereDoc（多行字符串）

*块符号* ：换行符变为空格

```
content:
   Arbitrary free text
   over multiple lines stopping only
   after the indentation changes...

```

*文字风格* ：保留换行符

```
content: |
   Arbitrary free text
   over "multiple lines" stopping
   after indentation changes...

```

*+指示器* ：在块后保留额外的换行符

```
content: |+
   Arbitrary free text with newlines after

```

*\-指示符* ：阻止后删除多余的换行

```
content: |-
   Arbitrary free text without newlines after it

```

*折叠样式* ：保留折叠的换行符

```
 content: >
   Arbitrary free text
   over "multiple lines" stopping
   after indentation changes...

```

请注意，YAML heredocs是转义特殊字符的方法：

```
 code:
    url: "https://example.com"        # sub key "url" with value 'https://...'

 code: |-                             # versus key "code" having value 'url: "https://..."'
    url: "https://example.com"

```

对于不同的Heredoc模式，有一个不错的在线预览器：https://yaml\-multiline.info/

### 多个文件

一个YAML文件可以有多个文档，这就是为什么每个文档都需要以“ \-\-\-”行开头的原因

```
 ---
 content: doc1
 ---
 content: doc2

```

这也意味着YAML解析器可能返回多个文档！

### 内容参考（别名）

```
 ---
 values:
   - &ref Something to reuse
   - *ref      # Literal "Something to reuse" is inserted here!

```

### 合并键

为像这样的哈希成像一些默认属性

```
 default_settings:
   install:
     dir: /usr/local
     owner: root
   config:
     enabled: false

```

使用“ <<：\*参考”在另一个哈希中使用它们

```
 # Derive settings for 'my_app' from default and change install::owner
 # and add further setting "group: my_group"

 my_app_settings:
   <<: *default_settings
   install:
     owner: my_user
     group: my_group

```

### 复杂映射

```
 ---
 ? - key
 :
   - value

```

注意：键和值可以是哈希语法无法实现的多个复杂结构！


