原文：[medium.com/better-prog…](https://medium.com/better-programming/21-vscode-shortcuts-to-make-coding-faster-and-more-fun-b18b25083def)
> 
> 译者：前端小智

> 点赞再看，养成习惯  
> 
> 本文 **GitHub：**[github.com/qq449245884…](https://github.com/qq449245884/xiaozhi) 上已经收录，更多往期高赞文章的分类，也整理了很多我的文档，和教程资料。欢迎Star和完善，大家面试可以参照考点复习，希望我们一起有点东西。

> **注意**：自己尝试的时候，Mac(17, pro) 与原文提供的快捷键盘不太一样，mac 对应的 Ctrl 要换成 command

做为前端开发者来说，大都数都用过 VSCode，并且也有很多是经常用的。但 VSCode 的一些快捷键可能我们不知道，也比较少用，毕竟这很好，因此本文就列出一些快捷键方便大家学习与记忆。

在这篇文章中，我将列出我最喜欢的快捷键，这些快捷键让我更快的编写代码，也让编码变得更有趣，以下是21 个 VSCode 快捷键，分享给你。
<!-- more -->
1\. 一次搜索所有文件的文本
---------------

> Windows: Ctrl + Shift + F Mac: Command + Shift + F

VSCode中我最喜欢的特性之一是能够在项目目录中的所有文件中搜索任何匹配的文本。

要使用此特性，可以按`Ctrl + Shift + f`打开视图，它将显示编辑器左侧的侧边栏：

![](https://user-gold-cdn.xitu.io/2019/7/22/16c17005bac46453?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

输入查找的內容并回车，VS code 将提供与输入内容匹配的结果列表，如下所示：

![](https://user-gold-cdn.xitu.io/2019/7/22/16c17006b977aee8?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

你还可以同时规制每个搜索果文件中的所有匹配内容。如果你单击左边的这个小箭头，它将在下面弹出第二个输入框，可以在这里输入要替换的文本，同时单击右边出现的小框：

![](https://user-gold-cdn.xitu.io/2019/7/22/16c170080b8bbaff?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

2.为 tabs 设置强调色（Material Theme）
------------------------------

你是否厌倦了每天看到相同的 tabs 底部颜色?可以使用 [Material Theme](https://marketplace.visualstudio.com/items?itemName=Equinusocio.vsc-material-theme) 来扩展 VsCode 的主题，这样就可以为 tabs 设置不同的颜色。

**红色：**

![](https://user-gold-cdn.xitu.io/2019/7/22/16c1700957cb662f?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

**紫色**

![](https://user-gold-cdn.xitu.io/2019/7/22/16c1700a7985aae8?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

**黄色**

![](https://user-gold-cdn.xitu.io/2019/7/22/16c1700bc7a85f5b?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

有16种不同的颜色可供选择。

因此，如果胸有安装此扩展，打开的命令面板(`Ctrl + Shift + P`)，选择 `Material Theme: Set accent color`并从列表中选择一个颜色，它将更改选项卡的下划线颜色，如下所示

![](https://user-gold-cdn.xitu.io/2019/7/22/16c170122b807f57?imageslim)

3.进程资源管理器
---------

你是否发现你的VsCode 编辑器有时有点慢?这时候你希望哪个进程在吃我们的内存？

好吧，如果你还不知道，VsCode 有一个进程资源管理器功能，如下所示：

![](https://user-gold-cdn.xitu.io/2019/7/22/16c170156292e487?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

是不是看起来很熟悉？

在**windows**[任务管理器](https://www.howtogeek.com/66622/stupid-geek-tricks-6-ways-to-open-windows-task-manager/?source=post_page---------------------------)中看到过这一点，在VsCode 中按`Ctrl + Alt + Delete`可以打开该任务管理器。

4.Expand Bracket Selection
--------------------------

打开键盘快捷键(`Ctrl + Shift + P` `或 command + Shift + p`)，搜索 `Expand Bracket Selection`。

这是我需要花费一些时间才能发现的，因为我无法猜出该功能的名称。使用此功能可以自动选择整个块，从开始的大括号到结束。

![](https://user-gold-cdn.xitu.io/2019/7/22/16c1701963a7d908?imageslim)

我发现这个功能在想要找到 `if/else` 对应的结束块很有用。

5\. 重新打开 关闭的编辑页面
----------------

> Windows: Ctrl + Shift + T Mac: command + Shift + T

当你处理一个文件很多的大型项目时，如果不小心关闭了一个页面，并且不得不在侧菜单中再次搜索它，这可能会有点令人沮丧。

现在，可以按 `Ctrl + Shift + T` 重新打开一个关闭的页面。

6\. 通过匹配文本打开文件
--------------

> Windows: Ctrl + T Mac: command + T

说到搜索文件，你可以动态地搜索和打开文件。这是我最喜欢的特性之一，因为不需要手动单击目录来重新打开一个不再打开的文件。

7\. 集成终端
--------

> Windows: Ctrl + `Mac: control +`

通过 \*\*Ctrl + `\*\*可以打开或关闭终端

8\. 查看正在运行插件
------------

你可以通过打开命令面板(`Ctrl + Shift + P`)并输入`Show running extensions`来查看所有你安装的正在运行的插件。

![](https://user-gold-cdn.xitu.io/2019/7/22/16c1701c1014caad?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

9\. 重新加载
--------

我个人认为这是 VsCode 最酷的特性之一。它允许你在重新加载编辑器时将窗口放在前面，同时具有与关闭和重新打开窗口相同的效果。

> Windows: Ctrl + Alt + R Mac: Control + Option + R

10\. 将选项卡交换到不同的组
----------------

在我开发的过程中，我习惯在错误的选项卡组中使用选项卡。 我也希望避免尽可能多地使用我的鼠标来解决问题，因为这会让我把手从键盘上抬起来，我很懒，手一起想放键盘上。

幸运的是，VsCode 有一种方法可以通过按`Ctrl + Alt +右箭头`（Mac：`Control + Option +右箭头）`将标签移动到右侧的组，或者按`Ctrl + Alt + 左箭头`将标签转移到单独的标签组 （Mac：`Control + Option +左箭头）`）将标签移动到左侧的组：

![](https://user-gold-cdn.xitu.io/2019/7/22/16c1701ff2b6a35b?imageslim)

11.选择左侧/右侧的所有内容
---------------

有时你想要删除光标右侧或左侧的所有内容。 你可以选择光标右侧或左侧的所有内容。例如，要选择右侧或左侧的所有内容：

> Windows: Ctrl + Shift + Home/End Mac: command + Shift + Home/End

苹果笔记本没home键，可以用组合键实现

*   fn键+左方向键是HOME
*   fn键+右方向键是END
*   fn+上方向键是page up
*   fn+下方向键是page down

![](https://user-gold-cdn.xitu.io/2019/7/22/16c17023a6418c3f?imageslim)

12.删除上一个单词
----------

要删除前一个单词，可以按`Ctrl + Backspace` (Mac: `option + delete`)。这在你打错字的时候非常有用。

![](https://user-gold-cdn.xitu.io/2019/7/22/16c170265b52cbc9?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

你可以在 VsCode 之外的任何地方使用它。

![](https://user-gold-cdn.xitu.io/2019/7/22/16c17029dea01d5b?imageslim)

13\. 启动性能
---------

有时候，缺乏关于性能问题的详细信息是一件非常痛苦的事情，同时还要找出哪些有性能问题。

有时候，如果你足够幸运，你会找到一个工具，它能给你所有的答案。在VsCode 中，启动性能是很重要的。这就是为什么你能弹出一个有用的窗口，奇迹般地提供所有你需要的信息：

![](https://user-gold-cdn.xitu.io/2019/7/22/16c1702b8e21e0ba?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

打开命令面板(`Ctrl + Shift + P`)，搜索S`tartup Performance`。

14.逐个选择文本
---------

可以通过快捷键`Ctrl + Shift +右箭头`(Mac: `option + Shift +右箭头`)和`Ctrl + Shift +左箭头`(Mac: option + Shift +左箭头)逐个选择文本。

15\. 重复的行
---------

一个非常强大和已知的功能是复制行。只需按 `Shift + Alt + 向下箭头` (Mac: `command + Shift + 向下箭头`)

![](https://user-gold-cdn.xitu.io/2019/7/22/16c1702ebf85905e?imageslim)

16.移至文件的开头/结尾
-------------

要使光标移到文件的第一行或最后一行，最快的方法是按`Ctrl + Home` (`Mac: command + Home`)键开头，然后按`Ctrl + End` (Mac: `command + End`)键结尾。

![](https://user-gold-cdn.xitu.io/2019/7/22/16c170328dad7f74?imageslim)

17\. 批量替换当前文件中所有匹配的文本
---------------------

可以选择任何一组文本，如果该选中文本出现多个，可以通过按`Ctrl + F2` (Mac: `command + F2`)一次改所有出现的文本。

![](https://user-gold-cdn.xitu.io/2019/7/22/16c17038f9bd0412?imageslim)

18\. 向上/向下移动一行
--------------

按`Alt + 向上箭头`(Mac: `option+ 向上箭头`)当前行向上移动，按`Alt + 向下箭头`(Mac: `option+ 向下箭头`))当前行向下移动。

![](https://user-gold-cdn.xitu.io/2019/7/22/16c1703c6aebcd36?imageslim)

19\. 删除一行
---------

有两种方法可以立即删除一行。

使用`Ctrl + X`剪切命令(`Mac：command + X`)来删除一行。

![](https://user-gold-cdn.xitu.io/2019/7/22/16c1704986bf49e1?imageslim)

或者使用 `Ctrl + Shift + K` (Mac: `command + Shift + K`)命令。

![](https://user-gold-cdn.xitu.io/2019/7/22/16c1704e0a2e293d?imageslim)

20.将编辑器向左或向右移动
--------------

如果你像我一样，你可能会有一种无法控制的欲望，想要在一个组中重新排列选项卡，其中选项卡相互关联，左边的选项卡是比较重要文件，而右边的选项卡是相对不重要的文件。 通过 Ctrl+Shift+PgUp/PgDown(command + +Shift+PgUp/PgDown)向左/向右移动编辑器。

![](https://user-gold-cdn.xitu.io/2019/7/22/16c1705155c6b655?imageslim)

21\. 复制光标向上或者向上批量添加内容
---------------------

在 VsCode 中复制游标可以证明是最节省时间的特性。

![](https://user-gold-cdn.xitu.io/2019/7/22/16c170549b1d2320?imageslim)

按`Ctrl + Alt +向上箭头`(Mac: `command + Option +向上箭头`)将光标添加到上面，按`Ctrl + Alt +向下箭头`(Mac: `command + Option + 向下箭头`)将光标添加到下面。

* * *

**代码部署后可能存在的BUG没法实时知道，事后为了解决这些BUG，花了大量的时间进行log 调试，这边顺便给大家推荐一个好用的BUG监控工具 [Fundebug](https://www.fundebug.com/?utm_source=xiaozhi)。**