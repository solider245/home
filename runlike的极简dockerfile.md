# 序言

看到一个docker周边软件runlike。发现他的dockerfile写的特别的好，所以就在这里备份一下.

```dockerfile
FROM docker

RUN apk add --no-cache python2 py-setuptools \
  && /usr/bin/easy_install-2.7 pip \
  && pip install runlike

ENTRYPOINT ["runlike"]
```