## 学习Node.js：初学者指南

毫无疑问，JavaScript无疑是当今最流行的编程语言之一。 它可以轻松地在浏览器，服务器，台式机甚至手机上作为应用程序运行。 其中最流行和最简单的方法JavaScript是用写 [Node.js的](https://nodejs.org/en/) 。

有很多学习Node.js的资源，但是并没有真正能为您提供成功编写Node代码所需的背景，工具和资源。

因此，我的目标是为您提供一份刚入门时希望得到的指导。 我将简要描述一下Node实际上是什么以及它在幕后正在做什么，然后为您提供一些具体示例，您可以在浏览器中尝试一下，最后，我将为您提供一些指导您完成一些更有用和更实际的示例/概念的资源。

请注意，本指南不会教您如何编码，而是会指导您了解Node运行时和npm的基础知识。

### 什么是节点

Node是在 [V8 JavaScript引擎](https://en.wikipedia.org/wiki/V8_%28JavaScript_engine%29) 上运行的服务器端跨平台运行时环境，该 [引擎](https://en.wikipedia.org/wiki/V8_%28JavaScript_engine%29) 为Google的Chrome浏览器提供了动力。 这实际上是Node的心脏，并且是实际解析和执行代码的组件。

V8引擎通过将JavaScript编译为本地机器代码来实现此目的，这使其比解释器快得多。 为了进一步加快处理速度，已编译的代码会在运行时根据代码执行配置文件的启发式方法进行动态优化（和重新优化）。 这意味着在程序运行时，引擎实际上会跟踪其性能，并根据所跟踪的某些因素使代码更快。

作为运行时，Node的主要重点是使用事件驱动的非阻塞IO模型来使其轻巧快速。 对于某些人来说，这种编程模型起初可能有些混乱，但实际上在简化诸如网站之类的重型IO应用程序的开发方面做得很好。

此设计是优化代码的吞吐量和可伸缩性的理想选择，这是代码如此流行的主要原因。 例如，有人得到它来处理 [600,000个并发Websocket连接](http://www.jayway.com/2015/04/13/600k-concurrent-websocket-connections-on-aws-using-node-js/) ，这很疯狂。 现在，他不得不做一些自定义配置，但这并没有使它印象深刻。 这就是为什么像IBM，Microsoft和PayPal这样的公司将Node用于其Web服务的原因。

现在，Node甚至不需要快速使其具有吸引力。 我最喜欢的功能之一是软件包管理器 [npm](https://npmjs.com/) 。 许多语言都缺乏像这样的优秀软件包管理器。 npm是一个命令行工具，可用于初始化模块，管理依赖项或运行测试等。

![npm徽标](https://s3.amazonaws.com/stackabuse/media/npm-logo-small.png)

公共存储库对任何人开放，供其下载和发布代码。 截至撰写本文时，npm托管着超过210,000个模块，从网站到命令行工具再到API包装器。

[这是](https://www.npmjs.com/package/camo) 我创建的程序包 [的示例](https://www.npmjs.com/package/camo) 。 您可以看到主页是README，它描述了软件包的功能以及使用方法。 您还可以快速了解其他信息，例如下载数量，存储库位置和使用的软件许可证。

#### 什么节点适合

除其他外，Node可能最适合用于构建需要实时，同步交互的网站和工具。 聊天站点/应用程序就是一个很好的例子，因为它们通常是大量的IO。 非阻塞事件驱动模型允许它同时处理大量请求。

对于创建Web API的前端（通过REST）也非常有用。 这是因为它针对事件驱动的IO（我已经讲过）进行了优化， *并且* 本机处理JSON，因此几乎不需要解析。

#### 是什么节点 **并不** 好

在另一方面，让我们看看Node *不* 擅长 什么 。 最值得注意的是，它非常不适合执行繁重的计算任务。 因此，如果您想 [使用Node进行机器学习之类的工作](https://stackabuse.com/neural-networks-in-javascript-with-brain-js/) ，那么您可能不会获得最佳体验。

Node也还很年轻，因此仍处于快速发展中。 在过去的几个月中，我们已经从 `v0.12.x` 转变为 `v5.1.x` 。 因此，如果您需要更稳定的东西，那么可能不适合您。

至于异步编程的“问题”，我认为 [Quora答案](http://qr.ae/RbHnFE) 的第一部分 很好地解释了这一问题：

> 缺乏固有的代码组织是一个巨大的劣势。 当整个开发团队对异步编程或标准设计模式不熟悉时，这种情况尤其会恶化。 有太多的方法使代码变得不规则和不可维护。

尽管异步编程总体上是一件好事，但确实会增加程序的复杂性。

### 节点REPL

好的，继续一些代码。 我们将非常简单地开始，只需在REPL（read\-eval\-print循环）中运行一些命令，这只是一个应用程序，可让您在shell中交互地运行Node代码。 此处编写的程序是分段执行的，而不是一次执行。

我假设您已经熟悉JavaScript，因此在本文中我们将仅介绍一些特定于Node的内容。

让我们尝试一下Node随附的内置模块之一，例如该 `crypto` 模块。

假设您已经安装了Node，请 `node` 在外壳 程序中运行 命令，并在提示符下逐行键入以下代码：

```javascript
var crypto = require('crypto');

crypto.createHash('md5').update('hello world').digest('hex');

```

在REPL中输入最后一行后，（或单击上面的“运行”按钮），您应该看到 *5eb63bbbe01eeed093cb22bb8f5acdc3* 打印到控制台。

该 `crypto` 模块是使用 `require()` 函数 加载的 ，该函数可以为您处理代码的解析和加载。 有关如何 [在这里](https://nodejs.org/api/modules.html#modules_modules) 工作的更多信息 。

加载模块后，您就可以使用其功能了，在这种情况下，我们将使用 `createHash()` 。 由于REPL分段执行代码，因此它们通常会打印出每一行的返回值，就像您在此处看到的那样。

您可以使用像这样的REPL快速测试代码，而不必将其写入新文件并执行。 这几乎就像一个通用沙箱环境。

### 您的第一个程序

REPL很有趣，但是它们只会带我们到现在为止。 因此，让我们继续编写第一个真实的Node程序。 我们现在还不必担心使用第三方模块（但是不用担心，稍后我们会），因此让我们首先看看对我们而言内置的代码是什么。 已经为您提供了大量代码，包括（但不限于）：

*   `fs` ：通过标准POSIX功能提供的简单包装器
*   `http` ：较低级别的HTTP服务器和客户端
*   `os` ：提供一些基本方法来告诉您有关底层操作系统的信息
*   `path` ：用于处理和转换文件路径的实用程序
*   `url` ：URL解析和解析的实用程序
*   `util` ：标准实用程序功能，例如调试，格式化和检查

内置的代码量不在Python级别上，但是可以完成。 真正的好处是当您开始进入第三方模块时。

广告

对于我们的第一个程序，我们将创建一个简单的实用程序，该实用程序使用您的IP地址来确定您的位置（我知道这有点令人毛骨悚然）：

```javascript
var http = require('http');

var options = {
    hostname: 'ipinfo.io',
    port: 80,
    path: '/json',
    method: 'GET'
};

var req = http.request(options, function(res) {
    var body = '';

    res.setEncoding('utf8');
    res.on('data', function(chunk) {
        body += chunk;
    });

    res.on('end', function() {
        var json = JSON.parse(body);
        console.log('Your location: ' + json.city + ', ' + json.region);
    });
});

req.end();

```

复制上面的代码，并将其粘贴到名为“ index.js”的文件中。 然后，在命令行上，导航到包含刚创建的文件的目录，并使用以下命令运行它：

```bash
$ node index.js

```

您应该在命令行上看到“您的位置：\[城市\]，\[地区\]”。 打印出的城市/地区可能与您很接近，但不完全准确。 另外，如果没有打印城市/地区，则意味着您的IP信息不在数据库中。

由于此代码不使用任何第三方依赖关系，因此不需要 `package.json` 文件或 `node_modules` 文件夹，我们将在下一节中对其进行详细说明。

### 您的第一个包裹

*请注意，在本文中，我交替使用“包”和“模块”。*

对于使用Node.js创建的几乎每个网站/工具/项目，您还需要围绕它创建一个模块。 这样，您就可以指定依赖项，测试，脚本，存储库等。

一个典型的模块包含一些重要的内容：

*   [package.json](https://docs.npmjs.com/files/package.json) ：包含所有模块信息的JSON文件
*   node\_modules /：包含所有依赖项的目录
*   index.js：主代码文件
*   README.md：有关该模块的文档
*   test /：模块的测试目录

您可以向模块添加大量其他内容（例如.npmignore文件，docs目录或编辑器配置文件），但是上面列出的内容是您将看到的一些最常见的内容。

为了展示所有这些工作原理，在本节的其余部分中，我们将在上一个示例的基础上创建自己的包。

我们将不仅仅根据IP地址告诉您您的位置，而是使用一些流行的Node包来创建一个工具，使您可以找到任何网站服务器的位置。 我们将其称为 `twenty` （ [看看为什么](https://www.urbandictionary.com/define.php?term=what%27s+your+20%3F) ）。

#### 初始化包

首先，为您的项目创建并导航到新目录：

#### 订阅我们的新闻

在收件箱中获取临时教程，指南和作业。 从来没有垃圾邮件。 随时退订。

订阅电子报

订阅

感谢您订阅“滥用堆栈”电子邮件列表！

```bash
$ mkdir twenty
$ cd twenty

```

然后，使用npm初始化项目：

```bash
$ npm init

This utility will walk you through creating a package.json file.
It only covers the most common items, and tries to guess sane defaults.

See `npm help json` for definitive documentation on these fields
and exactly what they do.

Use `npm install <pkg> --save` afterwards to install a package and
save it as a dependency in the package.json file.

Press ^C at any time to quit.
name: (twenty)
version: (0.0.1)
description: Locates the city/region of a given URL/IP address
entry point: (index.js)
test command:
git repository:
keywords:
license: (MIT)
About to write to /Users/scott/projects/twenty/package.json:

{
  "name": "twenty",
  "version": "0.0.1",
  "description": "Locates the city/region of a given URL/IP address",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "Scott Robinson <scott@stackabuse.com> (http://stackabuse.com)",
  "license": "MIT"
}

Is this ok? (yes) yes

```

填写每个提示（从开始 `name: (twenty)` ），或者不输入任何内容，只需按回车即可使用默认设置。 这将创建一个 `package.json` 包含以下JSON 的正确配置的 文件：

```json
{
  "name": "twenty",
  "version": "0.0.1",
  "description": "Locates the city/region of a given URL/IP address",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "Scott Robinson <scott@stackabuse.com> (http://stackabuse.com)",
  "license": "MIT"
}

```

此文件是保存所有特定于项目的信息的起点。

#### 安装依赖

要将依赖项添加到项目中，可以 `npm install` 从命令行 使用 命令。 例如，要添加第一个依赖项 `request` ，请尝试运行以下命令：

```bash
$ npm install --save request

```

该 `install` 命令 `request` 将从npm 下载最新的 软件包并将其保存在 `node_modules` 目录中。 添加 `--save` 标志告诉npm将软件包详细信息保存在package.json中的“ dependencies”部分下：

```json
"dependencies": {
    "request": "2.67.0"
}

```

现在，您将可以 `request` 在项目代码中的任何地方 使用该 模块。

#### 改进代码

该 `request` 模块为您提供了轻松实现各种HTTP请求的功能。 上面显示的HTTP示例还不错，但是 `request` 使代码更加紧凑和易于阅读。 使用的等效代码 `request` 如下所示：

```javascript
var request = require('request');

request('http://ipinfo.io/json', function(error, response, body) {
    var json = JSON.parse(body);
    console.log('Your location: ' + json.city + ', ' + json.region);
});

```

如果我们可以找到 *任何* IP地址 的位置 ，而不仅仅是我们自己的IP地址 的位置，那将更加有趣 ，因此让我们允许用户输入IP地址作为命令行参数。 像这样：

```bash
$ node index.js 8.8.8.8

```

广告

要在我们的程序中访问此参数，Node使其在全局 `process` 对象中作为 可用 `process.argv` ，它是一个数组。 对于我们上面刚刚执行的命令， `process.argv` 将为 `['node', 'index.js', '8.8.8.8']` 。

为了使事情变得更加简单，我们将使用 [yargs](https://www.npmjs.com/package/yargs) 包来帮助我们解析命令行参数。 使用这样的简单程序 `yargs` 并不是必须的，但是我将 `twenty` 在以后的文章中 进行改进 ，因此我们不妨现在添加它。

就像 `request` ，我们将使用以下命令进行安装：

```bash
$ npm install --save yargs

```

修改 `yargs` 用于获取参数 的代码 （如果未提供参数，则默认为我们自己的IP），最终得到以下结果：

```javascript
var request = require('request');
var argv = require('yargs').argv;

var path = 'json';

path = argv._[0] || path;

request('http://ipinfo.io/' + path, function(error, response, body) {
    var json = JSON.parse(body);
    console.log('Server location: ' + json.city + ', ' + json.region);
});

```

到目前为止，此工具非常适合命令行使用，但是如果有人想将其用作自己代码中的依赖项，该怎么办？ 到目前为止，该代码尚未导出，因此您将无法在命令行以外的其他任何地方使用它。 要告诉Node哪些功能/变量可用，我们可以使用 `module.exports` 。

```javascript
var request = require('request');
var argv = require('yargs').argv;

var findLocation = function(ip, callback) {
    var path;
    if (typeof(ip) === 'function' || !ip) path = 'json';
    else path = ip;

    request('http://ipinfo.io/' + path, function(error, response, body) {
        var json = JSON.parse(body);
        callback(null, json.city + ', ' + json.region);
    });
};

module.exports = findLocation;

```

大！ 现在，任何下载此软件包的人都可以在其代码中的任何位置使用它并使用该 `findLocation()` 功能。

但是，您可能已经注意到，现在我们不能再将其用作命令行工具了。 但是，我们 **不** 想像这样将其余的旧代码放在那里：

```javascript
var request = require('request');
var argv = require('yargs').argv;

var findLocation = function(ip, callback) {
    var path;
    if (typeof(ip) === 'function' || !ip) path = 'json';
    else path = ip;

    request('http://ipinfo.io/' + path, function(error, response, body) {
        var json = JSON.parse(body);
        callback(null, json.city + ', ' + json.region);
    });
};

var arg = argv._[0] || path;

// This runs every time the file is loaded
findLocation(arg, function(err, location) {
    console.log('Server location: ' + location);
});

module.exports = findLocation;

```

这很不好，因为任何时候只要有人 `require()` 使用此文件，该 `findLocation()` 功能就会在命令行中打印自己的位置。 我们需要一种方法来确定该文件是否 *直接* 使用 `node index.js` 而不是通过 `require()` 调用，因此，如果*直接* 调用了该文件 ，我们将在命令行中检查参数。 可以通过 `require.main` 针对 进行检查 `module` ，例如： `if (require.main === module) {...}` ，从而使我们拥有：

```javascript
var request = require('request');
var argv = require('yargs').argv;

var findLocation = function(ip, callback) {
    var path;
    if (typeof(ip) === 'function' || !ip) path = 'json';
    else path = ip;

    request('http://ipinfo.io/' + path, function(error, response, body) {
        var json = JSON.parse(body);
        callback(null, json.city + ', ' + json.region);
    });
};

if (require.main === module) {
    findLocation(argv._[0], function(err, location) {
        console.log('Server location: ' + location);
    });
}

module.exports = findLocation;

```

现在，我们既可以在命令行上 *也可以* 作为依赖项 使用此代码 。

*注意：有一种更好的方法来执行CLI /库混合，但是我们将使其保持简单并暂时使用此方法。有关更多信息，请参见Github上的[二十](https://github.com/scottwrobinson/twentyjs)，特别是`bin`目录和`package.json`设置。*

#### 发布您的包

最后，我们希望在npm上将其提供给其他人。 您需要做的所有事情来使软件包可用在软件包目录中运行：

```bash
$ npm publish

```

系统将提示您输入用户名和密码，然后将代码推送到注册表。

请记住， 由于我已经使用了“二十”这个名称 ，因此您将需要确定 软件包的 [范围](https://docs.npmjs.com/misc/scope) 或更改其名称。

### 从这往哪儿走

随着Node的迅速普及， 整个Internet上 都有 *大量* 资源。 这是我遇到的一些最受欢迎的书籍和课程，它们将比我在这里展示的内容教给您更多的东西：

*   通过Wes Bos**[学习Node.js](http://stackabu.se/wes-bos-learn-node)**
*   **[Node.js Web开发：使用节点10进行服务器端开发](http://stackabu.se/node-js-web-dev-server-side-dev-node-10)**
*   **[Node.js设计模式：掌握构建模块化和可扩展的服务器端Web应用程序的最佳实践](http://stackabu.se/node-js-design-patterns-master-best-practices)**
*   **[开始Node.js](http://stackabu.se/beginning-node-js-book)**

或者，如果您希望坚持使用一些简短的教程，那么以下一些来自Stack Abuse的教程可能会有所帮助：

*   [避免Node.js中的回调地狱](https://stackabuse.com/avoiding-callback-hell-in-node-js/)
*   [您可能不知道的有用节点程序包](https://stackabuse.com/useful-node-packages-you-might-not-know-about/)
*   [ES6类](https://stackabuse.com/es6-classes/)
*   [使用node\-cron在Node中运行定期任务](https://stackabuse.com/run-periodic-tasks-in-node-with-node-cron/)

请记住，您学习的绝大部分内容将来自您自己对语言，工具和软件包的探索。 因此，尽管类似这样的文章适合入门，但请确保您专注于编写代码，而不是 *阅读其他人的代码* 。 经验胜过其他一切。

### 结论

我们仅介绍了Node和npm所提供的一小部分，因此请查看我上面链接到的一些资源以了解更多信息。

而且，我无法强调获得实际编写代码的经验对您来说有多重要。 npm使浏览软件包和查找其存储库变得非常容易。 因此，找到一个对您有用或有趣的软件包，并查看其工作原理。

*您是Node的新手吗？您还想了解其他哪些Node主题？让我们在评论中知道！*