
## 如何在Node.js中使用module.exports

使用模块是使用Node.js构建完整的应用程序和软件系统的重要组成部分。 在没有模块的情况下，您的代码将分散且难以运行，更不用说随着时间的推移而进行维护了。 但是什么是模块？ 以及您究竟应该如何使用它 `module.exports` 来构建Node.js程序？

模块是一个离散程序，包含在Node.js的单个文件中。 因此，模块与文件绑定，每个文件一个模块。 模块提供其他编程语言。 Node.JS使用CommonJS模块系统，但是JavaScript生态系统中还使用了其他模块类型。 这些其他模块系统中最突出的是异步模块定义（AMD）和（ECMAScript 6）ES6模块系统。

正如我们将看到的， `module.exports` 是当前模块在另一个程序或模块中“需要”时返回的对象。

您可以在任何其他模块中包括来自其他模块的功能。 这样做被称为“要求”模块，它只是在调用表示模块功能的特定对象。

### 与module.exports共享代码

对于日常使用，模块使我们可以从较小的部分组成较大的程序。 他们定义了模块，这些 [模块共同](https://nodejs.org/api/modules.html) 成为更大的软件的基本构建块。

在幕后，该模块通过名为的对象跟踪自身 `module` 。 因此，在每个模块内部，“模块”是指代表当前模块的对象。 该对象保存有关模块的元数据，例如模块的文件名以及模块的ID。

下面是一些代码片段，您可以运行它们以查看模块上这些示例属性的值：

```javascript
// module1.js

console.log(module.filename);
console.log(module.id);
console.log(module.exports);

```

您可以使用命令运行此命令 `node module1.js` 。 例如，您将看到该 `module.filename` 属性设置为文件路径，该文件路径以此模块所在的文件的正确名称结尾 `module1.js` 。 这是上面代码的示例输出：

```sh
$ node module1.js
/Users/scott/projects/sandbox/javascript/module-test/module1.js
.
{}

```

在Node.js中，使模块的代码可供其他模块使用的做法称为“导出”值。

但是，如果要从单个模块构建一件复杂的软件，则您可能已经在考虑下一个问题：

**重要问题1** ：Node.js如何识别“主要”模块以开始运行程序？

Node.js通过传递给 `node` 可执行文件 的参数来标识要运行的主模块 。 例如，如果我们在 **server.js** 文件中包含一个模块，而在该程序的 其他部分在 **login.js** 和 **music\_stream.js** 文件中包含 `node server.js` 该 模块 ，则调用该命令 会将 **服务器** 模块 标识 为主模块。 反过来，该主模块将通过“要求”其他模块来调用其他模块的功能。

**重要问题2** ：一个模块如何与其他模块共享其代码？

该 `module` 对象具有一个称为的特殊属性， `exports` 该 属性 负责定义一个模块使其他模块可以使用的内容。 用Node.js术语 `module.exports` 定义模块导出的值。 请记住，“导出”只是使对象或值可用于其他模块的导入和使用。

因此，我们可以通过将其附加为 `module.exports` 对象 的属性来导出任何要导出的值或函数或其他 对象。 例如，如果我们要导出名为的变量 `temperature` ，则可以通过简单地将其添加为新属性，使它可在模块外部使用 `module.exports` ：

```javascript
module.exports.temperature = temperature;

```

### 使用module.exports导出和请求函数和变量

既然我们已经了解了模块的概念含义以及使用模块的原因，那么让我们通过实际创建模块，定义函数，然后导出这些函数以便可以被其他模块使用的方式，将这些想法付诸实践。

例如，这是一个提供书本推荐的新模块。 在下面的代码中，我定义了一个变量以及一些函数。 稍后，我们将从另一个模块访问这些推荐书的功能。

```javascript
// book_recommendations.js

// stores the favorite author in a constant variable
const favoriteAuthor = { name: "Ken Bruen", genre: "Noir", nationality: "Irish" };

// returns the favorite book
function favoriteBook() {
    return { title: "The Guards", author: "Ken Bruen" };
}

// returns a list of good books
function getBookRecommendations() {
    return [
        {id: 1, title: "The Guards", author: "Ken Bruen"},
        {id: 2, title: "The Stand", author: "Steven King"},
        {id: 3, title: "The Postman Always Rings Twice", author: "James M. Cain"}
    ];
}

// exports the variables and functions above so that other modules can use them
module.exports.favoriteAuthor = favoriteAuthor;
module.exports.favoriteBook = favoriteBook;
module.exports.getBookRecommendations = getBookRecommendations;

```

我们添加了所有要导出 `module.exports` 为对象属性 的变量和函数 。 我们刚刚完成了从 **book\_recommendations** 模块 导出这些函数和变量的**目标** 。

现在让我们看看如何导入该模块并从另一个模块访问其功能。

为了导入模块，我们需要使用一个特殊的关键字来导入事物，它被称为 [require](https://medium.freecodecamp.org/requiring-modules-in-node-js-everything-you-need-to-know-e7fbd119be8) 。 其中 `module.exports` 让我们的出口设置的东西， `require` 让我们指定要导入到当前模块的模块。

名为的模块中提供了用于导入模块的功能，该模块 `require` 在全局范围内可用。 该模块的主要导出是一个函数，我们可以将要导入的模块的路径传递给该函数。 例如，要导入在中定义的模块 `music.js` ，我们将 `require('./music')` 在其中指定了相对路径。

现在我们可以看到使用导入任何内容有多么容易 `require` 。 回到我们的 `book_recommendations` 模块，我们可以导入它并访问它导出的功能。 这显示在下一个代码清单中。 此模块打印一条消息，描述推荐的生日礼物。 它从导入的书推荐模块中获取推荐书，并将它们与音乐推荐结合在一起。

如下所示创建一个新模块，然后使用导入的 **book\_recommendations** 模块中 定义的功能，如先前所示运行它以查看它 。

```javascript
// birthday_gifts.js

// import the book recommendations module
let books = require('./book_recommendations');

// gets some music recommendations as well
let musicAlbums = [
    { artist: "The Killers", title: "Live From The Royal Albert Hall" },
    { artist: "Eminem", title: "The Marshall Mathers LP" }
];

// the two best items from each category
let topIdeas = function() {
    return [musicAlbums[0], books.favoriteBook()];
}

// outputs a message specifying the customer's recommended gifting items
let gifts = function() {
    console.log("Your recommended gifts are:\n");
    console.log("######MUSIC######");

    for (let i = 0, len = musicAlbums.length; i < len; i++) {
        console.log(musicAlbums[i].title + " by " + musicAlbums[i].artist);
    }

    console.log("######BOOKS######");

    let recommendedBooks = books.getBookRecommendations();

    for (let i = 0, len = recommendedBooks.length; i < len; i++) {
        console.log(recommendedBooks[i].title + " by " + recommendedBooks[i].author);
    }

    console.log("\n\nYours");
    console.log("Shop Staff\n*************");
    console.log("P.S. If you have a limited budget, you should just get the music album " + topIdeas()[0].title + " and the book " + topIdeas()[1].title + ".");
}

console.log("Welcome to our gift shop.\n");

// Get the gifts
gifts();

```

#### 订阅我们的新闻

在收件箱中获取临时教程，指南和作业。 从来没有垃圾邮件。 随时退订。

订阅电子报

订阅

如您所见，我们曾经 `require` 导入 **book\_recommendations** 模块。 在新模块内部，我们可以通过将变量和函数添加到中来访问已导出的变量和函数 `module.exports` 。

完成这两个模块后，调用会 `node birthday_gifts.js` 打印出一条整洁的消息，其中包含客户的完整礼物建议。 您可以在下图中看到输出。

```text
Welcome to our gift shop.

Your recommended gifts are:

######MUSIC######
Live From The Royal Albert Hall by The Killers
The Marshall Mathers LP by Eminem
######BOOKS######
The Guards by Ken Bruen
The Stand by Steven King
The Postman Always Rings Twice by James M. Cain

Yours
Shop Staff
*************
P.S. If you have a limited budget, you should just get the music album Live From The Royal Albert Hall and the book The Guards.

```

从较小的模块组成Node.js程序的这种模式是您经常会看到的东西，例如 [Express中间件](https://stackabuse.com/how-to-write-express-js-middleware/) 。

### 使用module.exports导出和请求类

除了函数和变量，我们还可以使用 `module.exports` 导出其他复杂的对象，例如类。 如果您不熟悉使用类或其他Node.js基础知识，则可以查看我们的 [Node.js初学者指南](https://stackabuse.com/learn-node-js-a-beginners-guide/) 。

在下面的示例中，我们创建一个Cat类，其中包含Cat对象的名称和年龄。 然后，通过将Cat类附加为 `module.exports` 对象 的属性来导出它 。 如您所见，这与我们之前导出函数和变量的方式没有什么不同。

```javascript
// cat.js

// constructor function for the Cat class
function Cat(name) {
    this.age = 0;
    this.name = name;
}

// now we export the class, so other modules can create Cat objects
module.exports = {
    Cat: Cat
}

```

现在，我们可以通过导入 `cat` 模块 来访问此Cat类 。 完成后，我们可以创建新的Cat对象，并在导入模块中使用它们，如以下示例所示。 同样，您应该尝试运行此代码， `node cat_school.js` 以在命令提示符下查看新猫的名称和年龄。

```javascript
// cat_school.js

// import the cat module
let cats = require('./cat');
let Cat = cats.Cat;

// creates some cats
let cat1 = new Cat("Manny");
let cat2 = new Cat("Lizzie");

// Let's find out the names and ages of cats in the class
console.log("There are two cats in the class, " + cat1.name + " and " + cat2.name + ".");
console.log("Manny is " + cat1.age + " years old " +  " and Lizzie is " + cat2.age + " years old.");

```

广告

如我们所见，导出类可以通过将类附加为 `module.exports` 对象 的属性来完成 。 首先，我们使用构造函数创建了一个类。 然后，我们使用导出了该类 `module.exports` 。 要使用该类，我们随后需要在另一个模块中使用它，然后创建该类的实例。

有关导出使用ES6语法创建的类的示例，请参见 `Book` 下面 的 类。

### 另一种选择：使用简写导出VS模块.exports

虽然我们可以继续将要导出的内容分配为的属性 `module.exports` ，但是存在一种从模块导出内容的简便方法。 此速记方式涉及使用just `exports` 代替 `module.exports` 。 [两者之间](https://medium.freecodecamp.org/node-js-module-exports-vs-exports-ec7e254d63ac) 有一些 [区别](https://medium.freecodecamp.org/node-js-module-exports-vs-exports-ec7e254d63ac) 。 但是，这里要注意的关键是，您 **必须** 将新值分配为快捷方式 `export` 对象的 属性 ，而不是直接分配对象以覆盖其 `export` 自身 的值 。

这是一个示例，其中我使用这种 **简化方法** 从名为**film\_school** 的模块中导出了几个对象 。

```javascript
// film_school.js

// a beginner film course
let film101 = {
    professor: 'Mr Caruthers',
    numberOfStudents: 20,
    level: 'easy'
}

// an expert film course
let film102 = {
    professor: 'Mrs Duguid',
    numberOfStudents: 8,
    level: 'challenging'
}

// export the courses so other modules can use them
exports.film101 = film101;
exports.film102 = film102;

```

请注意我们是如何分配的对象，例如， `exports.film101 = ...` 而不是 `exports = film101` 。 以后的分配不会导出变量，但是会完全破坏快捷方式的导出。

以上面的简写方式完成的导出，可以通过我们使用的长格式来实现，并 `module.exports` 使用以下几行进行导出。

```javascript
// export the courses so other modules can use them
module.exports.film101 = film101;
module.exports.film102 = film102;

```

我们也可以通过直接将对象分配给来导出两个对象， `module.exports` 但这不适用于 `exports` 。

```javascript
// export the courses so other modules can use them
module.exports = {
    film101: film101,
    film102: film102
}

```

两者非常相似，这是正确的。 这是完成同一件事的两种方法，但是 `exports` 如果您将一个对象分配为要分配给的出口方式，可能会使您绊倒 `module.exports` 。

### Node.js模块和ES6模块之间的差异

Node.js中使用的模块遵循称为 [CommonJS](http://requirejs.org/docs/commonjs.html) 规范 的模块 规范。 以ES6形式对JavaScript编程语言进行的最新更新指定了对语言的更改，并添加了诸如新类语法和 [模块系统之类的内容](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import) 。 该模块系统不同于Node.js模块。 ES6中的模块如下所示：

```javascript
// book.js
const favoriteBook = {
    title: "The Guards",
    author: "Ken Bruen"
}

// a Book class using ES6 class syntax
class Book {
    constructor(title, author) {
        this.title = title;
        this.author = author;
    }

    describeBook() {
        let description = this.title + " by " + this.author + ".";
        return description;
    }
}

// exporting looks different from Node.js but is almost as simple
export {favoriteBook, Book};

```

要导入此模块，我们将使用ES6 `import` 功能，如下所示。

```javascript
// library.js

// import the book module
import {favoriteBook, Book} from 'book';

// create some books and get their descriptions
let booksILike = [
    new Book("Under The Dome", "Steven King"),
    new Book("Julius Ceasar", "William Shakespeare")
];

console.log("My favorite book is " + favoriteBook + ".");
console.log("I also like " + booksILike[0].describeBook() + " and " + booksILike[1].describeBook());

```

ES6模块看起来几乎和我们在Node.js中使用的模块一样简单，但是它们与Node.js模块不兼容。 这与在两种格式之间以不同方式加载模块有关。 如果使用 [Babel](https://babeljs.io/) 这样的编译器 ，则可以混合和匹配模块格式。 但是，如果您打算仅使用Node.js在服务器上进行编码，则可以坚持使用我们之前介绍的Node.js的模块格式。

### 学到更多

想更多地了解Node.js的基础知识？ 就个人而言，我建议您使用在线课程，例如 [Wes Bos的Learn Node.js，](https://stackabu.se/wes-bos-learn-node) 因为这些视频更易于观看，您实际上将可以构建一个实际应用程序。

### 结论

使用 `module.exports` 允许我们从Node.js模块导出值，对象和样式。 结合使用 `require` 导入其他模块，我们拥有一个完整的生态系统，可以从较小的部分组成大型程序。 当我们将负责功能独特部分的许多模块组合在一起时，我们可以创建更大，更有用但易于维护的应用程序和软件系统。