常用工具库

相比原生Node.js，使用一些开源模块能够简化CLI工具的开发，提高开发效率。如下是开发CLI工具常用的模块：

*   commander: 注册、解析命令行参数
*   Inquirer: 让命令行与用户进行交互
*   chalk: 给命令行字符加颜色
*   shelljs: 跨平台调用shell命令的node封装
*   Ora: 命令行提示图标
*   progress: 命令行进度条
*   blessed-contrib: 命令行可视化组件
*   download-git-repo：拉取git仓库源代码