## 序言

部署lsyncd的时候，发现各种坑，这里做一个汇总。

## ```Host key verification failed```主机密钥验证失败

问题产生原因：
> 主要是因为lsyncd部署的时候默认的是使用的root用户的密钥。如果和其他服务器连接的时候不是root用户，就会产生密钥验证失败的情况。

问题解决办法：
```
sync {
  -- ...
  rsync = {
    rsh = "ssh -i /home/user/.ssh/id_rsa -o UserKnownHostsFile=/home/user/.ssh/known_hosts"
  }
}
```
> 使用rsh参数指定你的密钥。user记得替换成你自己的用户名

## rsyncssh与rsync的区别

请看下面两个案例：
```
lsyncd -rsync /home remotehost.org::share/
lsyncd -rsyncssh /home remotehost.org backup-home/
```
通常来说简单使用第一个例子就好，但是使用ssh的话，当你变更文件夹的时候，他会使用Mv命令远程移动文件夹，这样的话你的同步速度就要快得多。
文件少的时候无所谓，多的话，移动起来速度差异就可以看的出来了。

## 使用direct来同步本地目录 
如果仅仅是本地目录同步的话，那使用direct是最好的。

## targetdir与target写法上的区别

```lua
  target = "username@server.com:/home/projectA",
-- 下面是targetdir的写法
  host = "username@server.com",
  targetdir = "/home/projectA"
```
双方的差别就在于远程地址是否要分开。

## 启动lsyncd的方法

配置文件写好后可以输入以下代码：
```shell
sudo lsyncd -log Exec /etc/lsyncd.conf # 具体根据你的配置文件来
sudo service lsyncd restart 
sudo systemctl lsyncd start 
```

## 查看日记
```shell
cat /var/log/lsyncd/lsyncd.log
```
通过查看日记，你可以解决绝大多数问题。

## 查看官方案例学习
如果你忘记怎么写的话，官方提供了足够的案例，你可以进入以下目录来查看。
```shell
cd /usr/share/doc/lsyncd/examples 
ls 
```

## 重点参数说明：

```
--        # 注释符
settings  # 是全局配置
sync      # 定义同步参数
rsync     # 定义同步文件参数
ssh       # 定义服务器远程端口

```
## 参考文章

[主机密钥验证失败·问题＃345·axkibe / lsyncd](https://github.com/axkibe/lsyncd/issues/345)
[如何使用lsyncd镜像VPS上的本地和远程目录。 数字海洋](https://www.digitalocean.com/community/tutorials/how-to-mirror-local-and-remote-directories-on-a-vps-with-lsyncd#how-to-configure-remote-syncing-with-lsyncd)
[大神教你：Lsyncd复制并实时同步到远程服务器_Linuxprobe18的博客-CSDN博客_lsyncd](https://blog.csdn.net/Linuxprobe18/article/details/80554000)
> 这篇文章写了一些常见参数以及如何开机自启的办法
