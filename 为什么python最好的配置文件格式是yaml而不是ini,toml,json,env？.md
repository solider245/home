## 序言
最近学习Python,到了写配置文件这一步了。
起因是因为自己开发git加速软件，发现很多变量存放在python文件里不好管理，而且看上去也不美观。因此打算寻找一种办法，可以在一个文件里单独存放变量然后共享给其他py文件使用。
这篇文章就是最近总结下来的结果。

## 五种常见python配置文件与优缺点
**`python`有下列五种常见配置文件格式**：
1. ini
1. toml
1. yaml 
1. json 
1. env

一句话总结下各个配置文件的优缺点：
* ini 特别适合小型项目，他的配置写起来简单，看起来直观。但是缺点是不支持变量嵌套，这使得一旦面对稍微复杂或者大型的项目就会显得很吃力
* toml 可以算是升级的ini配置文件。缺点是写嵌套项目的时候很累，需要把每一个配置的子项目写清楚，但是通常是没必要的。
* yaml 现在最流行也是最通用的配置项目格式。他不但易于阅读，而且通过空白来定义变量的层次结构。
* json json的问题是看起来不是那么的直观，和yaml比起来，当然，你也可以根据自己的爱好来选择
* env 应用面比较窄，通常是在命令行中写入。

你可以通过图片来查看五种常见配置文件的格式：
ini :

![20201217174752_1490e62c11a7db9f64c91d38f3b7be06.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20201217174752_1490e62c11a7db9f64c91d38f3b7be06.png)

toml:

![20201217175000_7619ca6e6b4f24f1be628ae8811de8f8.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20201217175000_7619ca6e6b4f24f1be628ae8811de8f8.png)

yaml:
```yaml
appName: appName
logLevel: WARN

AWS:
    Region: us-east-1
    Resources:
      EC2:
        Type: "AWS::EC2::Instance"
        Properties:
          ImageId: "ami-0ff8a91507f77f867"
          InstanceType: t2.micro
          KeyName: testkey
          BlockDeviceMappings:
            -
              DeviceName: /dev/sdm
              Ebs:
                VolumeType: io1
                Iops: 200
                DeleteOnTermination: false
                VolumeSize: 20
      Lambda:
          Type: "AWS::Lambda::Function"
          Properties:
            Handler: "index.handler"
            Role:
              Fn::GetAtt:
                - "LambdaExecutionRole"
                - "Arn"
            Runtime: "python3.7"
            Timeout: 25
            TracingConfig:
              Mode: "Active"

routes:
  admin:
    url: /admin
    template: admin.html
    assets:
        templates: /templates
        static: /static
  dashboard:
    url: /dashboard
    template: dashboard.html
    assets:
        templates: /templates
        static: /static
  account:
    url: /account
    template: account.html
    assets:
        templates: /templates
        static: /static

databases:
  cassandra:
    host: example.cassandra.db
    username: user
    password: password
  redshift:
    jdbcURL: jdbc:redshift://<IP>:<PORT>/file?user=username&password=pass
    tempS3Dir: s3://path/to/redshift/temp/dir/
  redis:
    host: hostname
    port: port-number
    auth: authentication
    db: databaseconfig.yaml
```
json配置文件：
```json
{
   "appName": "appName",
   "logLevel": "WARN",
   "AWS": {
      "Region": "us-east-1",
      "Resources": {
         "EC2": {
            "Type": "AWS::EC2::Instance",
            "Properties": {
               "ImageId": "ami-0ff8a91507f77f867",
               "InstanceType": "t2.micro",
               "KeyName": "testkey",
               "BlockDeviceMappings": [
                  {
                     "DeviceName": "/dev/sdm",
                     "Ebs": {
                        "VolumeType": "io1",
                        "Iops": 200,
                        "DeleteOnTermination": false,
                        "VolumeSize": 20
                     }
                  }
               ]
            }
         },
         "Lambda": {
            "Type": "AWS::Lambda::Function",
            "Properties": {
               "Handler": "index.handler",
               "Role": {
                  "Fn::GetAtt": [
                     "LambdaExecutionRole",
                     "Arn"
                  ]
               },
               "Runtime": "python3.7",
               "Timeout": 25,
               "TracingConfig": {
                  "Mode": "Active"
               }
            }
         }
      }
   },
   "routes": {
      "admin": {
         "url": "/admin",
         "template": "admin.html",
         "assets": {
            "templates": "/templates",
            "static": "/static"
         }
      },
      "dashboard": {
         "url": "/dashboard",
         "template": "dashboard.html",
         "assets": {
            "templates": "/templates",
            "static": "/static"
         }
      },
      "account": {
         "url": "/account",
         "template": "account.html",
         "assets": {
            "templates": "/templates",
            "static": "/static"
         }
      }
   },
   "databases": {
      "cassandra": {
         "host": "example.cassandra.db",
         "username": "user",
         "password": "password"
      },
      "redshift": {
         "jdbcURL": "jdbc:redshift://<IP>:<PORT>/file?user=username&password=pass",
         "tempS3Dir": "s3://path/to/redshift/temp/dir/"
      },
      "redis": {
         "host": "hostname",
         "port": "port-number",
         "auth": "authentication",
         "db": "database"
      }
   }
}
```

## 通过configparser来写Ini配置文件

1. 创建一个`config.ini` 文件
```ini
[APP]
ENVIRONMENT = development
DEBUG = False

[DATABASE]
USERNAME = root
PASSWORD = p@ssw0rd
HOST = 127.0.0.1
PORT = 5432
DB = my_database

[LOGS]
ERRORS = logs/errors.log
INFO = data/info.log

[FILES]
STATIC_FOLDER = static
TEMPLATES_FOLDER = templates
```
2. 导入`configparser`模块然后开始调用
```py
"""Load configuration from .ini file."""
import configparser


# 读取 `config.ini` 配置文件.
config = configparser.ConfigPars()                                     
config.read('settings/config.ini')

# 从配置文件获得值的两种方式
config.get('DATABASE', 'HOST')
config['DATABASE']['HOST']
```
### 其他配置详解
项目中的配置文件，它是整个项目共用的。所以它要有一个项目使用的文件名，其后缀是.ini。例如：端口配置.ini

1）读取配置文件

*   read(filename) 直接读取ini文件内容
*   sections() 得到所有的section，并以列表的形式返回
*   options(section) 得到该section的所有option
*   items(section) 得到该section的所有键值对
*   get(section,option) 得到section中option的值，返回为string类型
*   getint(section,option) 得到section中option的值，返回为int类型，还有相应的getboolean()和getfloat() 函数。

2）写入配置文件

*   add\_section(section) 添加一个新的section
*   set( section, option, value) 对section中的option进行设置，需要调用write将内容写入配置文件

### configparser常用方法汇总
**config.read(filename,encoding)** 直接读取ini文件内容,finlename 文件地址，encoding 文件编码格式

**config.sections()** 得到所有的section，并以列表的形式返回

**config.options(section)** 得到该section的所有option

**config.items(section)** 得到该section的所有键值对

**config\[section\]\[option\]** 读取section中的option的值

**config.get(section,option)** 得到section中option的值，返回为string类型

**config.getint(section,option)** 得到section中option的值，返回为int类型

**config.getboolean(section,option)** 得到section中option的值，返回为bool类型

**config.getfloat(section,option)** 得到section中option的值，返回为float类型


## toml配置文件写法与使用
1. 创建`config.toml`文件
```toml
# Keys
title = "My TOML Config"

# Tables
[project]
name = "Faceback"
description = "Powerful AI which renders the back of somebody's head, based on their face."
version = "1.0.0"
updated = 1979-05-27T07:32:00Z
author = "Todd Birchard"

[database]
host = "127.0.0.1"
password = "p@ssw0rd"
port = 5432
name = "my_database"
connection_max = 5000
enabled = true

# Nested `tables`
[environments]
  [environments.dev]
    ip = "10.0.0.1"
    dc = "eqdc10"
  [environments.staging]
    ip = "10.0.0.2"
    dc = "eqdc10"
  [environments.production]
    ip = "10.0.0.3"
    dc = "eqdc10"

# Array of Tables
[[testers]]
id = 1
username = "JohnCena"
password = "YouCantSeeMe69"

[[testers]]
id = 3
username = "TheRock"
password = "CantCook123"
```
2. 导入toml模块并开始读取数据：
```py
"""Load configuration from .toml file."""
import toml


# Read local `config.toml` file.
config = toml.load('settings/config.toml')
print(config)

# Retrieving a dictionary of values
config['project']
config.get('project')

# Retrieving a value
config['project']['author']
config.get('project').get('author')
```

## 使用第三方模块confuse,pyyaml来写yaml配置

* [(Python *Confuse* 库)](https://github.com/sampsyo/confuse)
* [混淆：无痛配置-混淆1.4.0文档](https://confuse.readthedocs.io/en/latest/usage.html)

### confuse模块使用案例
1. 导入模块并定义配置文件位置
```py
"""Load configuration from .yaml file."""
import confuse

config = confuse.Configuration('MyApp', __name__)
runtime = config['AWS']['Lambda']['Runtime'].get()
print(runtime)
```
更多案例可以查询[(confuse文档)](https://confuse.readthedocs.io/en/latest/)
2. Confuse还进入了构建CLI的领域，允许我们使用YAML文件来通知可传递给CLI的参数及其潜在值
```py
config = confuse.Configuration('myapp')
parser = argparse.ArgumentParser()

parser.add_argument('--foo', help='a parameter')

args = parser.parse_args()
config.set_args(args)

print(config['foo'].get())
```
### 使用pyyaml模块来读写配置文件
1. 安装 
```
pip install pyyaml
```
2. 创建两个示例文件items.yaml与data.yaml 
```yaml 
# items.yaml文件
raincoat: 1
coins: 5
books: 23
spectacles: 2
chairs: 12
pens: 6
```

```yaml 
# data.yaml 
cities:
  - Bratislava
  - Kosice
  - Trnava
  - Moldava
  - Trencin
---
companies:
  - Eset
  - Slovnaft
  - Duslo Sala
  - Matador Puchov
```
>中有两个文件data.yaml。文件用分隔---

3. 创建read_yaml.py文件，用来读取yaml 
```py
import yaml

with open('items.yaml') as f:
    
    data = yaml.load(f, Loader=yaml.FullLoader)
    print(data)
```

我们打开 `items.yaml` 文件并使用该 `yaml.load()` 方法 加载内容 。 数据被打印到控制台。
输出内容如下：
```shell
$ python read_yaml.py
{'raincoat': 1, 'coins': 5, 'books': 23, 'spectacles': 2, 'chairs': 12, 'pens': 6}

```
#### 读取多个文档的方法：
```py
#!/usr/bin/env python3

import yaml

with open('data.yaml') as f:
    
    docs = yaml.load_all(f, Loader=yaml.FullLoader)

    for doc in docs:
        
        for k, v in doc.items():
            print(k, "->", v)
#该示例从data.yaml文件中读取两个文档。
```
```
$ python read_docs.py
cities -> ['Bratislava', 'Kosice', 'Trnava', 'Moldava', 'Trencin']
companies -> ['Eset', 'Slovnaft', 'Duslo Sala', 'Matador Puchov']
```
#### 写入yaml文件的方法
```py
#!/usr/bin/env python3

import yaml

users = [{'name': 'John Doe', 'occupation': 'gardener'},
         {'name': 'Lucy Black', 'occupation': 'teacher'}]

with open('users.yaml', 'w') as f:

    data = yaml.dump(users, f)

#该示例将字典列表写入 `users.yaml` 文件。

data = yaml.dump(users, f)

#我们使用该 `dump()` 方法 写入数据 。 第一个参数是数据，第二个参数是文件对象

```

## Dynaconf：Pyhton项目的动态配置

>上面介绍了常见的项目配置方法，最有介绍一个好用的python动态项目配置库：Dynaconf。dyanconf是OSM（Object Settings Mapper）, 能够从不同的配置数据存储方式中读取配置，例如python配置文件、系统环境变量、redis、ini文件、json文件等等。

### 使用方法
```py
# pip install dynaconf 安装
from dynaconf import settings
print settings.SOME_VARIABLE
or
print settings.get('SOME_VARIABLE')
```
>如果不希望配置跟随项目，可以通过系统环境变量来指定配置文件的位置

```shell
# using module name
export DYNACONF_SETTINGS=myproject.production_settings
# or using location path
export DYNACONF_SETTINGS=/etc/myprogram/settings.py

```
### 通过redis存储配置
我们也可以讲配置文件存储到redis中，达到在对不同的机器共享环境变量的效果，仅需要在settings.py文件中增加一下代码：

```py
# connection
REDIS_FOR_DYNACONF = {
    'host': 'localhost',
    'port': 6379,
    'db': 0
}

# and loader
LOADERS_FOR_DYNACONF = [
    'dynaconf.loaders.env_loader',
    'dynaconf.loaders.redis_loader' # 增加了redis的加载
]

```

现在可以讲配置存储到redis中，hash默认为DYNACONF_DYNACONF。dyanconf还提供了方法去将配置写入到redis：

```py
from dynaconf.utils import redis_writer
from dynaconf import settings

redis_writer.write(settings,name='test',mysql_host='localhost', MYSQL_PORT=3306)
```
查看Redis,存储的结果如下：
```shell
DYNACONF_DYNACONF:
    NAME='test'
    MYSQL_HOST='localhost'
    PORT='@int 3306'

```

## env配置文件写法指南
```ini
FLASK_ENV=development
FLASK_APP=wsgi.py
COMPRESSOR_DEBUG=True
STATIC_FOLDER=static
TEMPLATES_FOLDER=templates
```
读取env文件的方法：
```py 
"""App configuration."""
from os import environ, path
from dotenv import load_dotenv


# Find .env file
basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))


# General Config
SECRET_KEY = environ.get('SECRET_KEY')
FLASK_APP = environ.get('FLASK_APP')
FLASK_ENV = environ.get('FLASK_ENV')
```




## 参考文献
* [使用INI，TOML，YAML和ENV文件配置Python项目](https://hackersandslackers.com/simplify-your-python-projects-configuration/)
>非常好的文章，解答了我对配置文件使用哪种的困惑
* [如何在Python中解析YAML文件-代码日志](https://stackoverflow.com/questions/1773805/how-can-i-parse-a-yaml-file-in-python)
> 举了大量详实的案例，非常适合学习
* [python解析ini、conf、cfg文件 - jk409 - OSCHINA - 中文开源技术交流社区](https://my.oschina.net/jk409/blog/317766)
>很详细的解释了configparser模块的各种用法以及优缺点，而且中文友好
* [python 配置文件解析_爪哇人的博客-CSDN博客_python配置文件解析](https://blog.csdn.net/qq_34745295/article/details/79438774)
> 中文友好,xml文件格式也有,不过介绍的最多的还是configparser模块对配置文件读写的方法
* [15.8. configparser — 配置文件操作 |《Python 3 标准库实例教程》| Python 技术论坛](https://learnku.com/docs/pymotw/configparser-work-with-configuration-files/3455)
> 内容十分详细,基本可以算是中文文档了
* [用configparser模块读写ini配置文件 | Python笔记](https://www.pynote.net/archives/314)
* [configparser模块的高级用法 | Python笔记](https://www.pynote.net/archives/556)
>很详细的介绍了如何读写配置文件,而且是中文文档
* [python标准库 configparser读取config或ini配置文件 - 知乎](https://zhuanlan.zhihu.com/p/33218385)
> 中文友好，非常適合第一次看的小白了解前因后果
* [Day 6 - 编写配置文件 - 廖雪峰的官方网站](https://www.liaoxuefeng.com/wiki/1016959663602400/1018490750237280)
> 廖雪峰的网站，有一个实际中的举例，可以参考
* [python 配置文件读写 - 简书](https://www.jianshu.com/p/aadd86f8d38e)
> 有python2的写法参考
* [PYTHON多模块文件共享变量 - 简书](https://www.jianshu.com/p/4bb742d7d672)
>不使用模块解决变量共享的方法
* [13.10 读取配置文件 — python3-cookbook 3.0.0 文档](https://python3-cookbook.readthedocs.io/zh_CN/latest/c13/p10_read_configuration_files.html)
> 中文友好，写的非常详细，但是聚焦于读配置文件
* [Python YAML教程-在Python中使用YAML](http://zetcode.com/python/yaml/)
>非常好的说明了pyyaml对配置文件的读写以及如何序列化，加索引还有api调用的方法
* [﻿Python项目读取配置的正确姿势 - 知乎](https://zhuanlan.zhihu.com/p/54764686)
>最强的地方在于将配置保存在redis并且进行读写