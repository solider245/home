## 序言
看到一个很简单的shell下载脚本,保存下来,以后有用

## 原文转载

我一直在寻找一种使用方式， `pathogen` 但需要轻松更新并使其可移植，因此 `bash` 脚本（使用 `vim-plug` 功能） 可能会有所帮助 \-

```sh
#!/bin/sh

# inspired by https://github.com/thoughtbot/dotfiles/blob/master/hooks/post-up

# 命令解析:如果目标文件不存在,就从远程地址下载到目标文件并创建文件夹
if [ ! -e "$HOME"/.vim/autoload/pathogen.vim ]; then
  curl -fLo "$HOME"/.vim/autoload/pathogen.vim --create-dirs \
      https://raw.githubusercontent.com/tpope/vim-pathogen/master/autoload/pathogen.vim
fi

# 命令解析:如果目标文件存在,直接执行命令;
# 否则从远程地址下载文件到目标地址;
# 最后再执行一个命令
if [ -e "$HOME"/.vim/autoload/plug.vim ]; then
  vim -E -s +PlugUpgrade +qa
else
  curl -fLo "$HOME"/.vim/autoload/plug.vim --create-dirs \
      https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi
vim -u "$HOME"/.vimrc.bundles +PlugUpdate +PlugClean! +qa
```

## 讲解
上面这个脚本以后可以改良后用来下载文件,并且应用到自己的软件里. 