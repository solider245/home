## 限制wsl2占用过多内存

### 三种解决方式
* 重启wsl2
* 使用.wslconfig文件限制资源
* 调整内核参数定期释放cache内存

### 重启wsl2
```shell
wsl --shutdown
```
### 限制wsl2内存使用
>这个解决方案来自github，简单来说就是创建一个%UserProfile%\.wslconfig文件来限制wsl使用的内存总量。比如说我在Windows中使用的用户是tinychen，那么我就在C:\Users\tinychen中创建了一个.wslconfig文件，在里面加入以下内容来限制wsl2的内存总大小：
```powershell
[wsl2]
processors=8
memory=8GB
swap=8GB
localhostForwarding=true
```
注意修改完成之后需要重启wsl2才能生效。更多详细的配置可以查看官方文档.

### 定期释放cache内存
>Linux内核中有一个参数/proc/sys/vm/drop_caches，是可以用来手动释放Linux中的cache缓存，如果发现wsl2的cache过大影响到宿主机正常运行了，可以手动执行以下命令来释放cache：
```shell
echo 3 > /proc/sys/vm/drop_caches

```
当然也可以设置成定时任务，每隔一段时间释放一次。