## Go语言
## 安装
```shell
#以 root 或者其他 sudo 用户身份运行下面的命令，下载并且解压 Go 二进制文件到/usr/local目录：
wget -c https://dl.google.com/go/go1.14.2.linux-amd64.tar.gz -O - | sudo tar -xz -C /usr/local
# 国内用户
wget -c https://studygolang.com/dl/golang/go1.15.4.linux-amd64.tar.gz -O - | sudo tar -xz -C /usr/local
# go语言中文地址 https://studygolang.com/dl
# 这个可以通过添加下面的行到/etc/profile文件（系统范围内安装）或者$HOME/.profile文件（当前用户安装）：
export PATH=$PATH:/usr/local/go/bin
# 保存文件，并且重新加载新的PATH 环境变量到当前的 shell 会话：
source /etc/profile
# 验证安装
go version
```
## go加速
```shell
# 常规设置
go env -w GO111MODULE=on
go env -w GOPROXY=https://goproxy.cn,direct
# macOS 或 Linux
export GO111MODULE=on
export GOPROXY=https://goproxy.cn
# 或者
echo "export GO111MODULE=on" >> ~/.profile
echo "export GOPROXY=https://goproxy.cn" >> ~/.profile
source ~/.profile
# Windows 打开powershell
C:\> $env:GO111MODULE = "on"
C:\> $env:GOPROXY = "https://goproxy.cn"
```
## go语言常用命令
```shell
# 运行go文件
go run hello.go 
# 打包
go build.go
# 安装软件
go get package
```

## 参考文章
[Goproxy 中国](https://goproxy.cn/)
[Go下载 - Go语言中文网 - Golang中文社区](https://studygolang.com/dl)