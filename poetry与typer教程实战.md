## 序言
使用typer开发了一个软件,但是却发现里面有非常多的坑,这里做个备忘.

## poetry构建与发布流程

### 1. 使用Poetry创建一个项目rick-portal-gun.

```
poetry new rick-portal-gun
```
![20210111150424_936b8cdd49020e78095bfb9c078274d2.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210111150424_936b8cdd49020e78095bfb9c078274d2.png)

### 2. 使用poetry安装typer 
```
poetry add typer
```
### 3. 激活虚拟环境
```
poetry shell
```
安装完成后会如下图所示.
![20210111234254_5683e4f3f64122621cdcd0363954a984.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210111234254_5683e4f3f64122621cdcd0363954a984.png)

### 4. 创建main.py文件
现在，让我们创建一个非常简单的 **Typer** 应用程序。

使用以下方法创建文件 `rick_portal_gun/main.py` ：
```py
import typer

app = typer.Typer()

@app.callback()
def callback():
    """
 Awesome Portal Gun
 """

@app.command()
def shoot():
    """
 Shoot the portal gun
 """
    typer.echo("Shooting portal gun")

@app.command()
def load():
    """
 Load the portal gun
 """
    typer.echo("Loading portal gun")
```    


提示:
在创建可安装的Python程序包时，无需添加带有的部分 `if __name__ == "__main__:` 。

当前目录结构如下图:
![20210111235725_ac736b9f68c1a4ee2a83399774a139da.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210111235725_ac736b9f68c1a4ee2a83399774a139da.png)

### 5. 修改自述文件README.rst为README.md.
编辑文件`pyproject.toml`,增加行:
```diff
[tool.poetry]
name = "rick-portal-gun"
version = "0.1.0"
description = ""
authors = ["Rick Sanchez <rick@example.com>"]
+readme = "README.md"

[tool.poetry.dependencies]
python = "^3.6"
typer = {extras = ["all"], version = "^0.1.0"}

[tool.poetry.dev-dependencies]
pytest = "^5.2"

[build-system]
requires = ["poetry>=0.12"]
build-backend = "poetry.masonry.api"
```

### 6. 增加可执行脚本
还是编辑配置文件`pyproject.toml`
```diff 
[tool.poetry]
name = "rick-portal-gun"
version = "0.1.0"
description = ""
authors = ["Rick Sanchez <rick@example.com>"]
readme = "README.md"

+[tool.poetry.scripts]
+rick-portal-gun = "rick_portal_gun.main:app"
# 复制的时候去掉加号即可

[tool.poetry.dependencies]
python = "^3.6"
typer = {extras = ["all"], version = "^0.1.0"}

[tool.poetry.dev-dependencies]
pytest = "^5.2"

[build-system]
requires = ["poetry>=0.12"]
build-backend = "poetry.masonry.api"
```
代码讲解(这里很重要):
* `rick-portal-gun` ：将是CLI程序的名称。 这就是安装终端后我们将在终端中调用它的方式。
* `rick_portal_gun.main` 中 `"rick_portal_gun.main:app"` 带有下划线 的部分 ，是指要导入的Python模块。 那就是某人会在类似的部分中使用的内容：

`from rick_portal_gun.main import # something goes here`

* 在 `app` 在 `"rick_portal_gun.main:app"` 是事情的进口从模块，并调用一个函数，如：

`from rick_portal_gun.main import app
app()`

* 该配置节告诉Poetry，安装此软件包时，我们希望它创建一个名为的命令行程序 `rick-portal-gun` 。

* 而且要调用的对象（像一个函数）是 `app` 模块内部 变量中 的那个 `rick_portal_gun.main` 。
一句话总结:
`rick-portal-gun`程序将调用`rick_portal_gun` 安装包中的`main`模块,并且调用`app`函数.
### 7. 安装你的包
```
poetry install 
```
![20210112001700_1dcb7ec16ed1f519193c6c36acd9bb96.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210112001700_1dcb7ec16ed1f519193c6c36acd9bb96.png)

### 8. 在本地测试你的应用
![20210112001931_bba24471e4351c50dcdf142a6acd5225.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210112001931_bba24471e4351c50dcdf142a6acd5225.png)
如上图所示.

### 9. 使用poetry build创建whl软件安装包
```
Poetry build 
```
该命令会创建一个dist文件夹,并在该文件夹下创建应用的压缩包和whl文件.效果如下图所示:
![20210112002215_ef47fd37fe500313729dbb5499ac5388.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210112002215_ef47fd37fe500313729dbb5499ac5388.png)

### 10. 使用pip 安装 whl文件
```
# 安装命令
pip install --user /home/kali/Documents/python/rick-portal-gun/dist/rick_portal_gun-0.1.0-py3-none-any.whl
# 卸载命令   
pip uninstall --user /home/kali/Documents/python/rick-portal-gun/dist/rick_portal_gun-0.1.0-py3-none-any.whl   
```
请根据自己文件存放的文件夹修改对应的地址.

### 11. poetry publish 发布软件包到pypi 

首先,需要在[PyPI · Python 包索引](https://pypi.org/)网站上创建账号.

注册完成之后,就可以使用`poetry publish`来发布你的软件了.
发布软件时,每次都是要输入账号和密码,可以使用pypi的API令牌来简化使用.
首先,在pypi项目设置中找到api令牌,为你的项目创建一个令牌.
![20210112015117_ea38b15d15f92d2b71e145858e445d03.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210112015117_ea38b15d15f92d2b71e145858e445d03.png)

创建令牌之后,就可以添加API令牌，项目名称一旦输入之后，选择一个作用域，就会自动生成一个令牌。记得保存，因为他只会出现一次。
测试的时候选择单个项目就好。以后项目多了，再申请全局作用API。

![20210112015104_679011a95891877e52f89c91eed82d18.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210112015104_679011a95891877e52f89c91eed82d18.png)

有了令牌之后，可以使用下列命令配置.
```
poetry config pypi-token.pypi <你刚刚生成的token>
```
之后你再发布软件的时候就不再需要输入账户名和密码了.

## 常见问题与解决

### 错误一:找不到模块名字`ModuleNotFoundError: No module named 'cit'`
```
❯ cit
Traceback (most recent call last):
  File "/home/kali/.cache/pypoetry/virtualenvs/cit-1jZIsVM0-py3.8/bin/cit", line 2, in <module>
    from cit.main import app
ModuleNotFoundError: No module named 'cit'
```
如上所示,可能是你的包的名字或者位置了.
还有一种可能就是你的包的名字和位置是对的,但是因为缓存的关系依然没有改.
所以进入到报错的文件,直接修改文件即可.
使用vim打开报错的文件.修改文件内容为你的实际位置和名字即可.
```
vim /home/kali/.cache/pypoetry/virtualenvs/cit-1jZIsVM0-py3.8/bin/cit
```
![20210112020058_b63216ebcbe461f20ac2dfd4fc7d74c8.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210112020058_b63216ebcbe461f20ac2dfd4fc7d74c8.png)

### 错误二:poetry publish 无法上传
可能是你的本地的版本和与服务器的版本号相同.导致你无法上传.
解决办法是修改配置文件.`pyproject.toml`中的版本行号.
![20210112020337_59811ed81b5c50e3a795bb63e5d709f7.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20210112020337_59811ed81b5c50e3a795bb63e5d709f7.png)

如上图所示.改成一个和你的Pypi不同的版本或者更先进的版本号即可.

### zsh: no matches found: typer[all]
错误原因是使用的zsh而不是bash.而官方的文档是按照bash来写的.
```shell
poetry add typer[all] # bash命令
poetry add "typer[all]" # zsh命令
poetry add typer -E all # 这个命令使用后并不会安装其他模块
```


## 参考文章
[用诗歌创建和发布Python包·JF的开发博客](https://johnfraney.ca/posts/2019/05/28/create-publish-python-package-poetry/)
