commander开发一个命令行工具（hello world）

## 序言
作为一个开发者，熟悉命令行基本就是必然，用NOdejs开发一个命令行引用，也就成为了必要。
网上之前找了各种教程，结果全部是各种报错，没有一个靠谱，唯有这个视频是成功执行下来没有报错的。
所以这里写个小总结

## 实战

1. 初始化项目并开始安装
```shell
# 创建一个项目文件夹并且进入
mkdir cli_test && $_
# 将项目初始化
npm init -y
# 在根目录下创建一个index.js文件
touch index.js
# 安装commander,使用--save会写入项目依赖
npm install --save commander
```

2. 往index.js写入下列代码：
```js
var program = require('commander');

program
    .command('hello')
    .action(()=> {
        console.log("hello world!");
    });

program.parse(process.argv);
```
测试是否成功
```shell
node index.js hello
```
输出hello world则表示成功。

3. 给index.js引入变量
```js
var program = require('commander');

program
    .command('hello <arg>')
    .action((arg)=> {
        console.log("hello ",arg,"!");
    });

program.parse(process.argv);
```
接下来，在命令行输入：
```shell
node index.js hello <arg>
```
命令会根据你输入的变动而变动。

## 学习小结
1. install 与install --save的区别
   >加上save参数可以在package当中加上项目的版本依赖
2.  依赖什么就引入什么，require很重要
3.  command可以产生一个命令，参数用<包裹>
4.  action是函数，但是没有深入些，具体看例子
5. 后面还有更多，需要进一步来处理，因为在其他教程那里都碰了钉子，似乎是全局安装与局部安装的关系



