细节汇总

## 打包必备的环节
1. 软件两个 `linuxdeployqt、patchelf`
2. 需要有一个打包文件夹，把下载好的程序复制或者移动进去
3. 打包时需要使用sudo

## 打包时候应用到的代码

### 下载linuxdeployqt

```sh
# 下载软件
wget https://github.com/probonopd/linuxdeployqt/releases/download/7/linuxdeployqt-7-x86_64.AppImage
# 国内用户镜像链接：https://pd.zwc365.com/seturl/https://github.com/probonopd/linuxdeployqt/releases/download/7/linuxdeployqt-7-x86_64.AppImage
# 改名
sudo mv linuxdeployqt-7-x86_64.AppImage /usr/local/bin/linuxdeployqt
# 更改权限，教程用的是777权限，我感觉755也可以用，实战也没报错
chmod 755 /usr/local/bin/linuxdeployqt
# 查看版本
sudo linuxdeployqt --version
```

### 下载patchelf
```sh
# 下载、解压、变更目录、配置、编译、安装一条路
wget https://nixos.org/releases/patchelf/patchelf-0.9/patchelf-0.9.tar.gz
tar -xvf patchelf-0.9.tar.gz
cd patchelf-0.9/
./configure
make
sudo make install
```
## 打包流程

```sh
mkdir output && $_ # 创建打包目录并进入
wget <即将下载的软件连接> # 将要打包的软件包下载或者复制到当前目录
sudo linuxdeployqt <软件名> -appimage # 将软件打包成你想打包的名字
tar cvf output.tar.gz output # 软件压缩命令。output可以视为软件名称，根据你的需求替换
```
## 其他设备的解压流程
```sh
# 解压、进入目录、安装
tar xvf output.tar.gz
cd output
./Apprun
```

## 遇到问题以及解决方法

```sh
# 遇到的问题：
qmake: could not exec '/usr/lib/x86_64-linux-gnu/qt4/bin/qmake': No such file or directory

# 解决方案：
sudo vim /usr/lib/x86_64-linux-gnu/qt-default/qtchooser/default.conf

# 软件无法执行，可能是权限问题，用下列命令修改权限
chmod a+x Subsurface*.AppImage 

# 软件无法执行，还有可能是依赖太老，版本太新导致不兼容没法运行
```
![](https://img-blog.csdnimg.cn/20190330141218391.png)

保存退出，执行第二步的操作即可完成打包。

#