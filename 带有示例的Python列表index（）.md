列表是一个容器，用于按顺序存储不同数据类型（整数，浮点数，布尔值，字符串等）的项。 它是Python内置的重要数据结构。 数据写在方括号（\[\]）内，并且值之间用逗号（，）分隔。

使用从索引0开始的第一个元素为列表中的项目建立索引。您可以通过添加新项目或更新，删除现有项目来对创建的列表进行更改。 它也可以具有重复项和嵌套列表。

列表上有许多可用的方法，其中重要的一种是index（）。

在本教程中，您将学习：

*   [Python清单index（）](#1)
*   [使用for循环获取列表中元素的索引](#2)
*   [使用while循环和list.index（）](#3)
*   [使用列表推导获取列表中元素的索引](#4)
*   [使用枚举获取列表中元素的索引](#5)
*   [使用过滤器获取列表中元素的索引](#6)
*   [使用NumPy获取列表中元素的索引](#7)
*   [使用more\_itertools.locate（）获取列表中元素的索引](#8)

## Python清单index（）

列表index（）方法可帮助您找到给定元素的第一个最低索引。 如果列表中有重复的元素，则返回该元素的第一个索引。 这是获取索引的最简单直接的方法。

除了内置的list index（）方法之外，您还可以使用其他方法来获取索引，例如使用列表推导，enumerate（）和filter方法遍历列表。

list index（）方法返回给定元素的第一个最低索引。

### 句法

list.index(element, start, end)

### 参量

| **参量** | **描述** |
| 元件 | 您要获取索引的元素。 |
| 开始 | 此参数是可选的。 您可以定义start：索引以搜索元素。 如果未给出，则默认值为0。 |
| 结束 | 此参数是可选的。 您可以为要搜索的元素指定结束索引。 如果未给出，则一直考虑到列表末尾。 |

### 返回值

list index（）方法返回给定元素的索引。 如果列表中不存在该元素，则index（）方法将引发错误，例如ValueError：'Element'不在列表中。

### 示例：查找给定元素的索引。

在列表my\_list = \['A'，'B'，'C'，'D'，'E'，'F'\]中，我们想知道元素C和F的索引。

下面的示例显示如何获取索引。

my\_list = \['A', 'B', 'C', 'D', 'E', 'F'\]
print("The index of element C is ", my\_list.index('C'))
print("The index of element F is ", my\_list.index('F'))

输出：

The index of element C is  2
The index of element F is  5

### 示例：在index（）中使用start和end

在此示例中，将尝试限制使用开始和结束索引在列表中搜索索引。

my\_list = \['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'\]
print("The index of element C is ", my\_list.index('C', 1, 5))
print("The index of element F is ", my\_list.index('F', 3, 7))
#using just the startindex
print("The index of element D is ", my\_list.index('D', 1))

输出：

The index of element C is  2
The index of element F is  5
The index of element D is  3

### 示例：使用不存在的元素测试index（）方法。

当您尝试在列表中搜索不存在的元素的索引时，会出现如下错误：

my\_list = \['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'\]
print("The index of element C is ", my\_list.index('Z'))

输出：

Traceback (most recent call last):
File "display.py", line 3, in <module>
print("The index of element C is ", my\_list.index('Z'))
ValueError: 'Z' is not in list

## 使用for循环获取列表中元素的索引

通过list.index（）方法，我们已经看到它给出了作为参数传递的元素的索引。

现在将列表视为：my\_list = \['Guru'，'Siya'，'Tiya'，'Guru'，'Daksh'，'Riya'，'Guru'\]。 名称“ Guru”在索引中出现3次，我希望所有名称为“ Guru”的索引。

使用for循环，我们应该能够获得多个索引，如下例所示。

my\_list = \['Guru', 'Siya', 'Tiya', 'Guru', 'Daksh', 'Riya', 'Guru'\]
all\_indexes = \[\]
for i in range(0, len(my\_list)) :
    if my\_list\[i\] == 'Guru' :
        all\_indexes.append(i)
print("Originallist ", my\_list)
print("Indexes for element Guru : ", all\_indexes)

输出：

Originallist  \['Guru', 'Siya', 'Tiya', 'Guru', 'Daksh', 'Riya', 'Guru'\]
Indexes for element Guru :  \[0, 3, 6\]

## 使用while循环和list.index（）

使用while循环将遍历给定的列表以获取给定元素的所有索引。

在列表中：my\_list = \['Guru'，'Siya'，'Tiya'，'Guru'，'Daksh'，'Riya'，'Guru'\]，我们需要元素“ Guru”的所有索引。

下面给出的示例显示了如何使用while循环获取所有索引

my\_list = \['Guru', 'Siya', 'Tiya', 'Guru', 'Daksh', 'Riya', 'Guru'\]
result = \[\]
elementindex = \-1
while True:
    try:
        elementindex = my\_list.index('Guru', elementindex+1)
        result.append(elementindex)
    except  ValueError:
        break
print("OriginalList is ", my\_list)
print("The index for element Guru is ", result)

输出：

OriginalList is  \['Guru', 'Siya', 'Tiya', 'Guru', 'Daksh', 'Riya', 'Guru'\]
The index for element Guru is  \[0, 3, 6\]

## 使用列表推导获取列表中元素的索引

要获取所有索引，一种快速而直接的方法是利用列表上的列表理解。

列表推导是Python函数，用于创建新序列（例如列表，字典等），即使用已创建的序列。

它们有助于减少较长的循环，并使您的代码更易于阅读和维护。

以下示例显示了如何执行此操作：

my\_list = \['Guru', 'Siya', 'Tiya', 'Guru', 'Daksh', 'Riya', 'Guru'\]
print("Originallist ", my\_list)
all\_indexes = \[a for a in range(len(my\_list)) if my\_list\[a\] == 'Guru'\]
print("Indexes for element Guru : ", all\_indexes)

输出：

Originallist  \['Guru', 'Siya', 'Tiya', 'Guru', 'Daksh', 'Riya', 'Guru'\]
Indexes for element Guru :  \[0, 3, 6\]

## 使用枚举获取列表中元素的索引

Enumerate（）函数是python可用的内置函数。 您可以使用枚举来获取列表中元素的所有索引。 它以输入作为可迭代对象（即，可以循环的对象），而输出是带有每个项目计数器的对象。

下面的示例演示如何利用列表上的枚举来获取给定元素的所有索引。

my\_list = \['Guru', 'Siya', 'Tiya', 'Guru', 'Daksh', 'Riya', 'Guru'\]
print("Originallist ", my\_list)
print("Indexes for element Guru : ", \[i for i, e in enumerate(my\_list) if e == 'Guru'\])

输出：

Originallist  \['Guru', 'Siya', 'Tiya', 'Guru', 'Daksh', 'Riya', 'Guru'\]
Indexes for element Guru :  \[0, 3, 6\]

## 使用过滤器获取列表中元素的索引

filter（）方法根据给定的函数过滤给定的列表。 列表中的每个元素都将传递给函数，并且所需的元素将根据函数中给出的条件进行过滤。

让我们使用filter（）方法获取列表中给定元素的索引。

下面的示例显示如何在列表上使用过滤器。

my\_list = \['Guru', 'Siya', 'Tiya', 'Guru', 'Daksh', 'Riya', 'Guru'\]
print("Originallist ", my\_list)
all\_indexes = list(filter(lambda i: my\_list\[i\] == 'Guru', range(len(my\_list))))
print("Indexes for element Guru : ", all\_indexes)

输出：

Originallist  \['Guru', 'Siya', 'Tiya', 'Guru', 'Daksh', 'Riya', 'Guru'\]
Indexes for element Guru :  \[0, 3, 6\]

## 使用NumPy获取列表中元素的索引

NumPy库专门用于数组。 因此，这里将使用NumPy从给定的列表中获取所需元素的索引。

要使用NumPy，我们必须安装并导入它。

这是相同的步骤：

**步骤1）** 安装NumPy

pip install numpy

**步骤2）** 导入NumPy模块。

import numpy as np

**步骤3）** 利用np.array将列表转换为数组

my\_list = \['Guru', 'Siya', 'Tiya', 'Guru', 'Daksh', 'Riya', 'Guru'\]
np\_array = np.array(my\_list)

**步骤4）使用** np.where（）获取所需元素的索引

item\_index = np.where(np\_array == 'Guru')\[0\]

输出的最终工作代码如下：

import numpy as np
my\_list = \['Guru', 'Siya', 'Tiya', 'Guru', 'Daksh', 'Riya', 'Guru'\]
np\_array = np.array(my\_list)
item\_index = np.where(np\_array == 'Guru')\[0\]
print("Originallist ", my\_list)
print("Indexes for element Guru :", item\_index)

输出：

Originallist\['Guru', 'Siya', 'Tiya', 'Guru', 'Daksh', 'Riya', 'Guru'\]
Indexes for element Guru : \[0 3 6\]

## 使用more\_itertools.locate（）获取列表中元素的索引

more\_itertools.locate（）有助于查找列表中元素的索引。此模块可与python 3.5+版本一起使用。 必须先安装 **more\_itertools** 软件包 才能使用它。

以下是安装和使用more\_itertools的步骤

**步骤1）** 使用pip（python软件包管理器）安装more\_itertools。 该命令是

pip install more\_itertools

**步骤2）** 安装完成后， 如下所示 导入 **定位** 模块

from more\_itertools import locate

现在，您可以在列表中使用定位模块，如下例所示：

from more\_itertools import locate
my\_list = \['Guru', 'Siya', 'Tiya', 'Guru', 'Daksh', 'Riya', 'Guru'\]
print("Originallist : ", my\_list)
print("Indexes for element Guru :", list(locate(my\_list, lambda x: x == 'Guru')))

输出：

Originallist :  \['Guru', 'Siya', 'Tiya', 'Guru', 'Daksh', 'Riya', 'Guru'\]
Indexes for element Guru : \[0, 3, 6\]

### 概要：

*   列表index（）方法可帮助您找到给定元素的索引。 这是获取索引的最简单直接的方法。
*   list index（）方法返回给定元素的索引。
*   如果列表中不存在该元素，则index（）方法将引发错误，例如ValueError：'Element'不在列表中。
*   除了内置的list方法之外，您还可以利用其他方法来获取索引，例如遍历列表，使用列表推导，使用enumerate（），使用过滤器等。
*   使用for循环和while循环获取给定元素的多个索引。
*   要获取所有索引，一种快速而直接的方法是利用列表上的列表理解。
*   列表推导是用于创建新序列的Python函数。
*   它们有助于减少较长的循环，并使您的代码更易于阅读和维护。
*   您可以使用枚举来获取列表中元素的所有索引。
*   Enumerate（）函数是python可用的内置函数。 它以输入作为可迭代对象（即，可以循环的对象），而输出是带有每个项目计数器的对象。
*   filter（）方法根据给定的函数过滤给定的列表。
*   Numpy库专门用于数组。 您可以使用NumPy获取列表中给定元素的索引。
*   more\_itertools.locate（）是另一个python库，可帮助查找给定列表的索引。