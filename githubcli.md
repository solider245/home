githubcli下载与安装
## 安装
```shell
#下载地址
https://github.com/cli/cli/releases/tag/v1.2.0
#下载,centos请下载rpm
wget https://github.com/cli/cli/releases/download/v1.2.0/gh_1.2.0_linux_amd64.deb
#安装
sudo dpkg -i gh_1.2.0_linux_amd64.deb
```
## [手册| GitHub CLI](https://cli.github.com/manual/)

**上手 GitHub CLI**



## 常用命令
```shell
# 默认帮助命令
gh
# 登录，需要打开网页
gh auth login
# 设置编辑器
gh config set editor <editor>
#要设置首选的git协议，您可以使用
gh config set git_protocol { ssh | https }
#要禁用交互式提示，可以使用
gh config set prompt disabled
#使用创建速记 
gh alias set
#使用进行自定义API查询 
gh api
#登出
gh auth logout
# => 通过提示选择要注销的主机
gh auth logout --hostname enterprise.internal
# => 从指定主机注销
## 仓库管理
#创建一个新的GitHub存储库。
gh repo create [<name>] [flags]
# 使用当前目录名在帐户下创建存储库
gh repo create
# 创建具有特定名称的存储库
gh repo create my-project
# 在组织中创建存储库
gh repo create cli/my-project
```