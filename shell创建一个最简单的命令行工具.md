## 序言
代码如下:
```sh
#!/bin/bash
# author: xkeyC (https://github.com/xkeyC)
# author: Mivik (https://github.com/Mivik)

# Check arguments
if [ "$1" == "" ]; then 
    echo Error: Expected a GitHub repository URL after fgit.
    exit 1
fi

# Check URL
if [[ "$1" != *"github.com"* ]]; then
	echo "Error: $1 is not a GitHub URL!"
	exit 1
fi

first=${1/"github.com"/"hub.fastgit.org"}
shift

# Replace URL and call git
git clone $first $@
```

## 创建步骤
1. 创建文件夹
2. 创建bin文件夹(兼容windows需要同时创建bat文件夹)
3. 将脚本创建在bin文件夹下
4. 将项目中的脚本文件复制到系统bin目录下
```shell
sudo cp ./bin/fgit.sh /usr/local/bin
```
5. 使用 `fgit https://github.com/xxx/xxx.git` 来克隆一个项目

参考项目地址:
[FastGitORG/fgit: 🔧 A tool to replace your github URL to fastgit URL](https://github.com/FastGitORG/fgit)
