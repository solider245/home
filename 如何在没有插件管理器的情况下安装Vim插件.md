Vundle，Pathogen，Vim\-plug，VAM等可以节省大量时间，但有时您只需要自己做一些事情，这样您就可以了解省时软件在后台为您执行的工作。 对于我自己，我讨厌不了解这些对系统的影响。 我意识到我不了解vim插件的基础知识，所以我开始学习。

这是一个循序渐进的指南，教您如何在没有任何高级管理器帮助的情况下安装基本的vim插件。

您需要什么

兴趣爱好

| vim | × | 1个 |

Howchoo受读者支持。 当您通过我们网站上的链接进行购买时，我们可能会免费为您赚取一笔小的会员佣金。

目录

## 跳到步骤：

1.  [创建或找到您的.vim目录](#create-or-find-your-vim-directory)
2.  [在.vim文件夹中创建一个“ bundle”目录。](#create-a-bundle-directory-inside-the-vim-folder)
3.  [将您的插件复制到此目录](#copy-your-plugin-to-this-directory)
4.  [设置你的vim运行路径](#set-your-vim-runtimepath)
5.  [重新加载您的vimrc](#reload-your-vimrc)
6.  [免责声明](#disclaimer)

11

分享

[脸书](https://www.facebook.com/sharer/sharer.php?u=https://howchoo.com/vim/how-to-install-vim-plugins-without-a-plugin-manager) [Reddit](http://reddit.com/submit?url=https://howchoo.com/vim/how-to-install-vim-plugins-without-a-plugin-manager&title=How%20to%20install%20Vim%20plugins%20without%20a%20plugin%20manager)[推特](https://twitter.com/share?url=https://howchoo.com/vim/how-to-install-vim-plugins-without-a-plugin-manager&text=How%20to%20install%20Vim%20plugins%20without%20a%20plugin%20manager) [Pinterest的](https://www.pinterest.com/pin/create/button/?url=https://howchoo.com/vim/how-to-install-vim-plugins-without-a-plugin-manager&description=How%20to%20install%20Vim%20plugins%20without%20a%20plugin%20manager)[电子邮件](mailto:?subject=How%20to%20install%20Vim%20plugins%20without%20a%20plugin%20manager&body=Check%20out%20this%20guide%20I%20found%20on%20Howchoo!%0A%0Ahttps%3A%2F%2Fhowchoo.com%2Fvim%2Fhow-to-install-vim-plugins-without-a-plugin-manager%3Futm_medium%3Demail%26utm_source%3Duser-recommendation%26utm_campaign%3Dshare-buttons) [文字讯息](sms:?&body=https%3A%2F%2Fhowchoo.com%2Fvim%2Fhow-to-install-vim-plugins-without-a-plugin-manager)

1个

喜爱

[

5

讨论](#comments)[

1个

你需要](#parts-list)

为了这些利益

[![](https://howchoo.com/media/ow/fl/od/owflodm0zgz.png?height=80&crop=1:1&auto=webp)](https://howchoo.com/vim "Vim")

订阅

[Vim](https://howchoo.com/vim)

h / vim • 23个指南

[![](https://howchoo.com/media/zg/uy/mg/zguymgizytu.jpeg?width=80&crop=1:1&auto=webp)](https://howchoo.com/webdev "Web开发")

订阅

[Web开发](https://howchoo.com/webdev)

h / webdev • 59条指南

广告

1个

## [创建或找到您的.vim目录](#create-or-find-your-vim-directory)

通常，您的主文件夹中将有一个.vim目录。 在这里应该放置vim的插件和语法文件。 但是，这只是一个约定，因此可以随时将它们放置在所需的位置。

2

## [在.vim文件夹中创建一个“ bundle”目录。](#create-a-bundle-directory-inside-the-vim-folder)

这是另一种约定。

3

## [将您的插件复制到此目录](#copy-your-plugin-to-this-directory)

如果它托管在Git上，则可以在其中执行以下操作：

```bash
git clone <repository-url>
```

如果不是，那也没关系。 但是，您已经获得了它，只要它遵循vim的文件和目录命名约定，它就可以工作。 如果它只是一个带有.vim文件类型的文件，我建议在此处创建目录并将该文件放入其中。

4

## [设置你的vim运行路径](#set-your-vim-runtimepath)

现在您的插件就在我们需要的位置，我们需要在每次启动时告诉vim在哪里可以找到它。 您的运行时路径是vim在其中查找脚本，语法文件，插件和其他要包含在vim环境中的内容的地方。 在vim内部，您可以使用以下命令找到运行时路径：

```bash
:set runtimepath?
```

要更新您的运行时路径并加载新插件，我们将编辑您的.vimrc，该文件应位于您的主文件夹中。 将目录添加到运行时路径的语法如下所示：

```bash
set runtimepath^=~/.vim/bundle/ctrlp.vim
```

广告

5

## [重新加载您的vimrc](#reload-your-vimrc)

您可以调用以下命令：

```bash
:source ~/.vimrc
```

或者只是关闭并重新启动vim。

## 其他方法

# [install vim plugin local file offline](https://www.cnblogs.com/otfsenter/p/9627041.html)

```shell
download github file.zip

cd file
git init
git add .
git commit -m "init"

set rtp+=$HOME/.vim/bundle/Vundle.vim/
call vundle#begin('$HOME/.vim/bundle/')

Plugin 'file://D:/mtl/vim/jedi-vim'

call vundle#end()            " required
```

## 参考链接
[如何在没有插件管理器的情况下安装Vim插件-Howchoo](https://howchoo.com/vim/how-to-install-vim-plugins-without-a-plugin-manager)
[install vim plugin local file offline - otfsenter - 博客园](https://www.cnblogs.com/otfsenter/p/9627041.html)
[截至2020年8个vim最佳插件管理器-Slant](https://www.slant.co/topics/1224/~best-plugin-managers-for-vim)
[Shougo/dein.vim: Dark powered Vim/Neovim plugin manager](https://github.com/Shougo/dein.vim)
[编写我自己的Vim插件管理器](https://junegunn.kr/2013/09/writing-my-own-vim-plugin-manager/)