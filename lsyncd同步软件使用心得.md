## 简介

>Lysncd 实际上是lua语言封装了 inotify 和 rsync 工具。
在安装之后，通过一个对配置文件的修改了，调整配置，最后启动命令启动之后，会按照配置文件的配置来进行同步。
配置文件当中定义了本地源地址，目标（远程）地址，同步的线程等。rsync子命令这一般默认配置即可。稍微学习下还是很简单的。
<!-- more -->
## 下载

```shell
yum install lsyncd # centos直接下载
sudo apt-get install lsyncd # ubuntu下载，推荐
wget https://github.91chifun.workers.dev//https://github.com/axkibe/lsyncd/archive/release-2.2.3.zip
7z x release-2.2.3.zip # 解压
cmake .
make 
sudo make install
# 源码安装不推荐，要各种依赖，容易出错，各大软件包发行版应该都包括这个软件了
```
## 配置
lsyncd 主配置文件，假设放置在`/etc/lsyncd.conf`:
如果创建了密码文件，需要编辑
```
vim /etc/images.pas # 创建密码文件

```
```shell
123456 # 密码直接明文即可
```
修改文件权限，并启动。
```sh
chmod 600 /etc/images.pas # 修改密码文件权限
service lsyncd start # 启动
```


### 案例：使用lsyncd配置数据库备份多异地同步
```lua
settings {
    logfile = "/var/log/lsyncd.log", --日志路径
    status = "/var/log/lsyncd.status", --状态文件
    pidfile = "/var/run/lsyncd.pid", --pid文件路径
    statusInterval = 1,  --状态文件写入最短时间
    maxProcesses = 4,    --最大进程
    maxDelays = 1        --最大延迟
}
--多host同步
servers = {
    "192.168.1.1"
}
-- 多同步目录,源备份用路径和rsync节点名相同
bakpaths = {
    "mysql",
    "mongodb",
    "mssql"
}
-- 源路径
source_path='/dbbackup/'
for _, server in ipairs(servers) do
    for _, bakpath in ipairs(bakpaths) do
    sync {
        default.rsync,
        source = source_path..bakpath,
        target = server.."::"..bakpath,
        delete = "running",
        exclude = { 
        },  
        rsync = {
            binary = "/usr/local/bin/rsync", -- rsync 版本要到3以上
            archive = true,
            compress = true,
            owner = false,
            group = false,
            perms = true,
            verbose = true,
            copy_links = true
        }   
    }   
    end
end
```
注明：


### 案例2：可以按需裁剪的配置
```lua 
settings {
    logfile ="/usr/local/lsyncd-2.1.5/var/lsyncd.log",
    statusFile ="/usr/local/lsyncd-2.1.5/var/lsyncd.status",
    inotifyMode = "CloseWrite",
    maxProcesses = 8,
    }
-- I. 本地目录同步，direct：cp/rm/mv。 适用：500+万文件，变动不大
sync {
    default.direct,
    source    = "/tmp/src",
    target    = "/tmp/dest",
    delay = 1
    maxProcesses = 1
    }
-- II. 本地目录同步，rsync模式：rsync
sync {
    default.rsync,
    source    = "/tmp/src",
    target    = "/tmp/dest1",
    excludeFrom = "/etc/rsyncd.d/rsync_exclude.lst",
    rsync     = {
        binary = "/usr/bin/rsync",
        --password_file = "/etc/images.pas" ,如果配置了密码文件需要创建对应的密码文件。
        archive = true,
        compress = true,
        bwlimit   = 2000
        } 
    }
-- III. 远程目录同步，rsync模式 + rsyncd daemon
sync {
    default.rsync,
    source    = "/tmp/src",
    target    = "syncuser@172.29.88.223::module1",
    delete="running",
    exclude = { ".*", ".tmp" },
    delay = 30,
    init = false,
    rsync     = {
        binary = "/usr/bin/rsync",
        archive = true,
        compress = true,
        verbose   = true,
        password_file = "/etc/rsyncd.d/rsync.pwd",
        _extra    = {"--bwlimit=200"}
        }
    }
-- IV. 远程目录同步，rsync模式 + ssh shell
sync {
    default.rsync,
    source    = "/tmp/src",
    target    = "172.29.88.223:/tmp/dest",
    -- target    = "root@172.29.88.223:/remote/dest",
    -- 上面target，注意如果是普通用户，必须拥有写权限
    maxDelays = 5,
    delay = 30,
    -- init = true,
    rsync     = {
        binary = "/usr/bin/rsync",
        archive = true,
        compress = true,
        bwlimit   = 2000
        -- rsh = "/usr/bin/ssh -p 22 -o StrictHostKeyChecking=no"
        -- 如果要指定其它端口，请用上面的rsh
        }
    }
-- V. 远程目录同步，rsync模式 + rsyncssh，效果与上面相同
sync {
    default.rsyncssh,
    source    = "/tmp/src2",
    host      = "172.29.88.223",
    targetdir = "/remote/dir",
    excludeFrom = "/etc/rsyncd.d/rsync_exclude.lst",
    -- maxDelays = 5,
    delay = 0,
    -- init = false,
    rsync    = {
        binary = "/usr/bin/rsync",
        archive = true,
        compress = true,
        verbose   = true,
        _extra = {"--bwlimit=2000"},
        },
    ssh      = {
        port  =  1234
        }
    }

```
### 案例3：简单同步配置
```lua 
settings {
    logfile      ="/usr/local/lsyncd-2.1.5/var/lsyncd.log",
    statusFile   ="/usr/local/lsyncd-2.1.5/var/lsyncd.status",
    inotifyMode  = "CloseWrite",
    maxProcesses = 7,
    -- nodaemon =true,
    }
sync {
    default.rsync,
    source    = "/tmp/src",
    target    = "/tmp/dest",
    -- excludeFrom = "/etc/rsyncd.d/rsync_exclude.lst",
    rsync     = {
        binary    = "/usr/bin/rsync",
        archive   = true,
        compress  = true,
        verbose   = true
        }
    }

```

## 配置说明

**settings**

里面是全局设置，`--`开头表示注释，下面是几个常用选项说明：

*   `logfile` 定义日志文件
*   `stausFile` 定义状态文件
*   `nodaemon=true` 表示不启用守护模式，默认
*   `statusInterval` 将lsyncd的状态写入上面的statusFile的间隔，默认10秒
*   `inotifyMode` 指定inotify监控的事件，默认是`CloseWrite`，还可以是`Modify`或`CloseWrite or Modify`
*   `maxProcesses` 同步进程的最大个数。假如同时有20个文件需要同步，而`maxProcesses = 8`，则最大能看到有8个rysnc进程
*   `maxDelays` 累计到多少所监控的事件激活一次同步，即使后面的`delay`延迟时间还未到

**sync**

里面是定义同步参数，可以继续使用`maxDelays`来重写settings的全局变量。一般第一个参数指定`lsyncd`以什么模式运行：`rsync`、`rsyncssh`、`direct`三种模式：

*   `default.rsync` ：本地目录间同步，使用rsync，也可以达到使用ssh形式的远程rsync效果，或daemon方式连接远程rsyncd进程；
    `default.direct` ：本地目录间同步，使用`cp`、`rm`等命令完成差异文件备份；
    `default.rsyncssh` ：同步到远程主机目录，rsync的ssh模式，需要使用key来认证

*   `source` 同步的源目录，使用绝对路径。

*   `target` 定义目的地址.对应不同的模式有几种写法：
    `/tmp/dest` ：本地目录同步，可用于`direct`和`rsync`模式
    `172.29.88.223:/tmp/dest` ：同步到远程服务器目录，可用于`rsync`和`rsyncssh`模式，拼接的命令类似于`/usr/bin/rsync -ltsd --delete --include-from=- --exclude=* SOURCE TARGET`，剩下的就是rsync的内容了，比如指定username，免密码同步
    `172.29.88.223::module` ：同步到远程服务器目录，用于`rsync`模式
    三种模式的示例会在后面给出。

*   `init` 这是一个优化选项，当`init = false`，只同步进程启动以后发生改动事件的文件，原有的目录即使有差异也不会同步。默认是`true`

*   `delay` 累计事件，等待rsync同步延时时间，默认15秒（最大累计到1000个不可合并的事件）。也就是15s内监控目录下发生的改动，会累积到一次rsync同步，避免过于频繁的同步。（可合并的意思是，15s内两次修改了同一文件，最后只同步最新的文件）

*   `excludeFrom` 排除选项，后面指定排除的列表文件，如`excludeFrom = "/etc/lsyncd.exclude"`，如果是简单的排除，可以使用`exclude = LIST`。
    这里的排除规则写法与原生rsync有点不同，更为简单：
    *   监控路径里的任何部分匹配到一个文本，都会被排除，例如`/bin/foo/bar`可以匹配规则`foo`
    *   如果规则以斜线`/`开头，则从头开始要匹配全部
    *   如果规则以`/`结尾，则要匹配监控路径的末尾
    *   `?`匹配任何字符，但不包括`/`
    *   `*`匹配0或多个字符，但不包括`/`
    *   `**`匹配0或多个字符，可以是`/`

*   `delete` 为了保持target与souce完全同步，Lsyncd默认会`delete = true`来允许同步删除。它除了`false`，还有`startup`、`running`值，请参考 [Lsyncd 2.1.x ‖ Layer 4 Config ‖ Default Behavior](https://github.com/axkibe/lsyncd/wiki/Lsyncd%202.1.x%20%E2%80%96%20Layer%204%20Config%20%E2%80%96%20Default%20Behavior)。

**rsync**

（提示一下，`delete`和`exclude`本来都是**rsync**的选项，上面是配置在**sync**中的，我想这样做的原因是为了减少rsync的开销）

*   `bwlimit` 限速，单位kb/s，与rsync相同（这么重要的选项在文档里竟然没有标出）
*   `compress` 压缩传输默认为`true`。在带宽与cpu负载之间权衡，本地目录同步可以考虑把它设为`false`
*   `perms` 默认保留文件权限。
*   其它rsync的选项
* uid #进程对应的用户
* gid #进程对应的用户组
* use chroot #安全相关
* max connections #最大连接数 0代表不限制
* timeout = 300 #超时时间, 可选
* pid file #进程对应的进程号文件
* lock file #锁文件
* log file #日志文件
* [backup] #模块名称
* path #服务器提供访问的目录
* ignore errors #忽略错误, 可选
* read only = false #可选
* list = false #不能列表
* hosts allow = 192.168.10.0/24 #允许的ip地址,可选
* auth users #虚拟用户
* secrets file #虚拟密码


其它还有rsyncssh模式独有的配置项，如`host`、`targetdir`、`rsync_path`、`password_file`，见后文示例。`rsyncOps={"-avz","--delete"}`这样的写法在2.1.\*版本已经不支持。

`lsyncd.conf`可以有多个`sync`，各自的source，各自的target，各自的模式，互不影响。
## 忽略规则
需要忽略同步的文件或文件夹，excludeFrom 选项才配置该文件，exclude 类型的配置不用该配置文件。假设配置文件放在`/etc/lsyncd_exclude.lst`

```
.svn
Runtime/**
Uploads/**
```
## 启动命令
```shell
lsyncd -log Exec /etc/lsyncd.conf
# 手动安装可能要使用下列命令，因为安装的路径不同
lsyncd -log Exec /usr/local/lsyncd-2.1.5/etc/lsyncd.conf
```

## 常用命令
作为大多数Unix工具，Lsyncd将在用-help调用时打印其命令行选项的摘要。

lsyncd --help
lsyncd -help
Lsyncd中的两个连字符是冗余的。它没有短的一个字母选项，并且一个连字符总是与指定两个相同。

也像大多数Unix工具一样，--version或者-version让Lsyncd打印它的版本号。

lsyncd -version
Lsyncd 2.1被设计为主要通过配置文件进行配置（见下文）。配置文件因此可以是唯一的命令行选项。

lsyncd CONFIGFILE
尽管对于标准使用或快速测试，它可以通过命令行选项进行光标配置。以下内容将使用rsync保持本地源和目标目录同步：

lsyncd -rsync /home/USER/src /home/USER/dst
目标可以是Rsync可以识别的任何东西。

lsyncd -rsync /home/USER/src remotehost:dst
通过调用两次（或多次）-rsync配置两个（或更多）目标。

lsyncd -rsync /home/USER/src remotehost1:dst -rsync /home/USER/src remotehost2:dst 
Rsync同步的一个缺点是，通常目录和文件移动会导致移动源的删除和导线移动目标的重新传输。但是，Lsyncd 2可以使用ssh命令在目标上本地移动目录和文件。要-rsyncssh在本地源目录，远程主机和目标目录之后使用此用法。REMOTEHOST可以包含一个用户me@remotehost.com。

lsyncd -rsyncssh /home/USER/src REMOTEHOST TARGETDIR
测试Lsyncd配置时-nodaemon是一个非常方便的标志。使用此选项，Lsyncd不会分离，并且不会成为守护进程。所有日志消息都是在控制台上打印的配置日志记录工具（stdout和stderr）之外的。

lsyncd -nodaemon CONFIGFILE
使用-nodaemon运行时的行为有所不同。Lsyncd不会/像它在成为守护程序时那样将其工作目录更改为。因此，相对目标./target会起作用，-nodaemon但必须指定绝对路径才能在守护进程模式下工作。源目录也将由Lsyncd转换为绝对路径。源代码不解析为绝对路径的原因是因为Lsyncd本身并不关心目标说明符的格式，该目标说明符也可以是远程主机，rsyncd模块等。它不透明地传递给rsync。它关心观察到的目录。

所有日志消息按类别排序。默认情况下，Lsyncd缺少日志消息。通过指定，您可以将Lsyncd转换为motormouth -log all。

lsyncd -log all CONFIGFILE
这可能很容易变得太多。一个特别有用的类别是“Exec”，它将记录Lsyncd产生的所有进程的命令行。

lsyncd -log Exec CONFIGFILE
当默认初始启动同步失败时，Lsyncd将以错误消息终止。它是这样设计的，所以配置故障可见地报告给可能的初始用户。但是，在生产过程中可能会完成远程目标，但是您希望Lsyncd始终启动并不断尝试同步到远程目标，直到它启动。

lsyncd -insist -rsync /home/USER/src remotehost:dst
在生产模式下，建议坚持。它也可以在配置文件中的settings {}命令中指定。

## 参考文章
[Lsyncd - 实时文件同步工具（精译） - sunsky303 - 博客园](https://www.cnblogs.com/sunsky303/p/8976445.html)
[系统运维|lsyncd 实时同步搭建指南](https://linux.cn/article-5849-1.html)
[Lsyncd 实时同步配置及说明 - 简书](https://www.jianshu.com/p/808d173786c4)