20160503 修改了代码缩进，刚玩知乎，当时回答的时候点了插入代码，貌似没生效。谢谢

[@iliul](https://www.zhihu.com/people/f76264ff7c7d1530f54d2c080643ab22)

的修改的格式
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
使用Response类的接口iter\_content(chunk\_size=1)或者iter\_lines(chunk\_size=512)，chunk\_size可以设置你指定的大小，指定每次获取数据的最大值，注意：并不是每次请求回来的content块都是chunk\_size指定的大小。
stream=True是关键，这个参数指定响应内容体（response.content）为流。默认为False，在请求时将content一并全部加载；如果调用response.content仍然是全部加载，然后在执行后面的代码，只是加载时机变成了第一次调用response.content时。如果使用上面的两个迭代接口调用的话，可以在下载的过程中做一些事情，比如刷新进度条。

资料1：找到了这个迭代接口，不知道怎么用
资料2：有一个简单的用法，就是用for循环
ProgressBar一个进度显示的简单实现

```python3
with closing(requests.get(self.url(), stream=True)) as response:
    chunk_size = 1024 # 单次请求最大值
    content_size = int(response.headers['content-length']) # 内容体总大小
    progress = ProgressBar(self.file_name(), total=content_size,
                                     unit="KB", chunk_size=chunk_size, run_status="正在下载", fin_status="下载完成")
    with open(file_name, "wb") as file:
       for data in response.iter_content(chunk_size=chunk_size):
           file.write(data)
           progress.refresh(count=len(data))

```

```python3
class ProgressBar(object):

    def __init__(self, title,
                 count=0.0,
                 run_status=None,
                 fin_status=None,
                 total=100.0,
                 unit='', sep='/',
                 chunk_size=1.0):
        super(ProgressBar, self).__init__()
        self.info = "【%s】%s %.2f %s %s %.2f %s"
        self.title = title
        self.total = total
        self.count = count
        self.chunk_size = chunk_size
        self.status = run_status or ""
        self.fin_status = fin_status or " " * len(self.statue)
        self.unit = unit
        self.seq = sep

    def __get_info(self):
        # 【名称】状态 进度 单位 分割线 总数 单位
        _info = self.info % (self.title, self.status,
                             self.count/self.chunk_size, self.unit, self.seq, self.total/self.chunk_size, self.unit)
        return _info

    def refresh(self, count=1, status=None):
        self.count += count
        # if status is not None:
        self.status = status or self.status
        end_str = "\r"
        if self.count >= self.total:
            end_str = '\n'
            self.status = status or self.fin_status
        print(self.__get_info(), end=end_str)

```

参考资料：
1\. [高级用法 — Requests 1.1.0 文档](https://link.zhihu.com/?target=http%3A//cn.python-requests.org/en/latest/user/advanced.html)
2\. [Python网络操作大神之requests](https://link.zhihu.com/?target=http%3A//www.gaoxuewen.cn/index.php/python/1086.html)

博客地址：[Python3使用requests模块显示下载进度](https://link.zhihu.com/?target=http%3A//blog.csdn.net/supercooly/article/details/51046561)

感谢

[@微微寒](https://www.zhihu.com/people/e550daa0d4d7e6b257f15e6b4f883f2e)

分享代码，正好需要！还有你贴代码怎么不把格式加上呢QAQ 我改了好半天缩进

```text
#!/usr/bin/env python3

import requests
from contextlib import closing

"""
作者：微微寒
链接：https://www.zhihu.com/question/41132103/answer/93438156
来源：知乎
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
"""

class ProgressBar(object):
    def __init__(self, title, count=0.0, run_status=None, fin_status=None, total=100.0,    unit='', sep='/', chunk_size=1.0):
        super(ProgressBar, self).__init__()
        self.info = "[%s] %s %.2f %s %s %.2f %s"
        self.title = title
        self.total = total
        self.count = count
        self.chunk_size = chunk_size
        self.status = run_status or ""
        self.fin_status = fin_status or " " * len(self.statue)
        self.unit = unit
        self.seq = sep

    def __get_info(self):
        # 【名称】状态 进度 单位 分割线 总数 单位
        _info = self.info % (self.title, self.status, self.count/self.chunk_size, self.unit, self.seq, self.total/self.chunk_size, self.unit)
        return _info

    def refresh(self, count=1, status=None):
        self.count += count
        # if status is not None:
        self.status = status or self.status
        end_str = "\r"
        if self.count >= self.total:
            end_str = '\n'
            self.status = status or self.fin_status
        print(self.__get_info(), end=end_str)

def main():
	with closing(requests.get("http://www.futurecrew.com/skaven/song_files/mp3/razorback.mp3", stream=True)) as response:
		chunk_size = 1024
		content_size = int(response.headers['content-length'])
		progress = ProgressBar("razorback", total=content_size, unit="KB", chunk_size=chunk_size, run_status="正在下载", fin_status="下载完成")
		# chunk_size = chunk_size < content_size and chunk_size or content_size
		with open('./file.mp3', "wb") as file:
			for data in response.iter_content(chunk_size=chunk_size):
				file.write(data)
				progress.refresh(count=len(data))

if __name__ == '__main__':
	main()
```

```text
# -*- coding: utf-8 -*-
"""
@author:songhao
@file: c4.py
@time: 2017/12/19
# 微信公众号：zeropython
"""

import os
from urllib.request import urlopen

import requests
from tqdm import tqdm

def download_from_url(url, dst):
    """
    @param: url to download file
    @param: dst place to put the file
    """
    file_size = int(urlopen(url).info().get('Content-Length', -1))

    if os.path.exists(dst):
        first_byte = os.path.getsize(dst)
    else:
        first_byte = 0
    if first_byte >= file_size:
        return file_size
    header = {"Range": "bytes=%s-%s" % (first_byte, file_size)}
    pbar = tqdm(
        total=file_size, initial=first_byte,
        unit='B', unit_scale=True, desc=url.split('/')[-1])
    req = requests.get(url, headers=header, stream=True)
    with(open(dst, 'ab')) as f:
        for chunk in req.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
                pbar.update(1024)
    pbar.close()
    return file_size

if __name__ == '__main__':
    url = "http://newoss.maiziedu.com/machinelearning/pythonrm/pythonrm5.mp4"
    download_from_url(url, "./new.mp4")
```

原文地址: [Python3 使用requests模块显示下载视频并且显示进度](https://link.zhihu.com/?target=https%3A//www.168seo.cn/python/24286.html)

![](https://pic1.zhimg.com/50/v2-b1e0f92c3bee309fcd4b3306a3c07278_hd.gif?source=1940ef5c)

![](https://pic3.zhimg.com/50/v2-b1e0f92c3bee309fcd4b3306a3c07278_hd.jpg?source=1940ef5c)

1.下载续传

2.显示进度条

![](https://pic2.zhimg.com/50/v2-f9e33817106b8d7bb3c7c72756d619ec_hd.jpg?source=1940ef5c)

2 人赞同了该回答

```python
import sys,time,
        requests

            def chunk_report(bytes_so_far, total_size):
            percent = float(bytes_so_far) / total_size
            percent = round(percent * 100, 2
            )
                sys.stdout.write("Downloaded %d of
                %d bytes (%0.2f%%)\r" %
                    (bytes_so_far, total_size, percent))

                    if bytes_so_far >= total_size:
                    sys.stdout.write('\n'
                    )

                        try:
                        url = 'xxx'
                        r = requests.get(url, stream
                        =True)
                            content_size = int(r.headers['content-length'
                            ])

                                chunk_size = 65536
                                bytes_so_far = 0

                                f = open("xxx", "wb")
                                for chunk in r.iter_content(chunk_size
                                =chunk_size):
                                    if chunk:
                                    f.write(chunk)
                                    bytes_so_far += len(chunk)
                                    chunk_report(bytes_so_far, content_size)

                                    except:
                                    time.sleep(5)

                                    finally:
                                    f.close()
```

[发布于 2018\-09\-20](https://www.zhihu.com/question/41132103/answer/494984939)

简单写了一个下载进度的例子，可以自己封装一下。

```python
作者：潜行
链接：https://www.zhihu.com/question/41132103/answer/754189554
来源：知乎
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

# -*- coding: utf-8 -*-
import time
import requests

# 请求下载地址，以流式的。打开要下载的文件位置。
with requests.get('http://down.360safe.com/setup.exe', stream=True) as r, open('setup.exe', 'wb') as file:
    # 请求文件的大小单位字节B
    total_size = int(r.headers['content-length'])
    # 以下载的字节大小
    content_size = 0
    # 进度下载完成的百分比
    plan = 0
    # 请求开始的时间
    start_time = time.time()
    # 上秒的下载大小
    temp_size = 0
    # 开始下载每次请求1024字节
    for content in r.iter_content(chunk_size=1024):
        file.write(content)
        # 统计以下载大小
        content_size += len(content)
        # 计算下载进度
        plan = (content_size / total_size) * 100
        # 每一秒统计一次下载量
        if time.time() - start_time > 1:
            # 重置开始时间
            start_time = time.time()
            # 每秒的下载量
            speed = content_size - temp_size
            # KB级下载速度处理
            if 0 <= speed < (1024 ** 2):
                print(plan, '%', speed / 1024, 'KB/s')
            # MB级下载速度处理
            elif (1024 ** 2) <= speed < (1024 ** 3):
                print(plan, '%', speed / (1024 ** 2), 'MB/s')
            # GB级下载速度处理
            elif (1024 ** 3) <= speed < (1024 ** 4):
                print(plan, '%', speed / (1024 ** 3), 'GB/s')
            # TB级下载速度处理
            else:
                print(plan, '%', speed / (1024 ** 4), 'TB/s')
            # 重置以下载大小
            temp_size = content_size
```

[编辑于 2020\-08\-14](https://www.zhihu.com/question/41132103/answer/754189554)


