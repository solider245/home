## subprocess使用背景和介绍

`subprocess`模块用于创建子进程， 这个模块用于替换旧版本中的一些模块，比如：
*   os.system
*   os.spawn\*
*   os.popen\*--废弃
*   popen2.\* ----废弃
*   commands.\* --废弃，3.x中被移除

>运行python的时候，我们都是在创建并运行一个进程，linux中一个进程可以fork一个子进程，并让这个子进程exec另外一个程序。在python中，我们通过标准库中的subprocess包来fork一个子进程，并且运行一个外部的程序。subprocess包中定义有数个创建子进程的函数，这些函数分别以不同的方式创建子进程，所欲我们可以根据需要来从中选取一个使用。另外subprocess还提供了一些管理标准流(standard stream)和管道(pipe)的工具，从而在进程间使用文本通信。

## 旧有模块的使用举例：

### 1.os.system()

执行操作系统的命令，将结果输出到屏幕，只返回命令执行状态(0：成功，非 0 ： 失败)

```shell
import os
>>> a = os.system("df -Th")
Filesystem   Type  Size Used Avail Use% Mounted on
/dev/sda3   ext4  1.8T 436G 1.3T 26% /
tmpfs     tmpfs  16G   0  16G  0% /dev/shm
/dev/sda1   ext4  190M 118M  63M 66% /boot
>>> a
0     # 0 表示执行成功
# 执行错误的命令
>>> res = os.system("list")
sh: list: command not found
>>> res
32512    # 返回非 0 表示执行错误
```


### 2\. os.popen()


```shell
import os
>>> res = os.popen("ls -l")
# 将结果保存到内存中
>>> print res
<open file 'ls -l', mode 'r' at 0x7f02d249c390>
# 用read()读取内容
>>> print res.read()
total 267508
-rw-r--r-- 1 root root  260968 Jan 27 2016 AliIM.exe
-rw-------. 1 root root   1047 May 23 2016 anaconda-ks.cfg
-rw-r--r-- 1 root root  9130958 Nov 18 2015 apache-tomcat-8.0.28.tar.gz
-rw-r--r-- 1 root root     0 Oct 31 2016 badblocks.log
drwxr-xr-x 5 root root   4096 Jul 27 2016 certs-build
drwxr-xr-x 2 root root   4096 Jul 5 16:54 Desktop
-rw-r--r-- 1 root root   2462 Apr 20 11:50 Face_24px.ico

```

执行操作系统的命令，会将结果保存在内存当中，可以用`read()`方法读取出来

## subprocess模块

常用指令：
1. subprocess.run() 
   >执行命令
2. subprocess.call()
   >执行命令，返回命令的结果和执行状态，0或者非0
3. subprocess.check_call()
   >执行命令，返回结果和状态，正常为0 ，执行错误则抛出异常
4. subprocess.getstatusoutput()
   >接受字符串形式的命令，返回 一个元组形式的结果，第一个元素是命令执行状态，第二个为执行结果
5. subprocess.getoutput()
   >接受字符串形式的命令，放回执行结果
6. subprocess.check_output()
   >执行命令，返回执行的结果，而不是打印
7. subprocess.Popen()
   >其实以上subprocess使用的方法，都是对subprocess.Popen的封装，下面我们就来看看这个Popen方法。
   1. stdout
   2. stderr
   3. poll()
   4. wait()
   5. terminate()
   6. pid

> 其中，`run`使用最多最简单，而`Popen`则是更高级的用法。

### `run`用法指南

`run` 方法语法格式如下：
```py
subprocess.run(args, *, stdin=None, input=None, stdout=None, stderr=None, capture_output=False, shell=False, cwd=None, timeout=None, check=False, encoding=None, errors=None, text=None, env=None, universal_newlines=None)
```
最简单的例子：
   * 让系统执行`ls`命令：
```py
import subprocess
subprocess.run(["ls"])
```
   * 让系统执行`ls -l`命令
```py
import subprocess

subprocess.run(["ls", "-l"])

```


参数详解：
*   args：表示要执行的命令。必须是一个字符串，字符串参数列表。
*   stdin、stdout 和 stderr：子进程的标准输入、输出和错误。其值可以是 subprocess.PIPE、subprocess.DEVNULL、一个已经存在的文件描述符、已经打开的文件对象或者 None。subprocess.PIPE 表示为子进程创建新的管道。subprocess.DEVNULL 表示使用 os.devnull。默认使用的是 None，表示什么都不做。另外，stderr 可以合并到 stdout 里一起输出。
*   timeout：设置命令超时时间。如果命令执行时间超时，子进程将被杀死，并弹出 TimeoutExpired 异常。
*   check：如果该参数设置为 True，并且进程退出状态码不是 0，则弹 出 CalledProcessError 异常。
*   encoding: 如果指定了该参数，则 stdin、stdout 和 stderr 可以接收字符串数据，并以该编码方式编码。否则只接收 bytes 类型的数据。
*   shell：如果该参数为 True，将通过操作系统的 shell 执行指定的命令。

> run 方法调用方式返回 CompletedProcess 实例，和直接 Popen 差不多，实现是一样的，实际也是调用 Popen，与 Popen 构造函数大致相同，例如:
>
```shell
#执行ls -l /dev/null 命令
>>> subprocess.run(["ls", "-l", "/dev/null"])
crw-rw-rw-  1 root  wheel    3,   2  5  4 13:34 /dev/null
CompletedProcess(args=['ls', '-l', '/dev/null'], returncode=0)
```
**returncode**: 执行完子进程状态，通常返回状态为0则表明它已经运行完毕，若值为负值 "\-N",表明子进程被终。

简单实例：
```python
import subprocess
def runcmd(command):
    ret = subprocess.run(command,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE,encoding="utf-8",timeout=1)
    if ret.returncode == 0:
        print("success:",ret)
    else:
        print("error:",ret)


runcmd(["dir","/b"])#序列参数
runcmd("exit 1")#字符串参数
```
输出结果：
```sh
success: CompletedProcess(args=['dir', '/b'], returncode=0, stdout='test.py\n', stderr='')
error: CompletedProcess(args='exit 1', returncode=1, stdout='', stderr='')
```

### `Popen()` 方法

>Popen 是 subprocess的核心，子进程的创建和管理都靠它处理。

构造函数：

```python
class subprocess.Popen(args, bufsize=-1, executable=None, stdin=None, stdout=None, stderr=None, 
preexec_fn=None, close_fds=True, shell=False, cwd=None, env=None, universal_newlines=False, 
startupinfo=None, creationflags=0,restore_signals=True, start_new_session=False, pass_fds=(),
*, encoding=None, errors=None)
```
**常用参数：**

*   args：shell命令，可以是字符串或者序列类型（如：list，元组）
*   bufsize：缓冲区大小。当创建标准流的管道对象时使用，默认\-1。
    0：不使用缓冲区
    1：表示行缓冲，仅当universal\_newlines=True时可用，也就是文本模式
    正数：表示缓冲区大小
    负数：表示使用系统默认的缓冲区大小。

*   stdin, stdout, stderr：分别表示程序的标准输入、输出、错误句柄
*   preexec\_fn：只在 Unix 平台下有效，用于指定一个可执行对象（callable object），它将在子进程运行之前被调用
*   shell：如果该参数为 True，将通过操作系统的 shell 执行指定的命令。
*   cwd：用于设置子进程的当前目录。
*   env：用于指定子进程的环境变量。如果 env = None，子进程的环境变量将从父进程中继承。

1. 实例1：

```py
import subprocess
ret1 = subprocess.Popen(["mkdir","t1"])
ret2 = subprocess.Popen("mkdir t2", shell=True)
# 上述命令会产生两个文件夹，效果如同你在shell下执行mkdir t1;mkdir t2两个命令
```


终端输入的命令分为两种：

* 输入即可得到输出，如：`ifconfig`
* 输入进行某环境，依赖再输入，如：`python`

2. 实例2：
```py
import subprocess
 
obj = subprocess.Popen("mkdir t3", shell=True, cwd='/home/dev',)     #在cwd目录下执行命令
```

3. 实例3：
```py
import subprocess
 
obj = subprocess.Popen(["python"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
obj.stdin.write("print(1)\n")
obj.stdin.write("print(2)")
obj.stdin.close()
 
cmd_out = obj.stdout.read()
obj.stdout.close()
cmd_error = obj.stderr.read()
obj.stderr.close()
 
print(cmd_out)
print(cmd_error)

```
4. 实例4
   
```py
import subprocess
 
obj = subprocess.Popen(["python"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
obj.stdin.write("print(1)\n")
obj.stdin.write("print(2)")
 
out_error_list = obj.communicate()
print(out_error_list)

```
5. 实例5

```py
import subprocess
 
obj = subprocess.Popen(["python"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
out_error_list = obj.communicate('print("hello")')
print(out_error_list)

```

6. 实例6
>创建一个子进程，然后执行一个简单的命令:

```py
>>> import subprocess
>>> p = subprocess.Popen('ls -l', shell=True)
>>> total 164
-rw-r--r--  1 root root   133 Jul  4 16:25 admin-openrc.sh
-rw-r--r--  1 root root   268 Jul 10 15:55 admin-openrc-v3.sh
...
>>> p.returncode
>>> p.wait()
0
>>> p.returncode
```
>这里也可以使用 p = subprocess.Popen(\['ls', '\-cl'\]) 来创建子进程。

### Popen 对象方法

1.   poll(): 检查进程是否终止，如果终止返回 returncode，否则返回 None。
2.   wait(timeout): 等待子进程终止。
3.   communicate(input,timeout): 和子进程交互，发送和读取数据。
4.   send\_signal(singnal): 发送信号到子进程 。
5.   terminate(): 停止子进程,也就是发送SIGTERM信号到子进程。
6.   kill(): 杀死子进程。发送 SIGKILL 信号到子进程。

实例：
```python
import time
import subprocess

def cmd(command):
    subp = subprocess.Popen(command,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE,encoding="utf-8")
    subp.wait(2)
    if subp.poll() == 0:
        print(subp.communicate()[1])
    else:
        print("失败")



cmd("java -version")
cmd("exit 1")
```
输出结果：
```py
java version "1.8.0_31"
Java(TM) SE Runtime Environment (build 1.8.0_31-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.31-b07, mixed mode)

失败
```
其他例子：
```sh
# 执行基本的系统命令，直接输出结果

>>> ret = subprocess.call('ls /tmp',shell=True)
hsperfdata_root  supervisor.sock  tmp_fifo

# 执行系统命令，但是捕捉输出

>>> f = subprocess.Popen('ls /tmp',shell=True,stdout=subprocess.PIPE)
>>> print f.stdout.readlines()
['hsperfdata_root\n', 'supervisor.sock\n', 'tmp_fifo\n']


# 创建2个子进程，通过管道把他们连接起来。一个进程的输出作为另一个进程的输入

>>> f = subprocess.Popen('ls -l',shell=True,stdout=subprocess.PIPE)
>>> p = subprocess.Popen('wc', shell=True, stdin=f.stdout, stdout=subprocess.PIPE) 
>>> print p.stdout.readlines()
['     73     652    4161\n']
```






## 参考文章
* [subprocess --- 子进程管理 — Python 3.7.9 文档](https://docs.python.org/zh-cn/3.7/library/subprocess.html)

* [Python模块之subprocess用法实例详解 - 云+社区 - 腾讯云](https://cloud.tencent.com/developer/article/1445388)

* [Python3 subprocess | 菜鸟教程](https://www.runoob.com/w3cnote/python3-subprocess.html)
* [python commands模块在python3.x被subprocess取代_RonnyJiang的博客-CSDN博客](https://blog.csdn.net/RonnyJiang/article/details/53333538)
* [如何使用子进程运行方法在Python中执行Shell命令– Linux提示](https://linuxhint.com/execute_shell_python_subprocess_run_method/)
  > 缺点是英文阅读不友好,但是非常详细的解释了subprocess命令的6种常规举例