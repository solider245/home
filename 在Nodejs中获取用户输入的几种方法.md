## 序言

在python中获取用户输入很简单，只需要
```py
input("请输入你的内容")
```
即可完成用户输入。

但是在nodejs中，则稍许麻烦，这里做一个备忘，方便以后查询。

## nodejs中使用readline模块来获得用户输入

```js
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});
 
readline.question('Who are you?', name => {
  console.log(`Hey there ${name}!`);
  readline.close();
});
```
注意：console里的数据的符号不是引号。

## 使用 `prompt-sync`

prompt-sync是一个第三方节点模块，使用他之前先确保安装。
```
npm install prompt-sync 
```

代码如下：
```js
const prompt = require('prompt-sync')();

const name = prompt('what is your name?');
console.log(`嘿，老铁，我是${name}`);
```

如上所示，看上去要比readline简单不少。

### 让用户退出

默认情况下，大多数终端程序将以 Ctrl + 退出 C （这会发送一条 [`SIGINT`](https://en.wikipedia.org/wiki/Signal_%28IPC%29#SIGINT) “或信号中断”消息，指示用户要退出程序）。 使用 `prompt-sync` ，为了确保您的用户可以随意退出，请 `sigint: true` 在调用该 `prompt-sync` 函数 时 添加一个配置对象 ：

```js
const prompt = require('prompt-sync')({sigint: true});
```

### 使用数字

所有用户输入将被读取为字符串，因此为了将用户输入视为数字，您需要转换输入：

```js
const num = prompt('Enter a number: ');
console.log('Your number + 4 =');
console.log(Number(num) + 4);
```

### 制作基本应用

下面的代码创建一个少量的猜测应用程序。

```js
const prompt = require('prompt-sync')({sigint: true});
 
// Random number from 1 - 10
const numberToGuess = Math.floor(Math.random() * 10) + 1;
// This variable is used to determine if the app should continue prompting the user for input
let foundCorrectNumber = false;
 
while (!foundCorrectNumber) {
  // Get user input
  let guess = prompt('Guess a number from 1 to 10: ');
  // Convert the string input to a number
  guess = Number(guess);
 
  // Compare the guess to the secret answer and let the user know.
  if (guess === numberToGuess) {
    console.log('Congrats, you got it!');
    foundCorrectNumber = true;
  } else {
    console.log('Sorry, guess again!');
  }
}
```

## 使用execa


## 参考文章

[在Node.js中获取用户输入 密码学](https://www.codecademy.com/articles/getting-user-input-in-node-js)

