
## commander vs minimist vs nopt vs optimist vs optionator vs yargs vs caporal vs oclif vs nomnom vs meow
nodejs大家熟知的命令行工具脚手架主流的有commander与yargs，实际上还有另外8款，这里做一个汇总。

## 主要指数对比
![20201112104142_8dd55b7d913fd467b5ac8bf11d906a30.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20201112104142_8dd55b7d913fd467b5ac8bf11d906a30.png)

![20201112104200_fa4841f4b12565f9912e4c27833794e9.png](https://images-1255533533.cos.ap-shanghai.myqcloud.com/20201112104200_fa4841f4b12565f9912e4c27833794e9.png)
### 下载量commander第一，yargs第二，optimist第三



如上两图所示，下载量最大的还是commander，其次就是yargs，这也是为什么网上这两个框架文章和视频最多的原因罢。

### starss，commander第一，yargs第二，oclif第三

去看了下oclif的教程，似乎写的非常详尽，另外两个框架的强大之处是用的人多，所以，先写这些，后面看看，有需要再补充吧。
