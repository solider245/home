命令行搜索工具fd-find文档

*fd* 是 [*find的*](https://www.gnu.org/software/findutils/) 一种简单，快速且用户友好的替代方法 。

尽管它并不试图镜像 *find* 的 所有 强大功能，但它为 [80％](https://en.wikipedia.org/wiki/Pareto_principle) 的用例 提供了明智的（有选择的）默认值 。

## `[](#features)Features`

*   方便的语法： `fd PATTERN` 而不是 `find -iname '*PATTERN*'` 。
*   彩色终端输出（类似于 *ls* ）。
*   这是 *快* （见 [基准](#benchmark) 下文）。
*   区分大小写：默认情况下，搜索不区分大小写。 如果模式包含大写字母 [\*](http://vimdoc.sourceforge.net/htmldoc/options.html#'smartcase') ，则切换为区分大小写 。
*   默认情况下，忽略隐藏的目录和文件。
*   `.gitignore` 默认情况下， 忽略您的模式 。
*   常用表达。
*   Unicode意识。
*   命令名称 比 :\-) 短 *50％* [\*](https://github.com/ggreer/the_silver_searcher) `find` 。
*   并行命令执行，其语法类似于GNU Parallel。

## `[](#demo)Demo`

[![演示版](https://github.com/sharkdp/fd/raw/master/doc/screencast.svg)](https://github.com/sharkdp/fd/blob/master/doc/screencast.svg)

## `[](#benchmark)Benchmark`

让我们在主文件夹中搜索以结尾的文件 `[0-9].jpg` 。 它包含约190.000个子目录和大约一百万个文件。 为了进行平均和统计分析，我使用了 [hyperfine](https://github.com/sharkdp/hyperfine) 。 以下基准测试是通过“热” /预填充磁盘缓存执行的（“冷”磁盘缓存的结果显示了相同的趋势）。

让我们开始 `find` ：

```
Benchmark #1: find ~ -iregex '.*[0-9]\.jpg$'

  Time (mean ± σ):      7.236 s ±  0.090 s

  Range (min … max):    7.133 s …  7.385 s

```

`find` 如果不需要执行正则表达式搜索，则速度更快：

```
Benchmark #2: find ~ -iname '*[0-9].jpg'

  Time (mean ± σ):      3.914 s ±  0.027 s

  Range (min … max):    3.876 s …  3.964 s

```

现在让我们为尝试相同的操作 `fd` 。 请注意， `fd` *始终* 执行正则表达式搜索。 这些选项 `--hidden` 和 `--no-ignore` 是进行公平比较所需的，否则 `fd` 不必遍历隐藏的文件夹和忽略的路径（请参见下文）：

```
Benchmark #3: fd -HI '.*[0-9]\.jpg$' ~

  Time (mean ± σ):     811.6 ms ±  26.9 ms

  Range (min … max):   786.0 ms … 870.7 ms

```

对于此特定示例， `fd` 速度大约是的9倍 `find -iregex` ，大约是的5倍 `find -iname` 。 顺便说一下，两个工具都找到了完全相同的20880个文件 😄 。

最后，让我们在 `fd` 没有 `--hidden` 和 的情况下运行 `--no-ignore` （当然，这可能会导致不同的搜索结果）。 如果 *fd* 不必遍历隐藏和git忽略的文件夹，则速度快了一个数量级：

```
Benchmark #4: fd '[0-9]\.jpg$' ~

  Time (mean ± σ):     123.7 ms ±   6.0 ms

  Range (min … max):   118.8 ms … 140.0 ms

```

**注** ：这是 *一个特殊* 的基准 *一个特定的* 机器。 尽管我执行了许多不同的测试（并找到一致的结果），但您的情况可能会有所不同！ 我鼓励大家自己尝试一下。 有关 所有必需的脚本， 请参 [见此存储库](https://github.com/sharkdp/fd-benchmarks) 。

关于 *fd* 的速度，主要归功于 [ripgrep](https://github.com/BurntSushi/ripgrep) 中也使用 的 `regex` 和 `ignore` 板条箱 （请检查！）。[](https://github.com/BurntSushi/ripgrep)

## `[](#colorized-output)Colorized output`

`fd` 可以像一样通过扩展名给文件着色 `ls` 。 为了使它起作用， [`LS_COLORS`](https://linux.die.net/man/5/dir_colors) 必须设置 环境变量 。 通常，此变量的值由 `dircolors` 命令 设置，该 命令提供了方便的配置格式来定义不同文件格式的颜色。 在大多数发行版中， `LS_COLORS` 应已设置。 如果您使用的是Windows，或者正在寻找替代的，更完整的（或更彩色的）变体，请参见 [此处](https://github.com/sharkdp/vivid) ， [此处](https://github.com/seebi/dircolors-solarized) 或 [此处](https://github.com/trapd00r/LS_COLORS) 。

`fd` 也尊重 [`NO_COLOR`](https://no-color.org/) 环境变量。

## `[](#parallel-command-execution)Parallel command execution`

如果在 命令模板旁边指定 了 `-x` / `--exec` 选项，则将创建一个作业池以并行执行每个发现的路径作为输入的命令。 生成命令的语法与GNU Parallel相似：

*   `{}` ：占位符标记，将被搜索结果（ `documents/images/party.jpg` ） 的路径替换 。
*   `{.}` ：类似 `{}` ，但没有文件扩展名（ `documents/images/party` ）。
*   `{/}` ：一个占位符，将由搜索结果的基本名称（ `party.jpg` ） 代替 。
*   `{//}` ：使用发现的路径（ `documents/images` ） 的父级 。
*   `{/.}` ：使用基本名称，但扩展名已删除（ `party` ）。

```shell
# Convert all jpg files to png files:
fd -e jpg -x convert {} {.}.png

# Unpack all zip files (if no placeholder is given, the path is appended):
fd -e zip -x unzip

# Convert all flac files into opus files:
fd -e flac -x ffmpeg -i {} -c:a libopus {.}.opus

# Count the number of lines in Rust files (the command template can be terminated with ';'):
fd -x wc -l \; -e rs
```

可以使用 `--threads` / `-j` 选项 设置用于命令执行的线程数 。

## `[](#installation)Installation`

[![包装状态](https://camo.githubusercontent.com/ec7b01ec19c2f443cbc27a26f78b4235785bfc6da8ca7a37a0fcf5544e2c0db9/68747470733a2f2f7265706f6c6f67792e6f72672f62616467652f766572746963616c2d616c6c7265706f732f66642d66696e642e737667)](https://repology.org/project/fd-find/versions)

### `[](#on-ubuntu)On Ubuntu`

*...以及其他基于Debian的Linux发行版。*

如果您运行Ubuntu 19.04（Disco Dingo）或更高版本，则可以安装 [官方维护的软件包](https://packages.ubuntu.com/fd-find) ：

```
sudo apt install fd-find

```

请注意，二进制文件被称为 `fdfind` 是因为二进制文件名 `fd` 已被另一个软件包使用。 建议您 `alias fd=fdfind` 在外壳初始化文件中 添加一个 ，以便以 `fd` 与本文档中相同的方式 使用 。

如果您使用旧版本的Ubuntu，则可以 `.deb` 从 [发行版页面](https://github.com/sharkdp/fd/releases) 下载最新的 软件包， 并通过以下方式进行安装：

```shell
sudo dpkg -i fd_8.1.0_amd64.deb  # adapt version number and architecture
```

### `[](#on-debian)On Debian`

如果您运行Debian Buster或更高版本，则可以安装 [官方维护的Debian软件包](https://tracker.debian.org/pkg/rust-fd-find) ：

```
sudo apt-get install fd-find

```

请注意，二进制文件被称为 `fdfind` 是因为二进制文件名 `fd` 已被另一个软件包使用。 建议您 `alias fd=fdfind` 在外壳初始化文件中 添加一个 ，以便以 `fd` 与本文档中相同的方式 使用 。

### `[](#on-fedora)On Fedora`

从Fedora 28开始，您可以 `fd` 从官方软件包中 进行安装 ：

```shell
dnf install fd-find
```

对于较旧的版本，您可以使用此 [Fedora copr](https://copr.fedorainfracloud.org/coprs/keefle/fd/) 安装 `fd` ：

```shell
dnf copr enable keefle/fd
dnf install fd
```

### `[](#on-alpine-linux)On Alpine Linux`

如果 启用了适当的存储库，则 可以 从官方来源 安装 [fd软件包](https://pkgs.alpinelinux.org/packages?name=fd) ：

```
apk add fd

```

### `[](#on-arch-linux)On Arch Linux`

您可以 从官方仓库中 安装 [fd软件包](https://www.archlinux.org/packages/community/x86_64/fd/) ：

```
pacman -S fd

```

### `[](#on-gentoo-linux)On Gentoo Linux`

您可以使用 官方仓库中 [的fd ebuild](https://packages.gentoo.org/packages/sys-apps/fd) ：

```
emerge -av fd

```

### `[](#on-opensuse-linux)On openSUSE Linux`

您可以 从官方仓库中 安装 [fd软件包](https://software.opensuse.org/package/fd) ：

```
zypper in fd

```

### `[](#on-void-linux)On Void Linux`

您可以 `fd` 通过xbps\-install安装：

```
xbps-install -S fd

```

### `[](#on-macos)On macOS`

您可以 `fd` 使用 [Homebrew](https://formulae.brew.sh/formula/fd) 安装 ：

```
brew install fd

```

…或使用MacPorts：

```
sudo port install fd

```

### `[](#on-windows)On Windows`

您可以从 [发布页面](https://github.com/sharkdp/fd/releases) 下载预构建的二进制文件 。

另外，您可以 `fd` 通过 [Scoop](http://scoop.sh) 安装 ：

```
scoop install fd

```

或通过 [Chocolatey](https://chocolatey.org) ：

```
choco install fd

```

### `[](#on-nixos--via-nix)On NixOS / via Nix`

您可以使用 [Nix软件包管理器](https://nixos.org/nix/) 进行安装 `fd` ：

```
nix-env -i fd

```

### `[](#on-freebsd)On FreeBSD`

您可以 从官方仓库中 安装 [fd\-find软件包](https://www.freshports.org/sysutils/fd) ：

```
pkg install fd-find

```

### `[](#from-npm)From NPM`

在linux和macOS上，您可以安装 [fd\-find](https://npm.im/fd-find) 软件包：

```
npm install -g fd-find

```

### `[](#from-source)From source`

使用Rust的Package Manager [货物](https://github.com/rust-lang/cargo) ，您可以 通过以下 方式安装 *fd* ：

```
cargo install fd-find

```

请注意，需要rust *1.36.0* 或更高 版本 。

### `[](#from-binaries)From binaries`

该 [版本页面](https://github.com/sharkdp/fd/releases) 包括预编译的Linux，MacOS和Windows可执行文件。

## `[](#development)Development`

```shell
git clone https://github.com/sharkdp/fd

# Build
cd fd
cargo build

# Run unit tests and integration tests
cargo test

# Install
cargo install --path .
```

## `[](#command-line-options)Command-line options`

```
USAGE:
    fd [FLAGS/OPTIONS] [<pattern>] [<path>...]

FLAGS:
    -H, --hidden            Search hidden files and directories
    -I, --no-ignore         Do not respect .(git|fd)ignore files
    -s, --case-sensitive    Case-sensitive search (default: smart case)
    -i, --ignore-case       Case-insensitive search (default: smart case)
    -g, --glob              Glob-based search (default: regular expression)
    -a, --absolute-path     Show absolute instead of relative paths
    -l, --list-details      Use a long listing format with file metadata
    -L, --follow            Follow symbolic links
    -p, --full-path         Search full path (default: file-/dirname only)
    -0, --print0            Separate results by the null character
    -h, --help              Prints help information
    -V, --version           Prints version information

OPTIONS:
    -d, --max-depth <depth>            Set maximum search depth (default: none)
    -t, --type <filetype>...           Filter by type: file (f), directory (d), symlink (l),
                                       executable (x), empty (e), socket (s), pipe (p)
    -e, --extension <ext>...           Filter by file extension
    -x, --exec <cmd>                   Execute a command for each search result
    -X, --exec-batch <cmd>             Execute a command with all search results at once
    -E, --exclude <pattern>...         Exclude entries that match the given glob pattern
    -c, --color <when>                 When to use colors: never, *auto*, always
    -S, --size <size>...               Limit results based on the size of files.
        --changed-within <date|dur>    Filter by file modification time (newer than)
        --changed-before <date|dur>    Filter by file modification time (older than)

ARGS:
    <pattern>    the search pattern - a regular expression unless '--glob' is used (optional)
    <path>...    the root directory for the filesystem search (optional)

```

这是的输出 `fd -h` 。 要查看完整的命令行选项集，请使用 `fd --help` ，其中还包括更详细的帮助文本。

## `[](#tutorial)Tutorial`

首先，要获得所有可用命令行选项的概述，您可以运行 `fd -h` 以获取简洁的帮助消息（请参见上文）或 `fd --help` 获得更详细的版本。

### `[](#simple-search)Simple search`

*fd* 旨在查找文件系统中的条目。 您可以执行的最基本的搜索是 使用单个参数 运行 *fd* ：搜索模式。 例如，假设您要查找自己的旧脚本（名称包括在内 `netflix` ）：

```shell
> fd netfl
Software/python/imdb-ratings/netflix-details.py
```

如果仅用这样的单个参数调用，则 *fd会* 在当前目录中递归搜索所有 *包含* pattern的 条目 `netfl` 。

### `[](#regular-expression-search)Regular expression search`

搜索模式被视为正则表达式。 在这里，我们搜索以 `x` 和结尾的 条目 `rc` ：

```shell
> cd /etc
> fd '^x.*rc$'
X11/xinit/xinitrc
X11/xinit/xserverrc
```

fd使用的正则表达式语法在此处记录：

[https://docs.rs/regex/1.0.0/regex/#语法](https://docs.rs/regex/1.0.0/regex/#syntax)

### `[](#specifying-the-root-directory)Specifying the root directory`

如果我们要搜索特定目录，可以将其作为 *fd* 的第二个参数给出 ：

```shell
> fd passwd /etc
/etc/default/passwd
/etc/pam.d/passwd
/etc/passwd
```

### `[](#running-fd-without-any-arguments)Running *fd* without any arguments`

可以不带任何参数调用*fd* 。 以递归方式（类似于 `ls -R` ） 快速概览当前目录中的所有条目，这非常有用 ：

```shell
> cd fd/tests
> fd
testenv
testenv/mod.rs
tests.rs
```

如果要使用此功能列出给定目录中的所有文件，则必须使用全部捕获模式，例如 `.` 或 `^` ：

```shell
> fd . fd/tests/
testenv
testenv/mod.rs
tests.rs
```

### `[](#searching-for-a-particular-file-extension)Searching for a particular file extension`

通常，我们对特定类型的所有文件都感兴趣。 可以使用 `-e` （或 `--extension` ）选项 来完成 。 在这里，我们搜索fd存储库中的所有Markdown文件：

```shell
> cd fd
> fd -e md
CONTRIBUTING.md
README.md
```

该 `-e` 选项可以与搜索模式结合使用：

```shell
> fd -e rs mod
src/fshelper/mod.rs
src/lscolors/mod.rs
tests/testenv/mod.rs
```

### `[](#hidden-and-ignored-files)Hidden and ignored files`

默认情况下， *fd* 不搜索隐藏目录，也不在搜索结果中显示隐藏文件。 要禁用此行为，我们可以使用 `-H` （或 `--hidden` ）选项：

```shell
> fd pre-commit
> fd -H pre-commit
.git/hooks/pre-commit.sample
```

如果我们在Git存储库（或包含Git存储库）的目录中工作，则 *fd* 不会搜索与 `.gitignore` 模式 之一匹配的文件夹（并且不显示文件） 。 要禁用此行为，我们可以使用 `-I` （或 `--no-ignore` ）选项：

```shell
> fd num_cpu
> fd -I num_cpu
target/debug/deps/libnum_cpus-f5ce7ef99006aa05.rlib
```

要真正搜索 *所有* 文件和目录，只需组合隐藏和忽略功能即可显示所有内容（ `-HI` ）。

### `[](#excluding-specific-files-or-directories)Excluding specific files or directories`

有时我们想忽略特定子目录中的搜索结果。 例如，我们可能要搜索所有隐藏的文件和目录（ `-H` ），但要从 `.git` 目录中 排除所有匹配项 。 我们可以使用 `-E` （或 `--exclude` ）选项。 它采用任意的glob模式作为参数：

```shell
> fd -H -E .git …
```

我们还可以使用它跳过已挂载的目录：

```shell
> fd -E /mnt/external-drive …
```

..或跳过某些文件类型：

```shell
> fd -E '*.bak' …
```

要使诸如此类的排除模式永久化，您可以创建一个 `.fdignore` 文件。 它们的工作方式类似于 `.gitignore` 文件，但特定于 `fd` 。 例如：

```shell
> cat ~/.fdignore
/mnt/external-drive
*.bak
```

注意： `fd` 还支持 `.ignore` 其他程序（例如 `rg` 或） 使用的文件 `ag` 。

如果要 `fd` 全局忽略这些模式，可以将它们放在 `fd` 的全局忽略文件中。 它通常位于 `~/.config/fd/ignore` macOS或Linux以及 `%APPDATA%\fd\ignore` Windows中。

### [](#using-fd-with-xargs-or-parallel)将fd与 `xargs` 或一起使用 `parallel`

如果要对所有搜索结果运行命令，可以将输出传递给 `xargs` ：

```shell
> fd -0 -e rs | xargs -0 wc -l
```

在此，该 `-0` 选项告诉 *fd* 用NULL字符（而不是换行符）分隔搜索结果。 以相同的方式， `-0` 选项 `xargs` 告诉它以这种方式读取输入。

### `[](#deleting-files)Deleting files`

您可以 `fd` 用来删除所有与搜索模式匹配的文件和目录。 如果只想删除文件，则可以使用 `--exec-batch` / `-X` 选项来调用 `rm` 。 例如，要递归删除所有 `.DS_Store` 文件，请运行：

```shell
> fd -H '^\.DS_Store$' -tf -X rm
```

如果你不确定，随时调用 `fd` 无 `-X rm` 第一。 或者，使用 `rm` s“ interactive”选项：

```shell
> fd -H '^\.DS_Store$' -tf -X rm -i
```

如果您还想删除某一类目录，则可以使用相同的技术。 您将必须使用 `rm` s `--recursive` / `-r` 标志来删除目录。

注意：在某些情况下，使用 `fd … -X rm -r` 会导致竞争状况：如果您的路径类似于 `…/foo/bar/foo/…` 并且想要删除所有名为的目录 `foo` ，则可能会遇到以下情况： `foo` 首先删除 外部 目录，从而导致（无害） *“'foo / bar / foo'：* `rm` 呼叫中*没有这样的文件或目录* 错误 。

### `[](#troubleshooting)Troubleshooting`

#### [](#fd-does-not-find-my-file)`fd` 找不到我的文件！

请记住， `fd` 默认情况下 会 忽略隐藏的目录和文件。 它还会忽略 `.gitignore` 文件中的 模式 。 如果要确保绝对找到每个可能的文件，请始终使用选项 `-H` 并 `-I` 禁用这两个功能：

```shell
> fd -HI …
```

#### [](#fd-doesnt-seem-to-interpret-my-regex-pattern-correctly)`fd` 似乎无法正确解释我的正则表达式模式

很多特殊的正则表达式的字符（如 `[]` ， `^` ， `$` ，。）也是在你的shell特殊字符。 如有疑问，请务必在正则表达式模式周围加上单引号：

```shell
> fd '^[A-Z][0-9]+$'
```

如果您的模式以破折号开头，则必须添加 `--` 以指示命令行选项的结束。 否则，该模式将被解释为命令行选项。 或者，使用带有单个连字符的字符类：

```shell
> fd -- '-pattern'
> fd '[-]pattern'
```

### `[](#integration-with-other-programs)Integration with other programs`

#### [](#using-fd-with-fzf)将fd与 `fzf`

您可以使用 *fd* 为命令行模糊查找器 [fzf](https://github.com/junegunn/fzf) 生成输入 ：

```shell
export FZF_DEFAULT_COMMAND='fd --type file'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
```

然后，您可以 `vim <Ctrl-T>` 在终端上 键入 以打开fzf并搜索fd结果。

另外，您可能希望跟随符号链接并包括隐藏文件（但不包括 `.git` 文件夹）：

```shell
export FZF_DEFAULT_COMMAND='fd --type file --follow --hidden --exclude .git'
```

您甚至可以通过以下设置在fzf中使用fd的彩色输出：

```shell
export FZF_DEFAULT_COMMAND="fd --type file --color=always"
export FZF_DEFAULT_OPTS="--ansi"
```

有关更多详细信息，请参见 fzf自述文件 的“ [提示”部分](https://github.com/junegunn/fzf#tips) 。

#### [](#using-fd-with-emacs)将fd与 `emacs`

emacs软件包 [find\-file\-in\-project](https://github.com/technomancy/find-file-in-project) 可以使用 *fd* 查找文件。

安装后 `find-file-in-project` ，将该行添加 `(setq ffip-use-rust-fd t)` 到您的 `~/.emacs` 或 `~/.emacs.d/init.el` 文件中。

在emacs中，运行 `M-x find-file-in-project-by-selected` 以查找匹配的文件。 或者，运行 `M-x find-file-in-project` 以列出项目中所有可用的文件。

#### `[](#printing-fds-output-as-a-tree)Printing fd's output as a tree`

要格式化 `fd` 类似于 `tree` 命令 [`as-tree`](https://github.com/jez/as-tree) 的输出 ，请安装 并将其输出管道 `fd` 到 `as-tree` ：

```shell
fd | as-tree
```

这比单独运行更有用 `tree` ，因为 `tree` 默认情况下它不会忽略任何文件，也不会像 `fd` 控制打印内容 那样支持一系列丰富的选项 ：

```shell
❯ fd --extension rs | as-tree
.
├── build.rs
└── src
    ├── app.rs
    └── error.rs
```

有关详细信息 `as-tree` ，请参阅 [的 `as-tree` 自述](https://github.com/jez/as-tree) 。

## `[](#maintainers)Maintainers`

*   [鲨鱼](https://github.com/sharkdp)
*   [tmccombs](https://github.com/tmccombs)

## `[](#license)License`

版权所有（c）2017\-2020 fd开发人员

`fd` 根据MIT许可证和Apache许可证2.0的条款进行分发。

有关 许可证的详细信息， 请参阅 [LICENSE\-APACHE](https://github.com/sharkdp/fd/blob/master/LICENSE-APACHE) 和 [LICENSE\-MIT](https://github.com/sharkdp/fd/blob/master/LICENSE-MIT) 文件。