# bashrc模块化间断教程

我以前的 `.bashrc` 文件是一团糟， 一个 文件中有太多东西，很难管理。 我决定将其拆分并使其模块化。 为此，我使主 `.bashrc` 文件非常简单。 它唯一要做的就是遍历 `~/.bashrc.d/` 文件夹并运行其中的所有内容。

因此，我 `~/.bashrc` 文件 的内容 如下：

```shell
for file in ~/.bashrc.d/*.bashrc;
do
  source $file
done

```

然后，我将所有小型模块化设备都放在 `~/.bashrc.d/` 文件夹中。 看起来像这样：

```bash
├── activitystream.bashrc
├── default.bashrc
├── dotfiles.bashrc
├── git.bashrc
├── nvm.bashrc
└── pb.bashrc

```

您可以 [在此处](https://www.gaui.is/modular-bashrc/github.com/gaui/dotfiles/tree/master/.bashrc.d) 查看其所有内容 。 每个文件都能很好地完成一项或多项任务，因此添加，删除或更改它们非常容易。 我真的很喜欢这种方法。

## 参考文章

[模块化.bashrc – $ bash_functions（）](https://bashfunctions.wordpress.com/2015/12/21/modular-bashrc/)