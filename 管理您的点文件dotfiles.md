# 管理您的点文件

我一直在寻找一种管理点文件的简便方法。 点文件只是当今大多数系统使用的配置文件。 它们存储在您的主目录中 `$HOME` ，并带有前缀a `.` 使其隐藏。 点文件的一个例子： `.gitconfig` ， `.vimrc` ， `.npmrc` ， `.yarnrc`

我偶然发现了一篇关于 *Hacker News* 的精彩文章， 内容涉及 [如何管理您的dotfile](https://news.ycombinator.com/item?id=11071754) 。 我遵循了这些步骤，并决定写一些关于我的过程的信息。

## 创建一个裸git仓库

这将是您的点文件受版本控制的地方。 一个 *纯仓库* 只是包含Git对象并没有跟踪文件的存储库。 它们通常 `.git` 位于常规存储库中，但仅此而已。 当创建仅用于存储的存储库时，请 **使用nude** 。

```
git init --bare $HOME/.dotfiles

```

## 创建一个别名来管理您的点文件

```
echo alias dot='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME' >> ~/.bashrc

```

该别名将创建一个 `dot` 命令别名（您可以根据需要命名）， 该别名将以 `/usr/bin/git` 某种方式 调用 并将其存储在 `~/.bashrc` 文件中，因此每次打开Bash时都会被加载。

现在来解释这些标志：

*   `--git-dir=$HOME/.dotfiles/` ：无论您在何处调用该命令，它都将始终指向该特定目录。
*   `--work-tree=$HOME` ：它将始终与该特定目录中的文件一起使用。

因此，我们将版本控制文件的存储指向 `$HOME/.dotfiles` 该文件 ， 并且它所跟踪的文件将 `$HOME` 位于其中（因为所有dotfile都位于该文件中）。

## 隐藏未跟踪的文件

因为我们不会提交其中的所有文件，所以 `$HOME` 我们不希望它显示未跟踪的文件。 这不是必须的，只是因为我们在运行时没有可见的所有未跟踪文件 `dot status`

```shell
dot config --local status.showUntrackedFiles no

```

## 版本控制您的dotfile

现在，我们想对点文件进行版本控制，以便我们可以跟踪更改，回滚到以前的文件或发布文件。

### 获取未跟踪文件的列表

要获取未跟踪文件的列表，我们必须添加 `--untracked-files=normal` 标志，因为由于 `status.showUntrackedFiles no` 我们之前设置 的配置，默认情况下它们不可见 。

```shell
dot status --untracked-files=normal

```

### 提交他们

现在提交我们的文件，我们就像使用常规 命令 那样来做， `git` 但是我们使用 `dot` 命令。

```shell
dot add .gitconfig
dot commit -m Add .gitconfig
dot add .vimrc
dot commit -m Add .vimrc
dot add .bashrc
dot commit -m Add .bashrc

```

### 添加远程（例如，GitHub）

现在，如果要发布点文件，则可以轻松创建一个远程文件并将其推送到该文件。 就像我对GitHub所做的那样，因为 [我的dotfile是公开可用的](https://github.com/gaui/dotfiles) 。

```shell
dot remote add origin git@github.com:gaui/dotfiles.git
dot push
```